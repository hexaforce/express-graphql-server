import { GraphQLSchema, buildSchema } from 'graphql'

// Construct a schema, using GraphQL schema language
export var schema: GraphQLSchema = buildSchema(`
  type Query {
    rollDice(numDice: Int!, numSides: Int): [Int]
  }
`)

// The root provides a resolver function for each API endpoint
export const rootValue = {
  rollDice: ({ numDice, numSides }: { numDice: number; numSides: number }) => {
    const output: number[] = []
    for (let i = 0; i < numDice; i++) {
      output.push(1 + Math.floor(Math.random() * (numSides || 6)))
    }
    return output
  },
}
