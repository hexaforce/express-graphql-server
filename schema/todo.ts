import { GraphQLSchema, buildSchema } from 'graphql'

export const schema: GraphQLSchema = buildSchema(`
  type Query {
    hello: String
    todos: [Todo]
    todo(id: String): Todo
  }
  type Todo {
    id: String!
    title: String!
  }
`)

export type TodoType = {
  id: string
  title: string
}

const Todos: TodoType[] = [
  {
    id: '1',
    title: 'todo1',
  },
  {
    id: '2',
    title: 'todo2',
  },
  {
    id: '3',
    title: 'todo3',
  },
]

export const rootValue = {
  todo: ({ id }: { id: string }) => {
    return [...Todos].find((todo: TodoType) => todo.id === id)
  },
  todos: () => {
    return [...Todos]
  },
}
