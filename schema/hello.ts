import { GraphQLSchema, buildSchema } from 'graphql'

// Construct a schema, using GraphQL schema language
export var schema: GraphQLSchema = buildSchema(`
  type Query {
    hello: String
  }
`)

// The root provides a resolver function for each API endpoint
export const rootValue = {
  hello: (): string => {
    return 'Hello world!'
  },
}
