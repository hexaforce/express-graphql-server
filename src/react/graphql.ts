/* eslint-disable */
import { GraphQLClient } from 'graphql-request';
import { GraphQLClientRequestHeaders } from 'graphql-request/build/cjs/types';
import gql from 'graphql-tag';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = { [_ in K]?: never };
export type Incremental<T> = T | { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string; }
  String: { input: string; output: string; }
  Boolean: { input: boolean; output: boolean; }
  Int: { input: number; output: number; }
  Float: { input: number; output: number; }
  /** A location in a connection that can be used for resuming pagination. */
  Cursor: { input: any; output: any; }
  /**
   * A point in time as described by the [ISO
   * 8601](https://en.wikipedia.org/wiki/ISO_8601) standard. May or may not include a timezone.
   */
  Datetime: { input: any; output: any; }
  /** A universally unique identifier as defined by [RFC 4122](https://tools.ietf.org/html/rfc4122). */
  UUID: { input: any; output: any; }
};

export type AlembicVersion = Node & {
  __typename?: 'AlembicVersion';
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID']['output'];
  versionNum: Scalars['String']['output'];
};

/**
 * A condition to be used against `AlembicVersion` object types. All fields are
 * tested for equality and combined with a logical ‘and.’
 */
export type AlembicVersionCondition = {
  /** Checks for equality with the object’s `versionNum` field. */
  versionNum?: InputMaybe<Scalars['String']['input']>;
};

/** A filter to be used against `AlembicVersion` object types. All fields are combined with a logical ‘and.’ */
export type AlembicVersionFilter = {
  /** Checks for all expressions in this list. */
  and?: InputMaybe<Array<AlembicVersionFilter>>;
  /** Negates the expression. */
  not?: InputMaybe<AlembicVersionFilter>;
  /** Checks for any expressions in this list. */
  or?: InputMaybe<Array<AlembicVersionFilter>>;
  /** Filter by the object’s `versionNum` field. */
  versionNum?: InputMaybe<StringFilter>;
};

/** An input for mutations affecting `AlembicVersion` */
export type AlembicVersionInput = {
  versionNum: Scalars['String']['input'];
};

/** Represents an update to a `AlembicVersion`. Fields that are set will be updated. */
export type AlembicVersionPatch = {
  versionNum?: InputMaybe<Scalars['String']['input']>;
};

/** A connection to a list of `AlembicVersion` values. */
export type AlembicVersionsConnection = {
  __typename?: 'AlembicVersionsConnection';
  /** A list of edges which contains the `AlembicVersion` and cursor to aid in pagination. */
  edges: Array<AlembicVersionsEdge>;
  /** A list of `AlembicVersion` objects. */
  nodes: Array<Maybe<AlembicVersion>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `AlembicVersion` you could get from the connection. */
  totalCount: Scalars['Int']['output'];
};

/** A `AlembicVersion` edge in the connection. */
export type AlembicVersionsEdge = {
  __typename?: 'AlembicVersionsEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']['output']>;
  /** The `AlembicVersion` at the end of the edge. */
  node?: Maybe<AlembicVersion>;
};

/** Methods to use when ordering `AlembicVersion`. */
export enum AlembicVersionsOrderBy {
  Natural = 'NATURAL',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
  VersionNumAsc = 'VERSION_NUM_ASC',
  VersionNumDesc = 'VERSION_NUM_DESC'
}

/** A filter to be used against Boolean fields. All fields are combined with a logical ‘and.’ */
export type BooleanFilter = {
  /** Not equal to the specified value, treating null like an ordinary value. */
  distinctFrom?: InputMaybe<Scalars['Boolean']['input']>;
  /** Equal to the specified value. */
  equalTo?: InputMaybe<Scalars['Boolean']['input']>;
  /** Greater than the specified value. */
  greaterThan?: InputMaybe<Scalars['Boolean']['input']>;
  /** Greater than or equal to the specified value. */
  greaterThanOrEqualTo?: InputMaybe<Scalars['Boolean']['input']>;
  /** Included in the specified list. */
  in?: InputMaybe<Array<Scalars['Boolean']['input']>>;
  /** Is null (if `true` is specified) or is not null (if `false` is specified). */
  isNull?: InputMaybe<Scalars['Boolean']['input']>;
  /** Less than the specified value. */
  lessThan?: InputMaybe<Scalars['Boolean']['input']>;
  /** Less than or equal to the specified value. */
  lessThanOrEqualTo?: InputMaybe<Scalars['Boolean']['input']>;
  /** Equal to the specified value, treating null like an ordinary value. */
  notDistinctFrom?: InputMaybe<Scalars['Boolean']['input']>;
  /** Not equal to the specified value. */
  notEqualTo?: InputMaybe<Scalars['Boolean']['input']>;
  /** Not included in the specified list. */
  notIn?: InputMaybe<Array<Scalars['Boolean']['input']>>;
};

/** A connection to a list of `Company` values. */
export type CompaniesConnection = {
  __typename?: 'CompaniesConnection';
  /** A list of edges which contains the `Company` and cursor to aid in pagination. */
  edges: Array<CompaniesEdge>;
  /** A list of `Company` objects. */
  nodes: Array<Maybe<Company>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `Company` you could get from the connection. */
  totalCount: Scalars['Int']['output'];
};

/** A `Company` edge in the connection. */
export type CompaniesEdge = {
  __typename?: 'CompaniesEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']['output']>;
  /** The `Company` at the end of the edge. */
  node?: Maybe<Company>;
};

/** Methods to use when ordering `Company`. */
export enum CompaniesOrderBy {
  CreatedAtAsc = 'CREATED_AT_ASC',
  CreatedAtDesc = 'CREATED_AT_DESC',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  Natural = 'NATURAL',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
  SfAccountIdAsc = 'SF_ACCOUNT_ID_ASC',
  SfAccountIdDesc = 'SF_ACCOUNT_ID_DESC',
  UpdatedAtAsc = 'UPDATED_AT_ASC',
  UpdatedAtDesc = 'UPDATED_AT_DESC'
}

/** 企業テーブル */
export type Company = Node & {
  __typename?: 'Company';
  /** Reads and enables pagination through a set of `CompanyInfo`. */
  companyInfosByCompanyId: CompanyInfosConnection;
  createdAt?: Maybe<Scalars['Datetime']['output']>;
  /** 企業ID */
  id: Scalars['UUID']['output'];
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID']['output'];
  /** Reads and enables pagination through a set of `Product`. */
  productsByCompanyId: ProductsConnection;
  /** Salesforce企業ID */
  sfAccountId?: Maybe<Scalars['String']['output']>;
  updatedAt?: Maybe<Scalars['Datetime']['output']>;
};


/** 企業テーブル */
export type CompanyCompanyInfosByCompanyIdArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<CompanyInfoCondition>;
  filter?: InputMaybe<CompanyInfoFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CompanyInfosOrderBy>>;
};


/** 企業テーブル */
export type CompanyProductsByCompanyIdArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<ProductCondition>;
  filter?: InputMaybe<ProductFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<ProductsOrderBy>>;
};

/** A condition to be used against `Company` object types. All fields are tested for equality and combined with a logical ‘and.’ */
export type CompanyCondition = {
  /** Checks for equality with the object’s `createdAt` field. */
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** Checks for equality with the object’s `id` field. */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `sfAccountId` field. */
  sfAccountId?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** A filter to be used against `Company` object types. All fields are combined with a logical ‘and.’ */
export type CompanyFilter = {
  /** Checks for all expressions in this list. */
  and?: InputMaybe<Array<CompanyFilter>>;
  /** Filter by the object’s `createdAt` field. */
  createdAt?: InputMaybe<DatetimeFilter>;
  /** Filter by the object’s `id` field. */
  id?: InputMaybe<UuidFilter>;
  /** Negates the expression. */
  not?: InputMaybe<CompanyFilter>;
  /** Checks for any expressions in this list. */
  or?: InputMaybe<Array<CompanyFilter>>;
  /** Filter by the object’s `sfAccountId` field. */
  sfAccountId?: InputMaybe<StringFilter>;
  /** Filter by the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<DatetimeFilter>;
};

/** 契約情報/企業情報ID */
export type CompanyInfo = Node & {
  __typename?: 'CompanyInfo';
  address?: Maybe<Scalars['String']['output']>;
  addressBill?: Maybe<Scalars['String']['output']>;
  adsOperationManager?: Maybe<Scalars['String']['output']>;
  adsOperationStaff?: Maybe<Scalars['String']['output']>;
  approver?: Maybe<Scalars['String']['output']>;
  billingMethodProblem?: Maybe<Scalars['Boolean']['output']>;
  billingMethodProblemText?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Company` that is related to this `CompanyInfo`. */
  companyByCompanyId?: Maybe<Company>;
  /** 企業ID */
  companyId?: Maybe<Scalars['UUID']['output']>;
  corporateName?: Maybe<Scalars['String']['output']>;
  corporateNameBill?: Maybe<Scalars['String']['output']>;
  corporateNameKana?: Maybe<Scalars['String']['output']>;
  corporateNameKanaBill?: Maybe<Scalars['String']['output']>;
  createdAt?: Maybe<Scalars['Datetime']['output']>;
  creativeManager?: Maybe<Scalars['String']['output']>;
  departmentName?: Maybe<Scalars['String']['output']>;
  departmentNameBill?: Maybe<Scalars['String']['output']>;
  /** 契約情報/企業情報ID */
  id: Scalars['UUID']['output'];
  mobileNumber?: Maybe<Scalars['String']['output']>;
  mobileNumberBill?: Maybe<Scalars['String']['output']>;
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID']['output'];
  personInChargeEmail?: Maybe<Scalars['String']['output']>;
  personInChargeEmailBill?: Maybe<Scalars['String']['output']>;
  personInChargeName?: Maybe<Scalars['String']['output']>;
  personInChargeNameBill?: Maybe<Scalars['String']['output']>;
  postCode?: Maybe<Scalars['String']['output']>;
  postCodeBill?: Maybe<Scalars['String']['output']>;
  productNames?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  productPriorities?: Maybe<Array<Maybe<Scalars['Int']['output']>>>;
  projectManager?: Maybe<Scalars['String']['output']>;
  sendOriginalMail?: Maybe<Scalars['Boolean']['output']>;
  updatedAt?: Maybe<Scalars['Datetime']['output']>;
};

/**
 * A condition to be used against `CompanyInfo` object types. All fields are tested
 * for equality and combined with a logical ‘and.’
 */
export type CompanyInfoCondition = {
  /** Checks for equality with the object’s `address` field. */
  address?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `addressBill` field. */
  addressBill?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adsOperationManager` field. */
  adsOperationManager?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adsOperationStaff` field. */
  adsOperationStaff?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `approver` field. */
  approver?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `billingMethodProblem` field. */
  billingMethodProblem?: InputMaybe<Scalars['Boolean']['input']>;
  /** Checks for equality with the object’s `billingMethodProblemText` field. */
  billingMethodProblemText?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `companyId` field. */
  companyId?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `corporateName` field. */
  corporateName?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `corporateNameBill` field. */
  corporateNameBill?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `corporateNameKana` field. */
  corporateNameKana?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `corporateNameKanaBill` field. */
  corporateNameKanaBill?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `createdAt` field. */
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** Checks for equality with the object’s `creativeManager` field. */
  creativeManager?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `departmentName` field. */
  departmentName?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `departmentNameBill` field. */
  departmentNameBill?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `id` field. */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `mobileNumber` field. */
  mobileNumber?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `mobileNumberBill` field. */
  mobileNumberBill?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `personInChargeEmail` field. */
  personInChargeEmail?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `personInChargeEmailBill` field. */
  personInChargeEmailBill?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `personInChargeName` field. */
  personInChargeName?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `personInChargeNameBill` field. */
  personInChargeNameBill?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `postCode` field. */
  postCode?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `postCodeBill` field. */
  postCodeBill?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `productNames` field. */
  productNames?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Checks for equality with the object’s `productPriorities` field. */
  productPriorities?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  /** Checks for equality with the object’s `projectManager` field. */
  projectManager?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `sendOriginalMail` field. */
  sendOriginalMail?: InputMaybe<Scalars['Boolean']['input']>;
  /** Checks for equality with the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** A filter to be used against `CompanyInfo` object types. All fields are combined with a logical ‘and.’ */
export type CompanyInfoFilter = {
  /** Filter by the object’s `address` field. */
  address?: InputMaybe<StringFilter>;
  /** Filter by the object’s `addressBill` field. */
  addressBill?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adsOperationManager` field. */
  adsOperationManager?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adsOperationStaff` field. */
  adsOperationStaff?: InputMaybe<StringFilter>;
  /** Checks for all expressions in this list. */
  and?: InputMaybe<Array<CompanyInfoFilter>>;
  /** Filter by the object’s `approver` field. */
  approver?: InputMaybe<StringFilter>;
  /** Filter by the object’s `billingMethodProblem` field. */
  billingMethodProblem?: InputMaybe<BooleanFilter>;
  /** Filter by the object’s `billingMethodProblemText` field. */
  billingMethodProblemText?: InputMaybe<StringFilter>;
  /** Filter by the object’s `companyId` field. */
  companyId?: InputMaybe<UuidFilter>;
  /** Filter by the object’s `corporateName` field. */
  corporateName?: InputMaybe<StringFilter>;
  /** Filter by the object’s `corporateNameBill` field. */
  corporateNameBill?: InputMaybe<StringFilter>;
  /** Filter by the object’s `corporateNameKana` field. */
  corporateNameKana?: InputMaybe<StringFilter>;
  /** Filter by the object’s `corporateNameKanaBill` field. */
  corporateNameKanaBill?: InputMaybe<StringFilter>;
  /** Filter by the object’s `createdAt` field. */
  createdAt?: InputMaybe<DatetimeFilter>;
  /** Filter by the object’s `creativeManager` field. */
  creativeManager?: InputMaybe<StringFilter>;
  /** Filter by the object’s `departmentName` field. */
  departmentName?: InputMaybe<StringFilter>;
  /** Filter by the object’s `departmentNameBill` field. */
  departmentNameBill?: InputMaybe<StringFilter>;
  /** Filter by the object’s `id` field. */
  id?: InputMaybe<UuidFilter>;
  /** Filter by the object’s `mobileNumber` field. */
  mobileNumber?: InputMaybe<StringFilter>;
  /** Filter by the object’s `mobileNumberBill` field. */
  mobileNumberBill?: InputMaybe<StringFilter>;
  /** Negates the expression. */
  not?: InputMaybe<CompanyInfoFilter>;
  /** Checks for any expressions in this list. */
  or?: InputMaybe<Array<CompanyInfoFilter>>;
  /** Filter by the object’s `personInChargeEmail` field. */
  personInChargeEmail?: InputMaybe<StringFilter>;
  /** Filter by the object’s `personInChargeEmailBill` field. */
  personInChargeEmailBill?: InputMaybe<StringFilter>;
  /** Filter by the object’s `personInChargeName` field. */
  personInChargeName?: InputMaybe<StringFilter>;
  /** Filter by the object’s `personInChargeNameBill` field. */
  personInChargeNameBill?: InputMaybe<StringFilter>;
  /** Filter by the object’s `postCode` field. */
  postCode?: InputMaybe<StringFilter>;
  /** Filter by the object’s `postCodeBill` field. */
  postCodeBill?: InputMaybe<StringFilter>;
  /** Filter by the object’s `productNames` field. */
  productNames?: InputMaybe<StringListFilter>;
  /** Filter by the object’s `productPriorities` field. */
  productPriorities?: InputMaybe<IntListFilter>;
  /** Filter by the object’s `projectManager` field. */
  projectManager?: InputMaybe<StringFilter>;
  /** Filter by the object’s `sendOriginalMail` field. */
  sendOriginalMail?: InputMaybe<BooleanFilter>;
  /** Filter by the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<DatetimeFilter>;
};

/** An input for mutations affecting `CompanyInfo` */
export type CompanyInfoInput = {
  address?: InputMaybe<Scalars['String']['input']>;
  addressBill?: InputMaybe<Scalars['String']['input']>;
  adsOperationManager?: InputMaybe<Scalars['String']['input']>;
  adsOperationStaff?: InputMaybe<Scalars['String']['input']>;
  approver?: InputMaybe<Scalars['String']['input']>;
  billingMethodProblem?: InputMaybe<Scalars['Boolean']['input']>;
  billingMethodProblemText?: InputMaybe<Scalars['String']['input']>;
  /** 企業ID */
  companyId?: InputMaybe<Scalars['UUID']['input']>;
  corporateName?: InputMaybe<Scalars['String']['input']>;
  corporateNameBill?: InputMaybe<Scalars['String']['input']>;
  corporateNameKana?: InputMaybe<Scalars['String']['input']>;
  corporateNameKanaBill?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  creativeManager?: InputMaybe<Scalars['String']['input']>;
  departmentName?: InputMaybe<Scalars['String']['input']>;
  departmentNameBill?: InputMaybe<Scalars['String']['input']>;
  /** 契約情報/企業情報ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  mobileNumber?: InputMaybe<Scalars['String']['input']>;
  mobileNumberBill?: InputMaybe<Scalars['String']['input']>;
  personInChargeEmail?: InputMaybe<Scalars['String']['input']>;
  personInChargeEmailBill?: InputMaybe<Scalars['String']['input']>;
  personInChargeName?: InputMaybe<Scalars['String']['input']>;
  personInChargeNameBill?: InputMaybe<Scalars['String']['input']>;
  postCode?: InputMaybe<Scalars['String']['input']>;
  postCodeBill?: InputMaybe<Scalars['String']['input']>;
  productNames?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  productPriorities?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  projectManager?: InputMaybe<Scalars['String']['input']>;
  sendOriginalMail?: InputMaybe<Scalars['Boolean']['input']>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** Represents an update to a `CompanyInfo`. Fields that are set will be updated. */
export type CompanyInfoPatch = {
  address?: InputMaybe<Scalars['String']['input']>;
  addressBill?: InputMaybe<Scalars['String']['input']>;
  adsOperationManager?: InputMaybe<Scalars['String']['input']>;
  adsOperationStaff?: InputMaybe<Scalars['String']['input']>;
  approver?: InputMaybe<Scalars['String']['input']>;
  billingMethodProblem?: InputMaybe<Scalars['Boolean']['input']>;
  billingMethodProblemText?: InputMaybe<Scalars['String']['input']>;
  /** 企業ID */
  companyId?: InputMaybe<Scalars['UUID']['input']>;
  corporateName?: InputMaybe<Scalars['String']['input']>;
  corporateNameBill?: InputMaybe<Scalars['String']['input']>;
  corporateNameKana?: InputMaybe<Scalars['String']['input']>;
  corporateNameKanaBill?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  creativeManager?: InputMaybe<Scalars['String']['input']>;
  departmentName?: InputMaybe<Scalars['String']['input']>;
  departmentNameBill?: InputMaybe<Scalars['String']['input']>;
  /** 契約情報/企業情報ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  mobileNumber?: InputMaybe<Scalars['String']['input']>;
  mobileNumberBill?: InputMaybe<Scalars['String']['input']>;
  personInChargeEmail?: InputMaybe<Scalars['String']['input']>;
  personInChargeEmailBill?: InputMaybe<Scalars['String']['input']>;
  personInChargeName?: InputMaybe<Scalars['String']['input']>;
  personInChargeNameBill?: InputMaybe<Scalars['String']['input']>;
  postCode?: InputMaybe<Scalars['String']['input']>;
  postCodeBill?: InputMaybe<Scalars['String']['input']>;
  productNames?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  productPriorities?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  projectManager?: InputMaybe<Scalars['String']['input']>;
  sendOriginalMail?: InputMaybe<Scalars['Boolean']['input']>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** A connection to a list of `CompanyInfo` values. */
export type CompanyInfosConnection = {
  __typename?: 'CompanyInfosConnection';
  /** A list of edges which contains the `CompanyInfo` and cursor to aid in pagination. */
  edges: Array<CompanyInfosEdge>;
  /** A list of `CompanyInfo` objects. */
  nodes: Array<Maybe<CompanyInfo>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `CompanyInfo` you could get from the connection. */
  totalCount: Scalars['Int']['output'];
};

/** A `CompanyInfo` edge in the connection. */
export type CompanyInfosEdge = {
  __typename?: 'CompanyInfosEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']['output']>;
  /** The `CompanyInfo` at the end of the edge. */
  node?: Maybe<CompanyInfo>;
};

/** Methods to use when ordering `CompanyInfo`. */
export enum CompanyInfosOrderBy {
  AddressAsc = 'ADDRESS_ASC',
  AddressBillAsc = 'ADDRESS_BILL_ASC',
  AddressBillDesc = 'ADDRESS_BILL_DESC',
  AddressDesc = 'ADDRESS_DESC',
  AdsOperationManagerAsc = 'ADS_OPERATION_MANAGER_ASC',
  AdsOperationManagerDesc = 'ADS_OPERATION_MANAGER_DESC',
  AdsOperationStaffAsc = 'ADS_OPERATION_STAFF_ASC',
  AdsOperationStaffDesc = 'ADS_OPERATION_STAFF_DESC',
  ApproverAsc = 'APPROVER_ASC',
  ApproverDesc = 'APPROVER_DESC',
  BillingMethodProblemAsc = 'BILLING_METHOD_PROBLEM_ASC',
  BillingMethodProblemDesc = 'BILLING_METHOD_PROBLEM_DESC',
  BillingMethodProblemTextAsc = 'BILLING_METHOD_PROBLEM_TEXT_ASC',
  BillingMethodProblemTextDesc = 'BILLING_METHOD_PROBLEM_TEXT_DESC',
  CompanyIdAsc = 'COMPANY_ID_ASC',
  CompanyIdDesc = 'COMPANY_ID_DESC',
  CorporateNameAsc = 'CORPORATE_NAME_ASC',
  CorporateNameBillAsc = 'CORPORATE_NAME_BILL_ASC',
  CorporateNameBillDesc = 'CORPORATE_NAME_BILL_DESC',
  CorporateNameDesc = 'CORPORATE_NAME_DESC',
  CorporateNameKanaAsc = 'CORPORATE_NAME_KANA_ASC',
  CorporateNameKanaBillAsc = 'CORPORATE_NAME_KANA_BILL_ASC',
  CorporateNameKanaBillDesc = 'CORPORATE_NAME_KANA_BILL_DESC',
  CorporateNameKanaDesc = 'CORPORATE_NAME_KANA_DESC',
  CreatedAtAsc = 'CREATED_AT_ASC',
  CreatedAtDesc = 'CREATED_AT_DESC',
  CreativeManagerAsc = 'CREATIVE_MANAGER_ASC',
  CreativeManagerDesc = 'CREATIVE_MANAGER_DESC',
  DepartmentNameAsc = 'DEPARTMENT_NAME_ASC',
  DepartmentNameBillAsc = 'DEPARTMENT_NAME_BILL_ASC',
  DepartmentNameBillDesc = 'DEPARTMENT_NAME_BILL_DESC',
  DepartmentNameDesc = 'DEPARTMENT_NAME_DESC',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  MobileNumberAsc = 'MOBILE_NUMBER_ASC',
  MobileNumberBillAsc = 'MOBILE_NUMBER_BILL_ASC',
  MobileNumberBillDesc = 'MOBILE_NUMBER_BILL_DESC',
  MobileNumberDesc = 'MOBILE_NUMBER_DESC',
  Natural = 'NATURAL',
  PersonInChargeEmailAsc = 'PERSON_IN_CHARGE_EMAIL_ASC',
  PersonInChargeEmailBillAsc = 'PERSON_IN_CHARGE_EMAIL_BILL_ASC',
  PersonInChargeEmailBillDesc = 'PERSON_IN_CHARGE_EMAIL_BILL_DESC',
  PersonInChargeEmailDesc = 'PERSON_IN_CHARGE_EMAIL_DESC',
  PersonInChargeNameAsc = 'PERSON_IN_CHARGE_NAME_ASC',
  PersonInChargeNameBillAsc = 'PERSON_IN_CHARGE_NAME_BILL_ASC',
  PersonInChargeNameBillDesc = 'PERSON_IN_CHARGE_NAME_BILL_DESC',
  PersonInChargeNameDesc = 'PERSON_IN_CHARGE_NAME_DESC',
  PostCodeAsc = 'POST_CODE_ASC',
  PostCodeBillAsc = 'POST_CODE_BILL_ASC',
  PostCodeBillDesc = 'POST_CODE_BILL_DESC',
  PostCodeDesc = 'POST_CODE_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
  ProductNamesAsc = 'PRODUCT_NAMES_ASC',
  ProductNamesDesc = 'PRODUCT_NAMES_DESC',
  ProductPrioritiesAsc = 'PRODUCT_PRIORITIES_ASC',
  ProductPrioritiesDesc = 'PRODUCT_PRIORITIES_DESC',
  ProjectManagerAsc = 'PROJECT_MANAGER_ASC',
  ProjectManagerDesc = 'PROJECT_MANAGER_DESC',
  SendOriginalMailAsc = 'SEND_ORIGINAL_MAIL_ASC',
  SendOriginalMailDesc = 'SEND_ORIGINAL_MAIL_DESC',
  UpdatedAtAsc = 'UPDATED_AT_ASC',
  UpdatedAtDesc = 'UPDATED_AT_DESC'
}

/** An input for mutations affecting `Company` */
export type CompanyInput = {
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** 企業ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** Salesforce企業ID */
  sfAccountId?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** Represents an update to a `Company`. Fields that are set will be updated. */
export type CompanyPatch = {
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** 企業ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** Salesforce企業ID */
  sfAccountId?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** All input for the create `AlembicVersion` mutation. */
export type CreateAlembicVersionInput = {
  /** The `AlembicVersion` to be created by this mutation. */
  alembicVersion: AlembicVersionInput;
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
};

/** The output of our create `AlembicVersion` mutation. */
export type CreateAlembicVersionPayload = {
  __typename?: 'CreateAlembicVersionPayload';
  /** The `AlembicVersion` that was created by this mutation. */
  alembicVersion?: Maybe<AlembicVersion>;
  /** An edge for our `AlembicVersion`. May be used by Relay 1. */
  alembicVersionEdge?: Maybe<AlembicVersionsEdge>;
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our create `AlembicVersion` mutation. */
export type CreateAlembicVersionPayloadAlembicVersionEdgeArgs = {
  orderBy?: InputMaybe<Array<AlembicVersionsOrderBy>>;
};

/** All input for the create `CompanyInfo` mutation. */
export type CreateCompanyInfoInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The `CompanyInfo` to be created by this mutation. */
  companyInfo: CompanyInfoInput;
};

/** The output of our create `CompanyInfo` mutation. */
export type CreateCompanyInfoPayload = {
  __typename?: 'CreateCompanyInfoPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Company` that is related to this `CompanyInfo`. */
  companyByCompanyId?: Maybe<Company>;
  /** The `CompanyInfo` that was created by this mutation. */
  companyInfo?: Maybe<CompanyInfo>;
  /** An edge for our `CompanyInfo`. May be used by Relay 1. */
  companyInfoEdge?: Maybe<CompanyInfosEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our create `CompanyInfo` mutation. */
export type CreateCompanyInfoPayloadCompanyInfoEdgeArgs = {
  orderBy?: InputMaybe<Array<CompanyInfosOrderBy>>;
};

/** All input for the create `Company` mutation. */
export type CreateCompanyInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The `Company` to be created by this mutation. */
  company: CompanyInput;
};

/** The output of our create `Company` mutation. */
export type CreateCompanyPayload = {
  __typename?: 'CreateCompanyPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `Company` that was created by this mutation. */
  company?: Maybe<Company>;
  /** An edge for our `Company`. May be used by Relay 1. */
  companyEdge?: Maybe<CompaniesEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our create `Company` mutation. */
export type CreateCompanyPayloadCompanyEdgeArgs = {
  orderBy?: InputMaybe<Array<CompaniesOrderBy>>;
};

/** All input for the create `CustomerGroup` mutation. */
export type CreateCustomerGroupInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The `CustomerGroup` to be created by this mutation. */
  customerGroup: CustomerGroupInput;
};

/** The output of our create `CustomerGroup` mutation. */
export type CreateCustomerGroupPayload = {
  __typename?: 'CreateCustomerGroupPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `CustomerGroup` that was created by this mutation. */
  customerGroup?: Maybe<CustomerGroup>;
  /** An edge for our `CustomerGroup`. May be used by Relay 1. */
  customerGroupEdge?: Maybe<CustomerGroupsEdge>;
  /** Reads a single `Opportunity` that is related to this `CustomerGroup`. */
  opportunityByOpportunityId?: Maybe<Opportunity>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our create `CustomerGroup` mutation. */
export type CreateCustomerGroupPayloadCustomerGroupEdgeArgs = {
  orderBy?: InputMaybe<Array<CustomerGroupsOrderBy>>;
};

/** All input for the create `MarketingActivityInformation` mutation. */
export type CreateMarketingActivityInformationInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The `MarketingActivityInformation` to be created by this mutation. */
  marketingActivityInformation: MarketingActivityInformationInput;
};

/** The output of our create `MarketingActivityInformation` mutation. */
export type CreateMarketingActivityInformationPayload = {
  __typename?: 'CreateMarketingActivityInformationPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `MarketingActivityInformation` that was created by this mutation. */
  marketingActivityInformation?: Maybe<MarketingActivityInformation>;
  /** An edge for our `MarketingActivityInformation`. May be used by Relay 1. */
  marketingActivityInformationEdge?: Maybe<MarketingActivityInformationsEdge>;
  /** Reads a single `Product` that is related to this `MarketingActivityInformation`. */
  productByProductId?: Maybe<Product>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our create `MarketingActivityInformation` mutation. */
export type CreateMarketingActivityInformationPayloadMarketingActivityInformationEdgeArgs = {
  orderBy?: InputMaybe<Array<MarketingActivityInformationsOrderBy>>;
};

/** All input for the create `Notification` mutation. */
export type CreateNotificationInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The `Notification` to be created by this mutation. */
  notification: NotificationInput;
};

/** The output of our create `Notification` mutation. */
export type CreateNotificationPayload = {
  __typename?: 'CreateNotificationPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `Notification` that was created by this mutation. */
  notification?: Maybe<Notification>;
  /** An edge for our `Notification`. May be used by Relay 1. */
  notificationEdge?: Maybe<NotificationsEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our create `Notification` mutation. */
export type CreateNotificationPayloadNotificationEdgeArgs = {
  orderBy?: InputMaybe<Array<NotificationsOrderBy>>;
};

/** All input for the create `OperatorGroup` mutation. */
export type CreateOperatorGroupInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The `OperatorGroup` to be created by this mutation. */
  operatorGroup: OperatorGroupInput;
};

/** The output of our create `OperatorGroup` mutation. */
export type CreateOperatorGroupPayload = {
  __typename?: 'CreateOperatorGroupPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `OperatorGroup` that was created by this mutation. */
  operatorGroup?: Maybe<OperatorGroup>;
  /** An edge for our `OperatorGroup`. May be used by Relay 1. */
  operatorGroupEdge?: Maybe<OperatorGroupsEdge>;
  /** Reads a single `Opportunity` that is related to this `OperatorGroup`. */
  opportunityByOpportunityId?: Maybe<Opportunity>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our create `OperatorGroup` mutation. */
export type CreateOperatorGroupPayloadOperatorGroupEdgeArgs = {
  orderBy?: InputMaybe<Array<OperatorGroupsOrderBy>>;
};

/** All input for the create `OperatorName` mutation. */
export type CreateOperatorNameInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The `OperatorName` to be created by this mutation. */
  operatorName: OperatorNameInput;
};

/** The output of our create `OperatorName` mutation. */
export type CreateOperatorNamePayload = {
  __typename?: 'CreateOperatorNamePayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `OperatorName` that was created by this mutation. */
  operatorName?: Maybe<OperatorName>;
  /** An edge for our `OperatorName`. May be used by Relay 1. */
  operatorNameEdge?: Maybe<OperatorNamesEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our create `OperatorName` mutation. */
export type CreateOperatorNamePayloadOperatorNameEdgeArgs = {
  orderBy?: InputMaybe<Array<OperatorNamesOrderBy>>;
};

/** All input for the create `Opportunity` mutation. */
export type CreateOpportunityInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The `Opportunity` to be created by this mutation. */
  opportunity: OpportunityInput;
};

/** The output of our create `Opportunity` mutation. */
export type CreateOpportunityPayload = {
  __typename?: 'CreateOpportunityPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `Opportunity` that was created by this mutation. */
  opportunity?: Maybe<Opportunity>;
  /** An edge for our `Opportunity`. May be used by Relay 1. */
  opportunityEdge?: Maybe<OpportunitiesEdge>;
  /** Reads a single `Product` that is related to this `Opportunity`. */
  productByProductId?: Maybe<Product>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our create `Opportunity` mutation. */
export type CreateOpportunityPayloadOpportunityEdgeArgs = {
  orderBy?: InputMaybe<Array<OpportunitiesOrderBy>>;
};

/** All input for the create `ProductInformation` mutation. */
export type CreateProductInformationInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The `ProductInformation` to be created by this mutation. */
  productInformation: ProductInformationInput;
};

/** The output of our create `ProductInformation` mutation. */
export type CreateProductInformationPayload = {
  __typename?: 'CreateProductInformationPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Product` that is related to this `ProductInformation`. */
  productByProductId?: Maybe<Product>;
  /** The `ProductInformation` that was created by this mutation. */
  productInformation?: Maybe<ProductInformation>;
  /** An edge for our `ProductInformation`. May be used by Relay 1. */
  productInformationEdge?: Maybe<ProductInformationsEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our create `ProductInformation` mutation. */
export type CreateProductInformationPayloadProductInformationEdgeArgs = {
  orderBy?: InputMaybe<Array<ProductInformationsOrderBy>>;
};

/** All input for the create `Product` mutation. */
export type CreateProductInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The `Product` to be created by this mutation. */
  product: ProductInput;
};

/** The output of our create `Product` mutation. */
export type CreateProductPayload = {
  __typename?: 'CreateProductPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Company` that is related to this `Product`. */
  companyByCompanyId?: Maybe<Company>;
  /** The `Product` that was created by this mutation. */
  product?: Maybe<Product>;
  /** An edge for our `Product`. May be used by Relay 1. */
  productEdge?: Maybe<ProductsEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our create `Product` mutation. */
export type CreateProductPayloadProductEdgeArgs = {
  orderBy?: InputMaybe<Array<ProductsOrderBy>>;
};

/** All input for the create `RequestContent` mutation. */
export type CreateRequestContentInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The `RequestContent` to be created by this mutation. */
  requestContent: RequestContentInput;
};

/** The output of our create `RequestContent` mutation. */
export type CreateRequestContentPayload = {
  __typename?: 'CreateRequestContentPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Product` that is related to this `RequestContent`. */
  productByProductId?: Maybe<Product>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** The `RequestContent` that was created by this mutation. */
  requestContent?: Maybe<RequestContent>;
  /** An edge for our `RequestContent`. May be used by Relay 1. */
  requestContentEdge?: Maybe<RequestContentsEdge>;
};


/** The output of our create `RequestContent` mutation. */
export type CreateRequestContentPayloadRequestContentEdgeArgs = {
  orderBy?: InputMaybe<Array<RequestContentsOrderBy>>;
};

/** All input for the create `UserInformationMemo` mutation. */
export type CreateUserInformationMemoInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The `UserInformationMemo` to be created by this mutation. */
  userInformationMemo: UserInformationMemoInput;
};

/** The output of our create `UserInformationMemo` mutation. */
export type CreateUserInformationMemoPayload = {
  __typename?: 'CreateUserInformationMemoPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Product` that is related to this `UserInformationMemo`. */
  productByProductId?: Maybe<Product>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** The `UserInformationMemo` that was created by this mutation. */
  userInformationMemo?: Maybe<UserInformationMemo>;
  /** An edge for our `UserInformationMemo`. May be used by Relay 1. */
  userInformationMemoEdge?: Maybe<UserInformationMemosEdge>;
};


/** The output of our create `UserInformationMemo` mutation. */
export type CreateUserInformationMemoPayloadUserInformationMemoEdgeArgs = {
  orderBy?: InputMaybe<Array<UserInformationMemosOrderBy>>;
};

/** 顧客グループ */
export type CustomerGroup = Node & {
  __typename?: 'CustomerGroup';
  /** Cognito/Salesforceメール */
  email: Scalars['String']['output'];
  /** 顧客グループID */
  id: Scalars['Int']['output'];
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID']['output'];
  /** Reads a single `Opportunity` that is related to this `CustomerGroup`. */
  opportunityByOpportunityId?: Maybe<Opportunity>;
  /** 商談ID */
  opportunityId: Scalars['UUID']['output'];
  /** SalesforceユーザID */
  sfContactId: Scalars['String']['output'];
  /** Cognit ID */
  username?: Maybe<Scalars['String']['output']>;
};

/**
 * A condition to be used against `CustomerGroup` object types. All fields are
 * tested for equality and combined with a logical ‘and.’
 */
export type CustomerGroupCondition = {
  /** Checks for equality with the object’s `email` field. */
  email?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `id` field. */
  id?: InputMaybe<Scalars['Int']['input']>;
  /** Checks for equality with the object’s `opportunityId` field. */
  opportunityId?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `sfContactId` field. */
  sfContactId?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `username` field. */
  username?: InputMaybe<Scalars['String']['input']>;
};

/** A filter to be used against `CustomerGroup` object types. All fields are combined with a logical ‘and.’ */
export type CustomerGroupFilter = {
  /** Checks for all expressions in this list. */
  and?: InputMaybe<Array<CustomerGroupFilter>>;
  /** Filter by the object’s `email` field. */
  email?: InputMaybe<StringFilter>;
  /** Filter by the object’s `id` field. */
  id?: InputMaybe<IntFilter>;
  /** Negates the expression. */
  not?: InputMaybe<CustomerGroupFilter>;
  /** Filter by the object’s `opportunityId` field. */
  opportunityId?: InputMaybe<UuidFilter>;
  /** Checks for any expressions in this list. */
  or?: InputMaybe<Array<CustomerGroupFilter>>;
  /** Filter by the object’s `sfContactId` field. */
  sfContactId?: InputMaybe<StringFilter>;
  /** Filter by the object’s `username` field. */
  username?: InputMaybe<StringFilter>;
};

/** An input for mutations affecting `CustomerGroup` */
export type CustomerGroupInput = {
  /** Cognito/Salesforceメール */
  email: Scalars['String']['input'];
  /** 顧客グループID */
  id?: InputMaybe<Scalars['Int']['input']>;
  /** 商談ID */
  opportunityId: Scalars['UUID']['input'];
  /** SalesforceユーザID */
  sfContactId: Scalars['String']['input'];
  /** Cognit ID */
  username?: InputMaybe<Scalars['String']['input']>;
};

/** Represents an update to a `CustomerGroup`. Fields that are set will be updated. */
export type CustomerGroupPatch = {
  /** Cognito/Salesforceメール */
  email?: InputMaybe<Scalars['String']['input']>;
  /** 顧客グループID */
  id?: InputMaybe<Scalars['Int']['input']>;
  /** 商談ID */
  opportunityId?: InputMaybe<Scalars['UUID']['input']>;
  /** SalesforceユーザID */
  sfContactId?: InputMaybe<Scalars['String']['input']>;
  /** Cognit ID */
  username?: InputMaybe<Scalars['String']['input']>;
};

/** A connection to a list of `CustomerGroup` values. */
export type CustomerGroupsConnection = {
  __typename?: 'CustomerGroupsConnection';
  /** A list of edges which contains the `CustomerGroup` and cursor to aid in pagination. */
  edges: Array<CustomerGroupsEdge>;
  /** A list of `CustomerGroup` objects. */
  nodes: Array<Maybe<CustomerGroup>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `CustomerGroup` you could get from the connection. */
  totalCount: Scalars['Int']['output'];
};

/** A `CustomerGroup` edge in the connection. */
export type CustomerGroupsEdge = {
  __typename?: 'CustomerGroupsEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']['output']>;
  /** The `CustomerGroup` at the end of the edge. */
  node?: Maybe<CustomerGroup>;
};

/** Methods to use when ordering `CustomerGroup`. */
export enum CustomerGroupsOrderBy {
  EmailAsc = 'EMAIL_ASC',
  EmailDesc = 'EMAIL_DESC',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  Natural = 'NATURAL',
  OpportunityIdAsc = 'OPPORTUNITY_ID_ASC',
  OpportunityIdDesc = 'OPPORTUNITY_ID_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
  SfContactIdAsc = 'SF_CONTACT_ID_ASC',
  SfContactIdDesc = 'SF_CONTACT_ID_DESC',
  UsernameAsc = 'USERNAME_ASC',
  UsernameDesc = 'USERNAME_DESC'
}

/** A filter to be used against Datetime fields. All fields are combined with a logical ‘and.’ */
export type DatetimeFilter = {
  /** Not equal to the specified value, treating null like an ordinary value. */
  distinctFrom?: InputMaybe<Scalars['Datetime']['input']>;
  /** Equal to the specified value. */
  equalTo?: InputMaybe<Scalars['Datetime']['input']>;
  /** Greater than the specified value. */
  greaterThan?: InputMaybe<Scalars['Datetime']['input']>;
  /** Greater than or equal to the specified value. */
  greaterThanOrEqualTo?: InputMaybe<Scalars['Datetime']['input']>;
  /** Included in the specified list. */
  in?: InputMaybe<Array<Scalars['Datetime']['input']>>;
  /** Is null (if `true` is specified) or is not null (if `false` is specified). */
  isNull?: InputMaybe<Scalars['Boolean']['input']>;
  /** Less than the specified value. */
  lessThan?: InputMaybe<Scalars['Datetime']['input']>;
  /** Less than or equal to the specified value. */
  lessThanOrEqualTo?: InputMaybe<Scalars['Datetime']['input']>;
  /** Equal to the specified value, treating null like an ordinary value. */
  notDistinctFrom?: InputMaybe<Scalars['Datetime']['input']>;
  /** Not equal to the specified value. */
  notEqualTo?: InputMaybe<Scalars['Datetime']['input']>;
  /** Not included in the specified list. */
  notIn?: InputMaybe<Array<Scalars['Datetime']['input']>>;
};

/** All input for the `deleteAlembicVersionByVersionNum` mutation. */
export type DeleteAlembicVersionByVersionNumInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  versionNum: Scalars['String']['input'];
};

/** All input for the `deleteAlembicVersion` mutation. */
export type DeleteAlembicVersionInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `AlembicVersion` to be deleted. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our delete `AlembicVersion` mutation. */
export type DeleteAlembicVersionPayload = {
  __typename?: 'DeleteAlembicVersionPayload';
  /** The `AlembicVersion` that was deleted by this mutation. */
  alembicVersion?: Maybe<AlembicVersion>;
  /** An edge for our `AlembicVersion`. May be used by Relay 1. */
  alembicVersionEdge?: Maybe<AlembicVersionsEdge>;
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  deletedAlembicVersionId?: Maybe<Scalars['ID']['output']>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our delete `AlembicVersion` mutation. */
export type DeleteAlembicVersionPayloadAlembicVersionEdgeArgs = {
  orderBy?: InputMaybe<Array<AlembicVersionsOrderBy>>;
};

/** All input for the `deleteCompanyById` mutation. */
export type DeleteCompanyByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** 企業ID */
  id: Scalars['UUID']['input'];
};

/** All input for the `deleteCompanyInfoById` mutation. */
export type DeleteCompanyInfoByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** 契約情報/企業情報ID */
  id: Scalars['UUID']['input'];
};

/** All input for the `deleteCompanyInfo` mutation. */
export type DeleteCompanyInfoInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `CompanyInfo` to be deleted. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our delete `CompanyInfo` mutation. */
export type DeleteCompanyInfoPayload = {
  __typename?: 'DeleteCompanyInfoPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Company` that is related to this `CompanyInfo`. */
  companyByCompanyId?: Maybe<Company>;
  /** The `CompanyInfo` that was deleted by this mutation. */
  companyInfo?: Maybe<CompanyInfo>;
  /** An edge for our `CompanyInfo`. May be used by Relay 1. */
  companyInfoEdge?: Maybe<CompanyInfosEdge>;
  deletedCompanyInfoId?: Maybe<Scalars['ID']['output']>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our delete `CompanyInfo` mutation. */
export type DeleteCompanyInfoPayloadCompanyInfoEdgeArgs = {
  orderBy?: InputMaybe<Array<CompanyInfosOrderBy>>;
};

/** All input for the `deleteCompany` mutation. */
export type DeleteCompanyInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `Company` to be deleted. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our delete `Company` mutation. */
export type DeleteCompanyPayload = {
  __typename?: 'DeleteCompanyPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `Company` that was deleted by this mutation. */
  company?: Maybe<Company>;
  /** An edge for our `Company`. May be used by Relay 1. */
  companyEdge?: Maybe<CompaniesEdge>;
  deletedCompanyId?: Maybe<Scalars['ID']['output']>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our delete `Company` mutation. */
export type DeleteCompanyPayloadCompanyEdgeArgs = {
  orderBy?: InputMaybe<Array<CompaniesOrderBy>>;
};

/** All input for the `deleteCustomerGroupById` mutation. */
export type DeleteCustomerGroupByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** 顧客グループID */
  id: Scalars['Int']['input'];
};

/** All input for the `deleteCustomerGroupByOpportunityIdAndSfContactId` mutation. */
export type DeleteCustomerGroupByOpportunityIdAndSfContactIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** 商談ID */
  opportunityId: Scalars['UUID']['input'];
  /** SalesforceユーザID */
  sfContactId: Scalars['String']['input'];
};

/** All input for the `deleteCustomerGroup` mutation. */
export type DeleteCustomerGroupInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `CustomerGroup` to be deleted. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our delete `CustomerGroup` mutation. */
export type DeleteCustomerGroupPayload = {
  __typename?: 'DeleteCustomerGroupPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `CustomerGroup` that was deleted by this mutation. */
  customerGroup?: Maybe<CustomerGroup>;
  /** An edge for our `CustomerGroup`. May be used by Relay 1. */
  customerGroupEdge?: Maybe<CustomerGroupsEdge>;
  deletedCustomerGroupId?: Maybe<Scalars['ID']['output']>;
  /** Reads a single `Opportunity` that is related to this `CustomerGroup`. */
  opportunityByOpportunityId?: Maybe<Opportunity>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our delete `CustomerGroup` mutation. */
export type DeleteCustomerGroupPayloadCustomerGroupEdgeArgs = {
  orderBy?: InputMaybe<Array<CustomerGroupsOrderBy>>;
};

/** All input for the `deleteMarketingActivityInformationById` mutation. */
export type DeleteMarketingActivityInformationByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** マーケティング活動情報ID */
  id: Scalars['UUID']['input'];
};

/** All input for the `deleteMarketingActivityInformation` mutation. */
export type DeleteMarketingActivityInformationInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `MarketingActivityInformation` to be deleted. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our delete `MarketingActivityInformation` mutation. */
export type DeleteMarketingActivityInformationPayload = {
  __typename?: 'DeleteMarketingActivityInformationPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  deletedMarketingActivityInformationId?: Maybe<Scalars['ID']['output']>;
  /** The `MarketingActivityInformation` that was deleted by this mutation. */
  marketingActivityInformation?: Maybe<MarketingActivityInformation>;
  /** An edge for our `MarketingActivityInformation`. May be used by Relay 1. */
  marketingActivityInformationEdge?: Maybe<MarketingActivityInformationsEdge>;
  /** Reads a single `Product` that is related to this `MarketingActivityInformation`. */
  productByProductId?: Maybe<Product>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our delete `MarketingActivityInformation` mutation. */
export type DeleteMarketingActivityInformationPayloadMarketingActivityInformationEdgeArgs = {
  orderBy?: InputMaybe<Array<MarketingActivityInformationsOrderBy>>;
};

/** All input for the `deleteNotificationById` mutation. */
export type DeleteNotificationByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** お知らせID */
  id: Scalars['UUID']['input'];
};

/** All input for the `deleteNotification` mutation. */
export type DeleteNotificationInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `Notification` to be deleted. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our delete `Notification` mutation. */
export type DeleteNotificationPayload = {
  __typename?: 'DeleteNotificationPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  deletedNotificationId?: Maybe<Scalars['ID']['output']>;
  /** The `Notification` that was deleted by this mutation. */
  notification?: Maybe<Notification>;
  /** An edge for our `Notification`. May be used by Relay 1. */
  notificationEdge?: Maybe<NotificationsEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our delete `Notification` mutation. */
export type DeleteNotificationPayloadNotificationEdgeArgs = {
  orderBy?: InputMaybe<Array<NotificationsOrderBy>>;
};

/** All input for the `deleteOperatorGroupById` mutation. */
export type DeleteOperatorGroupByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** 管理者グループID */
  id: Scalars['Int']['input'];
};

/** All input for the `deleteOperatorGroup` mutation. */
export type DeleteOperatorGroupInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `OperatorGroup` to be deleted. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our delete `OperatorGroup` mutation. */
export type DeleteOperatorGroupPayload = {
  __typename?: 'DeleteOperatorGroupPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  deletedOperatorGroupId?: Maybe<Scalars['ID']['output']>;
  /** The `OperatorGroup` that was deleted by this mutation. */
  operatorGroup?: Maybe<OperatorGroup>;
  /** An edge for our `OperatorGroup`. May be used by Relay 1. */
  operatorGroupEdge?: Maybe<OperatorGroupsEdge>;
  /** Reads a single `Opportunity` that is related to this `OperatorGroup`. */
  opportunityByOpportunityId?: Maybe<Opportunity>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our delete `OperatorGroup` mutation. */
export type DeleteOperatorGroupPayloadOperatorGroupEdgeArgs = {
  orderBy?: InputMaybe<Array<OperatorGroupsOrderBy>>;
};

/** All input for the `deleteOperatorNameByEmail` mutation. */
export type DeleteOperatorNameByEmailInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  email: Scalars['String']['input'];
};

/** All input for the `deleteOperatorName` mutation. */
export type DeleteOperatorNameInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `OperatorName` to be deleted. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our delete `OperatorName` mutation. */
export type DeleteOperatorNamePayload = {
  __typename?: 'DeleteOperatorNamePayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  deletedOperatorNameId?: Maybe<Scalars['ID']['output']>;
  /** The `OperatorName` that was deleted by this mutation. */
  operatorName?: Maybe<OperatorName>;
  /** An edge for our `OperatorName`. May be used by Relay 1. */
  operatorNameEdge?: Maybe<OperatorNamesEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our delete `OperatorName` mutation. */
export type DeleteOperatorNamePayloadOperatorNameEdgeArgs = {
  orderBy?: InputMaybe<Array<OperatorNamesOrderBy>>;
};

/** All input for the `deleteOpportunityById` mutation. */
export type DeleteOpportunityByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** 商談ID */
  id: Scalars['UUID']['input'];
};

/** All input for the `deleteOpportunity` mutation. */
export type DeleteOpportunityInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `Opportunity` to be deleted. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our delete `Opportunity` mutation. */
export type DeleteOpportunityPayload = {
  __typename?: 'DeleteOpportunityPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  deletedOpportunityId?: Maybe<Scalars['ID']['output']>;
  /** The `Opportunity` that was deleted by this mutation. */
  opportunity?: Maybe<Opportunity>;
  /** An edge for our `Opportunity`. May be used by Relay 1. */
  opportunityEdge?: Maybe<OpportunitiesEdge>;
  /** Reads a single `Product` that is related to this `Opportunity`. */
  productByProductId?: Maybe<Product>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our delete `Opportunity` mutation. */
export type DeleteOpportunityPayloadOpportunityEdgeArgs = {
  orderBy?: InputMaybe<Array<OpportunitiesOrderBy>>;
};

/** All input for the `deleteProductByCompanyIdAndProductName` mutation. */
export type DeleteProductByCompanyIdAndProductNameInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** 企業ID */
  companyId: Scalars['UUID']['input'];
  /** 商材名 */
  productName: Scalars['String']['input'];
};

/** All input for the `deleteProductById` mutation. */
export type DeleteProductByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** 商材ID */
  id: Scalars['UUID']['input'];
};

/** All input for the `deleteProductInformationById` mutation. */
export type DeleteProductInformationByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** 商材情報ID */
  id: Scalars['UUID']['input'];
};

/** All input for the `deleteProductInformation` mutation. */
export type DeleteProductInformationInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `ProductInformation` to be deleted. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our delete `ProductInformation` mutation. */
export type DeleteProductInformationPayload = {
  __typename?: 'DeleteProductInformationPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  deletedProductInformationId?: Maybe<Scalars['ID']['output']>;
  /** Reads a single `Product` that is related to this `ProductInformation`. */
  productByProductId?: Maybe<Product>;
  /** The `ProductInformation` that was deleted by this mutation. */
  productInformation?: Maybe<ProductInformation>;
  /** An edge for our `ProductInformation`. May be used by Relay 1. */
  productInformationEdge?: Maybe<ProductInformationsEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our delete `ProductInformation` mutation. */
export type DeleteProductInformationPayloadProductInformationEdgeArgs = {
  orderBy?: InputMaybe<Array<ProductInformationsOrderBy>>;
};

/** All input for the `deleteProduct` mutation. */
export type DeleteProductInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `Product` to be deleted. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our delete `Product` mutation. */
export type DeleteProductPayload = {
  __typename?: 'DeleteProductPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Company` that is related to this `Product`. */
  companyByCompanyId?: Maybe<Company>;
  deletedProductId?: Maybe<Scalars['ID']['output']>;
  /** The `Product` that was deleted by this mutation. */
  product?: Maybe<Product>;
  /** An edge for our `Product`. May be used by Relay 1. */
  productEdge?: Maybe<ProductsEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our delete `Product` mutation. */
export type DeleteProductPayloadProductEdgeArgs = {
  orderBy?: InputMaybe<Array<ProductsOrderBy>>;
};

/** All input for the `deleteRequestContentById` mutation. */
export type DeleteRequestContentByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** ご依頼内容ID */
  id: Scalars['UUID']['input'];
};

/** All input for the `deleteRequestContent` mutation. */
export type DeleteRequestContentInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `RequestContent` to be deleted. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our delete `RequestContent` mutation. */
export type DeleteRequestContentPayload = {
  __typename?: 'DeleteRequestContentPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  deletedRequestContentId?: Maybe<Scalars['ID']['output']>;
  /** Reads a single `Product` that is related to this `RequestContent`. */
  productByProductId?: Maybe<Product>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** The `RequestContent` that was deleted by this mutation. */
  requestContent?: Maybe<RequestContent>;
  /** An edge for our `RequestContent`. May be used by Relay 1. */
  requestContentEdge?: Maybe<RequestContentsEdge>;
};


/** The output of our delete `RequestContent` mutation. */
export type DeleteRequestContentPayloadRequestContentEdgeArgs = {
  orderBy?: InputMaybe<Array<RequestContentsOrderBy>>;
};

/** All input for the `deleteUserInformationMemoById` mutation. */
export type DeleteUserInformationMemoByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** ご依頼内容ID */
  id: Scalars['UUID']['input'];
};

/** All input for the `deleteUserInformationMemo` mutation. */
export type DeleteUserInformationMemoInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `UserInformationMemo` to be deleted. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our delete `UserInformationMemo` mutation. */
export type DeleteUserInformationMemoPayload = {
  __typename?: 'DeleteUserInformationMemoPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  deletedUserInformationMemoId?: Maybe<Scalars['ID']['output']>;
  /** Reads a single `Product` that is related to this `UserInformationMemo`. */
  productByProductId?: Maybe<Product>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** The `UserInformationMemo` that was deleted by this mutation. */
  userInformationMemo?: Maybe<UserInformationMemo>;
  /** An edge for our `UserInformationMemo`. May be used by Relay 1. */
  userInformationMemoEdge?: Maybe<UserInformationMemosEdge>;
};


/** The output of our delete `UserInformationMemo` mutation. */
export type DeleteUserInformationMemoPayloadUserInformationMemoEdgeArgs = {
  orderBy?: InputMaybe<Array<UserInformationMemosOrderBy>>;
};

/** A filter to be used against Int fields. All fields are combined with a logical ‘and.’ */
export type IntFilter = {
  /** Not equal to the specified value, treating null like an ordinary value. */
  distinctFrom?: InputMaybe<Scalars['Int']['input']>;
  /** Equal to the specified value. */
  equalTo?: InputMaybe<Scalars['Int']['input']>;
  /** Greater than the specified value. */
  greaterThan?: InputMaybe<Scalars['Int']['input']>;
  /** Greater than or equal to the specified value. */
  greaterThanOrEqualTo?: InputMaybe<Scalars['Int']['input']>;
  /** Included in the specified list. */
  in?: InputMaybe<Array<Scalars['Int']['input']>>;
  /** Is null (if `true` is specified) or is not null (if `false` is specified). */
  isNull?: InputMaybe<Scalars['Boolean']['input']>;
  /** Less than the specified value. */
  lessThan?: InputMaybe<Scalars['Int']['input']>;
  /** Less than or equal to the specified value. */
  lessThanOrEqualTo?: InputMaybe<Scalars['Int']['input']>;
  /** Equal to the specified value, treating null like an ordinary value. */
  notDistinctFrom?: InputMaybe<Scalars['Int']['input']>;
  /** Not equal to the specified value. */
  notEqualTo?: InputMaybe<Scalars['Int']['input']>;
  /** Not included in the specified list. */
  notIn?: InputMaybe<Array<Scalars['Int']['input']>>;
};

/** A filter to be used against Int List fields. All fields are combined with a logical ‘and.’ */
export type IntListFilter = {
  /** Any array item is equal to the specified value. */
  anyEqualTo?: InputMaybe<Scalars['Int']['input']>;
  /** Any array item is greater than the specified value. */
  anyGreaterThan?: InputMaybe<Scalars['Int']['input']>;
  /** Any array item is greater than or equal to the specified value. */
  anyGreaterThanOrEqualTo?: InputMaybe<Scalars['Int']['input']>;
  /** Any array item is less than the specified value. */
  anyLessThan?: InputMaybe<Scalars['Int']['input']>;
  /** Any array item is less than or equal to the specified value. */
  anyLessThanOrEqualTo?: InputMaybe<Scalars['Int']['input']>;
  /** Any array item is not equal to the specified value. */
  anyNotEqualTo?: InputMaybe<Scalars['Int']['input']>;
  /** Contained by the specified list of values. */
  containedBy?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  /** Contains the specified list of values. */
  contains?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  /** Not equal to the specified value, treating null like an ordinary value. */
  distinctFrom?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  /** Equal to the specified value. */
  equalTo?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  /** Greater than the specified value. */
  greaterThan?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  /** Greater than or equal to the specified value. */
  greaterThanOrEqualTo?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  /** Is null (if `true` is specified) or is not null (if `false` is specified). */
  isNull?: InputMaybe<Scalars['Boolean']['input']>;
  /** Less than the specified value. */
  lessThan?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  /** Less than or equal to the specified value. */
  lessThanOrEqualTo?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  /** Equal to the specified value, treating null like an ordinary value. */
  notDistinctFrom?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  /** Not equal to the specified value. */
  notEqualTo?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  /** Overlaps the specified list of values. */
  overlaps?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
};

/** マーケティング活動情報 */
export type MarketingActivityInformation = Node & {
  __typename?: 'MarketingActivityInformation';
  adBudgetRatioListingDisplay?: Maybe<Scalars['String']['output']>;
  adMediaDecisionDone?: Maybe<Scalars['String']['output']>;
  adMonthlyBudget?: Maybe<Scalars['String']['output']>;
  adminCommentCvCpaDefinition?: Maybe<Scalars['String']['output']>;
  adminCommentDeliverySchedule?: Maybe<Scalars['String']['output']>;
  adminCommentMarketingBudget?: Maybe<Scalars['String']['output']>;
  adminCommentReference?: Maybe<Scalars['String']['output']>;
  cpaDefinition?: Maybe<Scalars['String']['output']>;
  createdAt?: Maybe<Scalars['Datetime']['output']>;
  customerConversionCorporate?: Maybe<Scalars['String']['output']>;
  customerConversionIndividual?: Maybe<Scalars['String']['output']>;
  displayAdBudgetRatio?: Maybe<Scalars['String']['output']>;
  displayAdBudgetRatioAnswerStatus?: Maybe<Scalars['String']['output']>;
  goalCalculationDone?: Maybe<Scalars['String']['output']>;
  havePlanSegment?: Maybe<Scalars['String']['output']>;
  /** マーケティング活動情報ID */
  id: Scalars['UUID']['output'];
  isCorporate?: Maybe<Scalars['Boolean']['output']>;
  kaizenFrequency?: Maybe<Scalars['String']['output']>;
  listingAdBudgetRatio?: Maybe<Scalars['String']['output']>;
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID']['output'];
  permissionResultAdToolView?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Product` that is related to this `MarketingActivityInformation`. */
  productByProductId?: Maybe<Product>;
  /** プロダクトID */
  productId?: Maybe<Scalars['UUID']['output']>;
  segmentDeliveryMethod?: Maybe<Scalars['String']['output']>;
  segmentMaterialsAnswerStatus?: Maybe<Scalars['String']['output']>;
  segmentMaterialsFileKeys?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  segmentMaterialsFiles?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  updatedAt?: Maybe<Scalars['Datetime']['output']>;
  withOtherAnalysisTool?: Maybe<Scalars['String']['output']>;
};

/**
 * A condition to be used against `MarketingActivityInformation` object types. All
 * fields are tested for equality and combined with a logical ‘and.’
 */
export type MarketingActivityInformationCondition = {
  /** Checks for equality with the object’s `adBudgetRatioListingDisplay` field. */
  adBudgetRatioListingDisplay?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adMediaDecisionDone` field. */
  adMediaDecisionDone?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adMonthlyBudget` field. */
  adMonthlyBudget?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentCvCpaDefinition` field. */
  adminCommentCvCpaDefinition?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentDeliverySchedule` field. */
  adminCommentDeliverySchedule?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentMarketingBudget` field. */
  adminCommentMarketingBudget?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentReference` field. */
  adminCommentReference?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `cpaDefinition` field. */
  cpaDefinition?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `createdAt` field. */
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** Checks for equality with the object’s `customerConversionCorporate` field. */
  customerConversionCorporate?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `customerConversionIndividual` field. */
  customerConversionIndividual?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `displayAdBudgetRatio` field. */
  displayAdBudgetRatio?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `displayAdBudgetRatioAnswerStatus` field. */
  displayAdBudgetRatioAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `goalCalculationDone` field. */
  goalCalculationDone?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `havePlanSegment` field. */
  havePlanSegment?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `id` field. */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `isCorporate` field. */
  isCorporate?: InputMaybe<Scalars['Boolean']['input']>;
  /** Checks for equality with the object’s `kaizenFrequency` field. */
  kaizenFrequency?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `listingAdBudgetRatio` field. */
  listingAdBudgetRatio?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `permissionResultAdToolView` field. */
  permissionResultAdToolView?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `productId` field. */
  productId?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `segmentDeliveryMethod` field. */
  segmentDeliveryMethod?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `segmentMaterialsAnswerStatus` field. */
  segmentMaterialsAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `segmentMaterialsFileKeys` field. */
  segmentMaterialsFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Checks for equality with the object’s `segmentMaterialsFiles` field. */
  segmentMaterialsFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Checks for equality with the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** Checks for equality with the object’s `withOtherAnalysisTool` field. */
  withOtherAnalysisTool?: InputMaybe<Scalars['String']['input']>;
};

/** A filter to be used against `MarketingActivityInformation` object types. All fields are combined with a logical ‘and.’ */
export type MarketingActivityInformationFilter = {
  /** Filter by the object’s `adBudgetRatioListingDisplay` field. */
  adBudgetRatioListingDisplay?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adMediaDecisionDone` field. */
  adMediaDecisionDone?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adMonthlyBudget` field. */
  adMonthlyBudget?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentCvCpaDefinition` field. */
  adminCommentCvCpaDefinition?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentDeliverySchedule` field. */
  adminCommentDeliverySchedule?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentMarketingBudget` field. */
  adminCommentMarketingBudget?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentReference` field. */
  adminCommentReference?: InputMaybe<StringFilter>;
  /** Checks for all expressions in this list. */
  and?: InputMaybe<Array<MarketingActivityInformationFilter>>;
  /** Filter by the object’s `cpaDefinition` field. */
  cpaDefinition?: InputMaybe<StringFilter>;
  /** Filter by the object’s `createdAt` field. */
  createdAt?: InputMaybe<DatetimeFilter>;
  /** Filter by the object’s `customerConversionCorporate` field. */
  customerConversionCorporate?: InputMaybe<StringFilter>;
  /** Filter by the object’s `customerConversionIndividual` field. */
  customerConversionIndividual?: InputMaybe<StringFilter>;
  /** Filter by the object’s `displayAdBudgetRatio` field. */
  displayAdBudgetRatio?: InputMaybe<StringFilter>;
  /** Filter by the object’s `displayAdBudgetRatioAnswerStatus` field. */
  displayAdBudgetRatioAnswerStatus?: InputMaybe<StringFilter>;
  /** Filter by the object’s `goalCalculationDone` field. */
  goalCalculationDone?: InputMaybe<StringFilter>;
  /** Filter by the object’s `havePlanSegment` field. */
  havePlanSegment?: InputMaybe<StringFilter>;
  /** Filter by the object’s `id` field. */
  id?: InputMaybe<UuidFilter>;
  /** Filter by the object’s `isCorporate` field. */
  isCorporate?: InputMaybe<BooleanFilter>;
  /** Filter by the object’s `kaizenFrequency` field. */
  kaizenFrequency?: InputMaybe<StringFilter>;
  /** Filter by the object’s `listingAdBudgetRatio` field. */
  listingAdBudgetRatio?: InputMaybe<StringFilter>;
  /** Negates the expression. */
  not?: InputMaybe<MarketingActivityInformationFilter>;
  /** Checks for any expressions in this list. */
  or?: InputMaybe<Array<MarketingActivityInformationFilter>>;
  /** Filter by the object’s `permissionResultAdToolView` field. */
  permissionResultAdToolView?: InputMaybe<StringFilter>;
  /** Filter by the object’s `productId` field. */
  productId?: InputMaybe<UuidFilter>;
  /** Filter by the object’s `segmentDeliveryMethod` field. */
  segmentDeliveryMethod?: InputMaybe<StringFilter>;
  /** Filter by the object’s `segmentMaterialsAnswerStatus` field. */
  segmentMaterialsAnswerStatus?: InputMaybe<StringFilter>;
  /** Filter by the object’s `segmentMaterialsFileKeys` field. */
  segmentMaterialsFileKeys?: InputMaybe<StringListFilter>;
  /** Filter by the object’s `segmentMaterialsFiles` field. */
  segmentMaterialsFiles?: InputMaybe<StringListFilter>;
  /** Filter by the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<DatetimeFilter>;
  /** Filter by the object’s `withOtherAnalysisTool` field. */
  withOtherAnalysisTool?: InputMaybe<StringFilter>;
};

/** An input for mutations affecting `MarketingActivityInformation` */
export type MarketingActivityInformationInput = {
  adBudgetRatioListingDisplay?: InputMaybe<Scalars['String']['input']>;
  adMediaDecisionDone?: InputMaybe<Scalars['String']['input']>;
  adMonthlyBudget?: InputMaybe<Scalars['String']['input']>;
  adminCommentCvCpaDefinition?: InputMaybe<Scalars['String']['input']>;
  adminCommentDeliverySchedule?: InputMaybe<Scalars['String']['input']>;
  adminCommentMarketingBudget?: InputMaybe<Scalars['String']['input']>;
  adminCommentReference?: InputMaybe<Scalars['String']['input']>;
  cpaDefinition?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  customerConversionCorporate?: InputMaybe<Scalars['String']['input']>;
  customerConversionIndividual?: InputMaybe<Scalars['String']['input']>;
  displayAdBudgetRatio?: InputMaybe<Scalars['String']['input']>;
  displayAdBudgetRatioAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  goalCalculationDone?: InputMaybe<Scalars['String']['input']>;
  havePlanSegment?: InputMaybe<Scalars['String']['input']>;
  /** マーケティング活動情報ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  isCorporate?: InputMaybe<Scalars['Boolean']['input']>;
  kaizenFrequency?: InputMaybe<Scalars['String']['input']>;
  listingAdBudgetRatio?: InputMaybe<Scalars['String']['input']>;
  permissionResultAdToolView?: InputMaybe<Scalars['String']['input']>;
  /** プロダクトID */
  productId?: InputMaybe<Scalars['UUID']['input']>;
  segmentDeliveryMethod?: InputMaybe<Scalars['String']['input']>;
  segmentMaterialsAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  segmentMaterialsFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  segmentMaterialsFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
  withOtherAnalysisTool?: InputMaybe<Scalars['String']['input']>;
};

/** Represents an update to a `MarketingActivityInformation`. Fields that are set will be updated. */
export type MarketingActivityInformationPatch = {
  adBudgetRatioListingDisplay?: InputMaybe<Scalars['String']['input']>;
  adMediaDecisionDone?: InputMaybe<Scalars['String']['input']>;
  adMonthlyBudget?: InputMaybe<Scalars['String']['input']>;
  adminCommentCvCpaDefinition?: InputMaybe<Scalars['String']['input']>;
  adminCommentDeliverySchedule?: InputMaybe<Scalars['String']['input']>;
  adminCommentMarketingBudget?: InputMaybe<Scalars['String']['input']>;
  adminCommentReference?: InputMaybe<Scalars['String']['input']>;
  cpaDefinition?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  customerConversionCorporate?: InputMaybe<Scalars['String']['input']>;
  customerConversionIndividual?: InputMaybe<Scalars['String']['input']>;
  displayAdBudgetRatio?: InputMaybe<Scalars['String']['input']>;
  displayAdBudgetRatioAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  goalCalculationDone?: InputMaybe<Scalars['String']['input']>;
  havePlanSegment?: InputMaybe<Scalars['String']['input']>;
  /** マーケティング活動情報ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  isCorporate?: InputMaybe<Scalars['Boolean']['input']>;
  kaizenFrequency?: InputMaybe<Scalars['String']['input']>;
  listingAdBudgetRatio?: InputMaybe<Scalars['String']['input']>;
  permissionResultAdToolView?: InputMaybe<Scalars['String']['input']>;
  /** プロダクトID */
  productId?: InputMaybe<Scalars['UUID']['input']>;
  segmentDeliveryMethod?: InputMaybe<Scalars['String']['input']>;
  segmentMaterialsAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  segmentMaterialsFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  segmentMaterialsFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
  withOtherAnalysisTool?: InputMaybe<Scalars['String']['input']>;
};

/** A connection to a list of `MarketingActivityInformation` values. */
export type MarketingActivityInformationsConnection = {
  __typename?: 'MarketingActivityInformationsConnection';
  /** A list of edges which contains the `MarketingActivityInformation` and cursor to aid in pagination. */
  edges: Array<MarketingActivityInformationsEdge>;
  /** A list of `MarketingActivityInformation` objects. */
  nodes: Array<Maybe<MarketingActivityInformation>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `MarketingActivityInformation` you could get from the connection. */
  totalCount: Scalars['Int']['output'];
};

/** A `MarketingActivityInformation` edge in the connection. */
export type MarketingActivityInformationsEdge = {
  __typename?: 'MarketingActivityInformationsEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']['output']>;
  /** The `MarketingActivityInformation` at the end of the edge. */
  node?: Maybe<MarketingActivityInformation>;
};

/** Methods to use when ordering `MarketingActivityInformation`. */
export enum MarketingActivityInformationsOrderBy {
  AdminCommentCvCpaDefinitionAsc = 'ADMIN_COMMENT_CV_CPA_DEFINITION_ASC',
  AdminCommentCvCpaDefinitionDesc = 'ADMIN_COMMENT_CV_CPA_DEFINITION_DESC',
  AdminCommentDeliveryScheduleAsc = 'ADMIN_COMMENT_DELIVERY_SCHEDULE_ASC',
  AdminCommentDeliveryScheduleDesc = 'ADMIN_COMMENT_DELIVERY_SCHEDULE_DESC',
  AdminCommentMarketingBudgetAsc = 'ADMIN_COMMENT_MARKETING_BUDGET_ASC',
  AdminCommentMarketingBudgetDesc = 'ADMIN_COMMENT_MARKETING_BUDGET_DESC',
  AdminCommentReferenceAsc = 'ADMIN_COMMENT_REFERENCE_ASC',
  AdminCommentReferenceDesc = 'ADMIN_COMMENT_REFERENCE_DESC',
  AdBudgetRatioListingDisplayAsc = 'AD_BUDGET_RATIO_LISTING_DISPLAY_ASC',
  AdBudgetRatioListingDisplayDesc = 'AD_BUDGET_RATIO_LISTING_DISPLAY_DESC',
  AdMediaDecisionDoneAsc = 'AD_MEDIA_DECISION_DONE_ASC',
  AdMediaDecisionDoneDesc = 'AD_MEDIA_DECISION_DONE_DESC',
  AdMonthlyBudgetAsc = 'AD_MONTHLY_BUDGET_ASC',
  AdMonthlyBudgetDesc = 'AD_MONTHLY_BUDGET_DESC',
  CpaDefinitionAsc = 'CPA_DEFINITION_ASC',
  CpaDefinitionDesc = 'CPA_DEFINITION_DESC',
  CreatedAtAsc = 'CREATED_AT_ASC',
  CreatedAtDesc = 'CREATED_AT_DESC',
  CustomerConversionCorporateAsc = 'CUSTOMER_CONVERSION_CORPORATE_ASC',
  CustomerConversionCorporateDesc = 'CUSTOMER_CONVERSION_CORPORATE_DESC',
  CustomerConversionIndividualAsc = 'CUSTOMER_CONVERSION_INDIVIDUAL_ASC',
  CustomerConversionIndividualDesc = 'CUSTOMER_CONVERSION_INDIVIDUAL_DESC',
  DisplayAdBudgetRatioAnswerStatusAsc = 'DISPLAY_AD_BUDGET_RATIO_ANSWER_STATUS_ASC',
  DisplayAdBudgetRatioAnswerStatusDesc = 'DISPLAY_AD_BUDGET_RATIO_ANSWER_STATUS_DESC',
  DisplayAdBudgetRatioAsc = 'DISPLAY_AD_BUDGET_RATIO_ASC',
  DisplayAdBudgetRatioDesc = 'DISPLAY_AD_BUDGET_RATIO_DESC',
  GoalCalculationDoneAsc = 'GOAL_CALCULATION_DONE_ASC',
  GoalCalculationDoneDesc = 'GOAL_CALCULATION_DONE_DESC',
  HavePlanSegmentAsc = 'HAVE_PLAN_SEGMENT_ASC',
  HavePlanSegmentDesc = 'HAVE_PLAN_SEGMENT_DESC',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  IsCorporateAsc = 'IS_CORPORATE_ASC',
  IsCorporateDesc = 'IS_CORPORATE_DESC',
  KaizenFrequencyAsc = 'KAIZEN_FREQUENCY_ASC',
  KaizenFrequencyDesc = 'KAIZEN_FREQUENCY_DESC',
  ListingAdBudgetRatioAsc = 'LISTING_AD_BUDGET_RATIO_ASC',
  ListingAdBudgetRatioDesc = 'LISTING_AD_BUDGET_RATIO_DESC',
  Natural = 'NATURAL',
  PermissionResultAdToolViewAsc = 'PERMISSION_RESULT_AD_TOOL_VIEW_ASC',
  PermissionResultAdToolViewDesc = 'PERMISSION_RESULT_AD_TOOL_VIEW_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
  ProductIdAsc = 'PRODUCT_ID_ASC',
  ProductIdDesc = 'PRODUCT_ID_DESC',
  SegmentDeliveryMethodAsc = 'SEGMENT_DELIVERY_METHOD_ASC',
  SegmentDeliveryMethodDesc = 'SEGMENT_DELIVERY_METHOD_DESC',
  SegmentMaterialsAnswerStatusAsc = 'SEGMENT_MATERIALS_ANSWER_STATUS_ASC',
  SegmentMaterialsAnswerStatusDesc = 'SEGMENT_MATERIALS_ANSWER_STATUS_DESC',
  SegmentMaterialsFilesAsc = 'SEGMENT_MATERIALS_FILES_ASC',
  SegmentMaterialsFilesDesc = 'SEGMENT_MATERIALS_FILES_DESC',
  SegmentMaterialsFileKeysAsc = 'SEGMENT_MATERIALS_FILE_KEYS_ASC',
  SegmentMaterialsFileKeysDesc = 'SEGMENT_MATERIALS_FILE_KEYS_DESC',
  UpdatedAtAsc = 'UPDATED_AT_ASC',
  UpdatedAtDesc = 'UPDATED_AT_DESC',
  WithOtherAnalysisToolAsc = 'WITH_OTHER_ANALYSIS_TOOL_ASC',
  WithOtherAnalysisToolDesc = 'WITH_OTHER_ANALYSIS_TOOL_DESC'
}

/** The root mutation type which contains root level fields which mutate data. */
export type Mutation = {
  __typename?: 'Mutation';
  /** Creates a single `AlembicVersion`. */
  createAlembicVersion?: Maybe<CreateAlembicVersionPayload>;
  /** Creates a single `Company`. */
  createCompany?: Maybe<CreateCompanyPayload>;
  /** Creates a single `CompanyInfo`. */
  createCompanyInfo?: Maybe<CreateCompanyInfoPayload>;
  /** Creates a single `CustomerGroup`. */
  createCustomerGroup?: Maybe<CreateCustomerGroupPayload>;
  /** Creates a single `MarketingActivityInformation`. */
  createMarketingActivityInformation?: Maybe<CreateMarketingActivityInformationPayload>;
  /** Creates a single `Notification`. */
  createNotification?: Maybe<CreateNotificationPayload>;
  /** Creates a single `OperatorGroup`. */
  createOperatorGroup?: Maybe<CreateOperatorGroupPayload>;
  /** Creates a single `OperatorName`. */
  createOperatorName?: Maybe<CreateOperatorNamePayload>;
  /** Creates a single `Opportunity`. */
  createOpportunity?: Maybe<CreateOpportunityPayload>;
  /** Creates a single `Product`. */
  createProduct?: Maybe<CreateProductPayload>;
  /** Creates a single `ProductInformation`. */
  createProductInformation?: Maybe<CreateProductInformationPayload>;
  /** Creates a single `RequestContent`. */
  createRequestContent?: Maybe<CreateRequestContentPayload>;
  /** Creates a single `UserInformationMemo`. */
  createUserInformationMemo?: Maybe<CreateUserInformationMemoPayload>;
  /** Deletes a single `AlembicVersion` using its globally unique id. */
  deleteAlembicVersion?: Maybe<DeleteAlembicVersionPayload>;
  /** Deletes a single `AlembicVersion` using a unique key. */
  deleteAlembicVersionByVersionNum?: Maybe<DeleteAlembicVersionPayload>;
  /** Deletes a single `Company` using its globally unique id. */
  deleteCompany?: Maybe<DeleteCompanyPayload>;
  /** Deletes a single `Company` using a unique key. */
  deleteCompanyById?: Maybe<DeleteCompanyPayload>;
  /** Deletes a single `CompanyInfo` using its globally unique id. */
  deleteCompanyInfo?: Maybe<DeleteCompanyInfoPayload>;
  /** Deletes a single `CompanyInfo` using a unique key. */
  deleteCompanyInfoById?: Maybe<DeleteCompanyInfoPayload>;
  /** Deletes a single `CustomerGroup` using its globally unique id. */
  deleteCustomerGroup?: Maybe<DeleteCustomerGroupPayload>;
  /** Deletes a single `CustomerGroup` using a unique key. */
  deleteCustomerGroupById?: Maybe<DeleteCustomerGroupPayload>;
  /** Deletes a single `CustomerGroup` using a unique key. */
  deleteCustomerGroupByOpportunityIdAndSfContactId?: Maybe<DeleteCustomerGroupPayload>;
  /** Deletes a single `MarketingActivityInformation` using its globally unique id. */
  deleteMarketingActivityInformation?: Maybe<DeleteMarketingActivityInformationPayload>;
  /** Deletes a single `MarketingActivityInformation` using a unique key. */
  deleteMarketingActivityInformationById?: Maybe<DeleteMarketingActivityInformationPayload>;
  /** Deletes a single `Notification` using its globally unique id. */
  deleteNotification?: Maybe<DeleteNotificationPayload>;
  /** Deletes a single `Notification` using a unique key. */
  deleteNotificationById?: Maybe<DeleteNotificationPayload>;
  /** Deletes a single `OperatorGroup` using its globally unique id. */
  deleteOperatorGroup?: Maybe<DeleteOperatorGroupPayload>;
  /** Deletes a single `OperatorGroup` using a unique key. */
  deleteOperatorGroupById?: Maybe<DeleteOperatorGroupPayload>;
  /** Deletes a single `OperatorName` using its globally unique id. */
  deleteOperatorName?: Maybe<DeleteOperatorNamePayload>;
  /** Deletes a single `OperatorName` using a unique key. */
  deleteOperatorNameByEmail?: Maybe<DeleteOperatorNamePayload>;
  /** Deletes a single `Opportunity` using its globally unique id. */
  deleteOpportunity?: Maybe<DeleteOpportunityPayload>;
  /** Deletes a single `Opportunity` using a unique key. */
  deleteOpportunityById?: Maybe<DeleteOpportunityPayload>;
  /** Deletes a single `Product` using its globally unique id. */
  deleteProduct?: Maybe<DeleteProductPayload>;
  /** Deletes a single `Product` using a unique key. */
  deleteProductByCompanyIdAndProductName?: Maybe<DeleteProductPayload>;
  /** Deletes a single `Product` using a unique key. */
  deleteProductById?: Maybe<DeleteProductPayload>;
  /** Deletes a single `ProductInformation` using its globally unique id. */
  deleteProductInformation?: Maybe<DeleteProductInformationPayload>;
  /** Deletes a single `ProductInformation` using a unique key. */
  deleteProductInformationById?: Maybe<DeleteProductInformationPayload>;
  /** Deletes a single `RequestContent` using its globally unique id. */
  deleteRequestContent?: Maybe<DeleteRequestContentPayload>;
  /** Deletes a single `RequestContent` using a unique key. */
  deleteRequestContentById?: Maybe<DeleteRequestContentPayload>;
  /** Deletes a single `UserInformationMemo` using its globally unique id. */
  deleteUserInformationMemo?: Maybe<DeleteUserInformationMemoPayload>;
  /** Deletes a single `UserInformationMemo` using a unique key. */
  deleteUserInformationMemoById?: Maybe<DeleteUserInformationMemoPayload>;
  /** Updates a single `AlembicVersion` using its globally unique id and a patch. */
  updateAlembicVersion?: Maybe<UpdateAlembicVersionPayload>;
  /** Updates a single `AlembicVersion` using a unique key and a patch. */
  updateAlembicVersionByVersionNum?: Maybe<UpdateAlembicVersionPayload>;
  /** Updates a single `Company` using its globally unique id and a patch. */
  updateCompany?: Maybe<UpdateCompanyPayload>;
  /** Updates a single `Company` using a unique key and a patch. */
  updateCompanyById?: Maybe<UpdateCompanyPayload>;
  /** Updates a single `CompanyInfo` using its globally unique id and a patch. */
  updateCompanyInfo?: Maybe<UpdateCompanyInfoPayload>;
  /** Updates a single `CompanyInfo` using a unique key and a patch. */
  updateCompanyInfoById?: Maybe<UpdateCompanyInfoPayload>;
  /** Updates a single `CustomerGroup` using its globally unique id and a patch. */
  updateCustomerGroup?: Maybe<UpdateCustomerGroupPayload>;
  /** Updates a single `CustomerGroup` using a unique key and a patch. */
  updateCustomerGroupById?: Maybe<UpdateCustomerGroupPayload>;
  /** Updates a single `CustomerGroup` using a unique key and a patch. */
  updateCustomerGroupByOpportunityIdAndSfContactId?: Maybe<UpdateCustomerGroupPayload>;
  /** Updates a single `MarketingActivityInformation` using its globally unique id and a patch. */
  updateMarketingActivityInformation?: Maybe<UpdateMarketingActivityInformationPayload>;
  /** Updates a single `MarketingActivityInformation` using a unique key and a patch. */
  updateMarketingActivityInformationById?: Maybe<UpdateMarketingActivityInformationPayload>;
  /** Updates a single `Notification` using its globally unique id and a patch. */
  updateNotification?: Maybe<UpdateNotificationPayload>;
  /** Updates a single `Notification` using a unique key and a patch. */
  updateNotificationById?: Maybe<UpdateNotificationPayload>;
  /** Updates a single `OperatorGroup` using its globally unique id and a patch. */
  updateOperatorGroup?: Maybe<UpdateOperatorGroupPayload>;
  /** Updates a single `OperatorGroup` using a unique key and a patch. */
  updateOperatorGroupById?: Maybe<UpdateOperatorGroupPayload>;
  /** Updates a single `OperatorName` using its globally unique id and a patch. */
  updateOperatorName?: Maybe<UpdateOperatorNamePayload>;
  /** Updates a single `OperatorName` using a unique key and a patch. */
  updateOperatorNameByEmail?: Maybe<UpdateOperatorNamePayload>;
  /** Updates a single `Opportunity` using its globally unique id and a patch. */
  updateOpportunity?: Maybe<UpdateOpportunityPayload>;
  /** Updates a single `Opportunity` using a unique key and a patch. */
  updateOpportunityById?: Maybe<UpdateOpportunityPayload>;
  /** Updates a single `Product` using its globally unique id and a patch. */
  updateProduct?: Maybe<UpdateProductPayload>;
  /** Updates a single `Product` using a unique key and a patch. */
  updateProductByCompanyIdAndProductName?: Maybe<UpdateProductPayload>;
  /** Updates a single `Product` using a unique key and a patch. */
  updateProductById?: Maybe<UpdateProductPayload>;
  /** Updates a single `ProductInformation` using its globally unique id and a patch. */
  updateProductInformation?: Maybe<UpdateProductInformationPayload>;
  /** Updates a single `ProductInformation` using a unique key and a patch. */
  updateProductInformationById?: Maybe<UpdateProductInformationPayload>;
  /** Updates a single `RequestContent` using its globally unique id and a patch. */
  updateRequestContent?: Maybe<UpdateRequestContentPayload>;
  /** Updates a single `RequestContent` using a unique key and a patch. */
  updateRequestContentById?: Maybe<UpdateRequestContentPayload>;
  /** Updates a single `UserInformationMemo` using its globally unique id and a patch. */
  updateUserInformationMemo?: Maybe<UpdateUserInformationMemoPayload>;
  /** Updates a single `UserInformationMemo` using a unique key and a patch. */
  updateUserInformationMemoById?: Maybe<UpdateUserInformationMemoPayload>;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateAlembicVersionArgs = {
  input: CreateAlembicVersionInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateCompanyArgs = {
  input: CreateCompanyInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateCompanyInfoArgs = {
  input: CreateCompanyInfoInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateCustomerGroupArgs = {
  input: CreateCustomerGroupInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateMarketingActivityInformationArgs = {
  input: CreateMarketingActivityInformationInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateNotificationArgs = {
  input: CreateNotificationInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateOperatorGroupArgs = {
  input: CreateOperatorGroupInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateOperatorNameArgs = {
  input: CreateOperatorNameInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateOpportunityArgs = {
  input: CreateOpportunityInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateProductArgs = {
  input: CreateProductInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateProductInformationArgs = {
  input: CreateProductInformationInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateRequestContentArgs = {
  input: CreateRequestContentInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateUserInformationMemoArgs = {
  input: CreateUserInformationMemoInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteAlembicVersionArgs = {
  input: DeleteAlembicVersionInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteAlembicVersionByVersionNumArgs = {
  input: DeleteAlembicVersionByVersionNumInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteCompanyArgs = {
  input: DeleteCompanyInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteCompanyByIdArgs = {
  input: DeleteCompanyByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteCompanyInfoArgs = {
  input: DeleteCompanyInfoInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteCompanyInfoByIdArgs = {
  input: DeleteCompanyInfoByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteCustomerGroupArgs = {
  input: DeleteCustomerGroupInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteCustomerGroupByIdArgs = {
  input: DeleteCustomerGroupByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteCustomerGroupByOpportunityIdAndSfContactIdArgs = {
  input: DeleteCustomerGroupByOpportunityIdAndSfContactIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteMarketingActivityInformationArgs = {
  input: DeleteMarketingActivityInformationInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteMarketingActivityInformationByIdArgs = {
  input: DeleteMarketingActivityInformationByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteNotificationArgs = {
  input: DeleteNotificationInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteNotificationByIdArgs = {
  input: DeleteNotificationByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteOperatorGroupArgs = {
  input: DeleteOperatorGroupInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteOperatorGroupByIdArgs = {
  input: DeleteOperatorGroupByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteOperatorNameArgs = {
  input: DeleteOperatorNameInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteOperatorNameByEmailArgs = {
  input: DeleteOperatorNameByEmailInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteOpportunityArgs = {
  input: DeleteOpportunityInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteOpportunityByIdArgs = {
  input: DeleteOpportunityByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteProductArgs = {
  input: DeleteProductInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteProductByCompanyIdAndProductNameArgs = {
  input: DeleteProductByCompanyIdAndProductNameInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteProductByIdArgs = {
  input: DeleteProductByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteProductInformationArgs = {
  input: DeleteProductInformationInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteProductInformationByIdArgs = {
  input: DeleteProductInformationByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteRequestContentArgs = {
  input: DeleteRequestContentInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteRequestContentByIdArgs = {
  input: DeleteRequestContentByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteUserInformationMemoArgs = {
  input: DeleteUserInformationMemoInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteUserInformationMemoByIdArgs = {
  input: DeleteUserInformationMemoByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateAlembicVersionArgs = {
  input: UpdateAlembicVersionInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateAlembicVersionByVersionNumArgs = {
  input: UpdateAlembicVersionByVersionNumInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateCompanyArgs = {
  input: UpdateCompanyInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateCompanyByIdArgs = {
  input: UpdateCompanyByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateCompanyInfoArgs = {
  input: UpdateCompanyInfoInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateCompanyInfoByIdArgs = {
  input: UpdateCompanyInfoByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateCustomerGroupArgs = {
  input: UpdateCustomerGroupInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateCustomerGroupByIdArgs = {
  input: UpdateCustomerGroupByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateCustomerGroupByOpportunityIdAndSfContactIdArgs = {
  input: UpdateCustomerGroupByOpportunityIdAndSfContactIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateMarketingActivityInformationArgs = {
  input: UpdateMarketingActivityInformationInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateMarketingActivityInformationByIdArgs = {
  input: UpdateMarketingActivityInformationByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateNotificationArgs = {
  input: UpdateNotificationInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateNotificationByIdArgs = {
  input: UpdateNotificationByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateOperatorGroupArgs = {
  input: UpdateOperatorGroupInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateOperatorGroupByIdArgs = {
  input: UpdateOperatorGroupByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateOperatorNameArgs = {
  input: UpdateOperatorNameInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateOperatorNameByEmailArgs = {
  input: UpdateOperatorNameByEmailInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateOpportunityArgs = {
  input: UpdateOpportunityInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateOpportunityByIdArgs = {
  input: UpdateOpportunityByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateProductArgs = {
  input: UpdateProductInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateProductByCompanyIdAndProductNameArgs = {
  input: UpdateProductByCompanyIdAndProductNameInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateProductByIdArgs = {
  input: UpdateProductByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateProductInformationArgs = {
  input: UpdateProductInformationInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateProductInformationByIdArgs = {
  input: UpdateProductInformationByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateRequestContentArgs = {
  input: UpdateRequestContentInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateRequestContentByIdArgs = {
  input: UpdateRequestContentByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateUserInformationMemoArgs = {
  input: UpdateUserInformationMemoInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateUserInformationMemoByIdArgs = {
  input: UpdateUserInformationMemoByIdInput;
};

/** An object with a globally unique `ID`. */
export type Node = {
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID']['output'];
};

/** お知らせテーブル */
export type Notification = Node & {
  __typename?: 'Notification';
  createdAt?: Maybe<Scalars['Datetime']['output']>;
  /** アイコン */
  icon: Scalars['String']['output'];
  /** お知らせID */
  id: Scalars['UUID']['output'];
  /** ページリンク */
  link?: Maybe<Scalars['String']['output']>;
  /** メッセージ */
  message: Scalars['String']['output'];
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID']['output'];
  /** 既読フラグ */
  read?: Maybe<Scalars['Boolean']['output']>;
  /** 対象者 */
  target: Scalars['String']['output'];
  updatedAt?: Maybe<Scalars['Datetime']['output']>;
};

/**
 * A condition to be used against `Notification` object types. All fields are
 * tested for equality and combined with a logical ‘and.’
 */
export type NotificationCondition = {
  /** Checks for equality with the object’s `createdAt` field. */
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** Checks for equality with the object’s `icon` field. */
  icon?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `id` field. */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `link` field. */
  link?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `message` field. */
  message?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `read` field. */
  read?: InputMaybe<Scalars['Boolean']['input']>;
  /** Checks for equality with the object’s `target` field. */
  target?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** A filter to be used against `Notification` object types. All fields are combined with a logical ‘and.’ */
export type NotificationFilter = {
  /** Checks for all expressions in this list. */
  and?: InputMaybe<Array<NotificationFilter>>;
  /** Filter by the object’s `createdAt` field. */
  createdAt?: InputMaybe<DatetimeFilter>;
  /** Filter by the object’s `icon` field. */
  icon?: InputMaybe<StringFilter>;
  /** Filter by the object’s `id` field. */
  id?: InputMaybe<UuidFilter>;
  /** Filter by the object’s `link` field. */
  link?: InputMaybe<StringFilter>;
  /** Filter by the object’s `message` field. */
  message?: InputMaybe<StringFilter>;
  /** Negates the expression. */
  not?: InputMaybe<NotificationFilter>;
  /** Checks for any expressions in this list. */
  or?: InputMaybe<Array<NotificationFilter>>;
  /** Filter by the object’s `read` field. */
  read?: InputMaybe<BooleanFilter>;
  /** Filter by the object’s `target` field. */
  target?: InputMaybe<StringFilter>;
  /** Filter by the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<DatetimeFilter>;
};

/** An input for mutations affecting `Notification` */
export type NotificationInput = {
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** アイコン */
  icon: Scalars['String']['input'];
  /** お知らせID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** ページリンク */
  link?: InputMaybe<Scalars['String']['input']>;
  /** メッセージ */
  message: Scalars['String']['input'];
  /** 既読フラグ */
  read?: InputMaybe<Scalars['Boolean']['input']>;
  /** 対象者 */
  target: Scalars['String']['input'];
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** Represents an update to a `Notification`. Fields that are set will be updated. */
export type NotificationPatch = {
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** アイコン */
  icon?: InputMaybe<Scalars['String']['input']>;
  /** お知らせID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** ページリンク */
  link?: InputMaybe<Scalars['String']['input']>;
  /** メッセージ */
  message?: InputMaybe<Scalars['String']['input']>;
  /** 既読フラグ */
  read?: InputMaybe<Scalars['Boolean']['input']>;
  /** 対象者 */
  target?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** A connection to a list of `Notification` values. */
export type NotificationsConnection = {
  __typename?: 'NotificationsConnection';
  /** A list of edges which contains the `Notification` and cursor to aid in pagination. */
  edges: Array<NotificationsEdge>;
  /** A list of `Notification` objects. */
  nodes: Array<Maybe<Notification>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `Notification` you could get from the connection. */
  totalCount: Scalars['Int']['output'];
};

/** A `Notification` edge in the connection. */
export type NotificationsEdge = {
  __typename?: 'NotificationsEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']['output']>;
  /** The `Notification` at the end of the edge. */
  node?: Maybe<Notification>;
};

/** Methods to use when ordering `Notification`. */
export enum NotificationsOrderBy {
  CreatedAtAsc = 'CREATED_AT_ASC',
  CreatedAtDesc = 'CREATED_AT_DESC',
  IconAsc = 'ICON_ASC',
  IconDesc = 'ICON_DESC',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  LinkAsc = 'LINK_ASC',
  LinkDesc = 'LINK_DESC',
  MessageAsc = 'MESSAGE_ASC',
  MessageDesc = 'MESSAGE_DESC',
  Natural = 'NATURAL',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
  ReadAsc = 'READ_ASC',
  ReadDesc = 'READ_DESC',
  TargetAsc = 'TARGET_ASC',
  TargetDesc = 'TARGET_DESC',
  UpdatedAtAsc = 'UPDATED_AT_ASC',
  UpdatedAtDesc = 'UPDATED_AT_DESC'
}

/** 管理者グループ */
export type OperatorGroup = Node & {
  __typename?: 'OperatorGroup';
  /** 所属部署 */
  department: Scalars['String']['output'];
  /** 担当者メールアドレス */
  email: Scalars['String']['output'];
  /** 管理者グループID */
  id: Scalars['Int']['output'];
  name: Scalars['String']['output'];
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID']['output'];
  /** Reads a single `Opportunity` that is related to this `OperatorGroup`. */
  opportunityByOpportunityId?: Maybe<Opportunity>;
  /** 商談ID */
  opportunityId: Scalars['UUID']['output'];
  /** Cognito ID */
  username: Scalars['String']['output'];
};

/**
 * A condition to be used against `OperatorGroup` object types. All fields are
 * tested for equality and combined with a logical ‘and.’
 */
export type OperatorGroupCondition = {
  /** Checks for equality with the object’s `department` field. */
  department?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `email` field. */
  email?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `id` field. */
  id?: InputMaybe<Scalars['Int']['input']>;
  /** Checks for equality with the object’s `name` field. */
  name?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `opportunityId` field. */
  opportunityId?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `username` field. */
  username?: InputMaybe<Scalars['String']['input']>;
};

/** A filter to be used against `OperatorGroup` object types. All fields are combined with a logical ‘and.’ */
export type OperatorGroupFilter = {
  /** Checks for all expressions in this list. */
  and?: InputMaybe<Array<OperatorGroupFilter>>;
  /** Filter by the object’s `department` field. */
  department?: InputMaybe<StringFilter>;
  /** Filter by the object’s `email` field. */
  email?: InputMaybe<StringFilter>;
  /** Filter by the object’s `id` field. */
  id?: InputMaybe<IntFilter>;
  /** Filter by the object’s `name` field. */
  name?: InputMaybe<StringFilter>;
  /** Negates the expression. */
  not?: InputMaybe<OperatorGroupFilter>;
  /** Filter by the object’s `opportunityId` field. */
  opportunityId?: InputMaybe<UuidFilter>;
  /** Checks for any expressions in this list. */
  or?: InputMaybe<Array<OperatorGroupFilter>>;
  /** Filter by the object’s `username` field. */
  username?: InputMaybe<StringFilter>;
};

/** An input for mutations affecting `OperatorGroup` */
export type OperatorGroupInput = {
  /** 所属部署 */
  department: Scalars['String']['input'];
  /** 担当者メールアドレス */
  email: Scalars['String']['input'];
  /** 管理者グループID */
  id?: InputMaybe<Scalars['Int']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  /** 商談ID */
  opportunityId: Scalars['UUID']['input'];
  /** Cognito ID */
  username: Scalars['String']['input'];
};

/** Represents an update to a `OperatorGroup`. Fields that are set will be updated. */
export type OperatorGroupPatch = {
  /** 所属部署 */
  department?: InputMaybe<Scalars['String']['input']>;
  /** 担当者メールアドレス */
  email?: InputMaybe<Scalars['String']['input']>;
  /** 管理者グループID */
  id?: InputMaybe<Scalars['Int']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  /** 商談ID */
  opportunityId?: InputMaybe<Scalars['UUID']['input']>;
  /** Cognito ID */
  username?: InputMaybe<Scalars['String']['input']>;
};

/** A connection to a list of `OperatorGroup` values. */
export type OperatorGroupsConnection = {
  __typename?: 'OperatorGroupsConnection';
  /** A list of edges which contains the `OperatorGroup` and cursor to aid in pagination. */
  edges: Array<OperatorGroupsEdge>;
  /** A list of `OperatorGroup` objects. */
  nodes: Array<Maybe<OperatorGroup>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `OperatorGroup` you could get from the connection. */
  totalCount: Scalars['Int']['output'];
};

/** A `OperatorGroup` edge in the connection. */
export type OperatorGroupsEdge = {
  __typename?: 'OperatorGroupsEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']['output']>;
  /** The `OperatorGroup` at the end of the edge. */
  node?: Maybe<OperatorGroup>;
};

/** Methods to use when ordering `OperatorGroup`. */
export enum OperatorGroupsOrderBy {
  DepartmentAsc = 'DEPARTMENT_ASC',
  DepartmentDesc = 'DEPARTMENT_DESC',
  EmailAsc = 'EMAIL_ASC',
  EmailDesc = 'EMAIL_DESC',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  NameAsc = 'NAME_ASC',
  NameDesc = 'NAME_DESC',
  Natural = 'NATURAL',
  OpportunityIdAsc = 'OPPORTUNITY_ID_ASC',
  OpportunityIdDesc = 'OPPORTUNITY_ID_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
  UsernameAsc = 'USERNAME_ASC',
  UsernameDesc = 'USERNAME_DESC'
}

/** 管理者名テーブル */
export type OperatorName = Node & {
  __typename?: 'OperatorName';
  email: Scalars['String']['output'];
  name: Scalars['String']['output'];
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID']['output'];
};

/**
 * A condition to be used against `OperatorName` object types. All fields are
 * tested for equality and combined with a logical ‘and.’
 */
export type OperatorNameCondition = {
  /** Checks for equality with the object’s `email` field. */
  email?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `name` field. */
  name?: InputMaybe<Scalars['String']['input']>;
};

/** A filter to be used against `OperatorName` object types. All fields are combined with a logical ‘and.’ */
export type OperatorNameFilter = {
  /** Checks for all expressions in this list. */
  and?: InputMaybe<Array<OperatorNameFilter>>;
  /** Filter by the object’s `email` field. */
  email?: InputMaybe<StringFilter>;
  /** Filter by the object’s `name` field. */
  name?: InputMaybe<StringFilter>;
  /** Negates the expression. */
  not?: InputMaybe<OperatorNameFilter>;
  /** Checks for any expressions in this list. */
  or?: InputMaybe<Array<OperatorNameFilter>>;
};

/** An input for mutations affecting `OperatorName` */
export type OperatorNameInput = {
  email: Scalars['String']['input'];
  name: Scalars['String']['input'];
};

/** Represents an update to a `OperatorName`. Fields that are set will be updated. */
export type OperatorNamePatch = {
  email?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
};

/** A connection to a list of `OperatorName` values. */
export type OperatorNamesConnection = {
  __typename?: 'OperatorNamesConnection';
  /** A list of edges which contains the `OperatorName` and cursor to aid in pagination. */
  edges: Array<OperatorNamesEdge>;
  /** A list of `OperatorName` objects. */
  nodes: Array<Maybe<OperatorName>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `OperatorName` you could get from the connection. */
  totalCount: Scalars['Int']['output'];
};

/** A `OperatorName` edge in the connection. */
export type OperatorNamesEdge = {
  __typename?: 'OperatorNamesEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']['output']>;
  /** The `OperatorName` at the end of the edge. */
  node?: Maybe<OperatorName>;
};

/** Methods to use when ordering `OperatorName`. */
export enum OperatorNamesOrderBy {
  EmailAsc = 'EMAIL_ASC',
  EmailDesc = 'EMAIL_DESC',
  NameAsc = 'NAME_ASC',
  NameDesc = 'NAME_DESC',
  Natural = 'NATURAL',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC'
}

/** A connection to a list of `Opportunity` values. */
export type OpportunitiesConnection = {
  __typename?: 'OpportunitiesConnection';
  /** A list of edges which contains the `Opportunity` and cursor to aid in pagination. */
  edges: Array<OpportunitiesEdge>;
  /** A list of `Opportunity` objects. */
  nodes: Array<Maybe<Opportunity>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `Opportunity` you could get from the connection. */
  totalCount: Scalars['Int']['output'];
};

/** A `Opportunity` edge in the connection. */
export type OpportunitiesEdge = {
  __typename?: 'OpportunitiesEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']['output']>;
  /** The `Opportunity` at the end of the edge. */
  node?: Maybe<Opportunity>;
};

/** Methods to use when ordering `Opportunity`. */
export enum OpportunitiesOrderBy {
  CreatedAtAsc = 'CREATED_AT_ASC',
  CreatedAtDesc = 'CREATED_AT_DESC',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  Natural = 'NATURAL',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
  ProductIdAsc = 'PRODUCT_ID_ASC',
  ProductIdDesc = 'PRODUCT_ID_DESC',
  SfOpportunityIdAsc = 'SF_OPPORTUNITY_ID_ASC',
  SfOpportunityIdDesc = 'SF_OPPORTUNITY_ID_DESC',
  SfProjectIdAsc = 'SF_PROJECT_ID_ASC',
  SfProjectIdDesc = 'SF_PROJECT_ID_DESC',
  UpdatedAtAsc = 'UPDATED_AT_ASC',
  UpdatedAtDesc = 'UPDATED_AT_DESC'
}

/** 商談テーブル */
export type Opportunity = Node & {
  __typename?: 'Opportunity';
  createdAt?: Maybe<Scalars['Datetime']['output']>;
  /** Reads and enables pagination through a set of `CustomerGroup`. */
  customerGroupsByOpportunityId: CustomerGroupsConnection;
  /** 商談ID */
  id: Scalars['UUID']['output'];
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID']['output'];
  /** Reads and enables pagination through a set of `OperatorGroup`. */
  operatorGroupsByOpportunityId: OperatorGroupsConnection;
  /** Reads a single `Product` that is related to this `Opportunity`. */
  productByProductId?: Maybe<Product>;
  /** 商材ID */
  productId: Scalars['UUID']['output'];
  /** Salesforce商談ID */
  sfOpportunityId?: Maybe<Scalars['String']['output']>;
  /** Salesforce プロジェクトID */
  sfProjectId: Scalars['String']['output'];
  updatedAt?: Maybe<Scalars['Datetime']['output']>;
};


/** 商談テーブル */
export type OpportunityCustomerGroupsByOpportunityIdArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<CustomerGroupCondition>;
  filter?: InputMaybe<CustomerGroupFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CustomerGroupsOrderBy>>;
};


/** 商談テーブル */
export type OpportunityOperatorGroupsByOpportunityIdArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<OperatorGroupCondition>;
  filter?: InputMaybe<OperatorGroupFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<OperatorGroupsOrderBy>>;
};

/**
 * A condition to be used against `Opportunity` object types. All fields are tested
 * for equality and combined with a logical ‘and.’
 */
export type OpportunityCondition = {
  /** Checks for equality with the object’s `createdAt` field. */
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** Checks for equality with the object’s `id` field. */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `productId` field. */
  productId?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `sfOpportunityId` field. */
  sfOpportunityId?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `sfProjectId` field. */
  sfProjectId?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** A filter to be used against `Opportunity` object types. All fields are combined with a logical ‘and.’ */
export type OpportunityFilter = {
  /** Checks for all expressions in this list. */
  and?: InputMaybe<Array<OpportunityFilter>>;
  /** Filter by the object’s `createdAt` field. */
  createdAt?: InputMaybe<DatetimeFilter>;
  /** Filter by the object’s `id` field. */
  id?: InputMaybe<UuidFilter>;
  /** Negates the expression. */
  not?: InputMaybe<OpportunityFilter>;
  /** Checks for any expressions in this list. */
  or?: InputMaybe<Array<OpportunityFilter>>;
  /** Filter by the object’s `productId` field. */
  productId?: InputMaybe<UuidFilter>;
  /** Filter by the object’s `sfOpportunityId` field. */
  sfOpportunityId?: InputMaybe<StringFilter>;
  /** Filter by the object’s `sfProjectId` field. */
  sfProjectId?: InputMaybe<StringFilter>;
  /** Filter by the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<DatetimeFilter>;
};

/** An input for mutations affecting `Opportunity` */
export type OpportunityInput = {
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** 商談ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** 商材ID */
  productId: Scalars['UUID']['input'];
  /** Salesforce商談ID */
  sfOpportunityId?: InputMaybe<Scalars['String']['input']>;
  /** Salesforce プロジェクトID */
  sfProjectId: Scalars['String']['input'];
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** Represents an update to a `Opportunity`. Fields that are set will be updated. */
export type OpportunityPatch = {
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** 商談ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** 商材ID */
  productId?: InputMaybe<Scalars['UUID']['input']>;
  /** Salesforce商談ID */
  sfOpportunityId?: InputMaybe<Scalars['String']['input']>;
  /** Salesforce プロジェクトID */
  sfProjectId?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** Information about pagination in a connection. */
export type PageInfo = {
  __typename?: 'PageInfo';
  /** When paginating forwards, the cursor to continue. */
  endCursor?: Maybe<Scalars['Cursor']['output']>;
  /** When paginating forwards, are there more items? */
  hasNextPage: Scalars['Boolean']['output'];
  /** When paginating backwards, are there more items? */
  hasPreviousPage: Scalars['Boolean']['output'];
  /** When paginating backwards, the cursor to continue. */
  startCursor?: Maybe<Scalars['Cursor']['output']>;
};

/** 商材テーブル */
export type Product = Node & {
  __typename?: 'Product';
  /** Reads a single `Company` that is related to this `Product`. */
  companyByCompanyId?: Maybe<Company>;
  /** 企業ID */
  companyId: Scalars['UUID']['output'];
  createdAt?: Maybe<Scalars['Datetime']['output']>;
  /** ヒアリング完了フラグ */
  hearingComplete?: Maybe<Scalars['Boolean']['output']>;
  /** 商材ID */
  id: Scalars['UUID']['output'];
  /** Reads and enables pagination through a set of `MarketingActivityInformation`. */
  marketingActivityInformationsByProductId: MarketingActivityInformationsConnection;
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID']['output'];
  /** Reads and enables pagination through a set of `Opportunity`. */
  opportunitiesByProductId: OpportunitiesConnection;
  /** Reads and enables pagination through a set of `ProductInformation`. */
  productInformationsByProductId: ProductInformationsConnection;
  /** 商材名 */
  productName?: Maybe<Scalars['String']['output']>;
  /** Reads and enables pagination through a set of `RequestContent`. */
  requestContentsByProductId: RequestContentsConnection;
  updatedAt?: Maybe<Scalars['Datetime']['output']>;
  /** Reads and enables pagination through a set of `UserInformationMemo`. */
  userInformationMemosByProductId: UserInformationMemosConnection;
};


/** 商材テーブル */
export type ProductMarketingActivityInformationsByProductIdArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<MarketingActivityInformationCondition>;
  filter?: InputMaybe<MarketingActivityInformationFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<MarketingActivityInformationsOrderBy>>;
};


/** 商材テーブル */
export type ProductOpportunitiesByProductIdArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<OpportunityCondition>;
  filter?: InputMaybe<OpportunityFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<OpportunitiesOrderBy>>;
};


/** 商材テーブル */
export type ProductProductInformationsByProductIdArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<ProductInformationCondition>;
  filter?: InputMaybe<ProductInformationFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<ProductInformationsOrderBy>>;
};


/** 商材テーブル */
export type ProductRequestContentsByProductIdArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<RequestContentCondition>;
  filter?: InputMaybe<RequestContentFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<RequestContentsOrderBy>>;
};


/** 商材テーブル */
export type ProductUserInformationMemosByProductIdArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<UserInformationMemoCondition>;
  filter?: InputMaybe<UserInformationMemoFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<UserInformationMemosOrderBy>>;
};

/** A condition to be used against `Product` object types. All fields are tested for equality and combined with a logical ‘and.’ */
export type ProductCondition = {
  /** Checks for equality with the object’s `companyId` field. */
  companyId?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `createdAt` field. */
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** Checks for equality with the object’s `hearingComplete` field. */
  hearingComplete?: InputMaybe<Scalars['Boolean']['input']>;
  /** Checks for equality with the object’s `id` field. */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `productName` field. */
  productName?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** A filter to be used against `Product` object types. All fields are combined with a logical ‘and.’ */
export type ProductFilter = {
  /** Checks for all expressions in this list. */
  and?: InputMaybe<Array<ProductFilter>>;
  /** Filter by the object’s `companyId` field. */
  companyId?: InputMaybe<UuidFilter>;
  /** Filter by the object’s `createdAt` field. */
  createdAt?: InputMaybe<DatetimeFilter>;
  /** Filter by the object’s `hearingComplete` field. */
  hearingComplete?: InputMaybe<BooleanFilter>;
  /** Filter by the object’s `id` field. */
  id?: InputMaybe<UuidFilter>;
  /** Negates the expression. */
  not?: InputMaybe<ProductFilter>;
  /** Checks for any expressions in this list. */
  or?: InputMaybe<Array<ProductFilter>>;
  /** Filter by the object’s `productName` field. */
  productName?: InputMaybe<StringFilter>;
  /** Filter by the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<DatetimeFilter>;
};

/** 商材情報Pkey */
export type ProductInformation = Node & {
  __typename?: 'ProductInformation';
  adTypeHistory?: Maybe<Scalars['String']['output']>;
  adminCommentCampaignOffer?: Maybe<Scalars['String']['output']>;
  adminCommentCompetitors?: Maybe<Scalars['String']['output']>;
  adminCommentCorporate?: Maybe<Scalars['String']['output']>;
  adminCommentIndividual?: Maybe<Scalars['String']['output']>;
  adminCommentMainTarget?: Maybe<Scalars['String']['output']>;
  adminCommentPastCases?: Maybe<Scalars['String']['output']>;
  adminCommentPastProductions?: Maybe<Scalars['String']['output']>;
  adminCommentProductAchievement?: Maybe<Scalars['String']['output']>;
  adminCommentProductOverview?: Maybe<Scalars['String']['output']>;
  adminCommentTargetSupplement?: Maybe<Scalars['String']['output']>;
  articleLpUrl?: Maybe<Scalars['String']['output']>;
  campaignOffer?: Maybe<Scalars['String']['output']>;
  competitorAnswerStatus?: Maybe<Scalars['String']['output']>;
  corporateIndustry?: Maybe<Scalars['String']['output']>;
  corporateJobTitle?: Maybe<Scalars['String']['output']>;
  corporateProblem?: Maybe<Scalars['String']['output']>;
  createdAt?: Maybe<Scalars['Datetime']['output']>;
  customerMaterialsAnswerStatus?: Maybe<Scalars['String']['output']>;
  customerMaterialsFileKeys?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  customerMaterialsFiles?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  deviceImpressionRatio?: Maybe<Scalars['String']['output']>;
  existingCreativeAnswerStatus?: Maybe<Scalars['String']['output']>;
  existingCreativeFileKeys?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  existingCreativeFiles?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  existingCreativeUrl?: Maybe<Scalars['String']['output']>;
  failureCase?: Maybe<Scalars['String']['output']>;
  /** 商材情報ID */
  id: Scalars['UUID']['output'];
  individualAge?: Maybe<Scalars['String']['output']>;
  individualFamilyIncome?: Maybe<Scalars['String']['output']>;
  individualFamilyStructure?: Maybe<Scalars['String']['output']>;
  individualGender?: Maybe<Scalars['String']['output']>;
  individualHobby?: Maybe<Scalars['String']['output']>;
  individualHobbyExpense?: Maybe<Scalars['String']['output']>;
  individualInternetUsage?: Maybe<Scalars['String']['output']>;
  individualOccupation?: Maybe<Scalars['String']['output']>;
  individualOwnedDevice?: Maybe<Scalars['String']['output']>;
  individualProblem?: Maybe<Scalars['String']['output']>;
  individualResidence?: Maybe<Scalars['String']['output']>;
  individualSnsUsually?: Maybe<Scalars['String']['output']>;
  isCorporate?: Maybe<Scalars['Boolean']['output']>;
  mainTarget?: Maybe<Scalars['String']['output']>;
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID']['output'];
  productAchievement?: Maybe<Scalars['String']['output']>;
  productAnswerStatus?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Product` that is related to this `ProductInformation`. */
  productByProductId?: Maybe<Product>;
  /** プロダクトID */
  productId?: Maybe<Scalars['UUID']['output']>;
  productMaterialsFileKeys?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  productMaterialsFiles?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  productMaterialsUrl?: Maybe<Scalars['String']['output']>;
  productsFeatures?: Maybe<Scalars['String']['output']>;
  referencedProduct?: Maybe<Scalars['String']['output']>;
  similarProduct?: Maybe<Scalars['String']['output']>;
  subTargetExistence?: Maybe<Scalars['String']['output']>;
  successCase?: Maybe<Scalars['String']['output']>;
  superiorProduct?: Maybe<Scalars['String']['output']>;
  uniqueStrength?: Maybe<Scalars['String']['output']>;
  updatedAt?: Maybe<Scalars['Datetime']['output']>;
};

/**
 * A condition to be used against `ProductInformation` object types. All fields are
 * tested for equality and combined with a logical ‘and.’
 */
export type ProductInformationCondition = {
  /** Checks for equality with the object’s `adTypeHistory` field. */
  adTypeHistory?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentCampaignOffer` field. */
  adminCommentCampaignOffer?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentCompetitors` field. */
  adminCommentCompetitors?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentCorporate` field. */
  adminCommentCorporate?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentIndividual` field. */
  adminCommentIndividual?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentMainTarget` field. */
  adminCommentMainTarget?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentPastCases` field. */
  adminCommentPastCases?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentPastProductions` field. */
  adminCommentPastProductions?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentProductAchievement` field. */
  adminCommentProductAchievement?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentProductOverview` field. */
  adminCommentProductOverview?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentTargetSupplement` field. */
  adminCommentTargetSupplement?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `articleLpUrl` field. */
  articleLpUrl?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `campaignOffer` field. */
  campaignOffer?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `competitorAnswerStatus` field. */
  competitorAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `corporateIndustry` field. */
  corporateIndustry?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `corporateJobTitle` field. */
  corporateJobTitle?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `corporateProblem` field. */
  corporateProblem?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `createdAt` field. */
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** Checks for equality with the object’s `customerMaterialsAnswerStatus` field. */
  customerMaterialsAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `customerMaterialsFileKeys` field. */
  customerMaterialsFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Checks for equality with the object’s `customerMaterialsFiles` field. */
  customerMaterialsFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Checks for equality with the object’s `deviceImpressionRatio` field. */
  deviceImpressionRatio?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `existingCreativeAnswerStatus` field. */
  existingCreativeAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `existingCreativeFileKeys` field. */
  existingCreativeFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Checks for equality with the object’s `existingCreativeFiles` field. */
  existingCreativeFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Checks for equality with the object’s `existingCreativeUrl` field. */
  existingCreativeUrl?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `failureCase` field. */
  failureCase?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `id` field. */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `individualAge` field. */
  individualAge?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `individualFamilyIncome` field. */
  individualFamilyIncome?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `individualFamilyStructure` field. */
  individualFamilyStructure?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `individualGender` field. */
  individualGender?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `individualHobby` field. */
  individualHobby?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `individualHobbyExpense` field. */
  individualHobbyExpense?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `individualInternetUsage` field. */
  individualInternetUsage?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `individualOccupation` field. */
  individualOccupation?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `individualOwnedDevice` field. */
  individualOwnedDevice?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `individualProblem` field. */
  individualProblem?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `individualResidence` field. */
  individualResidence?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `individualSnsUsually` field. */
  individualSnsUsually?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `isCorporate` field. */
  isCorporate?: InputMaybe<Scalars['Boolean']['input']>;
  /** Checks for equality with the object’s `mainTarget` field. */
  mainTarget?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `productAchievement` field. */
  productAchievement?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `productAnswerStatus` field. */
  productAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `productId` field. */
  productId?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `productMaterialsFileKeys` field. */
  productMaterialsFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Checks for equality with the object’s `productMaterialsFiles` field. */
  productMaterialsFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Checks for equality with the object’s `productMaterialsUrl` field. */
  productMaterialsUrl?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `productsFeatures` field. */
  productsFeatures?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `referencedProduct` field. */
  referencedProduct?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `similarProduct` field. */
  similarProduct?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `subTargetExistence` field. */
  subTargetExistence?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `successCase` field. */
  successCase?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `superiorProduct` field. */
  superiorProduct?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `uniqueStrength` field. */
  uniqueStrength?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** A filter to be used against `ProductInformation` object types. All fields are combined with a logical ‘and.’ */
export type ProductInformationFilter = {
  /** Filter by the object’s `adTypeHistory` field. */
  adTypeHistory?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentCampaignOffer` field. */
  adminCommentCampaignOffer?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentCompetitors` field. */
  adminCommentCompetitors?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentCorporate` field. */
  adminCommentCorporate?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentIndividual` field. */
  adminCommentIndividual?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentMainTarget` field. */
  adminCommentMainTarget?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentPastCases` field. */
  adminCommentPastCases?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentPastProductions` field. */
  adminCommentPastProductions?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentProductAchievement` field. */
  adminCommentProductAchievement?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentProductOverview` field. */
  adminCommentProductOverview?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentTargetSupplement` field. */
  adminCommentTargetSupplement?: InputMaybe<StringFilter>;
  /** Checks for all expressions in this list. */
  and?: InputMaybe<Array<ProductInformationFilter>>;
  /** Filter by the object’s `articleLpUrl` field. */
  articleLpUrl?: InputMaybe<StringFilter>;
  /** Filter by the object’s `campaignOffer` field. */
  campaignOffer?: InputMaybe<StringFilter>;
  /** Filter by the object’s `competitorAnswerStatus` field. */
  competitorAnswerStatus?: InputMaybe<StringFilter>;
  /** Filter by the object’s `corporateIndustry` field. */
  corporateIndustry?: InputMaybe<StringFilter>;
  /** Filter by the object’s `corporateJobTitle` field. */
  corporateJobTitle?: InputMaybe<StringFilter>;
  /** Filter by the object’s `corporateProblem` field. */
  corporateProblem?: InputMaybe<StringFilter>;
  /** Filter by the object’s `createdAt` field. */
  createdAt?: InputMaybe<DatetimeFilter>;
  /** Filter by the object’s `customerMaterialsAnswerStatus` field. */
  customerMaterialsAnswerStatus?: InputMaybe<StringFilter>;
  /** Filter by the object’s `customerMaterialsFileKeys` field. */
  customerMaterialsFileKeys?: InputMaybe<StringListFilter>;
  /** Filter by the object’s `customerMaterialsFiles` field. */
  customerMaterialsFiles?: InputMaybe<StringListFilter>;
  /** Filter by the object’s `deviceImpressionRatio` field. */
  deviceImpressionRatio?: InputMaybe<StringFilter>;
  /** Filter by the object’s `existingCreativeAnswerStatus` field. */
  existingCreativeAnswerStatus?: InputMaybe<StringFilter>;
  /** Filter by the object’s `existingCreativeFileKeys` field. */
  existingCreativeFileKeys?: InputMaybe<StringListFilter>;
  /** Filter by the object’s `existingCreativeFiles` field. */
  existingCreativeFiles?: InputMaybe<StringListFilter>;
  /** Filter by the object’s `existingCreativeUrl` field. */
  existingCreativeUrl?: InputMaybe<StringFilter>;
  /** Filter by the object’s `failureCase` field. */
  failureCase?: InputMaybe<StringFilter>;
  /** Filter by the object’s `id` field. */
  id?: InputMaybe<UuidFilter>;
  /** Filter by the object’s `individualAge` field. */
  individualAge?: InputMaybe<StringFilter>;
  /** Filter by the object’s `individualFamilyIncome` field. */
  individualFamilyIncome?: InputMaybe<StringFilter>;
  /** Filter by the object’s `individualFamilyStructure` field. */
  individualFamilyStructure?: InputMaybe<StringFilter>;
  /** Filter by the object’s `individualGender` field. */
  individualGender?: InputMaybe<StringFilter>;
  /** Filter by the object’s `individualHobby` field. */
  individualHobby?: InputMaybe<StringFilter>;
  /** Filter by the object’s `individualHobbyExpense` field. */
  individualHobbyExpense?: InputMaybe<StringFilter>;
  /** Filter by the object’s `individualInternetUsage` field. */
  individualInternetUsage?: InputMaybe<StringFilter>;
  /** Filter by the object’s `individualOccupation` field. */
  individualOccupation?: InputMaybe<StringFilter>;
  /** Filter by the object’s `individualOwnedDevice` field. */
  individualOwnedDevice?: InputMaybe<StringFilter>;
  /** Filter by the object’s `individualProblem` field. */
  individualProblem?: InputMaybe<StringFilter>;
  /** Filter by the object’s `individualResidence` field. */
  individualResidence?: InputMaybe<StringFilter>;
  /** Filter by the object’s `individualSnsUsually` field. */
  individualSnsUsually?: InputMaybe<StringFilter>;
  /** Filter by the object’s `isCorporate` field. */
  isCorporate?: InputMaybe<BooleanFilter>;
  /** Filter by the object’s `mainTarget` field. */
  mainTarget?: InputMaybe<StringFilter>;
  /** Negates the expression. */
  not?: InputMaybe<ProductInformationFilter>;
  /** Checks for any expressions in this list. */
  or?: InputMaybe<Array<ProductInformationFilter>>;
  /** Filter by the object’s `productAchievement` field. */
  productAchievement?: InputMaybe<StringFilter>;
  /** Filter by the object’s `productAnswerStatus` field. */
  productAnswerStatus?: InputMaybe<StringFilter>;
  /** Filter by the object’s `productId` field. */
  productId?: InputMaybe<UuidFilter>;
  /** Filter by the object’s `productMaterialsFileKeys` field. */
  productMaterialsFileKeys?: InputMaybe<StringListFilter>;
  /** Filter by the object’s `productMaterialsFiles` field. */
  productMaterialsFiles?: InputMaybe<StringListFilter>;
  /** Filter by the object’s `productMaterialsUrl` field. */
  productMaterialsUrl?: InputMaybe<StringFilter>;
  /** Filter by the object’s `productsFeatures` field. */
  productsFeatures?: InputMaybe<StringFilter>;
  /** Filter by the object’s `referencedProduct` field. */
  referencedProduct?: InputMaybe<StringFilter>;
  /** Filter by the object’s `similarProduct` field. */
  similarProduct?: InputMaybe<StringFilter>;
  /** Filter by the object’s `subTargetExistence` field. */
  subTargetExistence?: InputMaybe<StringFilter>;
  /** Filter by the object’s `successCase` field. */
  successCase?: InputMaybe<StringFilter>;
  /** Filter by the object’s `superiorProduct` field. */
  superiorProduct?: InputMaybe<StringFilter>;
  /** Filter by the object’s `uniqueStrength` field. */
  uniqueStrength?: InputMaybe<StringFilter>;
  /** Filter by the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<DatetimeFilter>;
};

/** An input for mutations affecting `ProductInformation` */
export type ProductInformationInput = {
  adTypeHistory?: InputMaybe<Scalars['String']['input']>;
  adminCommentCampaignOffer?: InputMaybe<Scalars['String']['input']>;
  adminCommentCompetitors?: InputMaybe<Scalars['String']['input']>;
  adminCommentCorporate?: InputMaybe<Scalars['String']['input']>;
  adminCommentIndividual?: InputMaybe<Scalars['String']['input']>;
  adminCommentMainTarget?: InputMaybe<Scalars['String']['input']>;
  adminCommentPastCases?: InputMaybe<Scalars['String']['input']>;
  adminCommentPastProductions?: InputMaybe<Scalars['String']['input']>;
  adminCommentProductAchievement?: InputMaybe<Scalars['String']['input']>;
  adminCommentProductOverview?: InputMaybe<Scalars['String']['input']>;
  adminCommentTargetSupplement?: InputMaybe<Scalars['String']['input']>;
  articleLpUrl?: InputMaybe<Scalars['String']['input']>;
  campaignOffer?: InputMaybe<Scalars['String']['input']>;
  competitorAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  corporateIndustry?: InputMaybe<Scalars['String']['input']>;
  corporateJobTitle?: InputMaybe<Scalars['String']['input']>;
  corporateProblem?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  customerMaterialsAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  customerMaterialsFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  customerMaterialsFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  deviceImpressionRatio?: InputMaybe<Scalars['String']['input']>;
  existingCreativeAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  existingCreativeFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  existingCreativeFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  existingCreativeUrl?: InputMaybe<Scalars['String']['input']>;
  failureCase?: InputMaybe<Scalars['String']['input']>;
  /** 商材情報ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  individualAge?: InputMaybe<Scalars['String']['input']>;
  individualFamilyIncome?: InputMaybe<Scalars['String']['input']>;
  individualFamilyStructure?: InputMaybe<Scalars['String']['input']>;
  individualGender?: InputMaybe<Scalars['String']['input']>;
  individualHobby?: InputMaybe<Scalars['String']['input']>;
  individualHobbyExpense?: InputMaybe<Scalars['String']['input']>;
  individualInternetUsage?: InputMaybe<Scalars['String']['input']>;
  individualOccupation?: InputMaybe<Scalars['String']['input']>;
  individualOwnedDevice?: InputMaybe<Scalars['String']['input']>;
  individualProblem?: InputMaybe<Scalars['String']['input']>;
  individualResidence?: InputMaybe<Scalars['String']['input']>;
  individualSnsUsually?: InputMaybe<Scalars['String']['input']>;
  isCorporate?: InputMaybe<Scalars['Boolean']['input']>;
  mainTarget?: InputMaybe<Scalars['String']['input']>;
  productAchievement?: InputMaybe<Scalars['String']['input']>;
  productAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  /** プロダクトID */
  productId?: InputMaybe<Scalars['UUID']['input']>;
  productMaterialsFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  productMaterialsFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  productMaterialsUrl?: InputMaybe<Scalars['String']['input']>;
  productsFeatures?: InputMaybe<Scalars['String']['input']>;
  referencedProduct?: InputMaybe<Scalars['String']['input']>;
  similarProduct?: InputMaybe<Scalars['String']['input']>;
  subTargetExistence?: InputMaybe<Scalars['String']['input']>;
  successCase?: InputMaybe<Scalars['String']['input']>;
  superiorProduct?: InputMaybe<Scalars['String']['input']>;
  uniqueStrength?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** Represents an update to a `ProductInformation`. Fields that are set will be updated. */
export type ProductInformationPatch = {
  adTypeHistory?: InputMaybe<Scalars['String']['input']>;
  adminCommentCampaignOffer?: InputMaybe<Scalars['String']['input']>;
  adminCommentCompetitors?: InputMaybe<Scalars['String']['input']>;
  adminCommentCorporate?: InputMaybe<Scalars['String']['input']>;
  adminCommentIndividual?: InputMaybe<Scalars['String']['input']>;
  adminCommentMainTarget?: InputMaybe<Scalars['String']['input']>;
  adminCommentPastCases?: InputMaybe<Scalars['String']['input']>;
  adminCommentPastProductions?: InputMaybe<Scalars['String']['input']>;
  adminCommentProductAchievement?: InputMaybe<Scalars['String']['input']>;
  adminCommentProductOverview?: InputMaybe<Scalars['String']['input']>;
  adminCommentTargetSupplement?: InputMaybe<Scalars['String']['input']>;
  articleLpUrl?: InputMaybe<Scalars['String']['input']>;
  campaignOffer?: InputMaybe<Scalars['String']['input']>;
  competitorAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  corporateIndustry?: InputMaybe<Scalars['String']['input']>;
  corporateJobTitle?: InputMaybe<Scalars['String']['input']>;
  corporateProblem?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  customerMaterialsAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  customerMaterialsFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  customerMaterialsFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  deviceImpressionRatio?: InputMaybe<Scalars['String']['input']>;
  existingCreativeAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  existingCreativeFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  existingCreativeFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  existingCreativeUrl?: InputMaybe<Scalars['String']['input']>;
  failureCase?: InputMaybe<Scalars['String']['input']>;
  /** 商材情報ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  individualAge?: InputMaybe<Scalars['String']['input']>;
  individualFamilyIncome?: InputMaybe<Scalars['String']['input']>;
  individualFamilyStructure?: InputMaybe<Scalars['String']['input']>;
  individualGender?: InputMaybe<Scalars['String']['input']>;
  individualHobby?: InputMaybe<Scalars['String']['input']>;
  individualHobbyExpense?: InputMaybe<Scalars['String']['input']>;
  individualInternetUsage?: InputMaybe<Scalars['String']['input']>;
  individualOccupation?: InputMaybe<Scalars['String']['input']>;
  individualOwnedDevice?: InputMaybe<Scalars['String']['input']>;
  individualProblem?: InputMaybe<Scalars['String']['input']>;
  individualResidence?: InputMaybe<Scalars['String']['input']>;
  individualSnsUsually?: InputMaybe<Scalars['String']['input']>;
  isCorporate?: InputMaybe<Scalars['Boolean']['input']>;
  mainTarget?: InputMaybe<Scalars['String']['input']>;
  productAchievement?: InputMaybe<Scalars['String']['input']>;
  productAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  /** プロダクトID */
  productId?: InputMaybe<Scalars['UUID']['input']>;
  productMaterialsFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  productMaterialsFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  productMaterialsUrl?: InputMaybe<Scalars['String']['input']>;
  productsFeatures?: InputMaybe<Scalars['String']['input']>;
  referencedProduct?: InputMaybe<Scalars['String']['input']>;
  similarProduct?: InputMaybe<Scalars['String']['input']>;
  subTargetExistence?: InputMaybe<Scalars['String']['input']>;
  successCase?: InputMaybe<Scalars['String']['input']>;
  superiorProduct?: InputMaybe<Scalars['String']['input']>;
  uniqueStrength?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** A connection to a list of `ProductInformation` values. */
export type ProductInformationsConnection = {
  __typename?: 'ProductInformationsConnection';
  /** A list of edges which contains the `ProductInformation` and cursor to aid in pagination. */
  edges: Array<ProductInformationsEdge>;
  /** A list of `ProductInformation` objects. */
  nodes: Array<Maybe<ProductInformation>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `ProductInformation` you could get from the connection. */
  totalCount: Scalars['Int']['output'];
};

/** A `ProductInformation` edge in the connection. */
export type ProductInformationsEdge = {
  __typename?: 'ProductInformationsEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']['output']>;
  /** The `ProductInformation` at the end of the edge. */
  node?: Maybe<ProductInformation>;
};

/** Methods to use when ordering `ProductInformation`. */
export enum ProductInformationsOrderBy {
  AdminCommentCampaignOfferAsc = 'ADMIN_COMMENT_CAMPAIGN_OFFER_ASC',
  AdminCommentCampaignOfferDesc = 'ADMIN_COMMENT_CAMPAIGN_OFFER_DESC',
  AdminCommentCompetitorsAsc = 'ADMIN_COMMENT_COMPETITORS_ASC',
  AdminCommentCompetitorsDesc = 'ADMIN_COMMENT_COMPETITORS_DESC',
  AdminCommentCorporateAsc = 'ADMIN_COMMENT_CORPORATE_ASC',
  AdminCommentCorporateDesc = 'ADMIN_COMMENT_CORPORATE_DESC',
  AdminCommentIndividualAsc = 'ADMIN_COMMENT_INDIVIDUAL_ASC',
  AdminCommentIndividualDesc = 'ADMIN_COMMENT_INDIVIDUAL_DESC',
  AdminCommentMainTargetAsc = 'ADMIN_COMMENT_MAIN_TARGET_ASC',
  AdminCommentMainTargetDesc = 'ADMIN_COMMENT_MAIN_TARGET_DESC',
  AdminCommentPastCasesAsc = 'ADMIN_COMMENT_PAST_CASES_ASC',
  AdminCommentPastCasesDesc = 'ADMIN_COMMENT_PAST_CASES_DESC',
  AdminCommentPastProductionsAsc = 'ADMIN_COMMENT_PAST_PRODUCTIONS_ASC',
  AdminCommentPastProductionsDesc = 'ADMIN_COMMENT_PAST_PRODUCTIONS_DESC',
  AdminCommentProductAchievementAsc = 'ADMIN_COMMENT_PRODUCT_ACHIEVEMENT_ASC',
  AdminCommentProductAchievementDesc = 'ADMIN_COMMENT_PRODUCT_ACHIEVEMENT_DESC',
  AdminCommentProductOverviewAsc = 'ADMIN_COMMENT_PRODUCT_OVERVIEW_ASC',
  AdminCommentProductOverviewDesc = 'ADMIN_COMMENT_PRODUCT_OVERVIEW_DESC',
  AdminCommentTargetSupplementAsc = 'ADMIN_COMMENT_TARGET_SUPPLEMENT_ASC',
  AdminCommentTargetSupplementDesc = 'ADMIN_COMMENT_TARGET_SUPPLEMENT_DESC',
  AdTypeHistoryAsc = 'AD_TYPE_HISTORY_ASC',
  AdTypeHistoryDesc = 'AD_TYPE_HISTORY_DESC',
  ArticleLpUrlAsc = 'ARTICLE_LP_URL_ASC',
  ArticleLpUrlDesc = 'ARTICLE_LP_URL_DESC',
  CampaignOfferAsc = 'CAMPAIGN_OFFER_ASC',
  CampaignOfferDesc = 'CAMPAIGN_OFFER_DESC',
  CompetitorAnswerStatusAsc = 'COMPETITOR_ANSWER_STATUS_ASC',
  CompetitorAnswerStatusDesc = 'COMPETITOR_ANSWER_STATUS_DESC',
  CorporateIndustryAsc = 'CORPORATE_INDUSTRY_ASC',
  CorporateIndustryDesc = 'CORPORATE_INDUSTRY_DESC',
  CorporateJobTitleAsc = 'CORPORATE_JOB_TITLE_ASC',
  CorporateJobTitleDesc = 'CORPORATE_JOB_TITLE_DESC',
  CorporateProblemAsc = 'CORPORATE_PROBLEM_ASC',
  CorporateProblemDesc = 'CORPORATE_PROBLEM_DESC',
  CreatedAtAsc = 'CREATED_AT_ASC',
  CreatedAtDesc = 'CREATED_AT_DESC',
  CustomerMaterialsAnswerStatusAsc = 'CUSTOMER_MATERIALS_ANSWER_STATUS_ASC',
  CustomerMaterialsAnswerStatusDesc = 'CUSTOMER_MATERIALS_ANSWER_STATUS_DESC',
  CustomerMaterialsFilesAsc = 'CUSTOMER_MATERIALS_FILES_ASC',
  CustomerMaterialsFilesDesc = 'CUSTOMER_MATERIALS_FILES_DESC',
  CustomerMaterialsFileKeysAsc = 'CUSTOMER_MATERIALS_FILE_KEYS_ASC',
  CustomerMaterialsFileKeysDesc = 'CUSTOMER_MATERIALS_FILE_KEYS_DESC',
  DeviceImpressionRatioAsc = 'DEVICE_IMPRESSION_RATIO_ASC',
  DeviceImpressionRatioDesc = 'DEVICE_IMPRESSION_RATIO_DESC',
  ExistingCreativeAnswerStatusAsc = 'EXISTING_CREATIVE_ANSWER_STATUS_ASC',
  ExistingCreativeAnswerStatusDesc = 'EXISTING_CREATIVE_ANSWER_STATUS_DESC',
  ExistingCreativeFilesAsc = 'EXISTING_CREATIVE_FILES_ASC',
  ExistingCreativeFilesDesc = 'EXISTING_CREATIVE_FILES_DESC',
  ExistingCreativeFileKeysAsc = 'EXISTING_CREATIVE_FILE_KEYS_ASC',
  ExistingCreativeFileKeysDesc = 'EXISTING_CREATIVE_FILE_KEYS_DESC',
  ExistingCreativeUrlAsc = 'EXISTING_CREATIVE_URL_ASC',
  ExistingCreativeUrlDesc = 'EXISTING_CREATIVE_URL_DESC',
  FailureCaseAsc = 'FAILURE_CASE_ASC',
  FailureCaseDesc = 'FAILURE_CASE_DESC',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  IndividualAgeAsc = 'INDIVIDUAL_AGE_ASC',
  IndividualAgeDesc = 'INDIVIDUAL_AGE_DESC',
  IndividualFamilyIncomeAsc = 'INDIVIDUAL_FAMILY_INCOME_ASC',
  IndividualFamilyIncomeDesc = 'INDIVIDUAL_FAMILY_INCOME_DESC',
  IndividualFamilyStructureAsc = 'INDIVIDUAL_FAMILY_STRUCTURE_ASC',
  IndividualFamilyStructureDesc = 'INDIVIDUAL_FAMILY_STRUCTURE_DESC',
  IndividualGenderAsc = 'INDIVIDUAL_GENDER_ASC',
  IndividualGenderDesc = 'INDIVIDUAL_GENDER_DESC',
  IndividualHobbyAsc = 'INDIVIDUAL_HOBBY_ASC',
  IndividualHobbyDesc = 'INDIVIDUAL_HOBBY_DESC',
  IndividualHobbyExpenseAsc = 'INDIVIDUAL_HOBBY_EXPENSE_ASC',
  IndividualHobbyExpenseDesc = 'INDIVIDUAL_HOBBY_EXPENSE_DESC',
  IndividualInternetUsageAsc = 'INDIVIDUAL_INTERNET_USAGE_ASC',
  IndividualInternetUsageDesc = 'INDIVIDUAL_INTERNET_USAGE_DESC',
  IndividualOccupationAsc = 'INDIVIDUAL_OCCUPATION_ASC',
  IndividualOccupationDesc = 'INDIVIDUAL_OCCUPATION_DESC',
  IndividualOwnedDeviceAsc = 'INDIVIDUAL_OWNED_DEVICE_ASC',
  IndividualOwnedDeviceDesc = 'INDIVIDUAL_OWNED_DEVICE_DESC',
  IndividualProblemAsc = 'INDIVIDUAL_PROBLEM_ASC',
  IndividualProblemDesc = 'INDIVIDUAL_PROBLEM_DESC',
  IndividualResidenceAsc = 'INDIVIDUAL_RESIDENCE_ASC',
  IndividualResidenceDesc = 'INDIVIDUAL_RESIDENCE_DESC',
  IndividualSnsUsuallyAsc = 'INDIVIDUAL_SNS_USUALLY_ASC',
  IndividualSnsUsuallyDesc = 'INDIVIDUAL_SNS_USUALLY_DESC',
  IsCorporateAsc = 'IS_CORPORATE_ASC',
  IsCorporateDesc = 'IS_CORPORATE_DESC',
  MainTargetAsc = 'MAIN_TARGET_ASC',
  MainTargetDesc = 'MAIN_TARGET_DESC',
  Natural = 'NATURAL',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
  ProductsFeaturesAsc = 'PRODUCTS_FEATURES_ASC',
  ProductsFeaturesDesc = 'PRODUCTS_FEATURES_DESC',
  ProductAchievementAsc = 'PRODUCT_ACHIEVEMENT_ASC',
  ProductAchievementDesc = 'PRODUCT_ACHIEVEMENT_DESC',
  ProductAnswerStatusAsc = 'PRODUCT_ANSWER_STATUS_ASC',
  ProductAnswerStatusDesc = 'PRODUCT_ANSWER_STATUS_DESC',
  ProductIdAsc = 'PRODUCT_ID_ASC',
  ProductIdDesc = 'PRODUCT_ID_DESC',
  ProductMaterialsFilesAsc = 'PRODUCT_MATERIALS_FILES_ASC',
  ProductMaterialsFilesDesc = 'PRODUCT_MATERIALS_FILES_DESC',
  ProductMaterialsFileKeysAsc = 'PRODUCT_MATERIALS_FILE_KEYS_ASC',
  ProductMaterialsFileKeysDesc = 'PRODUCT_MATERIALS_FILE_KEYS_DESC',
  ProductMaterialsUrlAsc = 'PRODUCT_MATERIALS_URL_ASC',
  ProductMaterialsUrlDesc = 'PRODUCT_MATERIALS_URL_DESC',
  ReferencedProductAsc = 'REFERENCED_PRODUCT_ASC',
  ReferencedProductDesc = 'REFERENCED_PRODUCT_DESC',
  SimilarProductAsc = 'SIMILAR_PRODUCT_ASC',
  SimilarProductDesc = 'SIMILAR_PRODUCT_DESC',
  SubTargetExistenceAsc = 'SUB_TARGET_EXISTENCE_ASC',
  SubTargetExistenceDesc = 'SUB_TARGET_EXISTENCE_DESC',
  SuccessCaseAsc = 'SUCCESS_CASE_ASC',
  SuccessCaseDesc = 'SUCCESS_CASE_DESC',
  SuperiorProductAsc = 'SUPERIOR_PRODUCT_ASC',
  SuperiorProductDesc = 'SUPERIOR_PRODUCT_DESC',
  UniqueStrengthAsc = 'UNIQUE_STRENGTH_ASC',
  UniqueStrengthDesc = 'UNIQUE_STRENGTH_DESC',
  UpdatedAtAsc = 'UPDATED_AT_ASC',
  UpdatedAtDesc = 'UPDATED_AT_DESC'
}

/** An input for mutations affecting `Product` */
export type ProductInput = {
  /** 企業ID */
  companyId: Scalars['UUID']['input'];
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** ヒアリング完了フラグ */
  hearingComplete?: InputMaybe<Scalars['Boolean']['input']>;
  /** 商材ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** 商材名 */
  productName?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** Represents an update to a `Product`. Fields that are set will be updated. */
export type ProductPatch = {
  /** 企業ID */
  companyId?: InputMaybe<Scalars['UUID']['input']>;
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** ヒアリング完了フラグ */
  hearingComplete?: InputMaybe<Scalars['Boolean']['input']>;
  /** 商材ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** 商材名 */
  productName?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** A connection to a list of `Product` values. */
export type ProductsConnection = {
  __typename?: 'ProductsConnection';
  /** A list of edges which contains the `Product` and cursor to aid in pagination. */
  edges: Array<ProductsEdge>;
  /** A list of `Product` objects. */
  nodes: Array<Maybe<Product>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `Product` you could get from the connection. */
  totalCount: Scalars['Int']['output'];
};

/** A `Product` edge in the connection. */
export type ProductsEdge = {
  __typename?: 'ProductsEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']['output']>;
  /** The `Product` at the end of the edge. */
  node?: Maybe<Product>;
};

/** Methods to use when ordering `Product`. */
export enum ProductsOrderBy {
  CompanyIdAsc = 'COMPANY_ID_ASC',
  CompanyIdDesc = 'COMPANY_ID_DESC',
  CreatedAtAsc = 'CREATED_AT_ASC',
  CreatedAtDesc = 'CREATED_AT_DESC',
  HearingCompleteAsc = 'HEARING_COMPLETE_ASC',
  HearingCompleteDesc = 'HEARING_COMPLETE_DESC',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  Natural = 'NATURAL',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
  ProductNameAsc = 'PRODUCT_NAME_ASC',
  ProductNameDesc = 'PRODUCT_NAME_DESC',
  UpdatedAtAsc = 'UPDATED_AT_ASC',
  UpdatedAtDesc = 'UPDATED_AT_DESC'
}

/** The root query type which gives access points into the data universe. */
export type Query = Node & {
  __typename?: 'Query';
  /** Reads a single `AlembicVersion` using its globally unique `ID`. */
  alembicVersion?: Maybe<AlembicVersion>;
  alembicVersionByVersionNum?: Maybe<AlembicVersion>;
  /** Reads and enables pagination through a set of `AlembicVersion`. */
  allAlembicVersions?: Maybe<AlembicVersionsConnection>;
  /** Reads and enables pagination through a set of `Company`. */
  allCompanies?: Maybe<CompaniesConnection>;
  /** Reads and enables pagination through a set of `CompanyInfo`. */
  allCompanyInfos?: Maybe<CompanyInfosConnection>;
  /** Reads and enables pagination through a set of `CustomerGroup`. */
  allCustomerGroups?: Maybe<CustomerGroupsConnection>;
  /** Reads and enables pagination through a set of `MarketingActivityInformation`. */
  allMarketingActivityInformations?: Maybe<MarketingActivityInformationsConnection>;
  /** Reads and enables pagination through a set of `Notification`. */
  allNotifications?: Maybe<NotificationsConnection>;
  /** Reads and enables pagination through a set of `OperatorGroup`. */
  allOperatorGroups?: Maybe<OperatorGroupsConnection>;
  /** Reads and enables pagination through a set of `OperatorName`. */
  allOperatorNames?: Maybe<OperatorNamesConnection>;
  /** Reads and enables pagination through a set of `Opportunity`. */
  allOpportunities?: Maybe<OpportunitiesConnection>;
  /** Reads and enables pagination through a set of `ProductInformation`. */
  allProductInformations?: Maybe<ProductInformationsConnection>;
  /** Reads and enables pagination through a set of `Product`. */
  allProducts?: Maybe<ProductsConnection>;
  /** Reads and enables pagination through a set of `RequestContent`. */
  allRequestContents?: Maybe<RequestContentsConnection>;
  /** Reads and enables pagination through a set of `UserInformationMemo`. */
  allUserInformationMemos?: Maybe<UserInformationMemosConnection>;
  /** Reads a single `Company` using its globally unique `ID`. */
  company?: Maybe<Company>;
  companyById?: Maybe<Company>;
  /** Reads a single `CompanyInfo` using its globally unique `ID`. */
  companyInfo?: Maybe<CompanyInfo>;
  companyInfoById?: Maybe<CompanyInfo>;
  /** Reads a single `CustomerGroup` using its globally unique `ID`. */
  customerGroup?: Maybe<CustomerGroup>;
  customerGroupById?: Maybe<CustomerGroup>;
  customerGroupByOpportunityIdAndSfContactId?: Maybe<CustomerGroup>;
  /** Reads a single `MarketingActivityInformation` using its globally unique `ID`. */
  marketingActivityInformation?: Maybe<MarketingActivityInformation>;
  marketingActivityInformationById?: Maybe<MarketingActivityInformation>;
  /** Fetches an object given its globally unique `ID`. */
  node?: Maybe<Node>;
  /** The root query type must be a `Node` to work well with Relay 1 mutations. This just resolves to `query`. */
  nodeId: Scalars['ID']['output'];
  /** Reads a single `Notification` using its globally unique `ID`. */
  notification?: Maybe<Notification>;
  notificationById?: Maybe<Notification>;
  /** Reads a single `OperatorGroup` using its globally unique `ID`. */
  operatorGroup?: Maybe<OperatorGroup>;
  operatorGroupById?: Maybe<OperatorGroup>;
  /** Reads a single `OperatorName` using its globally unique `ID`. */
  operatorName?: Maybe<OperatorName>;
  operatorNameByEmail?: Maybe<OperatorName>;
  /** Reads a single `Opportunity` using its globally unique `ID`. */
  opportunity?: Maybe<Opportunity>;
  opportunityById?: Maybe<Opportunity>;
  /** Reads a single `Product` using its globally unique `ID`. */
  product?: Maybe<Product>;
  productByCompanyIdAndProductName?: Maybe<Product>;
  productById?: Maybe<Product>;
  /** Reads a single `ProductInformation` using its globally unique `ID`. */
  productInformation?: Maybe<ProductInformation>;
  productInformationById?: Maybe<ProductInformation>;
  /**
   * Exposes the root query type nested one level down. This is helpful for Relay 1
   * which can only query top level fields if they are in a particular form.
   */
  query: Query;
  /** Reads a single `RequestContent` using its globally unique `ID`. */
  requestContent?: Maybe<RequestContent>;
  requestContentById?: Maybe<RequestContent>;
  /** Reads a single `UserInformationMemo` using its globally unique `ID`. */
  userInformationMemo?: Maybe<UserInformationMemo>;
  userInformationMemoById?: Maybe<UserInformationMemo>;
};


/** The root query type which gives access points into the data universe. */
export type QueryAlembicVersionArgs = {
  nodeId: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryAlembicVersionByVersionNumArgs = {
  versionNum: Scalars['String']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryAllAlembicVersionsArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<AlembicVersionCondition>;
  filter?: InputMaybe<AlembicVersionFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<AlembicVersionsOrderBy>>;
};


/** The root query type which gives access points into the data universe. */
export type QueryAllCompaniesArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<CompanyCondition>;
  filter?: InputMaybe<CompanyFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CompaniesOrderBy>>;
};


/** The root query type which gives access points into the data universe. */
export type QueryAllCompanyInfosArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<CompanyInfoCondition>;
  filter?: InputMaybe<CompanyInfoFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CompanyInfosOrderBy>>;
};


/** The root query type which gives access points into the data universe. */
export type QueryAllCustomerGroupsArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<CustomerGroupCondition>;
  filter?: InputMaybe<CustomerGroupFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CustomerGroupsOrderBy>>;
};


/** The root query type which gives access points into the data universe. */
export type QueryAllMarketingActivityInformationsArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<MarketingActivityInformationCondition>;
  filter?: InputMaybe<MarketingActivityInformationFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<MarketingActivityInformationsOrderBy>>;
};


/** The root query type which gives access points into the data universe. */
export type QueryAllNotificationsArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<NotificationCondition>;
  filter?: InputMaybe<NotificationFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<NotificationsOrderBy>>;
};


/** The root query type which gives access points into the data universe. */
export type QueryAllOperatorGroupsArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<OperatorGroupCondition>;
  filter?: InputMaybe<OperatorGroupFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<OperatorGroupsOrderBy>>;
};


/** The root query type which gives access points into the data universe. */
export type QueryAllOperatorNamesArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<OperatorNameCondition>;
  filter?: InputMaybe<OperatorNameFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<OperatorNamesOrderBy>>;
};


/** The root query type which gives access points into the data universe. */
export type QueryAllOpportunitiesArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<OpportunityCondition>;
  filter?: InputMaybe<OpportunityFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<OpportunitiesOrderBy>>;
};


/** The root query type which gives access points into the data universe. */
export type QueryAllProductInformationsArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<ProductInformationCondition>;
  filter?: InputMaybe<ProductInformationFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<ProductInformationsOrderBy>>;
};


/** The root query type which gives access points into the data universe. */
export type QueryAllProductsArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<ProductCondition>;
  filter?: InputMaybe<ProductFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<ProductsOrderBy>>;
};


/** The root query type which gives access points into the data universe. */
export type QueryAllRequestContentsArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<RequestContentCondition>;
  filter?: InputMaybe<RequestContentFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<RequestContentsOrderBy>>;
};


/** The root query type which gives access points into the data universe. */
export type QueryAllUserInformationMemosArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<UserInformationMemoCondition>;
  filter?: InputMaybe<UserInformationMemoFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<UserInformationMemosOrderBy>>;
};


/** The root query type which gives access points into the data universe. */
export type QueryCompanyArgs = {
  nodeId: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryCompanyByIdArgs = {
  id: Scalars['UUID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryCompanyInfoArgs = {
  nodeId: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryCompanyInfoByIdArgs = {
  id: Scalars['UUID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryCustomerGroupArgs = {
  nodeId: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryCustomerGroupByIdArgs = {
  id: Scalars['Int']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryCustomerGroupByOpportunityIdAndSfContactIdArgs = {
  opportunityId: Scalars['UUID']['input'];
  sfContactId: Scalars['String']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryMarketingActivityInformationArgs = {
  nodeId: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryMarketingActivityInformationByIdArgs = {
  id: Scalars['UUID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryNodeArgs = {
  nodeId: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryNotificationArgs = {
  nodeId: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryNotificationByIdArgs = {
  id: Scalars['UUID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryOperatorGroupArgs = {
  nodeId: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryOperatorGroupByIdArgs = {
  id: Scalars['Int']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryOperatorNameArgs = {
  nodeId: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryOperatorNameByEmailArgs = {
  email: Scalars['String']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryOpportunityArgs = {
  nodeId: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryOpportunityByIdArgs = {
  id: Scalars['UUID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryProductArgs = {
  nodeId: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryProductByCompanyIdAndProductNameArgs = {
  companyId: Scalars['UUID']['input'];
  productName: Scalars['String']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryProductByIdArgs = {
  id: Scalars['UUID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryProductInformationArgs = {
  nodeId: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryProductInformationByIdArgs = {
  id: Scalars['UUID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryRequestContentArgs = {
  nodeId: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryRequestContentByIdArgs = {
  id: Scalars['UUID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryUserInformationMemoArgs = {
  nodeId: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryUserInformationMemoByIdArgs = {
  id: Scalars['UUID']['input'];
};

/** ご依頼内容Pkey */
export type RequestContent = Node & {
  __typename?: 'RequestContent';
  adminCommentFirstProduction?: Maybe<Scalars['String']['output']>;
  adminCommentGoal?: Maybe<Scalars['String']['output']>;
  adminCommentIssue?: Maybe<Scalars['String']['output']>;
  adminCommentOrderBackground?: Maybe<Scalars['String']['output']>;
  adminCommentRegulation?: Maybe<Scalars['String']['output']>;
  anyAdditionalComments?: Maybe<Scalars['String']['output']>;
  createdAt?: Maybe<Scalars['Datetime']['output']>;
  creativeMaterialsAnswerStatus?: Maybe<Scalars['String']['output']>;
  creativeMaterialsFileKeys?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  creativeMaterialsFileNames?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  creativePreferredPlan?: Maybe<Scalars['String']['output']>;
  creativeRegulationAnswerStatus?: Maybe<Scalars['String']['output']>;
  creativeRegulationMaterialsFileKeys?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  creativeRegulationMaterialsFiles?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  creativeRegulationUrlOrText?: Maybe<Scalars['String']['output']>;
  /** ご依頼内容ID */
  id: Scalars['UUID']['output'];
  isCorporate?: Maybe<Scalars['Boolean']['output']>;
  lpExternalServiceExistence?: Maybe<Scalars['String']['output']>;
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID']['output'];
  orderBackground?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Product` that is related to this `RequestContent`. */
  productByProductId?: Maybe<Product>;
  /** プロダクトID */
  productId?: Maybe<Scalars['UUID']['output']>;
  qualitativeGoal?: Maybe<Scalars['String']['output']>;
  qualitativeIssue?: Maybe<Scalars['String']['output']>;
  quantitativeGoal?: Maybe<Scalars['String']['output']>;
  quantitativeGoalAnswerStatus?: Maybe<Scalars['String']['output']>;
  quantitativeIssue?: Maybe<Scalars['String']['output']>;
  reasonForChangeExistingCreative?: Maybe<Scalars['String']['output']>;
  requestFirstCreativeType?: Maybe<Scalars['String']['output']>;
  resultAdAnswerStatus?: Maybe<Scalars['String']['output']>;
  resultAdFileKeys?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  resultAdFiles?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  updatedAt?: Maybe<Scalars['Datetime']['output']>;
};

/**
 * A condition to be used against `RequestContent` object types. All fields are
 * tested for equality and combined with a logical ‘and.’
 */
export type RequestContentCondition = {
  /** Checks for equality with the object’s `adminCommentFirstProduction` field. */
  adminCommentFirstProduction?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentGoal` field. */
  adminCommentGoal?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentIssue` field. */
  adminCommentIssue?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentOrderBackground` field. */
  adminCommentOrderBackground?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentRegulation` field. */
  adminCommentRegulation?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `anyAdditionalComments` field. */
  anyAdditionalComments?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `createdAt` field. */
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** Checks for equality with the object’s `creativeMaterialsAnswerStatus` field. */
  creativeMaterialsAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `creativeMaterialsFileKeys` field. */
  creativeMaterialsFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Checks for equality with the object’s `creativeMaterialsFileNames` field. */
  creativeMaterialsFileNames?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Checks for equality with the object’s `creativePreferredPlan` field. */
  creativePreferredPlan?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `creativeRegulationAnswerStatus` field. */
  creativeRegulationAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `creativeRegulationMaterialsFileKeys` field. */
  creativeRegulationMaterialsFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Checks for equality with the object’s `creativeRegulationMaterialsFiles` field. */
  creativeRegulationMaterialsFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Checks for equality with the object’s `creativeRegulationUrlOrText` field. */
  creativeRegulationUrlOrText?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `id` field. */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `isCorporate` field. */
  isCorporate?: InputMaybe<Scalars['Boolean']['input']>;
  /** Checks for equality with the object’s `lpExternalServiceExistence` field. */
  lpExternalServiceExistence?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `orderBackground` field. */
  orderBackground?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `productId` field. */
  productId?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `qualitativeGoal` field. */
  qualitativeGoal?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `qualitativeIssue` field. */
  qualitativeIssue?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `quantitativeGoal` field. */
  quantitativeGoal?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `quantitativeGoalAnswerStatus` field. */
  quantitativeGoalAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `quantitativeIssue` field. */
  quantitativeIssue?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `reasonForChangeExistingCreative` field. */
  reasonForChangeExistingCreative?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `requestFirstCreativeType` field. */
  requestFirstCreativeType?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `resultAdAnswerStatus` field. */
  resultAdAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `resultAdFileKeys` field. */
  resultAdFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Checks for equality with the object’s `resultAdFiles` field. */
  resultAdFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Checks for equality with the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** A filter to be used against `RequestContent` object types. All fields are combined with a logical ‘and.’ */
export type RequestContentFilter = {
  /** Filter by the object’s `adminCommentFirstProduction` field. */
  adminCommentFirstProduction?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentGoal` field. */
  adminCommentGoal?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentIssue` field. */
  adminCommentIssue?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentOrderBackground` field. */
  adminCommentOrderBackground?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentRegulation` field. */
  adminCommentRegulation?: InputMaybe<StringFilter>;
  /** Checks for all expressions in this list. */
  and?: InputMaybe<Array<RequestContentFilter>>;
  /** Filter by the object’s `anyAdditionalComments` field. */
  anyAdditionalComments?: InputMaybe<StringFilter>;
  /** Filter by the object’s `createdAt` field. */
  createdAt?: InputMaybe<DatetimeFilter>;
  /** Filter by the object’s `creativeMaterialsAnswerStatus` field. */
  creativeMaterialsAnswerStatus?: InputMaybe<StringFilter>;
  /** Filter by the object’s `creativeMaterialsFileKeys` field. */
  creativeMaterialsFileKeys?: InputMaybe<StringListFilter>;
  /** Filter by the object’s `creativeMaterialsFileNames` field. */
  creativeMaterialsFileNames?: InputMaybe<StringListFilter>;
  /** Filter by the object’s `creativePreferredPlan` field. */
  creativePreferredPlan?: InputMaybe<StringFilter>;
  /** Filter by the object’s `creativeRegulationAnswerStatus` field. */
  creativeRegulationAnswerStatus?: InputMaybe<StringFilter>;
  /** Filter by the object’s `creativeRegulationMaterialsFileKeys` field. */
  creativeRegulationMaterialsFileKeys?: InputMaybe<StringListFilter>;
  /** Filter by the object’s `creativeRegulationMaterialsFiles` field. */
  creativeRegulationMaterialsFiles?: InputMaybe<StringListFilter>;
  /** Filter by the object’s `creativeRegulationUrlOrText` field. */
  creativeRegulationUrlOrText?: InputMaybe<StringFilter>;
  /** Filter by the object’s `id` field. */
  id?: InputMaybe<UuidFilter>;
  /** Filter by the object’s `isCorporate` field. */
  isCorporate?: InputMaybe<BooleanFilter>;
  /** Filter by the object’s `lpExternalServiceExistence` field. */
  lpExternalServiceExistence?: InputMaybe<StringFilter>;
  /** Negates the expression. */
  not?: InputMaybe<RequestContentFilter>;
  /** Checks for any expressions in this list. */
  or?: InputMaybe<Array<RequestContentFilter>>;
  /** Filter by the object’s `orderBackground` field. */
  orderBackground?: InputMaybe<StringFilter>;
  /** Filter by the object’s `productId` field. */
  productId?: InputMaybe<UuidFilter>;
  /** Filter by the object’s `qualitativeGoal` field. */
  qualitativeGoal?: InputMaybe<StringFilter>;
  /** Filter by the object’s `qualitativeIssue` field. */
  qualitativeIssue?: InputMaybe<StringFilter>;
  /** Filter by the object’s `quantitativeGoal` field. */
  quantitativeGoal?: InputMaybe<StringFilter>;
  /** Filter by the object’s `quantitativeGoalAnswerStatus` field. */
  quantitativeGoalAnswerStatus?: InputMaybe<StringFilter>;
  /** Filter by the object’s `quantitativeIssue` field. */
  quantitativeIssue?: InputMaybe<StringFilter>;
  /** Filter by the object’s `reasonForChangeExistingCreative` field. */
  reasonForChangeExistingCreative?: InputMaybe<StringFilter>;
  /** Filter by the object’s `requestFirstCreativeType` field. */
  requestFirstCreativeType?: InputMaybe<StringFilter>;
  /** Filter by the object’s `resultAdAnswerStatus` field. */
  resultAdAnswerStatus?: InputMaybe<StringFilter>;
  /** Filter by the object’s `resultAdFileKeys` field. */
  resultAdFileKeys?: InputMaybe<StringListFilter>;
  /** Filter by the object’s `resultAdFiles` field. */
  resultAdFiles?: InputMaybe<StringListFilter>;
  /** Filter by the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<DatetimeFilter>;
};

/** An input for mutations affecting `RequestContent` */
export type RequestContentInput = {
  adminCommentFirstProduction?: InputMaybe<Scalars['String']['input']>;
  adminCommentGoal?: InputMaybe<Scalars['String']['input']>;
  adminCommentIssue?: InputMaybe<Scalars['String']['input']>;
  adminCommentOrderBackground?: InputMaybe<Scalars['String']['input']>;
  adminCommentRegulation?: InputMaybe<Scalars['String']['input']>;
  anyAdditionalComments?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  creativeMaterialsAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  creativeMaterialsFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  creativeMaterialsFileNames?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  creativePreferredPlan?: InputMaybe<Scalars['String']['input']>;
  creativeRegulationAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  creativeRegulationMaterialsFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  creativeRegulationMaterialsFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  creativeRegulationUrlOrText?: InputMaybe<Scalars['String']['input']>;
  /** ご依頼内容ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  isCorporate?: InputMaybe<Scalars['Boolean']['input']>;
  lpExternalServiceExistence?: InputMaybe<Scalars['String']['input']>;
  orderBackground?: InputMaybe<Scalars['String']['input']>;
  /** プロダクトID */
  productId?: InputMaybe<Scalars['UUID']['input']>;
  qualitativeGoal?: InputMaybe<Scalars['String']['input']>;
  qualitativeIssue?: InputMaybe<Scalars['String']['input']>;
  quantitativeGoal?: InputMaybe<Scalars['String']['input']>;
  quantitativeGoalAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  quantitativeIssue?: InputMaybe<Scalars['String']['input']>;
  reasonForChangeExistingCreative?: InputMaybe<Scalars['String']['input']>;
  requestFirstCreativeType?: InputMaybe<Scalars['String']['input']>;
  resultAdAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  resultAdFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  resultAdFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** Represents an update to a `RequestContent`. Fields that are set will be updated. */
export type RequestContentPatch = {
  adminCommentFirstProduction?: InputMaybe<Scalars['String']['input']>;
  adminCommentGoal?: InputMaybe<Scalars['String']['input']>;
  adminCommentIssue?: InputMaybe<Scalars['String']['input']>;
  adminCommentOrderBackground?: InputMaybe<Scalars['String']['input']>;
  adminCommentRegulation?: InputMaybe<Scalars['String']['input']>;
  anyAdditionalComments?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  creativeMaterialsAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  creativeMaterialsFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  creativeMaterialsFileNames?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  creativePreferredPlan?: InputMaybe<Scalars['String']['input']>;
  creativeRegulationAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  creativeRegulationMaterialsFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  creativeRegulationMaterialsFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  creativeRegulationUrlOrText?: InputMaybe<Scalars['String']['input']>;
  /** ご依頼内容ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  isCorporate?: InputMaybe<Scalars['Boolean']['input']>;
  lpExternalServiceExistence?: InputMaybe<Scalars['String']['input']>;
  orderBackground?: InputMaybe<Scalars['String']['input']>;
  /** プロダクトID */
  productId?: InputMaybe<Scalars['UUID']['input']>;
  qualitativeGoal?: InputMaybe<Scalars['String']['input']>;
  qualitativeIssue?: InputMaybe<Scalars['String']['input']>;
  quantitativeGoal?: InputMaybe<Scalars['String']['input']>;
  quantitativeGoalAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  quantitativeIssue?: InputMaybe<Scalars['String']['input']>;
  reasonForChangeExistingCreative?: InputMaybe<Scalars['String']['input']>;
  requestFirstCreativeType?: InputMaybe<Scalars['String']['input']>;
  resultAdAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  resultAdFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  resultAdFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** A connection to a list of `RequestContent` values. */
export type RequestContentsConnection = {
  __typename?: 'RequestContentsConnection';
  /** A list of edges which contains the `RequestContent` and cursor to aid in pagination. */
  edges: Array<RequestContentsEdge>;
  /** A list of `RequestContent` objects. */
  nodes: Array<Maybe<RequestContent>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `RequestContent` you could get from the connection. */
  totalCount: Scalars['Int']['output'];
};

/** A `RequestContent` edge in the connection. */
export type RequestContentsEdge = {
  __typename?: 'RequestContentsEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']['output']>;
  /** The `RequestContent` at the end of the edge. */
  node?: Maybe<RequestContent>;
};

/** Methods to use when ordering `RequestContent`. */
export enum RequestContentsOrderBy {
  AdminCommentFirstProductionAsc = 'ADMIN_COMMENT_FIRST_PRODUCTION_ASC',
  AdminCommentFirstProductionDesc = 'ADMIN_COMMENT_FIRST_PRODUCTION_DESC',
  AdminCommentGoalAsc = 'ADMIN_COMMENT_GOAL_ASC',
  AdminCommentGoalDesc = 'ADMIN_COMMENT_GOAL_DESC',
  AdminCommentIssueAsc = 'ADMIN_COMMENT_ISSUE_ASC',
  AdminCommentIssueDesc = 'ADMIN_COMMENT_ISSUE_DESC',
  AdminCommentOrderBackgroundAsc = 'ADMIN_COMMENT_ORDER_BACKGROUND_ASC',
  AdminCommentOrderBackgroundDesc = 'ADMIN_COMMENT_ORDER_BACKGROUND_DESC',
  AdminCommentRegulationAsc = 'ADMIN_COMMENT_REGULATION_ASC',
  AdminCommentRegulationDesc = 'ADMIN_COMMENT_REGULATION_DESC',
  AnyAdditionalCommentsAsc = 'ANY_ADDITIONAL_COMMENTS_ASC',
  AnyAdditionalCommentsDesc = 'ANY_ADDITIONAL_COMMENTS_DESC',
  CreatedAtAsc = 'CREATED_AT_ASC',
  CreatedAtDesc = 'CREATED_AT_DESC',
  CreativeMaterialsAnswerStatusAsc = 'CREATIVE_MATERIALS_ANSWER_STATUS_ASC',
  CreativeMaterialsAnswerStatusDesc = 'CREATIVE_MATERIALS_ANSWER_STATUS_DESC',
  CreativeMaterialsFileKeysAsc = 'CREATIVE_MATERIALS_FILE_KEYS_ASC',
  CreativeMaterialsFileKeysDesc = 'CREATIVE_MATERIALS_FILE_KEYS_DESC',
  CreativeMaterialsFileNamesAsc = 'CREATIVE_MATERIALS_FILE_NAMES_ASC',
  CreativeMaterialsFileNamesDesc = 'CREATIVE_MATERIALS_FILE_NAMES_DESC',
  CreativePreferredPlanAsc = 'CREATIVE_PREFERRED_PLAN_ASC',
  CreativePreferredPlanDesc = 'CREATIVE_PREFERRED_PLAN_DESC',
  CreativeRegulationAnswerStatusAsc = 'CREATIVE_REGULATION_ANSWER_STATUS_ASC',
  CreativeRegulationAnswerStatusDesc = 'CREATIVE_REGULATION_ANSWER_STATUS_DESC',
  CreativeRegulationMaterialsFilesAsc = 'CREATIVE_REGULATION_MATERIALS_FILES_ASC',
  CreativeRegulationMaterialsFilesDesc = 'CREATIVE_REGULATION_MATERIALS_FILES_DESC',
  CreativeRegulationMaterialsFileKeysAsc = 'CREATIVE_REGULATION_MATERIALS_FILE_KEYS_ASC',
  CreativeRegulationMaterialsFileKeysDesc = 'CREATIVE_REGULATION_MATERIALS_FILE_KEYS_DESC',
  CreativeRegulationUrlOrTextAsc = 'CREATIVE_REGULATION_URL_OR_TEXT_ASC',
  CreativeRegulationUrlOrTextDesc = 'CREATIVE_REGULATION_URL_OR_TEXT_DESC',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  IsCorporateAsc = 'IS_CORPORATE_ASC',
  IsCorporateDesc = 'IS_CORPORATE_DESC',
  LpExternalServiceExistenceAsc = 'LP_EXTERNAL_SERVICE_EXISTENCE_ASC',
  LpExternalServiceExistenceDesc = 'LP_EXTERNAL_SERVICE_EXISTENCE_DESC',
  Natural = 'NATURAL',
  OrderBackgroundAsc = 'ORDER_BACKGROUND_ASC',
  OrderBackgroundDesc = 'ORDER_BACKGROUND_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
  ProductIdAsc = 'PRODUCT_ID_ASC',
  ProductIdDesc = 'PRODUCT_ID_DESC',
  QualitativeGoalAsc = 'QUALITATIVE_GOAL_ASC',
  QualitativeGoalDesc = 'QUALITATIVE_GOAL_DESC',
  QualitativeIssueAsc = 'QUALITATIVE_ISSUE_ASC',
  QualitativeIssueDesc = 'QUALITATIVE_ISSUE_DESC',
  QuantitativeGoalAnswerStatusAsc = 'QUANTITATIVE_GOAL_ANSWER_STATUS_ASC',
  QuantitativeGoalAnswerStatusDesc = 'QUANTITATIVE_GOAL_ANSWER_STATUS_DESC',
  QuantitativeGoalAsc = 'QUANTITATIVE_GOAL_ASC',
  QuantitativeGoalDesc = 'QUANTITATIVE_GOAL_DESC',
  QuantitativeIssueAsc = 'QUANTITATIVE_ISSUE_ASC',
  QuantitativeIssueDesc = 'QUANTITATIVE_ISSUE_DESC',
  ReasonForChangeExistingCreativeAsc = 'REASON_FOR_CHANGE_EXISTING_CREATIVE_ASC',
  ReasonForChangeExistingCreativeDesc = 'REASON_FOR_CHANGE_EXISTING_CREATIVE_DESC',
  RequestFirstCreativeTypeAsc = 'REQUEST_FIRST_CREATIVE_TYPE_ASC',
  RequestFirstCreativeTypeDesc = 'REQUEST_FIRST_CREATIVE_TYPE_DESC',
  ResultAdAnswerStatusAsc = 'RESULT_AD_ANSWER_STATUS_ASC',
  ResultAdAnswerStatusDesc = 'RESULT_AD_ANSWER_STATUS_DESC',
  ResultAdFilesAsc = 'RESULT_AD_FILES_ASC',
  ResultAdFilesDesc = 'RESULT_AD_FILES_DESC',
  ResultAdFileKeysAsc = 'RESULT_AD_FILE_KEYS_ASC',
  ResultAdFileKeysDesc = 'RESULT_AD_FILE_KEYS_DESC',
  UpdatedAtAsc = 'UPDATED_AT_ASC',
  UpdatedAtDesc = 'UPDATED_AT_DESC'
}

/** A filter to be used against String fields. All fields are combined with a logical ‘and.’ */
export type StringFilter = {
  /** Not equal to the specified value, treating null like an ordinary value. */
  distinctFrom?: InputMaybe<Scalars['String']['input']>;
  /** Not equal to the specified value, treating null like an ordinary value (case-insensitive). */
  distinctFromInsensitive?: InputMaybe<Scalars['String']['input']>;
  /** Ends with the specified string (case-sensitive). */
  endsWith?: InputMaybe<Scalars['String']['input']>;
  /** Ends with the specified string (case-insensitive). */
  endsWithInsensitive?: InputMaybe<Scalars['String']['input']>;
  /** Equal to the specified value. */
  equalTo?: InputMaybe<Scalars['String']['input']>;
  /** Equal to the specified value (case-insensitive). */
  equalToInsensitive?: InputMaybe<Scalars['String']['input']>;
  /** Greater than the specified value. */
  greaterThan?: InputMaybe<Scalars['String']['input']>;
  /** Greater than the specified value (case-insensitive). */
  greaterThanInsensitive?: InputMaybe<Scalars['String']['input']>;
  /** Greater than or equal to the specified value. */
  greaterThanOrEqualTo?: InputMaybe<Scalars['String']['input']>;
  /** Greater than or equal to the specified value (case-insensitive). */
  greaterThanOrEqualToInsensitive?: InputMaybe<Scalars['String']['input']>;
  /** Included in the specified list. */
  in?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Included in the specified list (case-insensitive). */
  inInsensitive?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Contains the specified string (case-sensitive). */
  includes?: InputMaybe<Scalars['String']['input']>;
  /** Contains the specified string (case-insensitive). */
  includesInsensitive?: InputMaybe<Scalars['String']['input']>;
  /** Is null (if `true` is specified) or is not null (if `false` is specified). */
  isNull?: InputMaybe<Scalars['Boolean']['input']>;
  /** Less than the specified value. */
  lessThan?: InputMaybe<Scalars['String']['input']>;
  /** Less than the specified value (case-insensitive). */
  lessThanInsensitive?: InputMaybe<Scalars['String']['input']>;
  /** Less than or equal to the specified value. */
  lessThanOrEqualTo?: InputMaybe<Scalars['String']['input']>;
  /** Less than or equal to the specified value (case-insensitive). */
  lessThanOrEqualToInsensitive?: InputMaybe<Scalars['String']['input']>;
  /** Matches the specified pattern (case-sensitive). An underscore (_) matches any single character; a percent sign (%) matches any sequence of zero or more characters. */
  like?: InputMaybe<Scalars['String']['input']>;
  /** Matches the specified pattern (case-insensitive). An underscore (_) matches any single character; a percent sign (%) matches any sequence of zero or more characters. */
  likeInsensitive?: InputMaybe<Scalars['String']['input']>;
  /** Equal to the specified value, treating null like an ordinary value. */
  notDistinctFrom?: InputMaybe<Scalars['String']['input']>;
  /** Equal to the specified value, treating null like an ordinary value (case-insensitive). */
  notDistinctFromInsensitive?: InputMaybe<Scalars['String']['input']>;
  /** Does not end with the specified string (case-sensitive). */
  notEndsWith?: InputMaybe<Scalars['String']['input']>;
  /** Does not end with the specified string (case-insensitive). */
  notEndsWithInsensitive?: InputMaybe<Scalars['String']['input']>;
  /** Not equal to the specified value. */
  notEqualTo?: InputMaybe<Scalars['String']['input']>;
  /** Not equal to the specified value (case-insensitive). */
  notEqualToInsensitive?: InputMaybe<Scalars['String']['input']>;
  /** Not included in the specified list. */
  notIn?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Not included in the specified list (case-insensitive). */
  notInInsensitive?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Does not contain the specified string (case-sensitive). */
  notIncludes?: InputMaybe<Scalars['String']['input']>;
  /** Does not contain the specified string (case-insensitive). */
  notIncludesInsensitive?: InputMaybe<Scalars['String']['input']>;
  /** Does not match the specified pattern (case-sensitive). An underscore (_) matches any single character; a percent sign (%) matches any sequence of zero or more characters. */
  notLike?: InputMaybe<Scalars['String']['input']>;
  /** Does not match the specified pattern (case-insensitive). An underscore (_) matches any single character; a percent sign (%) matches any sequence of zero or more characters. */
  notLikeInsensitive?: InputMaybe<Scalars['String']['input']>;
  /** Does not start with the specified string (case-sensitive). */
  notStartsWith?: InputMaybe<Scalars['String']['input']>;
  /** Does not start with the specified string (case-insensitive). */
  notStartsWithInsensitive?: InputMaybe<Scalars['String']['input']>;
  /** Starts with the specified string (case-sensitive). */
  startsWith?: InputMaybe<Scalars['String']['input']>;
  /** Starts with the specified string (case-insensitive). */
  startsWithInsensitive?: InputMaybe<Scalars['String']['input']>;
};

/** A filter to be used against String List fields. All fields are combined with a logical ‘and.’ */
export type StringListFilter = {
  /** Any array item is equal to the specified value. */
  anyEqualTo?: InputMaybe<Scalars['String']['input']>;
  /** Any array item is greater than the specified value. */
  anyGreaterThan?: InputMaybe<Scalars['String']['input']>;
  /** Any array item is greater than or equal to the specified value. */
  anyGreaterThanOrEqualTo?: InputMaybe<Scalars['String']['input']>;
  /** Any array item is less than the specified value. */
  anyLessThan?: InputMaybe<Scalars['String']['input']>;
  /** Any array item is less than or equal to the specified value. */
  anyLessThanOrEqualTo?: InputMaybe<Scalars['String']['input']>;
  /** Any array item is not equal to the specified value. */
  anyNotEqualTo?: InputMaybe<Scalars['String']['input']>;
  /** Contained by the specified list of values. */
  containedBy?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Contains the specified list of values. */
  contains?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Not equal to the specified value, treating null like an ordinary value. */
  distinctFrom?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Equal to the specified value. */
  equalTo?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Greater than the specified value. */
  greaterThan?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Greater than or equal to the specified value. */
  greaterThanOrEqualTo?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Is null (if `true` is specified) or is not null (if `false` is specified). */
  isNull?: InputMaybe<Scalars['Boolean']['input']>;
  /** Less than the specified value. */
  lessThan?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Less than or equal to the specified value. */
  lessThanOrEqualTo?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Equal to the specified value, treating null like an ordinary value. */
  notDistinctFrom?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Not equal to the specified value. */
  notEqualTo?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Overlaps the specified list of values. */
  overlaps?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};

/** A filter to be used against UUID fields. All fields are combined with a logical ‘and.’ */
export type UuidFilter = {
  /** Not equal to the specified value, treating null like an ordinary value. */
  distinctFrom?: InputMaybe<Scalars['UUID']['input']>;
  /** Equal to the specified value. */
  equalTo?: InputMaybe<Scalars['UUID']['input']>;
  /** Greater than the specified value. */
  greaterThan?: InputMaybe<Scalars['UUID']['input']>;
  /** Greater than or equal to the specified value. */
  greaterThanOrEqualTo?: InputMaybe<Scalars['UUID']['input']>;
  /** Included in the specified list. */
  in?: InputMaybe<Array<Scalars['UUID']['input']>>;
  /** Is null (if `true` is specified) or is not null (if `false` is specified). */
  isNull?: InputMaybe<Scalars['Boolean']['input']>;
  /** Less than the specified value. */
  lessThan?: InputMaybe<Scalars['UUID']['input']>;
  /** Less than or equal to the specified value. */
  lessThanOrEqualTo?: InputMaybe<Scalars['UUID']['input']>;
  /** Equal to the specified value, treating null like an ordinary value. */
  notDistinctFrom?: InputMaybe<Scalars['UUID']['input']>;
  /** Not equal to the specified value. */
  notEqualTo?: InputMaybe<Scalars['UUID']['input']>;
  /** Not included in the specified list. */
  notIn?: InputMaybe<Array<Scalars['UUID']['input']>>;
};

/** All input for the `updateAlembicVersionByVersionNum` mutation. */
export type UpdateAlembicVersionByVersionNumInput = {
  /** An object where the defined keys will be set on the `AlembicVersion` being updated. */
  alembicVersionPatch: AlembicVersionPatch;
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  versionNum: Scalars['String']['input'];
};

/** All input for the `updateAlembicVersion` mutation. */
export type UpdateAlembicVersionInput = {
  /** An object where the defined keys will be set on the `AlembicVersion` being updated. */
  alembicVersionPatch: AlembicVersionPatch;
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `AlembicVersion` to be updated. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our update `AlembicVersion` mutation. */
export type UpdateAlembicVersionPayload = {
  __typename?: 'UpdateAlembicVersionPayload';
  /** The `AlembicVersion` that was updated by this mutation. */
  alembicVersion?: Maybe<AlembicVersion>;
  /** An edge for our `AlembicVersion`. May be used by Relay 1. */
  alembicVersionEdge?: Maybe<AlembicVersionsEdge>;
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our update `AlembicVersion` mutation. */
export type UpdateAlembicVersionPayloadAlembicVersionEdgeArgs = {
  orderBy?: InputMaybe<Array<AlembicVersionsOrderBy>>;
};

/** All input for the `updateCompanyById` mutation. */
export type UpdateCompanyByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** An object where the defined keys will be set on the `Company` being updated. */
  companyPatch: CompanyPatch;
  /** 企業ID */
  id: Scalars['UUID']['input'];
};

/** All input for the `updateCompanyInfoById` mutation. */
export type UpdateCompanyInfoByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** An object where the defined keys will be set on the `CompanyInfo` being updated. */
  companyInfoPatch: CompanyInfoPatch;
  /** 契約情報/企業情報ID */
  id: Scalars['UUID']['input'];
};

/** All input for the `updateCompanyInfo` mutation. */
export type UpdateCompanyInfoInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** An object where the defined keys will be set on the `CompanyInfo` being updated. */
  companyInfoPatch: CompanyInfoPatch;
  /** The globally unique `ID` which will identify a single `CompanyInfo` to be updated. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our update `CompanyInfo` mutation. */
export type UpdateCompanyInfoPayload = {
  __typename?: 'UpdateCompanyInfoPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Company` that is related to this `CompanyInfo`. */
  companyByCompanyId?: Maybe<Company>;
  /** The `CompanyInfo` that was updated by this mutation. */
  companyInfo?: Maybe<CompanyInfo>;
  /** An edge for our `CompanyInfo`. May be used by Relay 1. */
  companyInfoEdge?: Maybe<CompanyInfosEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our update `CompanyInfo` mutation. */
export type UpdateCompanyInfoPayloadCompanyInfoEdgeArgs = {
  orderBy?: InputMaybe<Array<CompanyInfosOrderBy>>;
};

/** All input for the `updateCompany` mutation. */
export type UpdateCompanyInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** An object where the defined keys will be set on the `Company` being updated. */
  companyPatch: CompanyPatch;
  /** The globally unique `ID` which will identify a single `Company` to be updated. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our update `Company` mutation. */
export type UpdateCompanyPayload = {
  __typename?: 'UpdateCompanyPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `Company` that was updated by this mutation. */
  company?: Maybe<Company>;
  /** An edge for our `Company`. May be used by Relay 1. */
  companyEdge?: Maybe<CompaniesEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our update `Company` mutation. */
export type UpdateCompanyPayloadCompanyEdgeArgs = {
  orderBy?: InputMaybe<Array<CompaniesOrderBy>>;
};

/** All input for the `updateCustomerGroupById` mutation. */
export type UpdateCustomerGroupByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** An object where the defined keys will be set on the `CustomerGroup` being updated. */
  customerGroupPatch: CustomerGroupPatch;
  /** 顧客グループID */
  id: Scalars['Int']['input'];
};

/** All input for the `updateCustomerGroupByOpportunityIdAndSfContactId` mutation. */
export type UpdateCustomerGroupByOpportunityIdAndSfContactIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** An object where the defined keys will be set on the `CustomerGroup` being updated. */
  customerGroupPatch: CustomerGroupPatch;
  /** 商談ID */
  opportunityId: Scalars['UUID']['input'];
  /** SalesforceユーザID */
  sfContactId: Scalars['String']['input'];
};

/** All input for the `updateCustomerGroup` mutation. */
export type UpdateCustomerGroupInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** An object where the defined keys will be set on the `CustomerGroup` being updated. */
  customerGroupPatch: CustomerGroupPatch;
  /** The globally unique `ID` which will identify a single `CustomerGroup` to be updated. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our update `CustomerGroup` mutation. */
export type UpdateCustomerGroupPayload = {
  __typename?: 'UpdateCustomerGroupPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `CustomerGroup` that was updated by this mutation. */
  customerGroup?: Maybe<CustomerGroup>;
  /** An edge for our `CustomerGroup`. May be used by Relay 1. */
  customerGroupEdge?: Maybe<CustomerGroupsEdge>;
  /** Reads a single `Opportunity` that is related to this `CustomerGroup`. */
  opportunityByOpportunityId?: Maybe<Opportunity>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our update `CustomerGroup` mutation. */
export type UpdateCustomerGroupPayloadCustomerGroupEdgeArgs = {
  orderBy?: InputMaybe<Array<CustomerGroupsOrderBy>>;
};

/** All input for the `updateMarketingActivityInformationById` mutation. */
export type UpdateMarketingActivityInformationByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** マーケティング活動情報ID */
  id: Scalars['UUID']['input'];
  /** An object where the defined keys will be set on the `MarketingActivityInformation` being updated. */
  marketingActivityInformationPatch: MarketingActivityInformationPatch;
};

/** All input for the `updateMarketingActivityInformation` mutation. */
export type UpdateMarketingActivityInformationInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** An object where the defined keys will be set on the `MarketingActivityInformation` being updated. */
  marketingActivityInformationPatch: MarketingActivityInformationPatch;
  /** The globally unique `ID` which will identify a single `MarketingActivityInformation` to be updated. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our update `MarketingActivityInformation` mutation. */
export type UpdateMarketingActivityInformationPayload = {
  __typename?: 'UpdateMarketingActivityInformationPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `MarketingActivityInformation` that was updated by this mutation. */
  marketingActivityInformation?: Maybe<MarketingActivityInformation>;
  /** An edge for our `MarketingActivityInformation`. May be used by Relay 1. */
  marketingActivityInformationEdge?: Maybe<MarketingActivityInformationsEdge>;
  /** Reads a single `Product` that is related to this `MarketingActivityInformation`. */
  productByProductId?: Maybe<Product>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our update `MarketingActivityInformation` mutation. */
export type UpdateMarketingActivityInformationPayloadMarketingActivityInformationEdgeArgs = {
  orderBy?: InputMaybe<Array<MarketingActivityInformationsOrderBy>>;
};

/** All input for the `updateNotificationById` mutation. */
export type UpdateNotificationByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** お知らせID */
  id: Scalars['UUID']['input'];
  /** An object where the defined keys will be set on the `Notification` being updated. */
  notificationPatch: NotificationPatch;
};

/** All input for the `updateNotification` mutation. */
export type UpdateNotificationInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `Notification` to be updated. */
  nodeId: Scalars['ID']['input'];
  /** An object where the defined keys will be set on the `Notification` being updated. */
  notificationPatch: NotificationPatch;
};

/** The output of our update `Notification` mutation. */
export type UpdateNotificationPayload = {
  __typename?: 'UpdateNotificationPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `Notification` that was updated by this mutation. */
  notification?: Maybe<Notification>;
  /** An edge for our `Notification`. May be used by Relay 1. */
  notificationEdge?: Maybe<NotificationsEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our update `Notification` mutation. */
export type UpdateNotificationPayloadNotificationEdgeArgs = {
  orderBy?: InputMaybe<Array<NotificationsOrderBy>>;
};

/** All input for the `updateOperatorGroupById` mutation. */
export type UpdateOperatorGroupByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** 管理者グループID */
  id: Scalars['Int']['input'];
  /** An object where the defined keys will be set on the `OperatorGroup` being updated. */
  operatorGroupPatch: OperatorGroupPatch;
};

/** All input for the `updateOperatorGroup` mutation. */
export type UpdateOperatorGroupInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `OperatorGroup` to be updated. */
  nodeId: Scalars['ID']['input'];
  /** An object where the defined keys will be set on the `OperatorGroup` being updated. */
  operatorGroupPatch: OperatorGroupPatch;
};

/** The output of our update `OperatorGroup` mutation. */
export type UpdateOperatorGroupPayload = {
  __typename?: 'UpdateOperatorGroupPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `OperatorGroup` that was updated by this mutation. */
  operatorGroup?: Maybe<OperatorGroup>;
  /** An edge for our `OperatorGroup`. May be used by Relay 1. */
  operatorGroupEdge?: Maybe<OperatorGroupsEdge>;
  /** Reads a single `Opportunity` that is related to this `OperatorGroup`. */
  opportunityByOpportunityId?: Maybe<Opportunity>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our update `OperatorGroup` mutation. */
export type UpdateOperatorGroupPayloadOperatorGroupEdgeArgs = {
  orderBy?: InputMaybe<Array<OperatorGroupsOrderBy>>;
};

/** All input for the `updateOperatorNameByEmail` mutation. */
export type UpdateOperatorNameByEmailInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  email: Scalars['String']['input'];
  /** An object where the defined keys will be set on the `OperatorName` being updated. */
  operatorNamePatch: OperatorNamePatch;
};

/** All input for the `updateOperatorName` mutation. */
export type UpdateOperatorNameInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `OperatorName` to be updated. */
  nodeId: Scalars['ID']['input'];
  /** An object where the defined keys will be set on the `OperatorName` being updated. */
  operatorNamePatch: OperatorNamePatch;
};

/** The output of our update `OperatorName` mutation. */
export type UpdateOperatorNamePayload = {
  __typename?: 'UpdateOperatorNamePayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `OperatorName` that was updated by this mutation. */
  operatorName?: Maybe<OperatorName>;
  /** An edge for our `OperatorName`. May be used by Relay 1. */
  operatorNameEdge?: Maybe<OperatorNamesEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our update `OperatorName` mutation. */
export type UpdateOperatorNamePayloadOperatorNameEdgeArgs = {
  orderBy?: InputMaybe<Array<OperatorNamesOrderBy>>;
};

/** All input for the `updateOpportunityById` mutation. */
export type UpdateOpportunityByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** 商談ID */
  id: Scalars['UUID']['input'];
  /** An object where the defined keys will be set on the `Opportunity` being updated. */
  opportunityPatch: OpportunityPatch;
};

/** All input for the `updateOpportunity` mutation. */
export type UpdateOpportunityInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `Opportunity` to be updated. */
  nodeId: Scalars['ID']['input'];
  /** An object where the defined keys will be set on the `Opportunity` being updated. */
  opportunityPatch: OpportunityPatch;
};

/** The output of our update `Opportunity` mutation. */
export type UpdateOpportunityPayload = {
  __typename?: 'UpdateOpportunityPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `Opportunity` that was updated by this mutation. */
  opportunity?: Maybe<Opportunity>;
  /** An edge for our `Opportunity`. May be used by Relay 1. */
  opportunityEdge?: Maybe<OpportunitiesEdge>;
  /** Reads a single `Product` that is related to this `Opportunity`. */
  productByProductId?: Maybe<Product>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our update `Opportunity` mutation. */
export type UpdateOpportunityPayloadOpportunityEdgeArgs = {
  orderBy?: InputMaybe<Array<OpportunitiesOrderBy>>;
};

/** All input for the `updateProductByCompanyIdAndProductName` mutation. */
export type UpdateProductByCompanyIdAndProductNameInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** 企業ID */
  companyId: Scalars['UUID']['input'];
  /** 商材名 */
  productName: Scalars['String']['input'];
  /** An object where the defined keys will be set on the `Product` being updated. */
  productPatch: ProductPatch;
};

/** All input for the `updateProductById` mutation. */
export type UpdateProductByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** 商材ID */
  id: Scalars['UUID']['input'];
  /** An object where the defined keys will be set on the `Product` being updated. */
  productPatch: ProductPatch;
};

/** All input for the `updateProductInformationById` mutation. */
export type UpdateProductInformationByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** 商材情報ID */
  id: Scalars['UUID']['input'];
  /** An object where the defined keys will be set on the `ProductInformation` being updated. */
  productInformationPatch: ProductInformationPatch;
};

/** All input for the `updateProductInformation` mutation. */
export type UpdateProductInformationInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `ProductInformation` to be updated. */
  nodeId: Scalars['ID']['input'];
  /** An object where the defined keys will be set on the `ProductInformation` being updated. */
  productInformationPatch: ProductInformationPatch;
};

/** The output of our update `ProductInformation` mutation. */
export type UpdateProductInformationPayload = {
  __typename?: 'UpdateProductInformationPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Product` that is related to this `ProductInformation`. */
  productByProductId?: Maybe<Product>;
  /** The `ProductInformation` that was updated by this mutation. */
  productInformation?: Maybe<ProductInformation>;
  /** An edge for our `ProductInformation`. May be used by Relay 1. */
  productInformationEdge?: Maybe<ProductInformationsEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our update `ProductInformation` mutation. */
export type UpdateProductInformationPayloadProductInformationEdgeArgs = {
  orderBy?: InputMaybe<Array<ProductInformationsOrderBy>>;
};

/** All input for the `updateProduct` mutation. */
export type UpdateProductInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `Product` to be updated. */
  nodeId: Scalars['ID']['input'];
  /** An object where the defined keys will be set on the `Product` being updated. */
  productPatch: ProductPatch;
};

/** The output of our update `Product` mutation. */
export type UpdateProductPayload = {
  __typename?: 'UpdateProductPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Company` that is related to this `Product`. */
  companyByCompanyId?: Maybe<Company>;
  /** The `Product` that was updated by this mutation. */
  product?: Maybe<Product>;
  /** An edge for our `Product`. May be used by Relay 1. */
  productEdge?: Maybe<ProductsEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our update `Product` mutation. */
export type UpdateProductPayloadProductEdgeArgs = {
  orderBy?: InputMaybe<Array<ProductsOrderBy>>;
};

/** All input for the `updateRequestContentById` mutation. */
export type UpdateRequestContentByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** ご依頼内容ID */
  id: Scalars['UUID']['input'];
  /** An object where the defined keys will be set on the `RequestContent` being updated. */
  requestContentPatch: RequestContentPatch;
};

/** All input for the `updateRequestContent` mutation. */
export type UpdateRequestContentInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `RequestContent` to be updated. */
  nodeId: Scalars['ID']['input'];
  /** An object where the defined keys will be set on the `RequestContent` being updated. */
  requestContentPatch: RequestContentPatch;
};

/** The output of our update `RequestContent` mutation. */
export type UpdateRequestContentPayload = {
  __typename?: 'UpdateRequestContentPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Product` that is related to this `RequestContent`. */
  productByProductId?: Maybe<Product>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** The `RequestContent` that was updated by this mutation. */
  requestContent?: Maybe<RequestContent>;
  /** An edge for our `RequestContent`. May be used by Relay 1. */
  requestContentEdge?: Maybe<RequestContentsEdge>;
};


/** The output of our update `RequestContent` mutation. */
export type UpdateRequestContentPayloadRequestContentEdgeArgs = {
  orderBy?: InputMaybe<Array<RequestContentsOrderBy>>;
};

/** All input for the `updateUserInformationMemoById` mutation. */
export type UpdateUserInformationMemoByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** ご依頼内容ID */
  id: Scalars['UUID']['input'];
  /** An object where the defined keys will be set on the `UserInformationMemo` being updated. */
  userInformationMemoPatch: UserInformationMemoPatch;
};

/** All input for the `updateUserInformationMemo` mutation. */
export type UpdateUserInformationMemoInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `UserInformationMemo` to be updated. */
  nodeId: Scalars['ID']['input'];
  /** An object where the defined keys will be set on the `UserInformationMemo` being updated. */
  userInformationMemoPatch: UserInformationMemoPatch;
};

/** The output of our update `UserInformationMemo` mutation. */
export type UpdateUserInformationMemoPayload = {
  __typename?: 'UpdateUserInformationMemoPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Product` that is related to this `UserInformationMemo`. */
  productByProductId?: Maybe<Product>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** The `UserInformationMemo` that was updated by this mutation. */
  userInformationMemo?: Maybe<UserInformationMemo>;
  /** An edge for our `UserInformationMemo`. May be used by Relay 1. */
  userInformationMemoEdge?: Maybe<UserInformationMemosEdge>;
};


/** The output of our update `UserInformationMemo` mutation. */
export type UpdateUserInformationMemoPayloadUserInformationMemoEdgeArgs = {
  orderBy?: InputMaybe<Array<UserInformationMemosOrderBy>>;
};

/** メモPkey */
export type UserInformationMemo = Node & {
  __typename?: 'UserInformationMemo';
  adminMemo?: Maybe<Scalars['String']['output']>;
  createdAt?: Maybe<Scalars['Datetime']['output']>;
  /** ご依頼内容ID */
  id: Scalars['UUID']['output'];
  memo?: Maybe<Scalars['String']['output']>;
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID']['output'];
  /** Reads a single `Product` that is related to this `UserInformationMemo`. */
  productByProductId?: Maybe<Product>;
  /** プロダクトID */
  productId?: Maybe<Scalars['UUID']['output']>;
  updatedAt?: Maybe<Scalars['Datetime']['output']>;
};

/**
 * A condition to be used against `UserInformationMemo` object types. All fields
 * are tested for equality and combined with a logical ‘and.’
 */
export type UserInformationMemoCondition = {
  /** Checks for equality with the object’s `adminMemo` field. */
  adminMemo?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `createdAt` field. */
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** Checks for equality with the object’s `id` field. */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `memo` field. */
  memo?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `productId` field. */
  productId?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** A filter to be used against `UserInformationMemo` object types. All fields are combined with a logical ‘and.’ */
export type UserInformationMemoFilter = {
  /** Filter by the object’s `adminMemo` field. */
  adminMemo?: InputMaybe<StringFilter>;
  /** Checks for all expressions in this list. */
  and?: InputMaybe<Array<UserInformationMemoFilter>>;
  /** Filter by the object’s `createdAt` field. */
  createdAt?: InputMaybe<DatetimeFilter>;
  /** Filter by the object’s `id` field. */
  id?: InputMaybe<UuidFilter>;
  /** Filter by the object’s `memo` field. */
  memo?: InputMaybe<StringFilter>;
  /** Negates the expression. */
  not?: InputMaybe<UserInformationMemoFilter>;
  /** Checks for any expressions in this list. */
  or?: InputMaybe<Array<UserInformationMemoFilter>>;
  /** Filter by the object’s `productId` field. */
  productId?: InputMaybe<UuidFilter>;
  /** Filter by the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<DatetimeFilter>;
};

/** An input for mutations affecting `UserInformationMemo` */
export type UserInformationMemoInput = {
  adminMemo?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** ご依頼内容ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  memo?: InputMaybe<Scalars['String']['input']>;
  /** プロダクトID */
  productId?: InputMaybe<Scalars['UUID']['input']>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** Represents an update to a `UserInformationMemo`. Fields that are set will be updated. */
export type UserInformationMemoPatch = {
  adminMemo?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** ご依頼内容ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  memo?: InputMaybe<Scalars['String']['input']>;
  /** プロダクトID */
  productId?: InputMaybe<Scalars['UUID']['input']>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** A connection to a list of `UserInformationMemo` values. */
export type UserInformationMemosConnection = {
  __typename?: 'UserInformationMemosConnection';
  /** A list of edges which contains the `UserInformationMemo` and cursor to aid in pagination. */
  edges: Array<UserInformationMemosEdge>;
  /** A list of `UserInformationMemo` objects. */
  nodes: Array<Maybe<UserInformationMemo>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `UserInformationMemo` you could get from the connection. */
  totalCount: Scalars['Int']['output'];
};

/** A `UserInformationMemo` edge in the connection. */
export type UserInformationMemosEdge = {
  __typename?: 'UserInformationMemosEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']['output']>;
  /** The `UserInformationMemo` at the end of the edge. */
  node?: Maybe<UserInformationMemo>;
};

/** Methods to use when ordering `UserInformationMemo`. */
export enum UserInformationMemosOrderBy {
  AdminMemoAsc = 'ADMIN_MEMO_ASC',
  AdminMemoDesc = 'ADMIN_MEMO_DESC',
  CreatedAtAsc = 'CREATED_AT_ASC',
  CreatedAtDesc = 'CREATED_AT_DESC',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  MemoAsc = 'MEMO_ASC',
  MemoDesc = 'MEMO_DESC',
  Natural = 'NATURAL',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
  ProductIdAsc = 'PRODUCT_ID_ASC',
  ProductIdDesc = 'PRODUCT_ID_DESC',
  UpdatedAtAsc = 'UPDATED_AT_ASC',
  UpdatedAtDesc = 'UPDATED_AT_DESC'
}



export type SdkFunctionWrapper = <T>(action: (requestHeaders?:Record<string, string>) => Promise<T>, operationName: string, operationType?: string) => Promise<T>;


const defaultWrapper: SdkFunctionWrapper = (action, _operationName, _operationType) => action();

export function getSdk(client: GraphQLClient, withWrapper: SdkFunctionWrapper = defaultWrapper) {
  return {

  };
}
export type Sdk = ReturnType<typeof getSdk>;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string; }
  String: { input: string; output: string; }
  Boolean: { input: boolean; output: boolean; }
  Int: { input: number; output: number; }
  Float: { input: number; output: number; }
  /** A location in a connection that can be used for resuming pagination. */
  Cursor: { input: any; output: any; }
  /**
   * A point in time as described by the [ISO
   * 8601](https://en.wikipedia.org/wiki/ISO_8601) standard. May or may not include a timezone.
   */
  Datetime: { input: any; output: any; }
  /** A universally unique identifier as defined by [RFC 4122](https://tools.ietf.org/html/rfc4122). */
  UUID: { input: any; output: any; }
};

export type AlembicVersion = Node & {
  __typename?: 'AlembicVersion';
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID']['output'];
  versionNum: Scalars['String']['output'];
};

/**
 * A condition to be used against `AlembicVersion` object types. All fields are
 * tested for equality and combined with a logical ‘and.’
 */
export type AlembicVersionCondition = {
  /** Checks for equality with the object’s `versionNum` field. */
  versionNum?: InputMaybe<Scalars['String']['input']>;
};

/** A filter to be used against `AlembicVersion` object types. All fields are combined with a logical ‘and.’ */
export type AlembicVersionFilter = {
  /** Checks for all expressions in this list. */
  and?: InputMaybe<Array<AlembicVersionFilter>>;
  /** Negates the expression. */
  not?: InputMaybe<AlembicVersionFilter>;
  /** Checks for any expressions in this list. */
  or?: InputMaybe<Array<AlembicVersionFilter>>;
  /** Filter by the object’s `versionNum` field. */
  versionNum?: InputMaybe<StringFilter>;
};

/** An input for mutations affecting `AlembicVersion` */
export type AlembicVersionInput = {
  versionNum: Scalars['String']['input'];
};

/** Represents an update to a `AlembicVersion`. Fields that are set will be updated. */
export type AlembicVersionPatch = {
  versionNum?: InputMaybe<Scalars['String']['input']>;
};

/** A connection to a list of `AlembicVersion` values. */
export type AlembicVersionsConnection = {
  __typename?: 'AlembicVersionsConnection';
  /** A list of edges which contains the `AlembicVersion` and cursor to aid in pagination. */
  edges: Array<AlembicVersionsEdge>;
  /** A list of `AlembicVersion` objects. */
  nodes: Array<Maybe<AlembicVersion>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `AlembicVersion` you could get from the connection. */
  totalCount: Scalars['Int']['output'];
};

/** A `AlembicVersion` edge in the connection. */
export type AlembicVersionsEdge = {
  __typename?: 'AlembicVersionsEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']['output']>;
  /** The `AlembicVersion` at the end of the edge. */
  node?: Maybe<AlembicVersion>;
};

/** Methods to use when ordering `AlembicVersion`. */
export enum AlembicVersionsOrderBy {
  Natural = 'NATURAL',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
  VersionNumAsc = 'VERSION_NUM_ASC',
  VersionNumDesc = 'VERSION_NUM_DESC'
}

/** A filter to be used against Boolean fields. All fields are combined with a logical ‘and.’ */
export type BooleanFilter = {
  /** Not equal to the specified value, treating null like an ordinary value. */
  distinctFrom?: InputMaybe<Scalars['Boolean']['input']>;
  /** Equal to the specified value. */
  equalTo?: InputMaybe<Scalars['Boolean']['input']>;
  /** Greater than the specified value. */
  greaterThan?: InputMaybe<Scalars['Boolean']['input']>;
  /** Greater than or equal to the specified value. */
  greaterThanOrEqualTo?: InputMaybe<Scalars['Boolean']['input']>;
  /** Included in the specified list. */
  in?: InputMaybe<Array<Scalars['Boolean']['input']>>;
  /** Is null (if `true` is specified) or is not null (if `false` is specified). */
  isNull?: InputMaybe<Scalars['Boolean']['input']>;
  /** Less than the specified value. */
  lessThan?: InputMaybe<Scalars['Boolean']['input']>;
  /** Less than or equal to the specified value. */
  lessThanOrEqualTo?: InputMaybe<Scalars['Boolean']['input']>;
  /** Equal to the specified value, treating null like an ordinary value. */
  notDistinctFrom?: InputMaybe<Scalars['Boolean']['input']>;
  /** Not equal to the specified value. */
  notEqualTo?: InputMaybe<Scalars['Boolean']['input']>;
  /** Not included in the specified list. */
  notIn?: InputMaybe<Array<Scalars['Boolean']['input']>>;
};

/** A connection to a list of `Company` values. */
export type CompaniesConnection = {
  __typename?: 'CompaniesConnection';
  /** A list of edges which contains the `Company` and cursor to aid in pagination. */
  edges: Array<CompaniesEdge>;
  /** A list of `Company` objects. */
  nodes: Array<Maybe<Company>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `Company` you could get from the connection. */
  totalCount: Scalars['Int']['output'];
};

/** A `Company` edge in the connection. */
export type CompaniesEdge = {
  __typename?: 'CompaniesEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']['output']>;
  /** The `Company` at the end of the edge. */
  node?: Maybe<Company>;
};

/** Methods to use when ordering `Company`. */
export enum CompaniesOrderBy {
  CreatedAtAsc = 'CREATED_AT_ASC',
  CreatedAtDesc = 'CREATED_AT_DESC',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  Natural = 'NATURAL',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
  SfAccountIdAsc = 'SF_ACCOUNT_ID_ASC',
  SfAccountIdDesc = 'SF_ACCOUNT_ID_DESC',
  UpdatedAtAsc = 'UPDATED_AT_ASC',
  UpdatedAtDesc = 'UPDATED_AT_DESC'
}

/** 企業テーブル */
export type Company = Node & {
  __typename?: 'Company';
  /** Reads and enables pagination through a set of `CompanyInfo`. */
  companyInfosByCompanyId: CompanyInfosConnection;
  createdAt?: Maybe<Scalars['Datetime']['output']>;
  /** 企業ID */
  id: Scalars['UUID']['output'];
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID']['output'];
  /** Reads and enables pagination through a set of `Product`. */
  productsByCompanyId: ProductsConnection;
  /** Salesforce企業ID */
  sfAccountId?: Maybe<Scalars['String']['output']>;
  updatedAt?: Maybe<Scalars['Datetime']['output']>;
};


/** 企業テーブル */
export type CompanyCompanyInfosByCompanyIdArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<CompanyInfoCondition>;
  filter?: InputMaybe<CompanyInfoFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CompanyInfosOrderBy>>;
};


/** 企業テーブル */
export type CompanyProductsByCompanyIdArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<ProductCondition>;
  filter?: InputMaybe<ProductFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<ProductsOrderBy>>;
};

/** A condition to be used against `Company` object types. All fields are tested for equality and combined with a logical ‘and.’ */
export type CompanyCondition = {
  /** Checks for equality with the object’s `createdAt` field. */
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** Checks for equality with the object’s `id` field. */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `sfAccountId` field. */
  sfAccountId?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** A filter to be used against `Company` object types. All fields are combined with a logical ‘and.’ */
export type CompanyFilter = {
  /** Checks for all expressions in this list. */
  and?: InputMaybe<Array<CompanyFilter>>;
  /** Filter by the object’s `createdAt` field. */
  createdAt?: InputMaybe<DatetimeFilter>;
  /** Filter by the object’s `id` field. */
  id?: InputMaybe<UuidFilter>;
  /** Negates the expression. */
  not?: InputMaybe<CompanyFilter>;
  /** Checks for any expressions in this list. */
  or?: InputMaybe<Array<CompanyFilter>>;
  /** Filter by the object’s `sfAccountId` field. */
  sfAccountId?: InputMaybe<StringFilter>;
  /** Filter by the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<DatetimeFilter>;
};

/** 契約情報/企業情報ID */
export type CompanyInfo = Node & {
  __typename?: 'CompanyInfo';
  address?: Maybe<Scalars['String']['output']>;
  addressBill?: Maybe<Scalars['String']['output']>;
  adsOperationManager?: Maybe<Scalars['String']['output']>;
  adsOperationStaff?: Maybe<Scalars['String']['output']>;
  approver?: Maybe<Scalars['String']['output']>;
  billingMethodProblem?: Maybe<Scalars['Boolean']['output']>;
  billingMethodProblemText?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Company` that is related to this `CompanyInfo`. */
  companyByCompanyId?: Maybe<Company>;
  /** 企業ID */
  companyId?: Maybe<Scalars['UUID']['output']>;
  corporateName?: Maybe<Scalars['String']['output']>;
  corporateNameBill?: Maybe<Scalars['String']['output']>;
  corporateNameKana?: Maybe<Scalars['String']['output']>;
  corporateNameKanaBill?: Maybe<Scalars['String']['output']>;
  createdAt?: Maybe<Scalars['Datetime']['output']>;
  creativeManager?: Maybe<Scalars['String']['output']>;
  departmentName?: Maybe<Scalars['String']['output']>;
  departmentNameBill?: Maybe<Scalars['String']['output']>;
  /** 契約情報/企業情報ID */
  id: Scalars['UUID']['output'];
  mobileNumber?: Maybe<Scalars['String']['output']>;
  mobileNumberBill?: Maybe<Scalars['String']['output']>;
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID']['output'];
  personInChargeEmail?: Maybe<Scalars['String']['output']>;
  personInChargeEmailBill?: Maybe<Scalars['String']['output']>;
  personInChargeName?: Maybe<Scalars['String']['output']>;
  personInChargeNameBill?: Maybe<Scalars['String']['output']>;
  postCode?: Maybe<Scalars['String']['output']>;
  postCodeBill?: Maybe<Scalars['String']['output']>;
  productNames?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  productPriorities?: Maybe<Array<Maybe<Scalars['Int']['output']>>>;
  projectManager?: Maybe<Scalars['String']['output']>;
  sendOriginalMail?: Maybe<Scalars['Boolean']['output']>;
  updatedAt?: Maybe<Scalars['Datetime']['output']>;
};

/**
 * A condition to be used against `CompanyInfo` object types. All fields are tested
 * for equality and combined with a logical ‘and.’
 */
export type CompanyInfoCondition = {
  /** Checks for equality with the object’s `address` field. */
  address?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `addressBill` field. */
  addressBill?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adsOperationManager` field. */
  adsOperationManager?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adsOperationStaff` field. */
  adsOperationStaff?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `approver` field. */
  approver?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `billingMethodProblem` field. */
  billingMethodProblem?: InputMaybe<Scalars['Boolean']['input']>;
  /** Checks for equality with the object’s `billingMethodProblemText` field. */
  billingMethodProblemText?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `companyId` field. */
  companyId?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `corporateName` field. */
  corporateName?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `corporateNameBill` field. */
  corporateNameBill?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `corporateNameKana` field. */
  corporateNameKana?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `corporateNameKanaBill` field. */
  corporateNameKanaBill?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `createdAt` field. */
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** Checks for equality with the object’s `creativeManager` field. */
  creativeManager?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `departmentName` field. */
  departmentName?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `departmentNameBill` field. */
  departmentNameBill?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `id` field. */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `mobileNumber` field. */
  mobileNumber?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `mobileNumberBill` field. */
  mobileNumberBill?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `personInChargeEmail` field. */
  personInChargeEmail?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `personInChargeEmailBill` field. */
  personInChargeEmailBill?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `personInChargeName` field. */
  personInChargeName?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `personInChargeNameBill` field. */
  personInChargeNameBill?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `postCode` field. */
  postCode?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `postCodeBill` field. */
  postCodeBill?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `productNames` field. */
  productNames?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Checks for equality with the object’s `productPriorities` field. */
  productPriorities?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  /** Checks for equality with the object’s `projectManager` field. */
  projectManager?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `sendOriginalMail` field. */
  sendOriginalMail?: InputMaybe<Scalars['Boolean']['input']>;
  /** Checks for equality with the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** A filter to be used against `CompanyInfo` object types. All fields are combined with a logical ‘and.’ */
export type CompanyInfoFilter = {
  /** Filter by the object’s `address` field. */
  address?: InputMaybe<StringFilter>;
  /** Filter by the object’s `addressBill` field. */
  addressBill?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adsOperationManager` field. */
  adsOperationManager?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adsOperationStaff` field. */
  adsOperationStaff?: InputMaybe<StringFilter>;
  /** Checks for all expressions in this list. */
  and?: InputMaybe<Array<CompanyInfoFilter>>;
  /** Filter by the object’s `approver` field. */
  approver?: InputMaybe<StringFilter>;
  /** Filter by the object’s `billingMethodProblem` field. */
  billingMethodProblem?: InputMaybe<BooleanFilter>;
  /** Filter by the object’s `billingMethodProblemText` field. */
  billingMethodProblemText?: InputMaybe<StringFilter>;
  /** Filter by the object’s `companyId` field. */
  companyId?: InputMaybe<UuidFilter>;
  /** Filter by the object’s `corporateName` field. */
  corporateName?: InputMaybe<StringFilter>;
  /** Filter by the object’s `corporateNameBill` field. */
  corporateNameBill?: InputMaybe<StringFilter>;
  /** Filter by the object’s `corporateNameKana` field. */
  corporateNameKana?: InputMaybe<StringFilter>;
  /** Filter by the object’s `corporateNameKanaBill` field. */
  corporateNameKanaBill?: InputMaybe<StringFilter>;
  /** Filter by the object’s `createdAt` field. */
  createdAt?: InputMaybe<DatetimeFilter>;
  /** Filter by the object’s `creativeManager` field. */
  creativeManager?: InputMaybe<StringFilter>;
  /** Filter by the object’s `departmentName` field. */
  departmentName?: InputMaybe<StringFilter>;
  /** Filter by the object’s `departmentNameBill` field. */
  departmentNameBill?: InputMaybe<StringFilter>;
  /** Filter by the object’s `id` field. */
  id?: InputMaybe<UuidFilter>;
  /** Filter by the object’s `mobileNumber` field. */
  mobileNumber?: InputMaybe<StringFilter>;
  /** Filter by the object’s `mobileNumberBill` field. */
  mobileNumberBill?: InputMaybe<StringFilter>;
  /** Negates the expression. */
  not?: InputMaybe<CompanyInfoFilter>;
  /** Checks for any expressions in this list. */
  or?: InputMaybe<Array<CompanyInfoFilter>>;
  /** Filter by the object’s `personInChargeEmail` field. */
  personInChargeEmail?: InputMaybe<StringFilter>;
  /** Filter by the object’s `personInChargeEmailBill` field. */
  personInChargeEmailBill?: InputMaybe<StringFilter>;
  /** Filter by the object’s `personInChargeName` field. */
  personInChargeName?: InputMaybe<StringFilter>;
  /** Filter by the object’s `personInChargeNameBill` field. */
  personInChargeNameBill?: InputMaybe<StringFilter>;
  /** Filter by the object’s `postCode` field. */
  postCode?: InputMaybe<StringFilter>;
  /** Filter by the object’s `postCodeBill` field. */
  postCodeBill?: InputMaybe<StringFilter>;
  /** Filter by the object’s `productNames` field. */
  productNames?: InputMaybe<StringListFilter>;
  /** Filter by the object’s `productPriorities` field. */
  productPriorities?: InputMaybe<IntListFilter>;
  /** Filter by the object’s `projectManager` field. */
  projectManager?: InputMaybe<StringFilter>;
  /** Filter by the object’s `sendOriginalMail` field. */
  sendOriginalMail?: InputMaybe<BooleanFilter>;
  /** Filter by the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<DatetimeFilter>;
};

/** An input for mutations affecting `CompanyInfo` */
export type CompanyInfoInput = {
  address?: InputMaybe<Scalars['String']['input']>;
  addressBill?: InputMaybe<Scalars['String']['input']>;
  adsOperationManager?: InputMaybe<Scalars['String']['input']>;
  adsOperationStaff?: InputMaybe<Scalars['String']['input']>;
  approver?: InputMaybe<Scalars['String']['input']>;
  billingMethodProblem?: InputMaybe<Scalars['Boolean']['input']>;
  billingMethodProblemText?: InputMaybe<Scalars['String']['input']>;
  /** 企業ID */
  companyId?: InputMaybe<Scalars['UUID']['input']>;
  corporateName?: InputMaybe<Scalars['String']['input']>;
  corporateNameBill?: InputMaybe<Scalars['String']['input']>;
  corporateNameKana?: InputMaybe<Scalars['String']['input']>;
  corporateNameKanaBill?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  creativeManager?: InputMaybe<Scalars['String']['input']>;
  departmentName?: InputMaybe<Scalars['String']['input']>;
  departmentNameBill?: InputMaybe<Scalars['String']['input']>;
  /** 契約情報/企業情報ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  mobileNumber?: InputMaybe<Scalars['String']['input']>;
  mobileNumberBill?: InputMaybe<Scalars['String']['input']>;
  personInChargeEmail?: InputMaybe<Scalars['String']['input']>;
  personInChargeEmailBill?: InputMaybe<Scalars['String']['input']>;
  personInChargeName?: InputMaybe<Scalars['String']['input']>;
  personInChargeNameBill?: InputMaybe<Scalars['String']['input']>;
  postCode?: InputMaybe<Scalars['String']['input']>;
  postCodeBill?: InputMaybe<Scalars['String']['input']>;
  productNames?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  productPriorities?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  projectManager?: InputMaybe<Scalars['String']['input']>;
  sendOriginalMail?: InputMaybe<Scalars['Boolean']['input']>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** Represents an update to a `CompanyInfo`. Fields that are set will be updated. */
export type CompanyInfoPatch = {
  address?: InputMaybe<Scalars['String']['input']>;
  addressBill?: InputMaybe<Scalars['String']['input']>;
  adsOperationManager?: InputMaybe<Scalars['String']['input']>;
  adsOperationStaff?: InputMaybe<Scalars['String']['input']>;
  approver?: InputMaybe<Scalars['String']['input']>;
  billingMethodProblem?: InputMaybe<Scalars['Boolean']['input']>;
  billingMethodProblemText?: InputMaybe<Scalars['String']['input']>;
  /** 企業ID */
  companyId?: InputMaybe<Scalars['UUID']['input']>;
  corporateName?: InputMaybe<Scalars['String']['input']>;
  corporateNameBill?: InputMaybe<Scalars['String']['input']>;
  corporateNameKana?: InputMaybe<Scalars['String']['input']>;
  corporateNameKanaBill?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  creativeManager?: InputMaybe<Scalars['String']['input']>;
  departmentName?: InputMaybe<Scalars['String']['input']>;
  departmentNameBill?: InputMaybe<Scalars['String']['input']>;
  /** 契約情報/企業情報ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  mobileNumber?: InputMaybe<Scalars['String']['input']>;
  mobileNumberBill?: InputMaybe<Scalars['String']['input']>;
  personInChargeEmail?: InputMaybe<Scalars['String']['input']>;
  personInChargeEmailBill?: InputMaybe<Scalars['String']['input']>;
  personInChargeName?: InputMaybe<Scalars['String']['input']>;
  personInChargeNameBill?: InputMaybe<Scalars['String']['input']>;
  postCode?: InputMaybe<Scalars['String']['input']>;
  postCodeBill?: InputMaybe<Scalars['String']['input']>;
  productNames?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  productPriorities?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  projectManager?: InputMaybe<Scalars['String']['input']>;
  sendOriginalMail?: InputMaybe<Scalars['Boolean']['input']>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** A connection to a list of `CompanyInfo` values. */
export type CompanyInfosConnection = {
  __typename?: 'CompanyInfosConnection';
  /** A list of edges which contains the `CompanyInfo` and cursor to aid in pagination. */
  edges: Array<CompanyInfosEdge>;
  /** A list of `CompanyInfo` objects. */
  nodes: Array<Maybe<CompanyInfo>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `CompanyInfo` you could get from the connection. */
  totalCount: Scalars['Int']['output'];
};

/** A `CompanyInfo` edge in the connection. */
export type CompanyInfosEdge = {
  __typename?: 'CompanyInfosEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']['output']>;
  /** The `CompanyInfo` at the end of the edge. */
  node?: Maybe<CompanyInfo>;
};

/** Methods to use when ordering `CompanyInfo`. */
export enum CompanyInfosOrderBy {
  AddressAsc = 'ADDRESS_ASC',
  AddressBillAsc = 'ADDRESS_BILL_ASC',
  AddressBillDesc = 'ADDRESS_BILL_DESC',
  AddressDesc = 'ADDRESS_DESC',
  AdsOperationManagerAsc = 'ADS_OPERATION_MANAGER_ASC',
  AdsOperationManagerDesc = 'ADS_OPERATION_MANAGER_DESC',
  AdsOperationStaffAsc = 'ADS_OPERATION_STAFF_ASC',
  AdsOperationStaffDesc = 'ADS_OPERATION_STAFF_DESC',
  ApproverAsc = 'APPROVER_ASC',
  ApproverDesc = 'APPROVER_DESC',
  BillingMethodProblemAsc = 'BILLING_METHOD_PROBLEM_ASC',
  BillingMethodProblemDesc = 'BILLING_METHOD_PROBLEM_DESC',
  BillingMethodProblemTextAsc = 'BILLING_METHOD_PROBLEM_TEXT_ASC',
  BillingMethodProblemTextDesc = 'BILLING_METHOD_PROBLEM_TEXT_DESC',
  CompanyIdAsc = 'COMPANY_ID_ASC',
  CompanyIdDesc = 'COMPANY_ID_DESC',
  CorporateNameAsc = 'CORPORATE_NAME_ASC',
  CorporateNameBillAsc = 'CORPORATE_NAME_BILL_ASC',
  CorporateNameBillDesc = 'CORPORATE_NAME_BILL_DESC',
  CorporateNameDesc = 'CORPORATE_NAME_DESC',
  CorporateNameKanaAsc = 'CORPORATE_NAME_KANA_ASC',
  CorporateNameKanaBillAsc = 'CORPORATE_NAME_KANA_BILL_ASC',
  CorporateNameKanaBillDesc = 'CORPORATE_NAME_KANA_BILL_DESC',
  CorporateNameKanaDesc = 'CORPORATE_NAME_KANA_DESC',
  CreatedAtAsc = 'CREATED_AT_ASC',
  CreatedAtDesc = 'CREATED_AT_DESC',
  CreativeManagerAsc = 'CREATIVE_MANAGER_ASC',
  CreativeManagerDesc = 'CREATIVE_MANAGER_DESC',
  DepartmentNameAsc = 'DEPARTMENT_NAME_ASC',
  DepartmentNameBillAsc = 'DEPARTMENT_NAME_BILL_ASC',
  DepartmentNameBillDesc = 'DEPARTMENT_NAME_BILL_DESC',
  DepartmentNameDesc = 'DEPARTMENT_NAME_DESC',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  MobileNumberAsc = 'MOBILE_NUMBER_ASC',
  MobileNumberBillAsc = 'MOBILE_NUMBER_BILL_ASC',
  MobileNumberBillDesc = 'MOBILE_NUMBER_BILL_DESC',
  MobileNumberDesc = 'MOBILE_NUMBER_DESC',
  Natural = 'NATURAL',
  PersonInChargeEmailAsc = 'PERSON_IN_CHARGE_EMAIL_ASC',
  PersonInChargeEmailBillAsc = 'PERSON_IN_CHARGE_EMAIL_BILL_ASC',
  PersonInChargeEmailBillDesc = 'PERSON_IN_CHARGE_EMAIL_BILL_DESC',
  PersonInChargeEmailDesc = 'PERSON_IN_CHARGE_EMAIL_DESC',
  PersonInChargeNameAsc = 'PERSON_IN_CHARGE_NAME_ASC',
  PersonInChargeNameBillAsc = 'PERSON_IN_CHARGE_NAME_BILL_ASC',
  PersonInChargeNameBillDesc = 'PERSON_IN_CHARGE_NAME_BILL_DESC',
  PersonInChargeNameDesc = 'PERSON_IN_CHARGE_NAME_DESC',
  PostCodeAsc = 'POST_CODE_ASC',
  PostCodeBillAsc = 'POST_CODE_BILL_ASC',
  PostCodeBillDesc = 'POST_CODE_BILL_DESC',
  PostCodeDesc = 'POST_CODE_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
  ProductNamesAsc = 'PRODUCT_NAMES_ASC',
  ProductNamesDesc = 'PRODUCT_NAMES_DESC',
  ProductPrioritiesAsc = 'PRODUCT_PRIORITIES_ASC',
  ProductPrioritiesDesc = 'PRODUCT_PRIORITIES_DESC',
  ProjectManagerAsc = 'PROJECT_MANAGER_ASC',
  ProjectManagerDesc = 'PROJECT_MANAGER_DESC',
  SendOriginalMailAsc = 'SEND_ORIGINAL_MAIL_ASC',
  SendOriginalMailDesc = 'SEND_ORIGINAL_MAIL_DESC',
  UpdatedAtAsc = 'UPDATED_AT_ASC',
  UpdatedAtDesc = 'UPDATED_AT_DESC'
}

/** An input for mutations affecting `Company` */
export type CompanyInput = {
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** 企業ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** Salesforce企業ID */
  sfAccountId?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** Represents an update to a `Company`. Fields that are set will be updated. */
export type CompanyPatch = {
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** 企業ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** Salesforce企業ID */
  sfAccountId?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** All input for the create `AlembicVersion` mutation. */
export type CreateAlembicVersionInput = {
  /** The `AlembicVersion` to be created by this mutation. */
  alembicVersion: AlembicVersionInput;
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
};

/** The output of our create `AlembicVersion` mutation. */
export type CreateAlembicVersionPayload = {
  __typename?: 'CreateAlembicVersionPayload';
  /** The `AlembicVersion` that was created by this mutation. */
  alembicVersion?: Maybe<AlembicVersion>;
  /** An edge for our `AlembicVersion`. May be used by Relay 1. */
  alembicVersionEdge?: Maybe<AlembicVersionsEdge>;
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our create `AlembicVersion` mutation. */
export type CreateAlembicVersionPayloadAlembicVersionEdgeArgs = {
  orderBy?: InputMaybe<Array<AlembicVersionsOrderBy>>;
};

/** All input for the create `CompanyInfo` mutation. */
export type CreateCompanyInfoInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The `CompanyInfo` to be created by this mutation. */
  companyInfo: CompanyInfoInput;
};

/** The output of our create `CompanyInfo` mutation. */
export type CreateCompanyInfoPayload = {
  __typename?: 'CreateCompanyInfoPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Company` that is related to this `CompanyInfo`. */
  companyByCompanyId?: Maybe<Company>;
  /** The `CompanyInfo` that was created by this mutation. */
  companyInfo?: Maybe<CompanyInfo>;
  /** An edge for our `CompanyInfo`. May be used by Relay 1. */
  companyInfoEdge?: Maybe<CompanyInfosEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our create `CompanyInfo` mutation. */
export type CreateCompanyInfoPayloadCompanyInfoEdgeArgs = {
  orderBy?: InputMaybe<Array<CompanyInfosOrderBy>>;
};

/** All input for the create `Company` mutation. */
export type CreateCompanyInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The `Company` to be created by this mutation. */
  company: CompanyInput;
};

/** The output of our create `Company` mutation. */
export type CreateCompanyPayload = {
  __typename?: 'CreateCompanyPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `Company` that was created by this mutation. */
  company?: Maybe<Company>;
  /** An edge for our `Company`. May be used by Relay 1. */
  companyEdge?: Maybe<CompaniesEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our create `Company` mutation. */
export type CreateCompanyPayloadCompanyEdgeArgs = {
  orderBy?: InputMaybe<Array<CompaniesOrderBy>>;
};

/** All input for the create `CustomerGroup` mutation. */
export type CreateCustomerGroupInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The `CustomerGroup` to be created by this mutation. */
  customerGroup: CustomerGroupInput;
};

/** The output of our create `CustomerGroup` mutation. */
export type CreateCustomerGroupPayload = {
  __typename?: 'CreateCustomerGroupPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `CustomerGroup` that was created by this mutation. */
  customerGroup?: Maybe<CustomerGroup>;
  /** An edge for our `CustomerGroup`. May be used by Relay 1. */
  customerGroupEdge?: Maybe<CustomerGroupsEdge>;
  /** Reads a single `Opportunity` that is related to this `CustomerGroup`. */
  opportunityByOpportunityId?: Maybe<Opportunity>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our create `CustomerGroup` mutation. */
export type CreateCustomerGroupPayloadCustomerGroupEdgeArgs = {
  orderBy?: InputMaybe<Array<CustomerGroupsOrderBy>>;
};

/** All input for the create `MarketingActivityInformation` mutation. */
export type CreateMarketingActivityInformationInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The `MarketingActivityInformation` to be created by this mutation. */
  marketingActivityInformation: MarketingActivityInformationInput;
};

/** The output of our create `MarketingActivityInformation` mutation. */
export type CreateMarketingActivityInformationPayload = {
  __typename?: 'CreateMarketingActivityInformationPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `MarketingActivityInformation` that was created by this mutation. */
  marketingActivityInformation?: Maybe<MarketingActivityInformation>;
  /** An edge for our `MarketingActivityInformation`. May be used by Relay 1. */
  marketingActivityInformationEdge?: Maybe<MarketingActivityInformationsEdge>;
  /** Reads a single `Product` that is related to this `MarketingActivityInformation`. */
  productByProductId?: Maybe<Product>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our create `MarketingActivityInformation` mutation. */
export type CreateMarketingActivityInformationPayloadMarketingActivityInformationEdgeArgs = {
  orderBy?: InputMaybe<Array<MarketingActivityInformationsOrderBy>>;
};

/** All input for the create `Notification` mutation. */
export type CreateNotificationInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The `Notification` to be created by this mutation. */
  notification: NotificationInput;
};

/** The output of our create `Notification` mutation. */
export type CreateNotificationPayload = {
  __typename?: 'CreateNotificationPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `Notification` that was created by this mutation. */
  notification?: Maybe<Notification>;
  /** An edge for our `Notification`. May be used by Relay 1. */
  notificationEdge?: Maybe<NotificationsEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our create `Notification` mutation. */
export type CreateNotificationPayloadNotificationEdgeArgs = {
  orderBy?: InputMaybe<Array<NotificationsOrderBy>>;
};

/** All input for the create `OperatorGroup` mutation. */
export type CreateOperatorGroupInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The `OperatorGroup` to be created by this mutation. */
  operatorGroup: OperatorGroupInput;
};

/** The output of our create `OperatorGroup` mutation. */
export type CreateOperatorGroupPayload = {
  __typename?: 'CreateOperatorGroupPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `OperatorGroup` that was created by this mutation. */
  operatorGroup?: Maybe<OperatorGroup>;
  /** An edge for our `OperatorGroup`. May be used by Relay 1. */
  operatorGroupEdge?: Maybe<OperatorGroupsEdge>;
  /** Reads a single `Opportunity` that is related to this `OperatorGroup`. */
  opportunityByOpportunityId?: Maybe<Opportunity>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our create `OperatorGroup` mutation. */
export type CreateOperatorGroupPayloadOperatorGroupEdgeArgs = {
  orderBy?: InputMaybe<Array<OperatorGroupsOrderBy>>;
};

/** All input for the create `OperatorName` mutation. */
export type CreateOperatorNameInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The `OperatorName` to be created by this mutation. */
  operatorName: OperatorNameInput;
};

/** The output of our create `OperatorName` mutation. */
export type CreateOperatorNamePayload = {
  __typename?: 'CreateOperatorNamePayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `OperatorName` that was created by this mutation. */
  operatorName?: Maybe<OperatorName>;
  /** An edge for our `OperatorName`. May be used by Relay 1. */
  operatorNameEdge?: Maybe<OperatorNamesEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our create `OperatorName` mutation. */
export type CreateOperatorNamePayloadOperatorNameEdgeArgs = {
  orderBy?: InputMaybe<Array<OperatorNamesOrderBy>>;
};

/** All input for the create `Opportunity` mutation. */
export type CreateOpportunityInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The `Opportunity` to be created by this mutation. */
  opportunity: OpportunityInput;
};

/** The output of our create `Opportunity` mutation. */
export type CreateOpportunityPayload = {
  __typename?: 'CreateOpportunityPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `Opportunity` that was created by this mutation. */
  opportunity?: Maybe<Opportunity>;
  /** An edge for our `Opportunity`. May be used by Relay 1. */
  opportunityEdge?: Maybe<OpportunitiesEdge>;
  /** Reads a single `Product` that is related to this `Opportunity`. */
  productByProductId?: Maybe<Product>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our create `Opportunity` mutation. */
export type CreateOpportunityPayloadOpportunityEdgeArgs = {
  orderBy?: InputMaybe<Array<OpportunitiesOrderBy>>;
};

/** All input for the create `ProductInformation` mutation. */
export type CreateProductInformationInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The `ProductInformation` to be created by this mutation. */
  productInformation: ProductInformationInput;
};

/** The output of our create `ProductInformation` mutation. */
export type CreateProductInformationPayload = {
  __typename?: 'CreateProductInformationPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Product` that is related to this `ProductInformation`. */
  productByProductId?: Maybe<Product>;
  /** The `ProductInformation` that was created by this mutation. */
  productInformation?: Maybe<ProductInformation>;
  /** An edge for our `ProductInformation`. May be used by Relay 1. */
  productInformationEdge?: Maybe<ProductInformationsEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our create `ProductInformation` mutation. */
export type CreateProductInformationPayloadProductInformationEdgeArgs = {
  orderBy?: InputMaybe<Array<ProductInformationsOrderBy>>;
};

/** All input for the create `Product` mutation. */
export type CreateProductInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The `Product` to be created by this mutation. */
  product: ProductInput;
};

/** The output of our create `Product` mutation. */
export type CreateProductPayload = {
  __typename?: 'CreateProductPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Company` that is related to this `Product`. */
  companyByCompanyId?: Maybe<Company>;
  /** The `Product` that was created by this mutation. */
  product?: Maybe<Product>;
  /** An edge for our `Product`. May be used by Relay 1. */
  productEdge?: Maybe<ProductsEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our create `Product` mutation. */
export type CreateProductPayloadProductEdgeArgs = {
  orderBy?: InputMaybe<Array<ProductsOrderBy>>;
};

/** All input for the create `RequestContent` mutation. */
export type CreateRequestContentInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The `RequestContent` to be created by this mutation. */
  requestContent: RequestContentInput;
};

/** The output of our create `RequestContent` mutation. */
export type CreateRequestContentPayload = {
  __typename?: 'CreateRequestContentPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Product` that is related to this `RequestContent`. */
  productByProductId?: Maybe<Product>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** The `RequestContent` that was created by this mutation. */
  requestContent?: Maybe<RequestContent>;
  /** An edge for our `RequestContent`. May be used by Relay 1. */
  requestContentEdge?: Maybe<RequestContentsEdge>;
};


/** The output of our create `RequestContent` mutation. */
export type CreateRequestContentPayloadRequestContentEdgeArgs = {
  orderBy?: InputMaybe<Array<RequestContentsOrderBy>>;
};

/** All input for the create `UserInformationMemo` mutation. */
export type CreateUserInformationMemoInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The `UserInformationMemo` to be created by this mutation. */
  userInformationMemo: UserInformationMemoInput;
};

/** The output of our create `UserInformationMemo` mutation. */
export type CreateUserInformationMemoPayload = {
  __typename?: 'CreateUserInformationMemoPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Product` that is related to this `UserInformationMemo`. */
  productByProductId?: Maybe<Product>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** The `UserInformationMemo` that was created by this mutation. */
  userInformationMemo?: Maybe<UserInformationMemo>;
  /** An edge for our `UserInformationMemo`. May be used by Relay 1. */
  userInformationMemoEdge?: Maybe<UserInformationMemosEdge>;
};


/** The output of our create `UserInformationMemo` mutation. */
export type CreateUserInformationMemoPayloadUserInformationMemoEdgeArgs = {
  orderBy?: InputMaybe<Array<UserInformationMemosOrderBy>>;
};

/** 顧客グループ */
export type CustomerGroup = Node & {
  __typename?: 'CustomerGroup';
  /** Cognito/Salesforceメール */
  email: Scalars['String']['output'];
  /** 顧客グループID */
  id: Scalars['Int']['output'];
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID']['output'];
  /** Reads a single `Opportunity` that is related to this `CustomerGroup`. */
  opportunityByOpportunityId?: Maybe<Opportunity>;
  /** 商談ID */
  opportunityId: Scalars['UUID']['output'];
  /** SalesforceユーザID */
  sfContactId: Scalars['String']['output'];
  /** Cognit ID */
  username?: Maybe<Scalars['String']['output']>;
};

/**
 * A condition to be used against `CustomerGroup` object types. All fields are
 * tested for equality and combined with a logical ‘and.’
 */
export type CustomerGroupCondition = {
  /** Checks for equality with the object’s `email` field. */
  email?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `id` field. */
  id?: InputMaybe<Scalars['Int']['input']>;
  /** Checks for equality with the object’s `opportunityId` field. */
  opportunityId?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `sfContactId` field. */
  sfContactId?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `username` field. */
  username?: InputMaybe<Scalars['String']['input']>;
};

/** A filter to be used against `CustomerGroup` object types. All fields are combined with a logical ‘and.’ */
export type CustomerGroupFilter = {
  /** Checks for all expressions in this list. */
  and?: InputMaybe<Array<CustomerGroupFilter>>;
  /** Filter by the object’s `email` field. */
  email?: InputMaybe<StringFilter>;
  /** Filter by the object’s `id` field. */
  id?: InputMaybe<IntFilter>;
  /** Negates the expression. */
  not?: InputMaybe<CustomerGroupFilter>;
  /** Filter by the object’s `opportunityId` field. */
  opportunityId?: InputMaybe<UuidFilter>;
  /** Checks for any expressions in this list. */
  or?: InputMaybe<Array<CustomerGroupFilter>>;
  /** Filter by the object’s `sfContactId` field. */
  sfContactId?: InputMaybe<StringFilter>;
  /** Filter by the object’s `username` field. */
  username?: InputMaybe<StringFilter>;
};

/** An input for mutations affecting `CustomerGroup` */
export type CustomerGroupInput = {
  /** Cognito/Salesforceメール */
  email: Scalars['String']['input'];
  /** 顧客グループID */
  id?: InputMaybe<Scalars['Int']['input']>;
  /** 商談ID */
  opportunityId: Scalars['UUID']['input'];
  /** SalesforceユーザID */
  sfContactId: Scalars['String']['input'];
  /** Cognit ID */
  username?: InputMaybe<Scalars['String']['input']>;
};

/** Represents an update to a `CustomerGroup`. Fields that are set will be updated. */
export type CustomerGroupPatch = {
  /** Cognito/Salesforceメール */
  email?: InputMaybe<Scalars['String']['input']>;
  /** 顧客グループID */
  id?: InputMaybe<Scalars['Int']['input']>;
  /** 商談ID */
  opportunityId?: InputMaybe<Scalars['UUID']['input']>;
  /** SalesforceユーザID */
  sfContactId?: InputMaybe<Scalars['String']['input']>;
  /** Cognit ID */
  username?: InputMaybe<Scalars['String']['input']>;
};

/** A connection to a list of `CustomerGroup` values. */
export type CustomerGroupsConnection = {
  __typename?: 'CustomerGroupsConnection';
  /** A list of edges which contains the `CustomerGroup` and cursor to aid in pagination. */
  edges: Array<CustomerGroupsEdge>;
  /** A list of `CustomerGroup` objects. */
  nodes: Array<Maybe<CustomerGroup>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `CustomerGroup` you could get from the connection. */
  totalCount: Scalars['Int']['output'];
};

/** A `CustomerGroup` edge in the connection. */
export type CustomerGroupsEdge = {
  __typename?: 'CustomerGroupsEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']['output']>;
  /** The `CustomerGroup` at the end of the edge. */
  node?: Maybe<CustomerGroup>;
};

/** Methods to use when ordering `CustomerGroup`. */
export enum CustomerGroupsOrderBy {
  EmailAsc = 'EMAIL_ASC',
  EmailDesc = 'EMAIL_DESC',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  Natural = 'NATURAL',
  OpportunityIdAsc = 'OPPORTUNITY_ID_ASC',
  OpportunityIdDesc = 'OPPORTUNITY_ID_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
  SfContactIdAsc = 'SF_CONTACT_ID_ASC',
  SfContactIdDesc = 'SF_CONTACT_ID_DESC',
  UsernameAsc = 'USERNAME_ASC',
  UsernameDesc = 'USERNAME_DESC'
}

/** A filter to be used against Datetime fields. All fields are combined with a logical ‘and.’ */
export type DatetimeFilter = {
  /** Not equal to the specified value, treating null like an ordinary value. */
  distinctFrom?: InputMaybe<Scalars['Datetime']['input']>;
  /** Equal to the specified value. */
  equalTo?: InputMaybe<Scalars['Datetime']['input']>;
  /** Greater than the specified value. */
  greaterThan?: InputMaybe<Scalars['Datetime']['input']>;
  /** Greater than or equal to the specified value. */
  greaterThanOrEqualTo?: InputMaybe<Scalars['Datetime']['input']>;
  /** Included in the specified list. */
  in?: InputMaybe<Array<Scalars['Datetime']['input']>>;
  /** Is null (if `true` is specified) or is not null (if `false` is specified). */
  isNull?: InputMaybe<Scalars['Boolean']['input']>;
  /** Less than the specified value. */
  lessThan?: InputMaybe<Scalars['Datetime']['input']>;
  /** Less than or equal to the specified value. */
  lessThanOrEqualTo?: InputMaybe<Scalars['Datetime']['input']>;
  /** Equal to the specified value, treating null like an ordinary value. */
  notDistinctFrom?: InputMaybe<Scalars['Datetime']['input']>;
  /** Not equal to the specified value. */
  notEqualTo?: InputMaybe<Scalars['Datetime']['input']>;
  /** Not included in the specified list. */
  notIn?: InputMaybe<Array<Scalars['Datetime']['input']>>;
};

/** All input for the `deleteAlembicVersionByVersionNum` mutation. */
export type DeleteAlembicVersionByVersionNumInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  versionNum: Scalars['String']['input'];
};

/** All input for the `deleteAlembicVersion` mutation. */
export type DeleteAlembicVersionInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `AlembicVersion` to be deleted. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our delete `AlembicVersion` mutation. */
export type DeleteAlembicVersionPayload = {
  __typename?: 'DeleteAlembicVersionPayload';
  /** The `AlembicVersion` that was deleted by this mutation. */
  alembicVersion?: Maybe<AlembicVersion>;
  /** An edge for our `AlembicVersion`. May be used by Relay 1. */
  alembicVersionEdge?: Maybe<AlembicVersionsEdge>;
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  deletedAlembicVersionId?: Maybe<Scalars['ID']['output']>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our delete `AlembicVersion` mutation. */
export type DeleteAlembicVersionPayloadAlembicVersionEdgeArgs = {
  orderBy?: InputMaybe<Array<AlembicVersionsOrderBy>>;
};

/** All input for the `deleteCompanyById` mutation. */
export type DeleteCompanyByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** 企業ID */
  id: Scalars['UUID']['input'];
};

/** All input for the `deleteCompanyInfoById` mutation. */
export type DeleteCompanyInfoByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** 契約情報/企業情報ID */
  id: Scalars['UUID']['input'];
};

/** All input for the `deleteCompanyInfo` mutation. */
export type DeleteCompanyInfoInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `CompanyInfo` to be deleted. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our delete `CompanyInfo` mutation. */
export type DeleteCompanyInfoPayload = {
  __typename?: 'DeleteCompanyInfoPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Company` that is related to this `CompanyInfo`. */
  companyByCompanyId?: Maybe<Company>;
  /** The `CompanyInfo` that was deleted by this mutation. */
  companyInfo?: Maybe<CompanyInfo>;
  /** An edge for our `CompanyInfo`. May be used by Relay 1. */
  companyInfoEdge?: Maybe<CompanyInfosEdge>;
  deletedCompanyInfoId?: Maybe<Scalars['ID']['output']>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our delete `CompanyInfo` mutation. */
export type DeleteCompanyInfoPayloadCompanyInfoEdgeArgs = {
  orderBy?: InputMaybe<Array<CompanyInfosOrderBy>>;
};

/** All input for the `deleteCompany` mutation. */
export type DeleteCompanyInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `Company` to be deleted. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our delete `Company` mutation. */
export type DeleteCompanyPayload = {
  __typename?: 'DeleteCompanyPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `Company` that was deleted by this mutation. */
  company?: Maybe<Company>;
  /** An edge for our `Company`. May be used by Relay 1. */
  companyEdge?: Maybe<CompaniesEdge>;
  deletedCompanyId?: Maybe<Scalars['ID']['output']>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our delete `Company` mutation. */
export type DeleteCompanyPayloadCompanyEdgeArgs = {
  orderBy?: InputMaybe<Array<CompaniesOrderBy>>;
};

/** All input for the `deleteCustomerGroupById` mutation. */
export type DeleteCustomerGroupByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** 顧客グループID */
  id: Scalars['Int']['input'];
};

/** All input for the `deleteCustomerGroupByOpportunityIdAndSfContactId` mutation. */
export type DeleteCustomerGroupByOpportunityIdAndSfContactIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** 商談ID */
  opportunityId: Scalars['UUID']['input'];
  /** SalesforceユーザID */
  sfContactId: Scalars['String']['input'];
};

/** All input for the `deleteCustomerGroup` mutation. */
export type DeleteCustomerGroupInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `CustomerGroup` to be deleted. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our delete `CustomerGroup` mutation. */
export type DeleteCustomerGroupPayload = {
  __typename?: 'DeleteCustomerGroupPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `CustomerGroup` that was deleted by this mutation. */
  customerGroup?: Maybe<CustomerGroup>;
  /** An edge for our `CustomerGroup`. May be used by Relay 1. */
  customerGroupEdge?: Maybe<CustomerGroupsEdge>;
  deletedCustomerGroupId?: Maybe<Scalars['ID']['output']>;
  /** Reads a single `Opportunity` that is related to this `CustomerGroup`. */
  opportunityByOpportunityId?: Maybe<Opportunity>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our delete `CustomerGroup` mutation. */
export type DeleteCustomerGroupPayloadCustomerGroupEdgeArgs = {
  orderBy?: InputMaybe<Array<CustomerGroupsOrderBy>>;
};

/** All input for the `deleteMarketingActivityInformationById` mutation. */
export type DeleteMarketingActivityInformationByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** マーケティング活動情報ID */
  id: Scalars['UUID']['input'];
};

/** All input for the `deleteMarketingActivityInformation` mutation. */
export type DeleteMarketingActivityInformationInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `MarketingActivityInformation` to be deleted. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our delete `MarketingActivityInformation` mutation. */
export type DeleteMarketingActivityInformationPayload = {
  __typename?: 'DeleteMarketingActivityInformationPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  deletedMarketingActivityInformationId?: Maybe<Scalars['ID']['output']>;
  /** The `MarketingActivityInformation` that was deleted by this mutation. */
  marketingActivityInformation?: Maybe<MarketingActivityInformation>;
  /** An edge for our `MarketingActivityInformation`. May be used by Relay 1. */
  marketingActivityInformationEdge?: Maybe<MarketingActivityInformationsEdge>;
  /** Reads a single `Product` that is related to this `MarketingActivityInformation`. */
  productByProductId?: Maybe<Product>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our delete `MarketingActivityInformation` mutation. */
export type DeleteMarketingActivityInformationPayloadMarketingActivityInformationEdgeArgs = {
  orderBy?: InputMaybe<Array<MarketingActivityInformationsOrderBy>>;
};

/** All input for the `deleteNotificationById` mutation. */
export type DeleteNotificationByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** お知らせID */
  id: Scalars['UUID']['input'];
};

/** All input for the `deleteNotification` mutation. */
export type DeleteNotificationInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `Notification` to be deleted. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our delete `Notification` mutation. */
export type DeleteNotificationPayload = {
  __typename?: 'DeleteNotificationPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  deletedNotificationId?: Maybe<Scalars['ID']['output']>;
  /** The `Notification` that was deleted by this mutation. */
  notification?: Maybe<Notification>;
  /** An edge for our `Notification`. May be used by Relay 1. */
  notificationEdge?: Maybe<NotificationsEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our delete `Notification` mutation. */
export type DeleteNotificationPayloadNotificationEdgeArgs = {
  orderBy?: InputMaybe<Array<NotificationsOrderBy>>;
};

/** All input for the `deleteOperatorGroupById` mutation. */
export type DeleteOperatorGroupByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** 管理者グループID */
  id: Scalars['Int']['input'];
};

/** All input for the `deleteOperatorGroup` mutation. */
export type DeleteOperatorGroupInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `OperatorGroup` to be deleted. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our delete `OperatorGroup` mutation. */
export type DeleteOperatorGroupPayload = {
  __typename?: 'DeleteOperatorGroupPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  deletedOperatorGroupId?: Maybe<Scalars['ID']['output']>;
  /** The `OperatorGroup` that was deleted by this mutation. */
  operatorGroup?: Maybe<OperatorGroup>;
  /** An edge for our `OperatorGroup`. May be used by Relay 1. */
  operatorGroupEdge?: Maybe<OperatorGroupsEdge>;
  /** Reads a single `Opportunity` that is related to this `OperatorGroup`. */
  opportunityByOpportunityId?: Maybe<Opportunity>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our delete `OperatorGroup` mutation. */
export type DeleteOperatorGroupPayloadOperatorGroupEdgeArgs = {
  orderBy?: InputMaybe<Array<OperatorGroupsOrderBy>>;
};

/** All input for the `deleteOperatorNameByEmail` mutation. */
export type DeleteOperatorNameByEmailInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  email: Scalars['String']['input'];
};

/** All input for the `deleteOperatorName` mutation. */
export type DeleteOperatorNameInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `OperatorName` to be deleted. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our delete `OperatorName` mutation. */
export type DeleteOperatorNamePayload = {
  __typename?: 'DeleteOperatorNamePayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  deletedOperatorNameId?: Maybe<Scalars['ID']['output']>;
  /** The `OperatorName` that was deleted by this mutation. */
  operatorName?: Maybe<OperatorName>;
  /** An edge for our `OperatorName`. May be used by Relay 1. */
  operatorNameEdge?: Maybe<OperatorNamesEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our delete `OperatorName` mutation. */
export type DeleteOperatorNamePayloadOperatorNameEdgeArgs = {
  orderBy?: InputMaybe<Array<OperatorNamesOrderBy>>;
};

/** All input for the `deleteOpportunityById` mutation. */
export type DeleteOpportunityByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** 商談ID */
  id: Scalars['UUID']['input'];
};

/** All input for the `deleteOpportunity` mutation. */
export type DeleteOpportunityInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `Opportunity` to be deleted. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our delete `Opportunity` mutation. */
export type DeleteOpportunityPayload = {
  __typename?: 'DeleteOpportunityPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  deletedOpportunityId?: Maybe<Scalars['ID']['output']>;
  /** The `Opportunity` that was deleted by this mutation. */
  opportunity?: Maybe<Opportunity>;
  /** An edge for our `Opportunity`. May be used by Relay 1. */
  opportunityEdge?: Maybe<OpportunitiesEdge>;
  /** Reads a single `Product` that is related to this `Opportunity`. */
  productByProductId?: Maybe<Product>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our delete `Opportunity` mutation. */
export type DeleteOpportunityPayloadOpportunityEdgeArgs = {
  orderBy?: InputMaybe<Array<OpportunitiesOrderBy>>;
};

/** All input for the `deleteProductByCompanyIdAndProductName` mutation. */
export type DeleteProductByCompanyIdAndProductNameInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** 企業ID */
  companyId: Scalars['UUID']['input'];
  /** 商材名 */
  productName: Scalars['String']['input'];
};

/** All input for the `deleteProductById` mutation. */
export type DeleteProductByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** 商材ID */
  id: Scalars['UUID']['input'];
};

/** All input for the `deleteProductInformationById` mutation. */
export type DeleteProductInformationByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** 商材情報ID */
  id: Scalars['UUID']['input'];
};

/** All input for the `deleteProductInformation` mutation. */
export type DeleteProductInformationInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `ProductInformation` to be deleted. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our delete `ProductInformation` mutation. */
export type DeleteProductInformationPayload = {
  __typename?: 'DeleteProductInformationPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  deletedProductInformationId?: Maybe<Scalars['ID']['output']>;
  /** Reads a single `Product` that is related to this `ProductInformation`. */
  productByProductId?: Maybe<Product>;
  /** The `ProductInformation` that was deleted by this mutation. */
  productInformation?: Maybe<ProductInformation>;
  /** An edge for our `ProductInformation`. May be used by Relay 1. */
  productInformationEdge?: Maybe<ProductInformationsEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our delete `ProductInformation` mutation. */
export type DeleteProductInformationPayloadProductInformationEdgeArgs = {
  orderBy?: InputMaybe<Array<ProductInformationsOrderBy>>;
};

/** All input for the `deleteProduct` mutation. */
export type DeleteProductInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `Product` to be deleted. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our delete `Product` mutation. */
export type DeleteProductPayload = {
  __typename?: 'DeleteProductPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Company` that is related to this `Product`. */
  companyByCompanyId?: Maybe<Company>;
  deletedProductId?: Maybe<Scalars['ID']['output']>;
  /** The `Product` that was deleted by this mutation. */
  product?: Maybe<Product>;
  /** An edge for our `Product`. May be used by Relay 1. */
  productEdge?: Maybe<ProductsEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our delete `Product` mutation. */
export type DeleteProductPayloadProductEdgeArgs = {
  orderBy?: InputMaybe<Array<ProductsOrderBy>>;
};

/** All input for the `deleteRequestContentById` mutation. */
export type DeleteRequestContentByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** ご依頼内容ID */
  id: Scalars['UUID']['input'];
};

/** All input for the `deleteRequestContent` mutation. */
export type DeleteRequestContentInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `RequestContent` to be deleted. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our delete `RequestContent` mutation. */
export type DeleteRequestContentPayload = {
  __typename?: 'DeleteRequestContentPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  deletedRequestContentId?: Maybe<Scalars['ID']['output']>;
  /** Reads a single `Product` that is related to this `RequestContent`. */
  productByProductId?: Maybe<Product>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** The `RequestContent` that was deleted by this mutation. */
  requestContent?: Maybe<RequestContent>;
  /** An edge for our `RequestContent`. May be used by Relay 1. */
  requestContentEdge?: Maybe<RequestContentsEdge>;
};


/** The output of our delete `RequestContent` mutation. */
export type DeleteRequestContentPayloadRequestContentEdgeArgs = {
  orderBy?: InputMaybe<Array<RequestContentsOrderBy>>;
};

/** All input for the `deleteUserInformationMemoById` mutation. */
export type DeleteUserInformationMemoByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** ご依頼内容ID */
  id: Scalars['UUID']['input'];
};

/** All input for the `deleteUserInformationMemo` mutation. */
export type DeleteUserInformationMemoInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `UserInformationMemo` to be deleted. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our delete `UserInformationMemo` mutation. */
export type DeleteUserInformationMemoPayload = {
  __typename?: 'DeleteUserInformationMemoPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  deletedUserInformationMemoId?: Maybe<Scalars['ID']['output']>;
  /** Reads a single `Product` that is related to this `UserInformationMemo`. */
  productByProductId?: Maybe<Product>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** The `UserInformationMemo` that was deleted by this mutation. */
  userInformationMemo?: Maybe<UserInformationMemo>;
  /** An edge for our `UserInformationMemo`. May be used by Relay 1. */
  userInformationMemoEdge?: Maybe<UserInformationMemosEdge>;
};


/** The output of our delete `UserInformationMemo` mutation. */
export type DeleteUserInformationMemoPayloadUserInformationMemoEdgeArgs = {
  orderBy?: InputMaybe<Array<UserInformationMemosOrderBy>>;
};

/** A filter to be used against Int fields. All fields are combined with a logical ‘and.’ */
export type IntFilter = {
  /** Not equal to the specified value, treating null like an ordinary value. */
  distinctFrom?: InputMaybe<Scalars['Int']['input']>;
  /** Equal to the specified value. */
  equalTo?: InputMaybe<Scalars['Int']['input']>;
  /** Greater than the specified value. */
  greaterThan?: InputMaybe<Scalars['Int']['input']>;
  /** Greater than or equal to the specified value. */
  greaterThanOrEqualTo?: InputMaybe<Scalars['Int']['input']>;
  /** Included in the specified list. */
  in?: InputMaybe<Array<Scalars['Int']['input']>>;
  /** Is null (if `true` is specified) or is not null (if `false` is specified). */
  isNull?: InputMaybe<Scalars['Boolean']['input']>;
  /** Less than the specified value. */
  lessThan?: InputMaybe<Scalars['Int']['input']>;
  /** Less than or equal to the specified value. */
  lessThanOrEqualTo?: InputMaybe<Scalars['Int']['input']>;
  /** Equal to the specified value, treating null like an ordinary value. */
  notDistinctFrom?: InputMaybe<Scalars['Int']['input']>;
  /** Not equal to the specified value. */
  notEqualTo?: InputMaybe<Scalars['Int']['input']>;
  /** Not included in the specified list. */
  notIn?: InputMaybe<Array<Scalars['Int']['input']>>;
};

/** A filter to be used against Int List fields. All fields are combined with a logical ‘and.’ */
export type IntListFilter = {
  /** Any array item is equal to the specified value. */
  anyEqualTo?: InputMaybe<Scalars['Int']['input']>;
  /** Any array item is greater than the specified value. */
  anyGreaterThan?: InputMaybe<Scalars['Int']['input']>;
  /** Any array item is greater than or equal to the specified value. */
  anyGreaterThanOrEqualTo?: InputMaybe<Scalars['Int']['input']>;
  /** Any array item is less than the specified value. */
  anyLessThan?: InputMaybe<Scalars['Int']['input']>;
  /** Any array item is less than or equal to the specified value. */
  anyLessThanOrEqualTo?: InputMaybe<Scalars['Int']['input']>;
  /** Any array item is not equal to the specified value. */
  anyNotEqualTo?: InputMaybe<Scalars['Int']['input']>;
  /** Contained by the specified list of values. */
  containedBy?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  /** Contains the specified list of values. */
  contains?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  /** Not equal to the specified value, treating null like an ordinary value. */
  distinctFrom?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  /** Equal to the specified value. */
  equalTo?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  /** Greater than the specified value. */
  greaterThan?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  /** Greater than or equal to the specified value. */
  greaterThanOrEqualTo?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  /** Is null (if `true` is specified) or is not null (if `false` is specified). */
  isNull?: InputMaybe<Scalars['Boolean']['input']>;
  /** Less than the specified value. */
  lessThan?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  /** Less than or equal to the specified value. */
  lessThanOrEqualTo?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  /** Equal to the specified value, treating null like an ordinary value. */
  notDistinctFrom?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  /** Not equal to the specified value. */
  notEqualTo?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  /** Overlaps the specified list of values. */
  overlaps?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
};

/** マーケティング活動情報 */
export type MarketingActivityInformation = Node & {
  __typename?: 'MarketingActivityInformation';
  adBudgetRatioListingDisplay?: Maybe<Scalars['String']['output']>;
  adMediaDecisionDone?: Maybe<Scalars['String']['output']>;
  adMonthlyBudget?: Maybe<Scalars['String']['output']>;
  adminCommentCvCpaDefinition?: Maybe<Scalars['String']['output']>;
  adminCommentDeliverySchedule?: Maybe<Scalars['String']['output']>;
  adminCommentMarketingBudget?: Maybe<Scalars['String']['output']>;
  adminCommentReference?: Maybe<Scalars['String']['output']>;
  cpaDefinition?: Maybe<Scalars['String']['output']>;
  createdAt?: Maybe<Scalars['Datetime']['output']>;
  customerConversionCorporate?: Maybe<Scalars['String']['output']>;
  customerConversionIndividual?: Maybe<Scalars['String']['output']>;
  displayAdBudgetRatio?: Maybe<Scalars['String']['output']>;
  displayAdBudgetRatioAnswerStatus?: Maybe<Scalars['String']['output']>;
  goalCalculationDone?: Maybe<Scalars['String']['output']>;
  havePlanSegment?: Maybe<Scalars['String']['output']>;
  /** マーケティング活動情報ID */
  id: Scalars['UUID']['output'];
  isCorporate?: Maybe<Scalars['Boolean']['output']>;
  kaizenFrequency?: Maybe<Scalars['String']['output']>;
  listingAdBudgetRatio?: Maybe<Scalars['String']['output']>;
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID']['output'];
  permissionResultAdToolView?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Product` that is related to this `MarketingActivityInformation`. */
  productByProductId?: Maybe<Product>;
  /** プロダクトID */
  productId?: Maybe<Scalars['UUID']['output']>;
  segmentDeliveryMethod?: Maybe<Scalars['String']['output']>;
  segmentMaterialsAnswerStatus?: Maybe<Scalars['String']['output']>;
  segmentMaterialsFileKeys?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  segmentMaterialsFiles?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  updatedAt?: Maybe<Scalars['Datetime']['output']>;
  withOtherAnalysisTool?: Maybe<Scalars['String']['output']>;
};

/**
 * A condition to be used against `MarketingActivityInformation` object types. All
 * fields are tested for equality and combined with a logical ‘and.’
 */
export type MarketingActivityInformationCondition = {
  /** Checks for equality with the object’s `adBudgetRatioListingDisplay` field. */
  adBudgetRatioListingDisplay?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adMediaDecisionDone` field. */
  adMediaDecisionDone?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adMonthlyBudget` field. */
  adMonthlyBudget?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentCvCpaDefinition` field. */
  adminCommentCvCpaDefinition?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentDeliverySchedule` field. */
  adminCommentDeliverySchedule?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentMarketingBudget` field. */
  adminCommentMarketingBudget?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentReference` field. */
  adminCommentReference?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `cpaDefinition` field. */
  cpaDefinition?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `createdAt` field. */
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** Checks for equality with the object’s `customerConversionCorporate` field. */
  customerConversionCorporate?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `customerConversionIndividual` field. */
  customerConversionIndividual?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `displayAdBudgetRatio` field. */
  displayAdBudgetRatio?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `displayAdBudgetRatioAnswerStatus` field. */
  displayAdBudgetRatioAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `goalCalculationDone` field. */
  goalCalculationDone?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `havePlanSegment` field. */
  havePlanSegment?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `id` field. */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `isCorporate` field. */
  isCorporate?: InputMaybe<Scalars['Boolean']['input']>;
  /** Checks for equality with the object’s `kaizenFrequency` field. */
  kaizenFrequency?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `listingAdBudgetRatio` field. */
  listingAdBudgetRatio?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `permissionResultAdToolView` field. */
  permissionResultAdToolView?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `productId` field. */
  productId?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `segmentDeliveryMethod` field. */
  segmentDeliveryMethod?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `segmentMaterialsAnswerStatus` field. */
  segmentMaterialsAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `segmentMaterialsFileKeys` field. */
  segmentMaterialsFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Checks for equality with the object’s `segmentMaterialsFiles` field. */
  segmentMaterialsFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Checks for equality with the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** Checks for equality with the object’s `withOtherAnalysisTool` field. */
  withOtherAnalysisTool?: InputMaybe<Scalars['String']['input']>;
};

/** A filter to be used against `MarketingActivityInformation` object types. All fields are combined with a logical ‘and.’ */
export type MarketingActivityInformationFilter = {
  /** Filter by the object’s `adBudgetRatioListingDisplay` field. */
  adBudgetRatioListingDisplay?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adMediaDecisionDone` field. */
  adMediaDecisionDone?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adMonthlyBudget` field. */
  adMonthlyBudget?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentCvCpaDefinition` field. */
  adminCommentCvCpaDefinition?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentDeliverySchedule` field. */
  adminCommentDeliverySchedule?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentMarketingBudget` field. */
  adminCommentMarketingBudget?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentReference` field. */
  adminCommentReference?: InputMaybe<StringFilter>;
  /** Checks for all expressions in this list. */
  and?: InputMaybe<Array<MarketingActivityInformationFilter>>;
  /** Filter by the object’s `cpaDefinition` field. */
  cpaDefinition?: InputMaybe<StringFilter>;
  /** Filter by the object’s `createdAt` field. */
  createdAt?: InputMaybe<DatetimeFilter>;
  /** Filter by the object’s `customerConversionCorporate` field. */
  customerConversionCorporate?: InputMaybe<StringFilter>;
  /** Filter by the object’s `customerConversionIndividual` field. */
  customerConversionIndividual?: InputMaybe<StringFilter>;
  /** Filter by the object’s `displayAdBudgetRatio` field. */
  displayAdBudgetRatio?: InputMaybe<StringFilter>;
  /** Filter by the object’s `displayAdBudgetRatioAnswerStatus` field. */
  displayAdBudgetRatioAnswerStatus?: InputMaybe<StringFilter>;
  /** Filter by the object’s `goalCalculationDone` field. */
  goalCalculationDone?: InputMaybe<StringFilter>;
  /** Filter by the object’s `havePlanSegment` field. */
  havePlanSegment?: InputMaybe<StringFilter>;
  /** Filter by the object’s `id` field. */
  id?: InputMaybe<UuidFilter>;
  /** Filter by the object’s `isCorporate` field. */
  isCorporate?: InputMaybe<BooleanFilter>;
  /** Filter by the object’s `kaizenFrequency` field. */
  kaizenFrequency?: InputMaybe<StringFilter>;
  /** Filter by the object’s `listingAdBudgetRatio` field. */
  listingAdBudgetRatio?: InputMaybe<StringFilter>;
  /** Negates the expression. */
  not?: InputMaybe<MarketingActivityInformationFilter>;
  /** Checks for any expressions in this list. */
  or?: InputMaybe<Array<MarketingActivityInformationFilter>>;
  /** Filter by the object’s `permissionResultAdToolView` field. */
  permissionResultAdToolView?: InputMaybe<StringFilter>;
  /** Filter by the object’s `productId` field. */
  productId?: InputMaybe<UuidFilter>;
  /** Filter by the object’s `segmentDeliveryMethod` field. */
  segmentDeliveryMethod?: InputMaybe<StringFilter>;
  /** Filter by the object’s `segmentMaterialsAnswerStatus` field. */
  segmentMaterialsAnswerStatus?: InputMaybe<StringFilter>;
  /** Filter by the object’s `segmentMaterialsFileKeys` field. */
  segmentMaterialsFileKeys?: InputMaybe<StringListFilter>;
  /** Filter by the object’s `segmentMaterialsFiles` field. */
  segmentMaterialsFiles?: InputMaybe<StringListFilter>;
  /** Filter by the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<DatetimeFilter>;
  /** Filter by the object’s `withOtherAnalysisTool` field. */
  withOtherAnalysisTool?: InputMaybe<StringFilter>;
};

/** An input for mutations affecting `MarketingActivityInformation` */
export type MarketingActivityInformationInput = {
  adBudgetRatioListingDisplay?: InputMaybe<Scalars['String']['input']>;
  adMediaDecisionDone?: InputMaybe<Scalars['String']['input']>;
  adMonthlyBudget?: InputMaybe<Scalars['String']['input']>;
  adminCommentCvCpaDefinition?: InputMaybe<Scalars['String']['input']>;
  adminCommentDeliverySchedule?: InputMaybe<Scalars['String']['input']>;
  adminCommentMarketingBudget?: InputMaybe<Scalars['String']['input']>;
  adminCommentReference?: InputMaybe<Scalars['String']['input']>;
  cpaDefinition?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  customerConversionCorporate?: InputMaybe<Scalars['String']['input']>;
  customerConversionIndividual?: InputMaybe<Scalars['String']['input']>;
  displayAdBudgetRatio?: InputMaybe<Scalars['String']['input']>;
  displayAdBudgetRatioAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  goalCalculationDone?: InputMaybe<Scalars['String']['input']>;
  havePlanSegment?: InputMaybe<Scalars['String']['input']>;
  /** マーケティング活動情報ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  isCorporate?: InputMaybe<Scalars['Boolean']['input']>;
  kaizenFrequency?: InputMaybe<Scalars['String']['input']>;
  listingAdBudgetRatio?: InputMaybe<Scalars['String']['input']>;
  permissionResultAdToolView?: InputMaybe<Scalars['String']['input']>;
  /** プロダクトID */
  productId?: InputMaybe<Scalars['UUID']['input']>;
  segmentDeliveryMethod?: InputMaybe<Scalars['String']['input']>;
  segmentMaterialsAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  segmentMaterialsFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  segmentMaterialsFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
  withOtherAnalysisTool?: InputMaybe<Scalars['String']['input']>;
};

/** Represents an update to a `MarketingActivityInformation`. Fields that are set will be updated. */
export type MarketingActivityInformationPatch = {
  adBudgetRatioListingDisplay?: InputMaybe<Scalars['String']['input']>;
  adMediaDecisionDone?: InputMaybe<Scalars['String']['input']>;
  adMonthlyBudget?: InputMaybe<Scalars['String']['input']>;
  adminCommentCvCpaDefinition?: InputMaybe<Scalars['String']['input']>;
  adminCommentDeliverySchedule?: InputMaybe<Scalars['String']['input']>;
  adminCommentMarketingBudget?: InputMaybe<Scalars['String']['input']>;
  adminCommentReference?: InputMaybe<Scalars['String']['input']>;
  cpaDefinition?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  customerConversionCorporate?: InputMaybe<Scalars['String']['input']>;
  customerConversionIndividual?: InputMaybe<Scalars['String']['input']>;
  displayAdBudgetRatio?: InputMaybe<Scalars['String']['input']>;
  displayAdBudgetRatioAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  goalCalculationDone?: InputMaybe<Scalars['String']['input']>;
  havePlanSegment?: InputMaybe<Scalars['String']['input']>;
  /** マーケティング活動情報ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  isCorporate?: InputMaybe<Scalars['Boolean']['input']>;
  kaizenFrequency?: InputMaybe<Scalars['String']['input']>;
  listingAdBudgetRatio?: InputMaybe<Scalars['String']['input']>;
  permissionResultAdToolView?: InputMaybe<Scalars['String']['input']>;
  /** プロダクトID */
  productId?: InputMaybe<Scalars['UUID']['input']>;
  segmentDeliveryMethod?: InputMaybe<Scalars['String']['input']>;
  segmentMaterialsAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  segmentMaterialsFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  segmentMaterialsFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
  withOtherAnalysisTool?: InputMaybe<Scalars['String']['input']>;
};

/** A connection to a list of `MarketingActivityInformation` values. */
export type MarketingActivityInformationsConnection = {
  __typename?: 'MarketingActivityInformationsConnection';
  /** A list of edges which contains the `MarketingActivityInformation` and cursor to aid in pagination. */
  edges: Array<MarketingActivityInformationsEdge>;
  /** A list of `MarketingActivityInformation` objects. */
  nodes: Array<Maybe<MarketingActivityInformation>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `MarketingActivityInformation` you could get from the connection. */
  totalCount: Scalars['Int']['output'];
};

/** A `MarketingActivityInformation` edge in the connection. */
export type MarketingActivityInformationsEdge = {
  __typename?: 'MarketingActivityInformationsEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']['output']>;
  /** The `MarketingActivityInformation` at the end of the edge. */
  node?: Maybe<MarketingActivityInformation>;
};

/** Methods to use when ordering `MarketingActivityInformation`. */
export enum MarketingActivityInformationsOrderBy {
  AdminCommentCvCpaDefinitionAsc = 'ADMIN_COMMENT_CV_CPA_DEFINITION_ASC',
  AdminCommentCvCpaDefinitionDesc = 'ADMIN_COMMENT_CV_CPA_DEFINITION_DESC',
  AdminCommentDeliveryScheduleAsc = 'ADMIN_COMMENT_DELIVERY_SCHEDULE_ASC',
  AdminCommentDeliveryScheduleDesc = 'ADMIN_COMMENT_DELIVERY_SCHEDULE_DESC',
  AdminCommentMarketingBudgetAsc = 'ADMIN_COMMENT_MARKETING_BUDGET_ASC',
  AdminCommentMarketingBudgetDesc = 'ADMIN_COMMENT_MARKETING_BUDGET_DESC',
  AdminCommentReferenceAsc = 'ADMIN_COMMENT_REFERENCE_ASC',
  AdminCommentReferenceDesc = 'ADMIN_COMMENT_REFERENCE_DESC',
  AdBudgetRatioListingDisplayAsc = 'AD_BUDGET_RATIO_LISTING_DISPLAY_ASC',
  AdBudgetRatioListingDisplayDesc = 'AD_BUDGET_RATIO_LISTING_DISPLAY_DESC',
  AdMediaDecisionDoneAsc = 'AD_MEDIA_DECISION_DONE_ASC',
  AdMediaDecisionDoneDesc = 'AD_MEDIA_DECISION_DONE_DESC',
  AdMonthlyBudgetAsc = 'AD_MONTHLY_BUDGET_ASC',
  AdMonthlyBudgetDesc = 'AD_MONTHLY_BUDGET_DESC',
  CpaDefinitionAsc = 'CPA_DEFINITION_ASC',
  CpaDefinitionDesc = 'CPA_DEFINITION_DESC',
  CreatedAtAsc = 'CREATED_AT_ASC',
  CreatedAtDesc = 'CREATED_AT_DESC',
  CustomerConversionCorporateAsc = 'CUSTOMER_CONVERSION_CORPORATE_ASC',
  CustomerConversionCorporateDesc = 'CUSTOMER_CONVERSION_CORPORATE_DESC',
  CustomerConversionIndividualAsc = 'CUSTOMER_CONVERSION_INDIVIDUAL_ASC',
  CustomerConversionIndividualDesc = 'CUSTOMER_CONVERSION_INDIVIDUAL_DESC',
  DisplayAdBudgetRatioAnswerStatusAsc = 'DISPLAY_AD_BUDGET_RATIO_ANSWER_STATUS_ASC',
  DisplayAdBudgetRatioAnswerStatusDesc = 'DISPLAY_AD_BUDGET_RATIO_ANSWER_STATUS_DESC',
  DisplayAdBudgetRatioAsc = 'DISPLAY_AD_BUDGET_RATIO_ASC',
  DisplayAdBudgetRatioDesc = 'DISPLAY_AD_BUDGET_RATIO_DESC',
  GoalCalculationDoneAsc = 'GOAL_CALCULATION_DONE_ASC',
  GoalCalculationDoneDesc = 'GOAL_CALCULATION_DONE_DESC',
  HavePlanSegmentAsc = 'HAVE_PLAN_SEGMENT_ASC',
  HavePlanSegmentDesc = 'HAVE_PLAN_SEGMENT_DESC',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  IsCorporateAsc = 'IS_CORPORATE_ASC',
  IsCorporateDesc = 'IS_CORPORATE_DESC',
  KaizenFrequencyAsc = 'KAIZEN_FREQUENCY_ASC',
  KaizenFrequencyDesc = 'KAIZEN_FREQUENCY_DESC',
  ListingAdBudgetRatioAsc = 'LISTING_AD_BUDGET_RATIO_ASC',
  ListingAdBudgetRatioDesc = 'LISTING_AD_BUDGET_RATIO_DESC',
  Natural = 'NATURAL',
  PermissionResultAdToolViewAsc = 'PERMISSION_RESULT_AD_TOOL_VIEW_ASC',
  PermissionResultAdToolViewDesc = 'PERMISSION_RESULT_AD_TOOL_VIEW_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
  ProductIdAsc = 'PRODUCT_ID_ASC',
  ProductIdDesc = 'PRODUCT_ID_DESC',
  SegmentDeliveryMethodAsc = 'SEGMENT_DELIVERY_METHOD_ASC',
  SegmentDeliveryMethodDesc = 'SEGMENT_DELIVERY_METHOD_DESC',
  SegmentMaterialsAnswerStatusAsc = 'SEGMENT_MATERIALS_ANSWER_STATUS_ASC',
  SegmentMaterialsAnswerStatusDesc = 'SEGMENT_MATERIALS_ANSWER_STATUS_DESC',
  SegmentMaterialsFilesAsc = 'SEGMENT_MATERIALS_FILES_ASC',
  SegmentMaterialsFilesDesc = 'SEGMENT_MATERIALS_FILES_DESC',
  SegmentMaterialsFileKeysAsc = 'SEGMENT_MATERIALS_FILE_KEYS_ASC',
  SegmentMaterialsFileKeysDesc = 'SEGMENT_MATERIALS_FILE_KEYS_DESC',
  UpdatedAtAsc = 'UPDATED_AT_ASC',
  UpdatedAtDesc = 'UPDATED_AT_DESC',
  WithOtherAnalysisToolAsc = 'WITH_OTHER_ANALYSIS_TOOL_ASC',
  WithOtherAnalysisToolDesc = 'WITH_OTHER_ANALYSIS_TOOL_DESC'
}

/** The root mutation type which contains root level fields which mutate data. */
export type Mutation = {
  __typename?: 'Mutation';
  /** Creates a single `AlembicVersion`. */
  createAlembicVersion?: Maybe<CreateAlembicVersionPayload>;
  /** Creates a single `Company`. */
  createCompany?: Maybe<CreateCompanyPayload>;
  /** Creates a single `CompanyInfo`. */
  createCompanyInfo?: Maybe<CreateCompanyInfoPayload>;
  /** Creates a single `CustomerGroup`. */
  createCustomerGroup?: Maybe<CreateCustomerGroupPayload>;
  /** Creates a single `MarketingActivityInformation`. */
  createMarketingActivityInformation?: Maybe<CreateMarketingActivityInformationPayload>;
  /** Creates a single `Notification`. */
  createNotification?: Maybe<CreateNotificationPayload>;
  /** Creates a single `OperatorGroup`. */
  createOperatorGroup?: Maybe<CreateOperatorGroupPayload>;
  /** Creates a single `OperatorName`. */
  createOperatorName?: Maybe<CreateOperatorNamePayload>;
  /** Creates a single `Opportunity`. */
  createOpportunity?: Maybe<CreateOpportunityPayload>;
  /** Creates a single `Product`. */
  createProduct?: Maybe<CreateProductPayload>;
  /** Creates a single `ProductInformation`. */
  createProductInformation?: Maybe<CreateProductInformationPayload>;
  /** Creates a single `RequestContent`. */
  createRequestContent?: Maybe<CreateRequestContentPayload>;
  /** Creates a single `UserInformationMemo`. */
  createUserInformationMemo?: Maybe<CreateUserInformationMemoPayload>;
  /** Deletes a single `AlembicVersion` using its globally unique id. */
  deleteAlembicVersion?: Maybe<DeleteAlembicVersionPayload>;
  /** Deletes a single `AlembicVersion` using a unique key. */
  deleteAlembicVersionByVersionNum?: Maybe<DeleteAlembicVersionPayload>;
  /** Deletes a single `Company` using its globally unique id. */
  deleteCompany?: Maybe<DeleteCompanyPayload>;
  /** Deletes a single `Company` using a unique key. */
  deleteCompanyById?: Maybe<DeleteCompanyPayload>;
  /** Deletes a single `CompanyInfo` using its globally unique id. */
  deleteCompanyInfo?: Maybe<DeleteCompanyInfoPayload>;
  /** Deletes a single `CompanyInfo` using a unique key. */
  deleteCompanyInfoById?: Maybe<DeleteCompanyInfoPayload>;
  /** Deletes a single `CustomerGroup` using its globally unique id. */
  deleteCustomerGroup?: Maybe<DeleteCustomerGroupPayload>;
  /** Deletes a single `CustomerGroup` using a unique key. */
  deleteCustomerGroupById?: Maybe<DeleteCustomerGroupPayload>;
  /** Deletes a single `CustomerGroup` using a unique key. */
  deleteCustomerGroupByOpportunityIdAndSfContactId?: Maybe<DeleteCustomerGroupPayload>;
  /** Deletes a single `MarketingActivityInformation` using its globally unique id. */
  deleteMarketingActivityInformation?: Maybe<DeleteMarketingActivityInformationPayload>;
  /** Deletes a single `MarketingActivityInformation` using a unique key. */
  deleteMarketingActivityInformationById?: Maybe<DeleteMarketingActivityInformationPayload>;
  /** Deletes a single `Notification` using its globally unique id. */
  deleteNotification?: Maybe<DeleteNotificationPayload>;
  /** Deletes a single `Notification` using a unique key. */
  deleteNotificationById?: Maybe<DeleteNotificationPayload>;
  /** Deletes a single `OperatorGroup` using its globally unique id. */
  deleteOperatorGroup?: Maybe<DeleteOperatorGroupPayload>;
  /** Deletes a single `OperatorGroup` using a unique key. */
  deleteOperatorGroupById?: Maybe<DeleteOperatorGroupPayload>;
  /** Deletes a single `OperatorName` using its globally unique id. */
  deleteOperatorName?: Maybe<DeleteOperatorNamePayload>;
  /** Deletes a single `OperatorName` using a unique key. */
  deleteOperatorNameByEmail?: Maybe<DeleteOperatorNamePayload>;
  /** Deletes a single `Opportunity` using its globally unique id. */
  deleteOpportunity?: Maybe<DeleteOpportunityPayload>;
  /** Deletes a single `Opportunity` using a unique key. */
  deleteOpportunityById?: Maybe<DeleteOpportunityPayload>;
  /** Deletes a single `Product` using its globally unique id. */
  deleteProduct?: Maybe<DeleteProductPayload>;
  /** Deletes a single `Product` using a unique key. */
  deleteProductByCompanyIdAndProductName?: Maybe<DeleteProductPayload>;
  /** Deletes a single `Product` using a unique key. */
  deleteProductById?: Maybe<DeleteProductPayload>;
  /** Deletes a single `ProductInformation` using its globally unique id. */
  deleteProductInformation?: Maybe<DeleteProductInformationPayload>;
  /** Deletes a single `ProductInformation` using a unique key. */
  deleteProductInformationById?: Maybe<DeleteProductInformationPayload>;
  /** Deletes a single `RequestContent` using its globally unique id. */
  deleteRequestContent?: Maybe<DeleteRequestContentPayload>;
  /** Deletes a single `RequestContent` using a unique key. */
  deleteRequestContentById?: Maybe<DeleteRequestContentPayload>;
  /** Deletes a single `UserInformationMemo` using its globally unique id. */
  deleteUserInformationMemo?: Maybe<DeleteUserInformationMemoPayload>;
  /** Deletes a single `UserInformationMemo` using a unique key. */
  deleteUserInformationMemoById?: Maybe<DeleteUserInformationMemoPayload>;
  /** Updates a single `AlembicVersion` using its globally unique id and a patch. */
  updateAlembicVersion?: Maybe<UpdateAlembicVersionPayload>;
  /** Updates a single `AlembicVersion` using a unique key and a patch. */
  updateAlembicVersionByVersionNum?: Maybe<UpdateAlembicVersionPayload>;
  /** Updates a single `Company` using its globally unique id and a patch. */
  updateCompany?: Maybe<UpdateCompanyPayload>;
  /** Updates a single `Company` using a unique key and a patch. */
  updateCompanyById?: Maybe<UpdateCompanyPayload>;
  /** Updates a single `CompanyInfo` using its globally unique id and a patch. */
  updateCompanyInfo?: Maybe<UpdateCompanyInfoPayload>;
  /** Updates a single `CompanyInfo` using a unique key and a patch. */
  updateCompanyInfoById?: Maybe<UpdateCompanyInfoPayload>;
  /** Updates a single `CustomerGroup` using its globally unique id and a patch. */
  updateCustomerGroup?: Maybe<UpdateCustomerGroupPayload>;
  /** Updates a single `CustomerGroup` using a unique key and a patch. */
  updateCustomerGroupById?: Maybe<UpdateCustomerGroupPayload>;
  /** Updates a single `CustomerGroup` using a unique key and a patch. */
  updateCustomerGroupByOpportunityIdAndSfContactId?: Maybe<UpdateCustomerGroupPayload>;
  /** Updates a single `MarketingActivityInformation` using its globally unique id and a patch. */
  updateMarketingActivityInformation?: Maybe<UpdateMarketingActivityInformationPayload>;
  /** Updates a single `MarketingActivityInformation` using a unique key and a patch. */
  updateMarketingActivityInformationById?: Maybe<UpdateMarketingActivityInformationPayload>;
  /** Updates a single `Notification` using its globally unique id and a patch. */
  updateNotification?: Maybe<UpdateNotificationPayload>;
  /** Updates a single `Notification` using a unique key and a patch. */
  updateNotificationById?: Maybe<UpdateNotificationPayload>;
  /** Updates a single `OperatorGroup` using its globally unique id and a patch. */
  updateOperatorGroup?: Maybe<UpdateOperatorGroupPayload>;
  /** Updates a single `OperatorGroup` using a unique key and a patch. */
  updateOperatorGroupById?: Maybe<UpdateOperatorGroupPayload>;
  /** Updates a single `OperatorName` using its globally unique id and a patch. */
  updateOperatorName?: Maybe<UpdateOperatorNamePayload>;
  /** Updates a single `OperatorName` using a unique key and a patch. */
  updateOperatorNameByEmail?: Maybe<UpdateOperatorNamePayload>;
  /** Updates a single `Opportunity` using its globally unique id and a patch. */
  updateOpportunity?: Maybe<UpdateOpportunityPayload>;
  /** Updates a single `Opportunity` using a unique key and a patch. */
  updateOpportunityById?: Maybe<UpdateOpportunityPayload>;
  /** Updates a single `Product` using its globally unique id and a patch. */
  updateProduct?: Maybe<UpdateProductPayload>;
  /** Updates a single `Product` using a unique key and a patch. */
  updateProductByCompanyIdAndProductName?: Maybe<UpdateProductPayload>;
  /** Updates a single `Product` using a unique key and a patch. */
  updateProductById?: Maybe<UpdateProductPayload>;
  /** Updates a single `ProductInformation` using its globally unique id and a patch. */
  updateProductInformation?: Maybe<UpdateProductInformationPayload>;
  /** Updates a single `ProductInformation` using a unique key and a patch. */
  updateProductInformationById?: Maybe<UpdateProductInformationPayload>;
  /** Updates a single `RequestContent` using its globally unique id and a patch. */
  updateRequestContent?: Maybe<UpdateRequestContentPayload>;
  /** Updates a single `RequestContent` using a unique key and a patch. */
  updateRequestContentById?: Maybe<UpdateRequestContentPayload>;
  /** Updates a single `UserInformationMemo` using its globally unique id and a patch. */
  updateUserInformationMemo?: Maybe<UpdateUserInformationMemoPayload>;
  /** Updates a single `UserInformationMemo` using a unique key and a patch. */
  updateUserInformationMemoById?: Maybe<UpdateUserInformationMemoPayload>;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateAlembicVersionArgs = {
  input: CreateAlembicVersionInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateCompanyArgs = {
  input: CreateCompanyInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateCompanyInfoArgs = {
  input: CreateCompanyInfoInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateCustomerGroupArgs = {
  input: CreateCustomerGroupInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateMarketingActivityInformationArgs = {
  input: CreateMarketingActivityInformationInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateNotificationArgs = {
  input: CreateNotificationInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateOperatorGroupArgs = {
  input: CreateOperatorGroupInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateOperatorNameArgs = {
  input: CreateOperatorNameInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateOpportunityArgs = {
  input: CreateOpportunityInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateProductArgs = {
  input: CreateProductInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateProductInformationArgs = {
  input: CreateProductInformationInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateRequestContentArgs = {
  input: CreateRequestContentInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateUserInformationMemoArgs = {
  input: CreateUserInformationMemoInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteAlembicVersionArgs = {
  input: DeleteAlembicVersionInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteAlembicVersionByVersionNumArgs = {
  input: DeleteAlembicVersionByVersionNumInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteCompanyArgs = {
  input: DeleteCompanyInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteCompanyByIdArgs = {
  input: DeleteCompanyByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteCompanyInfoArgs = {
  input: DeleteCompanyInfoInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteCompanyInfoByIdArgs = {
  input: DeleteCompanyInfoByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteCustomerGroupArgs = {
  input: DeleteCustomerGroupInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteCustomerGroupByIdArgs = {
  input: DeleteCustomerGroupByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteCustomerGroupByOpportunityIdAndSfContactIdArgs = {
  input: DeleteCustomerGroupByOpportunityIdAndSfContactIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteMarketingActivityInformationArgs = {
  input: DeleteMarketingActivityInformationInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteMarketingActivityInformationByIdArgs = {
  input: DeleteMarketingActivityInformationByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteNotificationArgs = {
  input: DeleteNotificationInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteNotificationByIdArgs = {
  input: DeleteNotificationByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteOperatorGroupArgs = {
  input: DeleteOperatorGroupInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteOperatorGroupByIdArgs = {
  input: DeleteOperatorGroupByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteOperatorNameArgs = {
  input: DeleteOperatorNameInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteOperatorNameByEmailArgs = {
  input: DeleteOperatorNameByEmailInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteOpportunityArgs = {
  input: DeleteOpportunityInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteOpportunityByIdArgs = {
  input: DeleteOpportunityByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteProductArgs = {
  input: DeleteProductInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteProductByCompanyIdAndProductNameArgs = {
  input: DeleteProductByCompanyIdAndProductNameInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteProductByIdArgs = {
  input: DeleteProductByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteProductInformationArgs = {
  input: DeleteProductInformationInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteProductInformationByIdArgs = {
  input: DeleteProductInformationByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteRequestContentArgs = {
  input: DeleteRequestContentInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteRequestContentByIdArgs = {
  input: DeleteRequestContentByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteUserInformationMemoArgs = {
  input: DeleteUserInformationMemoInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteUserInformationMemoByIdArgs = {
  input: DeleteUserInformationMemoByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateAlembicVersionArgs = {
  input: UpdateAlembicVersionInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateAlembicVersionByVersionNumArgs = {
  input: UpdateAlembicVersionByVersionNumInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateCompanyArgs = {
  input: UpdateCompanyInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateCompanyByIdArgs = {
  input: UpdateCompanyByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateCompanyInfoArgs = {
  input: UpdateCompanyInfoInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateCompanyInfoByIdArgs = {
  input: UpdateCompanyInfoByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateCustomerGroupArgs = {
  input: UpdateCustomerGroupInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateCustomerGroupByIdArgs = {
  input: UpdateCustomerGroupByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateCustomerGroupByOpportunityIdAndSfContactIdArgs = {
  input: UpdateCustomerGroupByOpportunityIdAndSfContactIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateMarketingActivityInformationArgs = {
  input: UpdateMarketingActivityInformationInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateMarketingActivityInformationByIdArgs = {
  input: UpdateMarketingActivityInformationByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateNotificationArgs = {
  input: UpdateNotificationInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateNotificationByIdArgs = {
  input: UpdateNotificationByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateOperatorGroupArgs = {
  input: UpdateOperatorGroupInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateOperatorGroupByIdArgs = {
  input: UpdateOperatorGroupByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateOperatorNameArgs = {
  input: UpdateOperatorNameInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateOperatorNameByEmailArgs = {
  input: UpdateOperatorNameByEmailInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateOpportunityArgs = {
  input: UpdateOpportunityInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateOpportunityByIdArgs = {
  input: UpdateOpportunityByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateProductArgs = {
  input: UpdateProductInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateProductByCompanyIdAndProductNameArgs = {
  input: UpdateProductByCompanyIdAndProductNameInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateProductByIdArgs = {
  input: UpdateProductByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateProductInformationArgs = {
  input: UpdateProductInformationInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateProductInformationByIdArgs = {
  input: UpdateProductInformationByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateRequestContentArgs = {
  input: UpdateRequestContentInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateRequestContentByIdArgs = {
  input: UpdateRequestContentByIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateUserInformationMemoArgs = {
  input: UpdateUserInformationMemoInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateUserInformationMemoByIdArgs = {
  input: UpdateUserInformationMemoByIdInput;
};

/** An object with a globally unique `ID`. */
export type Node = {
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID']['output'];
};

/** お知らせテーブル */
export type Notification = Node & {
  __typename?: 'Notification';
  createdAt?: Maybe<Scalars['Datetime']['output']>;
  /** アイコン */
  icon: Scalars['String']['output'];
  /** お知らせID */
  id: Scalars['UUID']['output'];
  /** ページリンク */
  link?: Maybe<Scalars['String']['output']>;
  /** メッセージ */
  message: Scalars['String']['output'];
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID']['output'];
  /** 既読フラグ */
  read?: Maybe<Scalars['Boolean']['output']>;
  /** 対象者 */
  target: Scalars['String']['output'];
  updatedAt?: Maybe<Scalars['Datetime']['output']>;
};

/**
 * A condition to be used against `Notification` object types. All fields are
 * tested for equality and combined with a logical ‘and.’
 */
export type NotificationCondition = {
  /** Checks for equality with the object’s `createdAt` field. */
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** Checks for equality with the object’s `icon` field. */
  icon?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `id` field. */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `link` field. */
  link?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `message` field. */
  message?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `read` field. */
  read?: InputMaybe<Scalars['Boolean']['input']>;
  /** Checks for equality with the object’s `target` field. */
  target?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** A filter to be used against `Notification` object types. All fields are combined with a logical ‘and.’ */
export type NotificationFilter = {
  /** Checks for all expressions in this list. */
  and?: InputMaybe<Array<NotificationFilter>>;
  /** Filter by the object’s `createdAt` field. */
  createdAt?: InputMaybe<DatetimeFilter>;
  /** Filter by the object’s `icon` field. */
  icon?: InputMaybe<StringFilter>;
  /** Filter by the object’s `id` field. */
  id?: InputMaybe<UuidFilter>;
  /** Filter by the object’s `link` field. */
  link?: InputMaybe<StringFilter>;
  /** Filter by the object’s `message` field. */
  message?: InputMaybe<StringFilter>;
  /** Negates the expression. */
  not?: InputMaybe<NotificationFilter>;
  /** Checks for any expressions in this list. */
  or?: InputMaybe<Array<NotificationFilter>>;
  /** Filter by the object’s `read` field. */
  read?: InputMaybe<BooleanFilter>;
  /** Filter by the object’s `target` field. */
  target?: InputMaybe<StringFilter>;
  /** Filter by the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<DatetimeFilter>;
};

/** An input for mutations affecting `Notification` */
export type NotificationInput = {
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** アイコン */
  icon: Scalars['String']['input'];
  /** お知らせID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** ページリンク */
  link?: InputMaybe<Scalars['String']['input']>;
  /** メッセージ */
  message: Scalars['String']['input'];
  /** 既読フラグ */
  read?: InputMaybe<Scalars['Boolean']['input']>;
  /** 対象者 */
  target: Scalars['String']['input'];
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** Represents an update to a `Notification`. Fields that are set will be updated. */
export type NotificationPatch = {
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** アイコン */
  icon?: InputMaybe<Scalars['String']['input']>;
  /** お知らせID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** ページリンク */
  link?: InputMaybe<Scalars['String']['input']>;
  /** メッセージ */
  message?: InputMaybe<Scalars['String']['input']>;
  /** 既読フラグ */
  read?: InputMaybe<Scalars['Boolean']['input']>;
  /** 対象者 */
  target?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** A connection to a list of `Notification` values. */
export type NotificationsConnection = {
  __typename?: 'NotificationsConnection';
  /** A list of edges which contains the `Notification` and cursor to aid in pagination. */
  edges: Array<NotificationsEdge>;
  /** A list of `Notification` objects. */
  nodes: Array<Maybe<Notification>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `Notification` you could get from the connection. */
  totalCount: Scalars['Int']['output'];
};

/** A `Notification` edge in the connection. */
export type NotificationsEdge = {
  __typename?: 'NotificationsEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']['output']>;
  /** The `Notification` at the end of the edge. */
  node?: Maybe<Notification>;
};

/** Methods to use when ordering `Notification`. */
export enum NotificationsOrderBy {
  CreatedAtAsc = 'CREATED_AT_ASC',
  CreatedAtDesc = 'CREATED_AT_DESC',
  IconAsc = 'ICON_ASC',
  IconDesc = 'ICON_DESC',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  LinkAsc = 'LINK_ASC',
  LinkDesc = 'LINK_DESC',
  MessageAsc = 'MESSAGE_ASC',
  MessageDesc = 'MESSAGE_DESC',
  Natural = 'NATURAL',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
  ReadAsc = 'READ_ASC',
  ReadDesc = 'READ_DESC',
  TargetAsc = 'TARGET_ASC',
  TargetDesc = 'TARGET_DESC',
  UpdatedAtAsc = 'UPDATED_AT_ASC',
  UpdatedAtDesc = 'UPDATED_AT_DESC'
}

/** 管理者グループ */
export type OperatorGroup = Node & {
  __typename?: 'OperatorGroup';
  /** 所属部署 */
  department: Scalars['String']['output'];
  /** 担当者メールアドレス */
  email: Scalars['String']['output'];
  /** 管理者グループID */
  id: Scalars['Int']['output'];
  name: Scalars['String']['output'];
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID']['output'];
  /** Reads a single `Opportunity` that is related to this `OperatorGroup`. */
  opportunityByOpportunityId?: Maybe<Opportunity>;
  /** 商談ID */
  opportunityId: Scalars['UUID']['output'];
  /** Cognito ID */
  username: Scalars['String']['output'];
};

/**
 * A condition to be used against `OperatorGroup` object types. All fields are
 * tested for equality and combined with a logical ‘and.’
 */
export type OperatorGroupCondition = {
  /** Checks for equality with the object’s `department` field. */
  department?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `email` field. */
  email?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `id` field. */
  id?: InputMaybe<Scalars['Int']['input']>;
  /** Checks for equality with the object’s `name` field. */
  name?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `opportunityId` field. */
  opportunityId?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `username` field. */
  username?: InputMaybe<Scalars['String']['input']>;
};

/** A filter to be used against `OperatorGroup` object types. All fields are combined with a logical ‘and.’ */
export type OperatorGroupFilter = {
  /** Checks for all expressions in this list. */
  and?: InputMaybe<Array<OperatorGroupFilter>>;
  /** Filter by the object’s `department` field. */
  department?: InputMaybe<StringFilter>;
  /** Filter by the object’s `email` field. */
  email?: InputMaybe<StringFilter>;
  /** Filter by the object’s `id` field. */
  id?: InputMaybe<IntFilter>;
  /** Filter by the object’s `name` field. */
  name?: InputMaybe<StringFilter>;
  /** Negates the expression. */
  not?: InputMaybe<OperatorGroupFilter>;
  /** Filter by the object’s `opportunityId` field. */
  opportunityId?: InputMaybe<UuidFilter>;
  /** Checks for any expressions in this list. */
  or?: InputMaybe<Array<OperatorGroupFilter>>;
  /** Filter by the object’s `username` field. */
  username?: InputMaybe<StringFilter>;
};

/** An input for mutations affecting `OperatorGroup` */
export type OperatorGroupInput = {
  /** 所属部署 */
  department: Scalars['String']['input'];
  /** 担当者メールアドレス */
  email: Scalars['String']['input'];
  /** 管理者グループID */
  id?: InputMaybe<Scalars['Int']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  /** 商談ID */
  opportunityId: Scalars['UUID']['input'];
  /** Cognito ID */
  username: Scalars['String']['input'];
};

/** Represents an update to a `OperatorGroup`. Fields that are set will be updated. */
export type OperatorGroupPatch = {
  /** 所属部署 */
  department?: InputMaybe<Scalars['String']['input']>;
  /** 担当者メールアドレス */
  email?: InputMaybe<Scalars['String']['input']>;
  /** 管理者グループID */
  id?: InputMaybe<Scalars['Int']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  /** 商談ID */
  opportunityId?: InputMaybe<Scalars['UUID']['input']>;
  /** Cognito ID */
  username?: InputMaybe<Scalars['String']['input']>;
};

/** A connection to a list of `OperatorGroup` values. */
export type OperatorGroupsConnection = {
  __typename?: 'OperatorGroupsConnection';
  /** A list of edges which contains the `OperatorGroup` and cursor to aid in pagination. */
  edges: Array<OperatorGroupsEdge>;
  /** A list of `OperatorGroup` objects. */
  nodes: Array<Maybe<OperatorGroup>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `OperatorGroup` you could get from the connection. */
  totalCount: Scalars['Int']['output'];
};

/** A `OperatorGroup` edge in the connection. */
export type OperatorGroupsEdge = {
  __typename?: 'OperatorGroupsEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']['output']>;
  /** The `OperatorGroup` at the end of the edge. */
  node?: Maybe<OperatorGroup>;
};

/** Methods to use when ordering `OperatorGroup`. */
export enum OperatorGroupsOrderBy {
  DepartmentAsc = 'DEPARTMENT_ASC',
  DepartmentDesc = 'DEPARTMENT_DESC',
  EmailAsc = 'EMAIL_ASC',
  EmailDesc = 'EMAIL_DESC',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  NameAsc = 'NAME_ASC',
  NameDesc = 'NAME_DESC',
  Natural = 'NATURAL',
  OpportunityIdAsc = 'OPPORTUNITY_ID_ASC',
  OpportunityIdDesc = 'OPPORTUNITY_ID_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
  UsernameAsc = 'USERNAME_ASC',
  UsernameDesc = 'USERNAME_DESC'
}

/** 管理者名テーブル */
export type OperatorName = Node & {
  __typename?: 'OperatorName';
  email: Scalars['String']['output'];
  name: Scalars['String']['output'];
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID']['output'];
};

/**
 * A condition to be used against `OperatorName` object types. All fields are
 * tested for equality and combined with a logical ‘and.’
 */
export type OperatorNameCondition = {
  /** Checks for equality with the object’s `email` field. */
  email?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `name` field. */
  name?: InputMaybe<Scalars['String']['input']>;
};

/** A filter to be used against `OperatorName` object types. All fields are combined with a logical ‘and.’ */
export type OperatorNameFilter = {
  /** Checks for all expressions in this list. */
  and?: InputMaybe<Array<OperatorNameFilter>>;
  /** Filter by the object’s `email` field. */
  email?: InputMaybe<StringFilter>;
  /** Filter by the object’s `name` field. */
  name?: InputMaybe<StringFilter>;
  /** Negates the expression. */
  not?: InputMaybe<OperatorNameFilter>;
  /** Checks for any expressions in this list. */
  or?: InputMaybe<Array<OperatorNameFilter>>;
};

/** An input for mutations affecting `OperatorName` */
export type OperatorNameInput = {
  email: Scalars['String']['input'];
  name: Scalars['String']['input'];
};

/** Represents an update to a `OperatorName`. Fields that are set will be updated. */
export type OperatorNamePatch = {
  email?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
};

/** A connection to a list of `OperatorName` values. */
export type OperatorNamesConnection = {
  __typename?: 'OperatorNamesConnection';
  /** A list of edges which contains the `OperatorName` and cursor to aid in pagination. */
  edges: Array<OperatorNamesEdge>;
  /** A list of `OperatorName` objects. */
  nodes: Array<Maybe<OperatorName>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `OperatorName` you could get from the connection. */
  totalCount: Scalars['Int']['output'];
};

/** A `OperatorName` edge in the connection. */
export type OperatorNamesEdge = {
  __typename?: 'OperatorNamesEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']['output']>;
  /** The `OperatorName` at the end of the edge. */
  node?: Maybe<OperatorName>;
};

/** Methods to use when ordering `OperatorName`. */
export enum OperatorNamesOrderBy {
  EmailAsc = 'EMAIL_ASC',
  EmailDesc = 'EMAIL_DESC',
  NameAsc = 'NAME_ASC',
  NameDesc = 'NAME_DESC',
  Natural = 'NATURAL',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC'
}

/** A connection to a list of `Opportunity` values. */
export type OpportunitiesConnection = {
  __typename?: 'OpportunitiesConnection';
  /** A list of edges which contains the `Opportunity` and cursor to aid in pagination. */
  edges: Array<OpportunitiesEdge>;
  /** A list of `Opportunity` objects. */
  nodes: Array<Maybe<Opportunity>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `Opportunity` you could get from the connection. */
  totalCount: Scalars['Int']['output'];
};

/** A `Opportunity` edge in the connection. */
export type OpportunitiesEdge = {
  __typename?: 'OpportunitiesEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']['output']>;
  /** The `Opportunity` at the end of the edge. */
  node?: Maybe<Opportunity>;
};

/** Methods to use when ordering `Opportunity`. */
export enum OpportunitiesOrderBy {
  CreatedAtAsc = 'CREATED_AT_ASC',
  CreatedAtDesc = 'CREATED_AT_DESC',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  Natural = 'NATURAL',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
  ProductIdAsc = 'PRODUCT_ID_ASC',
  ProductIdDesc = 'PRODUCT_ID_DESC',
  SfOpportunityIdAsc = 'SF_OPPORTUNITY_ID_ASC',
  SfOpportunityIdDesc = 'SF_OPPORTUNITY_ID_DESC',
  SfProjectIdAsc = 'SF_PROJECT_ID_ASC',
  SfProjectIdDesc = 'SF_PROJECT_ID_DESC',
  UpdatedAtAsc = 'UPDATED_AT_ASC',
  UpdatedAtDesc = 'UPDATED_AT_DESC'
}

/** 商談テーブル */
export type Opportunity = Node & {
  __typename?: 'Opportunity';
  createdAt?: Maybe<Scalars['Datetime']['output']>;
  /** Reads and enables pagination through a set of `CustomerGroup`. */
  customerGroupsByOpportunityId: CustomerGroupsConnection;
  /** 商談ID */
  id: Scalars['UUID']['output'];
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID']['output'];
  /** Reads and enables pagination through a set of `OperatorGroup`. */
  operatorGroupsByOpportunityId: OperatorGroupsConnection;
  /** Reads a single `Product` that is related to this `Opportunity`. */
  productByProductId?: Maybe<Product>;
  /** 商材ID */
  productId: Scalars['UUID']['output'];
  /** Salesforce商談ID */
  sfOpportunityId?: Maybe<Scalars['String']['output']>;
  /** Salesforce プロジェクトID */
  sfProjectId: Scalars['String']['output'];
  updatedAt?: Maybe<Scalars['Datetime']['output']>;
};


/** 商談テーブル */
export type OpportunityCustomerGroupsByOpportunityIdArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<CustomerGroupCondition>;
  filter?: InputMaybe<CustomerGroupFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CustomerGroupsOrderBy>>;
};


/** 商談テーブル */
export type OpportunityOperatorGroupsByOpportunityIdArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<OperatorGroupCondition>;
  filter?: InputMaybe<OperatorGroupFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<OperatorGroupsOrderBy>>;
};

/**
 * A condition to be used against `Opportunity` object types. All fields are tested
 * for equality and combined with a logical ‘and.’
 */
export type OpportunityCondition = {
  /** Checks for equality with the object’s `createdAt` field. */
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** Checks for equality with the object’s `id` field. */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `productId` field. */
  productId?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `sfOpportunityId` field. */
  sfOpportunityId?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `sfProjectId` field. */
  sfProjectId?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** A filter to be used against `Opportunity` object types. All fields are combined with a logical ‘and.’ */
export type OpportunityFilter = {
  /** Checks for all expressions in this list. */
  and?: InputMaybe<Array<OpportunityFilter>>;
  /** Filter by the object’s `createdAt` field. */
  createdAt?: InputMaybe<DatetimeFilter>;
  /** Filter by the object’s `id` field. */
  id?: InputMaybe<UuidFilter>;
  /** Negates the expression. */
  not?: InputMaybe<OpportunityFilter>;
  /** Checks for any expressions in this list. */
  or?: InputMaybe<Array<OpportunityFilter>>;
  /** Filter by the object’s `productId` field. */
  productId?: InputMaybe<UuidFilter>;
  /** Filter by the object’s `sfOpportunityId` field. */
  sfOpportunityId?: InputMaybe<StringFilter>;
  /** Filter by the object’s `sfProjectId` field. */
  sfProjectId?: InputMaybe<StringFilter>;
  /** Filter by the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<DatetimeFilter>;
};

/** An input for mutations affecting `Opportunity` */
export type OpportunityInput = {
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** 商談ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** 商材ID */
  productId: Scalars['UUID']['input'];
  /** Salesforce商談ID */
  sfOpportunityId?: InputMaybe<Scalars['String']['input']>;
  /** Salesforce プロジェクトID */
  sfProjectId: Scalars['String']['input'];
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** Represents an update to a `Opportunity`. Fields that are set will be updated. */
export type OpportunityPatch = {
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** 商談ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** 商材ID */
  productId?: InputMaybe<Scalars['UUID']['input']>;
  /** Salesforce商談ID */
  sfOpportunityId?: InputMaybe<Scalars['String']['input']>;
  /** Salesforce プロジェクトID */
  sfProjectId?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** Information about pagination in a connection. */
export type PageInfo = {
  __typename?: 'PageInfo';
  /** When paginating forwards, the cursor to continue. */
  endCursor?: Maybe<Scalars['Cursor']['output']>;
  /** When paginating forwards, are there more items? */
  hasNextPage: Scalars['Boolean']['output'];
  /** When paginating backwards, are there more items? */
  hasPreviousPage: Scalars['Boolean']['output'];
  /** When paginating backwards, the cursor to continue. */
  startCursor?: Maybe<Scalars['Cursor']['output']>;
};

/** 商材テーブル */
export type Product = Node & {
  __typename?: 'Product';
  /** Reads a single `Company` that is related to this `Product`. */
  companyByCompanyId?: Maybe<Company>;
  /** 企業ID */
  companyId: Scalars['UUID']['output'];
  createdAt?: Maybe<Scalars['Datetime']['output']>;
  /** ヒアリング完了フラグ */
  hearingComplete?: Maybe<Scalars['Boolean']['output']>;
  /** 商材ID */
  id: Scalars['UUID']['output'];
  /** Reads and enables pagination through a set of `MarketingActivityInformation`. */
  marketingActivityInformationsByProductId: MarketingActivityInformationsConnection;
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID']['output'];
  /** Reads and enables pagination through a set of `Opportunity`. */
  opportunitiesByProductId: OpportunitiesConnection;
  /** Reads and enables pagination through a set of `ProductInformation`. */
  productInformationsByProductId: ProductInformationsConnection;
  /** 商材名 */
  productName?: Maybe<Scalars['String']['output']>;
  /** Reads and enables pagination through a set of `RequestContent`. */
  requestContentsByProductId: RequestContentsConnection;
  updatedAt?: Maybe<Scalars['Datetime']['output']>;
  /** Reads and enables pagination through a set of `UserInformationMemo`. */
  userInformationMemosByProductId: UserInformationMemosConnection;
};


/** 商材テーブル */
export type ProductMarketingActivityInformationsByProductIdArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<MarketingActivityInformationCondition>;
  filter?: InputMaybe<MarketingActivityInformationFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<MarketingActivityInformationsOrderBy>>;
};


/** 商材テーブル */
export type ProductOpportunitiesByProductIdArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<OpportunityCondition>;
  filter?: InputMaybe<OpportunityFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<OpportunitiesOrderBy>>;
};


/** 商材テーブル */
export type ProductProductInformationsByProductIdArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<ProductInformationCondition>;
  filter?: InputMaybe<ProductInformationFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<ProductInformationsOrderBy>>;
};


/** 商材テーブル */
export type ProductRequestContentsByProductIdArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<RequestContentCondition>;
  filter?: InputMaybe<RequestContentFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<RequestContentsOrderBy>>;
};


/** 商材テーブル */
export type ProductUserInformationMemosByProductIdArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<UserInformationMemoCondition>;
  filter?: InputMaybe<UserInformationMemoFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<UserInformationMemosOrderBy>>;
};

/** A condition to be used against `Product` object types. All fields are tested for equality and combined with a logical ‘and.’ */
export type ProductCondition = {
  /** Checks for equality with the object’s `companyId` field. */
  companyId?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `createdAt` field. */
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** Checks for equality with the object’s `hearingComplete` field. */
  hearingComplete?: InputMaybe<Scalars['Boolean']['input']>;
  /** Checks for equality with the object’s `id` field. */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `productName` field. */
  productName?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** A filter to be used against `Product` object types. All fields are combined with a logical ‘and.’ */
export type ProductFilter = {
  /** Checks for all expressions in this list. */
  and?: InputMaybe<Array<ProductFilter>>;
  /** Filter by the object’s `companyId` field. */
  companyId?: InputMaybe<UuidFilter>;
  /** Filter by the object’s `createdAt` field. */
  createdAt?: InputMaybe<DatetimeFilter>;
  /** Filter by the object’s `hearingComplete` field. */
  hearingComplete?: InputMaybe<BooleanFilter>;
  /** Filter by the object’s `id` field. */
  id?: InputMaybe<UuidFilter>;
  /** Negates the expression. */
  not?: InputMaybe<ProductFilter>;
  /** Checks for any expressions in this list. */
  or?: InputMaybe<Array<ProductFilter>>;
  /** Filter by the object’s `productName` field. */
  productName?: InputMaybe<StringFilter>;
  /** Filter by the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<DatetimeFilter>;
};

/** 商材情報Pkey */
export type ProductInformation = Node & {
  __typename?: 'ProductInformation';
  adTypeHistory?: Maybe<Scalars['String']['output']>;
  adminCommentCampaignOffer?: Maybe<Scalars['String']['output']>;
  adminCommentCompetitors?: Maybe<Scalars['String']['output']>;
  adminCommentCorporate?: Maybe<Scalars['String']['output']>;
  adminCommentIndividual?: Maybe<Scalars['String']['output']>;
  adminCommentMainTarget?: Maybe<Scalars['String']['output']>;
  adminCommentPastCases?: Maybe<Scalars['String']['output']>;
  adminCommentPastProductions?: Maybe<Scalars['String']['output']>;
  adminCommentProductAchievement?: Maybe<Scalars['String']['output']>;
  adminCommentProductOverview?: Maybe<Scalars['String']['output']>;
  adminCommentTargetSupplement?: Maybe<Scalars['String']['output']>;
  articleLpUrl?: Maybe<Scalars['String']['output']>;
  campaignOffer?: Maybe<Scalars['String']['output']>;
  competitorAnswerStatus?: Maybe<Scalars['String']['output']>;
  corporateIndustry?: Maybe<Scalars['String']['output']>;
  corporateJobTitle?: Maybe<Scalars['String']['output']>;
  corporateProblem?: Maybe<Scalars['String']['output']>;
  createdAt?: Maybe<Scalars['Datetime']['output']>;
  customerMaterialsAnswerStatus?: Maybe<Scalars['String']['output']>;
  customerMaterialsFileKeys?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  customerMaterialsFiles?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  deviceImpressionRatio?: Maybe<Scalars['String']['output']>;
  existingCreativeAnswerStatus?: Maybe<Scalars['String']['output']>;
  existingCreativeFileKeys?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  existingCreativeFiles?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  existingCreativeUrl?: Maybe<Scalars['String']['output']>;
  failureCase?: Maybe<Scalars['String']['output']>;
  /** 商材情報ID */
  id: Scalars['UUID']['output'];
  individualAge?: Maybe<Scalars['String']['output']>;
  individualFamilyIncome?: Maybe<Scalars['String']['output']>;
  individualFamilyStructure?: Maybe<Scalars['String']['output']>;
  individualGender?: Maybe<Scalars['String']['output']>;
  individualHobby?: Maybe<Scalars['String']['output']>;
  individualHobbyExpense?: Maybe<Scalars['String']['output']>;
  individualInternetUsage?: Maybe<Scalars['String']['output']>;
  individualOccupation?: Maybe<Scalars['String']['output']>;
  individualOwnedDevice?: Maybe<Scalars['String']['output']>;
  individualProblem?: Maybe<Scalars['String']['output']>;
  individualResidence?: Maybe<Scalars['String']['output']>;
  individualSnsUsually?: Maybe<Scalars['String']['output']>;
  isCorporate?: Maybe<Scalars['Boolean']['output']>;
  mainTarget?: Maybe<Scalars['String']['output']>;
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID']['output'];
  productAchievement?: Maybe<Scalars['String']['output']>;
  productAnswerStatus?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Product` that is related to this `ProductInformation`. */
  productByProductId?: Maybe<Product>;
  /** プロダクトID */
  productId?: Maybe<Scalars['UUID']['output']>;
  productMaterialsFileKeys?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  productMaterialsFiles?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  productMaterialsUrl?: Maybe<Scalars['String']['output']>;
  productsFeatures?: Maybe<Scalars['String']['output']>;
  referencedProduct?: Maybe<Scalars['String']['output']>;
  similarProduct?: Maybe<Scalars['String']['output']>;
  subTargetExistence?: Maybe<Scalars['String']['output']>;
  successCase?: Maybe<Scalars['String']['output']>;
  superiorProduct?: Maybe<Scalars['String']['output']>;
  uniqueStrength?: Maybe<Scalars['String']['output']>;
  updatedAt?: Maybe<Scalars['Datetime']['output']>;
};

/**
 * A condition to be used against `ProductInformation` object types. All fields are
 * tested for equality and combined with a logical ‘and.’
 */
export type ProductInformationCondition = {
  /** Checks for equality with the object’s `adTypeHistory` field. */
  adTypeHistory?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentCampaignOffer` field. */
  adminCommentCampaignOffer?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentCompetitors` field. */
  adminCommentCompetitors?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentCorporate` field. */
  adminCommentCorporate?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentIndividual` field. */
  adminCommentIndividual?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentMainTarget` field. */
  adminCommentMainTarget?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentPastCases` field. */
  adminCommentPastCases?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentPastProductions` field. */
  adminCommentPastProductions?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentProductAchievement` field. */
  adminCommentProductAchievement?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentProductOverview` field. */
  adminCommentProductOverview?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentTargetSupplement` field. */
  adminCommentTargetSupplement?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `articleLpUrl` field. */
  articleLpUrl?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `campaignOffer` field. */
  campaignOffer?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `competitorAnswerStatus` field. */
  competitorAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `corporateIndustry` field. */
  corporateIndustry?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `corporateJobTitle` field. */
  corporateJobTitle?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `corporateProblem` field. */
  corporateProblem?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `createdAt` field. */
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** Checks for equality with the object’s `customerMaterialsAnswerStatus` field. */
  customerMaterialsAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `customerMaterialsFileKeys` field. */
  customerMaterialsFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Checks for equality with the object’s `customerMaterialsFiles` field. */
  customerMaterialsFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Checks for equality with the object’s `deviceImpressionRatio` field. */
  deviceImpressionRatio?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `existingCreativeAnswerStatus` field. */
  existingCreativeAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `existingCreativeFileKeys` field. */
  existingCreativeFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Checks for equality with the object’s `existingCreativeFiles` field. */
  existingCreativeFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Checks for equality with the object’s `existingCreativeUrl` field. */
  existingCreativeUrl?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `failureCase` field. */
  failureCase?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `id` field. */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `individualAge` field. */
  individualAge?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `individualFamilyIncome` field. */
  individualFamilyIncome?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `individualFamilyStructure` field. */
  individualFamilyStructure?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `individualGender` field. */
  individualGender?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `individualHobby` field. */
  individualHobby?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `individualHobbyExpense` field. */
  individualHobbyExpense?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `individualInternetUsage` field. */
  individualInternetUsage?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `individualOccupation` field. */
  individualOccupation?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `individualOwnedDevice` field. */
  individualOwnedDevice?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `individualProblem` field. */
  individualProblem?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `individualResidence` field. */
  individualResidence?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `individualSnsUsually` field. */
  individualSnsUsually?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `isCorporate` field. */
  isCorporate?: InputMaybe<Scalars['Boolean']['input']>;
  /** Checks for equality with the object’s `mainTarget` field. */
  mainTarget?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `productAchievement` field. */
  productAchievement?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `productAnswerStatus` field. */
  productAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `productId` field. */
  productId?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `productMaterialsFileKeys` field. */
  productMaterialsFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Checks for equality with the object’s `productMaterialsFiles` field. */
  productMaterialsFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Checks for equality with the object’s `productMaterialsUrl` field. */
  productMaterialsUrl?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `productsFeatures` field. */
  productsFeatures?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `referencedProduct` field. */
  referencedProduct?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `similarProduct` field. */
  similarProduct?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `subTargetExistence` field. */
  subTargetExistence?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `successCase` field. */
  successCase?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `superiorProduct` field. */
  superiorProduct?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `uniqueStrength` field. */
  uniqueStrength?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** A filter to be used against `ProductInformation` object types. All fields are combined with a logical ‘and.’ */
export type ProductInformationFilter = {
  /** Filter by the object’s `adTypeHistory` field. */
  adTypeHistory?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentCampaignOffer` field. */
  adminCommentCampaignOffer?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentCompetitors` field. */
  adminCommentCompetitors?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentCorporate` field. */
  adminCommentCorporate?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentIndividual` field. */
  adminCommentIndividual?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentMainTarget` field. */
  adminCommentMainTarget?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentPastCases` field. */
  adminCommentPastCases?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentPastProductions` field. */
  adminCommentPastProductions?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentProductAchievement` field. */
  adminCommentProductAchievement?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentProductOverview` field. */
  adminCommentProductOverview?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentTargetSupplement` field. */
  adminCommentTargetSupplement?: InputMaybe<StringFilter>;
  /** Checks for all expressions in this list. */
  and?: InputMaybe<Array<ProductInformationFilter>>;
  /** Filter by the object’s `articleLpUrl` field. */
  articleLpUrl?: InputMaybe<StringFilter>;
  /** Filter by the object’s `campaignOffer` field. */
  campaignOffer?: InputMaybe<StringFilter>;
  /** Filter by the object’s `competitorAnswerStatus` field. */
  competitorAnswerStatus?: InputMaybe<StringFilter>;
  /** Filter by the object’s `corporateIndustry` field. */
  corporateIndustry?: InputMaybe<StringFilter>;
  /** Filter by the object’s `corporateJobTitle` field. */
  corporateJobTitle?: InputMaybe<StringFilter>;
  /** Filter by the object’s `corporateProblem` field. */
  corporateProblem?: InputMaybe<StringFilter>;
  /** Filter by the object’s `createdAt` field. */
  createdAt?: InputMaybe<DatetimeFilter>;
  /** Filter by the object’s `customerMaterialsAnswerStatus` field. */
  customerMaterialsAnswerStatus?: InputMaybe<StringFilter>;
  /** Filter by the object’s `customerMaterialsFileKeys` field. */
  customerMaterialsFileKeys?: InputMaybe<StringListFilter>;
  /** Filter by the object’s `customerMaterialsFiles` field. */
  customerMaterialsFiles?: InputMaybe<StringListFilter>;
  /** Filter by the object’s `deviceImpressionRatio` field. */
  deviceImpressionRatio?: InputMaybe<StringFilter>;
  /** Filter by the object’s `existingCreativeAnswerStatus` field. */
  existingCreativeAnswerStatus?: InputMaybe<StringFilter>;
  /** Filter by the object’s `existingCreativeFileKeys` field. */
  existingCreativeFileKeys?: InputMaybe<StringListFilter>;
  /** Filter by the object’s `existingCreativeFiles` field. */
  existingCreativeFiles?: InputMaybe<StringListFilter>;
  /** Filter by the object’s `existingCreativeUrl` field. */
  existingCreativeUrl?: InputMaybe<StringFilter>;
  /** Filter by the object’s `failureCase` field. */
  failureCase?: InputMaybe<StringFilter>;
  /** Filter by the object’s `id` field. */
  id?: InputMaybe<UuidFilter>;
  /** Filter by the object’s `individualAge` field. */
  individualAge?: InputMaybe<StringFilter>;
  /** Filter by the object’s `individualFamilyIncome` field. */
  individualFamilyIncome?: InputMaybe<StringFilter>;
  /** Filter by the object’s `individualFamilyStructure` field. */
  individualFamilyStructure?: InputMaybe<StringFilter>;
  /** Filter by the object’s `individualGender` field. */
  individualGender?: InputMaybe<StringFilter>;
  /** Filter by the object’s `individualHobby` field. */
  individualHobby?: InputMaybe<StringFilter>;
  /** Filter by the object’s `individualHobbyExpense` field. */
  individualHobbyExpense?: InputMaybe<StringFilter>;
  /** Filter by the object’s `individualInternetUsage` field. */
  individualInternetUsage?: InputMaybe<StringFilter>;
  /** Filter by the object’s `individualOccupation` field. */
  individualOccupation?: InputMaybe<StringFilter>;
  /** Filter by the object’s `individualOwnedDevice` field. */
  individualOwnedDevice?: InputMaybe<StringFilter>;
  /** Filter by the object’s `individualProblem` field. */
  individualProblem?: InputMaybe<StringFilter>;
  /** Filter by the object’s `individualResidence` field. */
  individualResidence?: InputMaybe<StringFilter>;
  /** Filter by the object’s `individualSnsUsually` field. */
  individualSnsUsually?: InputMaybe<StringFilter>;
  /** Filter by the object’s `isCorporate` field. */
  isCorporate?: InputMaybe<BooleanFilter>;
  /** Filter by the object’s `mainTarget` field. */
  mainTarget?: InputMaybe<StringFilter>;
  /** Negates the expression. */
  not?: InputMaybe<ProductInformationFilter>;
  /** Checks for any expressions in this list. */
  or?: InputMaybe<Array<ProductInformationFilter>>;
  /** Filter by the object’s `productAchievement` field. */
  productAchievement?: InputMaybe<StringFilter>;
  /** Filter by the object’s `productAnswerStatus` field. */
  productAnswerStatus?: InputMaybe<StringFilter>;
  /** Filter by the object’s `productId` field. */
  productId?: InputMaybe<UuidFilter>;
  /** Filter by the object’s `productMaterialsFileKeys` field. */
  productMaterialsFileKeys?: InputMaybe<StringListFilter>;
  /** Filter by the object’s `productMaterialsFiles` field. */
  productMaterialsFiles?: InputMaybe<StringListFilter>;
  /** Filter by the object’s `productMaterialsUrl` field. */
  productMaterialsUrl?: InputMaybe<StringFilter>;
  /** Filter by the object’s `productsFeatures` field. */
  productsFeatures?: InputMaybe<StringFilter>;
  /** Filter by the object’s `referencedProduct` field. */
  referencedProduct?: InputMaybe<StringFilter>;
  /** Filter by the object’s `similarProduct` field. */
  similarProduct?: InputMaybe<StringFilter>;
  /** Filter by the object’s `subTargetExistence` field. */
  subTargetExistence?: InputMaybe<StringFilter>;
  /** Filter by the object’s `successCase` field. */
  successCase?: InputMaybe<StringFilter>;
  /** Filter by the object’s `superiorProduct` field. */
  superiorProduct?: InputMaybe<StringFilter>;
  /** Filter by the object’s `uniqueStrength` field. */
  uniqueStrength?: InputMaybe<StringFilter>;
  /** Filter by the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<DatetimeFilter>;
};

/** An input for mutations affecting `ProductInformation` */
export type ProductInformationInput = {
  adTypeHistory?: InputMaybe<Scalars['String']['input']>;
  adminCommentCampaignOffer?: InputMaybe<Scalars['String']['input']>;
  adminCommentCompetitors?: InputMaybe<Scalars['String']['input']>;
  adminCommentCorporate?: InputMaybe<Scalars['String']['input']>;
  adminCommentIndividual?: InputMaybe<Scalars['String']['input']>;
  adminCommentMainTarget?: InputMaybe<Scalars['String']['input']>;
  adminCommentPastCases?: InputMaybe<Scalars['String']['input']>;
  adminCommentPastProductions?: InputMaybe<Scalars['String']['input']>;
  adminCommentProductAchievement?: InputMaybe<Scalars['String']['input']>;
  adminCommentProductOverview?: InputMaybe<Scalars['String']['input']>;
  adminCommentTargetSupplement?: InputMaybe<Scalars['String']['input']>;
  articleLpUrl?: InputMaybe<Scalars['String']['input']>;
  campaignOffer?: InputMaybe<Scalars['String']['input']>;
  competitorAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  corporateIndustry?: InputMaybe<Scalars['String']['input']>;
  corporateJobTitle?: InputMaybe<Scalars['String']['input']>;
  corporateProblem?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  customerMaterialsAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  customerMaterialsFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  customerMaterialsFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  deviceImpressionRatio?: InputMaybe<Scalars['String']['input']>;
  existingCreativeAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  existingCreativeFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  existingCreativeFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  existingCreativeUrl?: InputMaybe<Scalars['String']['input']>;
  failureCase?: InputMaybe<Scalars['String']['input']>;
  /** 商材情報ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  individualAge?: InputMaybe<Scalars['String']['input']>;
  individualFamilyIncome?: InputMaybe<Scalars['String']['input']>;
  individualFamilyStructure?: InputMaybe<Scalars['String']['input']>;
  individualGender?: InputMaybe<Scalars['String']['input']>;
  individualHobby?: InputMaybe<Scalars['String']['input']>;
  individualHobbyExpense?: InputMaybe<Scalars['String']['input']>;
  individualInternetUsage?: InputMaybe<Scalars['String']['input']>;
  individualOccupation?: InputMaybe<Scalars['String']['input']>;
  individualOwnedDevice?: InputMaybe<Scalars['String']['input']>;
  individualProblem?: InputMaybe<Scalars['String']['input']>;
  individualResidence?: InputMaybe<Scalars['String']['input']>;
  individualSnsUsually?: InputMaybe<Scalars['String']['input']>;
  isCorporate?: InputMaybe<Scalars['Boolean']['input']>;
  mainTarget?: InputMaybe<Scalars['String']['input']>;
  productAchievement?: InputMaybe<Scalars['String']['input']>;
  productAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  /** プロダクトID */
  productId?: InputMaybe<Scalars['UUID']['input']>;
  productMaterialsFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  productMaterialsFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  productMaterialsUrl?: InputMaybe<Scalars['String']['input']>;
  productsFeatures?: InputMaybe<Scalars['String']['input']>;
  referencedProduct?: InputMaybe<Scalars['String']['input']>;
  similarProduct?: InputMaybe<Scalars['String']['input']>;
  subTargetExistence?: InputMaybe<Scalars['String']['input']>;
  successCase?: InputMaybe<Scalars['String']['input']>;
  superiorProduct?: InputMaybe<Scalars['String']['input']>;
  uniqueStrength?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** Represents an update to a `ProductInformation`. Fields that are set will be updated. */
export type ProductInformationPatch = {
  adTypeHistory?: InputMaybe<Scalars['String']['input']>;
  adminCommentCampaignOffer?: InputMaybe<Scalars['String']['input']>;
  adminCommentCompetitors?: InputMaybe<Scalars['String']['input']>;
  adminCommentCorporate?: InputMaybe<Scalars['String']['input']>;
  adminCommentIndividual?: InputMaybe<Scalars['String']['input']>;
  adminCommentMainTarget?: InputMaybe<Scalars['String']['input']>;
  adminCommentPastCases?: InputMaybe<Scalars['String']['input']>;
  adminCommentPastProductions?: InputMaybe<Scalars['String']['input']>;
  adminCommentProductAchievement?: InputMaybe<Scalars['String']['input']>;
  adminCommentProductOverview?: InputMaybe<Scalars['String']['input']>;
  adminCommentTargetSupplement?: InputMaybe<Scalars['String']['input']>;
  articleLpUrl?: InputMaybe<Scalars['String']['input']>;
  campaignOffer?: InputMaybe<Scalars['String']['input']>;
  competitorAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  corporateIndustry?: InputMaybe<Scalars['String']['input']>;
  corporateJobTitle?: InputMaybe<Scalars['String']['input']>;
  corporateProblem?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  customerMaterialsAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  customerMaterialsFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  customerMaterialsFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  deviceImpressionRatio?: InputMaybe<Scalars['String']['input']>;
  existingCreativeAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  existingCreativeFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  existingCreativeFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  existingCreativeUrl?: InputMaybe<Scalars['String']['input']>;
  failureCase?: InputMaybe<Scalars['String']['input']>;
  /** 商材情報ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  individualAge?: InputMaybe<Scalars['String']['input']>;
  individualFamilyIncome?: InputMaybe<Scalars['String']['input']>;
  individualFamilyStructure?: InputMaybe<Scalars['String']['input']>;
  individualGender?: InputMaybe<Scalars['String']['input']>;
  individualHobby?: InputMaybe<Scalars['String']['input']>;
  individualHobbyExpense?: InputMaybe<Scalars['String']['input']>;
  individualInternetUsage?: InputMaybe<Scalars['String']['input']>;
  individualOccupation?: InputMaybe<Scalars['String']['input']>;
  individualOwnedDevice?: InputMaybe<Scalars['String']['input']>;
  individualProblem?: InputMaybe<Scalars['String']['input']>;
  individualResidence?: InputMaybe<Scalars['String']['input']>;
  individualSnsUsually?: InputMaybe<Scalars['String']['input']>;
  isCorporate?: InputMaybe<Scalars['Boolean']['input']>;
  mainTarget?: InputMaybe<Scalars['String']['input']>;
  productAchievement?: InputMaybe<Scalars['String']['input']>;
  productAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  /** プロダクトID */
  productId?: InputMaybe<Scalars['UUID']['input']>;
  productMaterialsFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  productMaterialsFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  productMaterialsUrl?: InputMaybe<Scalars['String']['input']>;
  productsFeatures?: InputMaybe<Scalars['String']['input']>;
  referencedProduct?: InputMaybe<Scalars['String']['input']>;
  similarProduct?: InputMaybe<Scalars['String']['input']>;
  subTargetExistence?: InputMaybe<Scalars['String']['input']>;
  successCase?: InputMaybe<Scalars['String']['input']>;
  superiorProduct?: InputMaybe<Scalars['String']['input']>;
  uniqueStrength?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** A connection to a list of `ProductInformation` values. */
export type ProductInformationsConnection = {
  __typename?: 'ProductInformationsConnection';
  /** A list of edges which contains the `ProductInformation` and cursor to aid in pagination. */
  edges: Array<ProductInformationsEdge>;
  /** A list of `ProductInformation` objects. */
  nodes: Array<Maybe<ProductInformation>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `ProductInformation` you could get from the connection. */
  totalCount: Scalars['Int']['output'];
};

/** A `ProductInformation` edge in the connection. */
export type ProductInformationsEdge = {
  __typename?: 'ProductInformationsEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']['output']>;
  /** The `ProductInformation` at the end of the edge. */
  node?: Maybe<ProductInformation>;
};

/** Methods to use when ordering `ProductInformation`. */
export enum ProductInformationsOrderBy {
  AdminCommentCampaignOfferAsc = 'ADMIN_COMMENT_CAMPAIGN_OFFER_ASC',
  AdminCommentCampaignOfferDesc = 'ADMIN_COMMENT_CAMPAIGN_OFFER_DESC',
  AdminCommentCompetitorsAsc = 'ADMIN_COMMENT_COMPETITORS_ASC',
  AdminCommentCompetitorsDesc = 'ADMIN_COMMENT_COMPETITORS_DESC',
  AdminCommentCorporateAsc = 'ADMIN_COMMENT_CORPORATE_ASC',
  AdminCommentCorporateDesc = 'ADMIN_COMMENT_CORPORATE_DESC',
  AdminCommentIndividualAsc = 'ADMIN_COMMENT_INDIVIDUAL_ASC',
  AdminCommentIndividualDesc = 'ADMIN_COMMENT_INDIVIDUAL_DESC',
  AdminCommentMainTargetAsc = 'ADMIN_COMMENT_MAIN_TARGET_ASC',
  AdminCommentMainTargetDesc = 'ADMIN_COMMENT_MAIN_TARGET_DESC',
  AdminCommentPastCasesAsc = 'ADMIN_COMMENT_PAST_CASES_ASC',
  AdminCommentPastCasesDesc = 'ADMIN_COMMENT_PAST_CASES_DESC',
  AdminCommentPastProductionsAsc = 'ADMIN_COMMENT_PAST_PRODUCTIONS_ASC',
  AdminCommentPastProductionsDesc = 'ADMIN_COMMENT_PAST_PRODUCTIONS_DESC',
  AdminCommentProductAchievementAsc = 'ADMIN_COMMENT_PRODUCT_ACHIEVEMENT_ASC',
  AdminCommentProductAchievementDesc = 'ADMIN_COMMENT_PRODUCT_ACHIEVEMENT_DESC',
  AdminCommentProductOverviewAsc = 'ADMIN_COMMENT_PRODUCT_OVERVIEW_ASC',
  AdminCommentProductOverviewDesc = 'ADMIN_COMMENT_PRODUCT_OVERVIEW_DESC',
  AdminCommentTargetSupplementAsc = 'ADMIN_COMMENT_TARGET_SUPPLEMENT_ASC',
  AdminCommentTargetSupplementDesc = 'ADMIN_COMMENT_TARGET_SUPPLEMENT_DESC',
  AdTypeHistoryAsc = 'AD_TYPE_HISTORY_ASC',
  AdTypeHistoryDesc = 'AD_TYPE_HISTORY_DESC',
  ArticleLpUrlAsc = 'ARTICLE_LP_URL_ASC',
  ArticleLpUrlDesc = 'ARTICLE_LP_URL_DESC',
  CampaignOfferAsc = 'CAMPAIGN_OFFER_ASC',
  CampaignOfferDesc = 'CAMPAIGN_OFFER_DESC',
  CompetitorAnswerStatusAsc = 'COMPETITOR_ANSWER_STATUS_ASC',
  CompetitorAnswerStatusDesc = 'COMPETITOR_ANSWER_STATUS_DESC',
  CorporateIndustryAsc = 'CORPORATE_INDUSTRY_ASC',
  CorporateIndustryDesc = 'CORPORATE_INDUSTRY_DESC',
  CorporateJobTitleAsc = 'CORPORATE_JOB_TITLE_ASC',
  CorporateJobTitleDesc = 'CORPORATE_JOB_TITLE_DESC',
  CorporateProblemAsc = 'CORPORATE_PROBLEM_ASC',
  CorporateProblemDesc = 'CORPORATE_PROBLEM_DESC',
  CreatedAtAsc = 'CREATED_AT_ASC',
  CreatedAtDesc = 'CREATED_AT_DESC',
  CustomerMaterialsAnswerStatusAsc = 'CUSTOMER_MATERIALS_ANSWER_STATUS_ASC',
  CustomerMaterialsAnswerStatusDesc = 'CUSTOMER_MATERIALS_ANSWER_STATUS_DESC',
  CustomerMaterialsFilesAsc = 'CUSTOMER_MATERIALS_FILES_ASC',
  CustomerMaterialsFilesDesc = 'CUSTOMER_MATERIALS_FILES_DESC',
  CustomerMaterialsFileKeysAsc = 'CUSTOMER_MATERIALS_FILE_KEYS_ASC',
  CustomerMaterialsFileKeysDesc = 'CUSTOMER_MATERIALS_FILE_KEYS_DESC',
  DeviceImpressionRatioAsc = 'DEVICE_IMPRESSION_RATIO_ASC',
  DeviceImpressionRatioDesc = 'DEVICE_IMPRESSION_RATIO_DESC',
  ExistingCreativeAnswerStatusAsc = 'EXISTING_CREATIVE_ANSWER_STATUS_ASC',
  ExistingCreativeAnswerStatusDesc = 'EXISTING_CREATIVE_ANSWER_STATUS_DESC',
  ExistingCreativeFilesAsc = 'EXISTING_CREATIVE_FILES_ASC',
  ExistingCreativeFilesDesc = 'EXISTING_CREATIVE_FILES_DESC',
  ExistingCreativeFileKeysAsc = 'EXISTING_CREATIVE_FILE_KEYS_ASC',
  ExistingCreativeFileKeysDesc = 'EXISTING_CREATIVE_FILE_KEYS_DESC',
  ExistingCreativeUrlAsc = 'EXISTING_CREATIVE_URL_ASC',
  ExistingCreativeUrlDesc = 'EXISTING_CREATIVE_URL_DESC',
  FailureCaseAsc = 'FAILURE_CASE_ASC',
  FailureCaseDesc = 'FAILURE_CASE_DESC',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  IndividualAgeAsc = 'INDIVIDUAL_AGE_ASC',
  IndividualAgeDesc = 'INDIVIDUAL_AGE_DESC',
  IndividualFamilyIncomeAsc = 'INDIVIDUAL_FAMILY_INCOME_ASC',
  IndividualFamilyIncomeDesc = 'INDIVIDUAL_FAMILY_INCOME_DESC',
  IndividualFamilyStructureAsc = 'INDIVIDUAL_FAMILY_STRUCTURE_ASC',
  IndividualFamilyStructureDesc = 'INDIVIDUAL_FAMILY_STRUCTURE_DESC',
  IndividualGenderAsc = 'INDIVIDUAL_GENDER_ASC',
  IndividualGenderDesc = 'INDIVIDUAL_GENDER_DESC',
  IndividualHobbyAsc = 'INDIVIDUAL_HOBBY_ASC',
  IndividualHobbyDesc = 'INDIVIDUAL_HOBBY_DESC',
  IndividualHobbyExpenseAsc = 'INDIVIDUAL_HOBBY_EXPENSE_ASC',
  IndividualHobbyExpenseDesc = 'INDIVIDUAL_HOBBY_EXPENSE_DESC',
  IndividualInternetUsageAsc = 'INDIVIDUAL_INTERNET_USAGE_ASC',
  IndividualInternetUsageDesc = 'INDIVIDUAL_INTERNET_USAGE_DESC',
  IndividualOccupationAsc = 'INDIVIDUAL_OCCUPATION_ASC',
  IndividualOccupationDesc = 'INDIVIDUAL_OCCUPATION_DESC',
  IndividualOwnedDeviceAsc = 'INDIVIDUAL_OWNED_DEVICE_ASC',
  IndividualOwnedDeviceDesc = 'INDIVIDUAL_OWNED_DEVICE_DESC',
  IndividualProblemAsc = 'INDIVIDUAL_PROBLEM_ASC',
  IndividualProblemDesc = 'INDIVIDUAL_PROBLEM_DESC',
  IndividualResidenceAsc = 'INDIVIDUAL_RESIDENCE_ASC',
  IndividualResidenceDesc = 'INDIVIDUAL_RESIDENCE_DESC',
  IndividualSnsUsuallyAsc = 'INDIVIDUAL_SNS_USUALLY_ASC',
  IndividualSnsUsuallyDesc = 'INDIVIDUAL_SNS_USUALLY_DESC',
  IsCorporateAsc = 'IS_CORPORATE_ASC',
  IsCorporateDesc = 'IS_CORPORATE_DESC',
  MainTargetAsc = 'MAIN_TARGET_ASC',
  MainTargetDesc = 'MAIN_TARGET_DESC',
  Natural = 'NATURAL',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
  ProductsFeaturesAsc = 'PRODUCTS_FEATURES_ASC',
  ProductsFeaturesDesc = 'PRODUCTS_FEATURES_DESC',
  ProductAchievementAsc = 'PRODUCT_ACHIEVEMENT_ASC',
  ProductAchievementDesc = 'PRODUCT_ACHIEVEMENT_DESC',
  ProductAnswerStatusAsc = 'PRODUCT_ANSWER_STATUS_ASC',
  ProductAnswerStatusDesc = 'PRODUCT_ANSWER_STATUS_DESC',
  ProductIdAsc = 'PRODUCT_ID_ASC',
  ProductIdDesc = 'PRODUCT_ID_DESC',
  ProductMaterialsFilesAsc = 'PRODUCT_MATERIALS_FILES_ASC',
  ProductMaterialsFilesDesc = 'PRODUCT_MATERIALS_FILES_DESC',
  ProductMaterialsFileKeysAsc = 'PRODUCT_MATERIALS_FILE_KEYS_ASC',
  ProductMaterialsFileKeysDesc = 'PRODUCT_MATERIALS_FILE_KEYS_DESC',
  ProductMaterialsUrlAsc = 'PRODUCT_MATERIALS_URL_ASC',
  ProductMaterialsUrlDesc = 'PRODUCT_MATERIALS_URL_DESC',
  ReferencedProductAsc = 'REFERENCED_PRODUCT_ASC',
  ReferencedProductDesc = 'REFERENCED_PRODUCT_DESC',
  SimilarProductAsc = 'SIMILAR_PRODUCT_ASC',
  SimilarProductDesc = 'SIMILAR_PRODUCT_DESC',
  SubTargetExistenceAsc = 'SUB_TARGET_EXISTENCE_ASC',
  SubTargetExistenceDesc = 'SUB_TARGET_EXISTENCE_DESC',
  SuccessCaseAsc = 'SUCCESS_CASE_ASC',
  SuccessCaseDesc = 'SUCCESS_CASE_DESC',
  SuperiorProductAsc = 'SUPERIOR_PRODUCT_ASC',
  SuperiorProductDesc = 'SUPERIOR_PRODUCT_DESC',
  UniqueStrengthAsc = 'UNIQUE_STRENGTH_ASC',
  UniqueStrengthDesc = 'UNIQUE_STRENGTH_DESC',
  UpdatedAtAsc = 'UPDATED_AT_ASC',
  UpdatedAtDesc = 'UPDATED_AT_DESC'
}

/** An input for mutations affecting `Product` */
export type ProductInput = {
  /** 企業ID */
  companyId: Scalars['UUID']['input'];
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** ヒアリング完了フラグ */
  hearingComplete?: InputMaybe<Scalars['Boolean']['input']>;
  /** 商材ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** 商材名 */
  productName?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** Represents an update to a `Product`. Fields that are set will be updated. */
export type ProductPatch = {
  /** 企業ID */
  companyId?: InputMaybe<Scalars['UUID']['input']>;
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** ヒアリング完了フラグ */
  hearingComplete?: InputMaybe<Scalars['Boolean']['input']>;
  /** 商材ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** 商材名 */
  productName?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** A connection to a list of `Product` values. */
export type ProductsConnection = {
  __typename?: 'ProductsConnection';
  /** A list of edges which contains the `Product` and cursor to aid in pagination. */
  edges: Array<ProductsEdge>;
  /** A list of `Product` objects. */
  nodes: Array<Maybe<Product>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `Product` you could get from the connection. */
  totalCount: Scalars['Int']['output'];
};

/** A `Product` edge in the connection. */
export type ProductsEdge = {
  __typename?: 'ProductsEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']['output']>;
  /** The `Product` at the end of the edge. */
  node?: Maybe<Product>;
};

/** Methods to use when ordering `Product`. */
export enum ProductsOrderBy {
  CompanyIdAsc = 'COMPANY_ID_ASC',
  CompanyIdDesc = 'COMPANY_ID_DESC',
  CreatedAtAsc = 'CREATED_AT_ASC',
  CreatedAtDesc = 'CREATED_AT_DESC',
  HearingCompleteAsc = 'HEARING_COMPLETE_ASC',
  HearingCompleteDesc = 'HEARING_COMPLETE_DESC',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  Natural = 'NATURAL',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
  ProductNameAsc = 'PRODUCT_NAME_ASC',
  ProductNameDesc = 'PRODUCT_NAME_DESC',
  UpdatedAtAsc = 'UPDATED_AT_ASC',
  UpdatedAtDesc = 'UPDATED_AT_DESC'
}

/** The root query type which gives access points into the data universe. */
export type Query = Node & {
  __typename?: 'Query';
  /** Reads a single `AlembicVersion` using its globally unique `ID`. */
  alembicVersion?: Maybe<AlembicVersion>;
  alembicVersionByVersionNum?: Maybe<AlembicVersion>;
  /** Reads and enables pagination through a set of `AlembicVersion`. */
  allAlembicVersions?: Maybe<AlembicVersionsConnection>;
  /** Reads and enables pagination through a set of `Company`. */
  allCompanies?: Maybe<CompaniesConnection>;
  /** Reads and enables pagination through a set of `CompanyInfo`. */
  allCompanyInfos?: Maybe<CompanyInfosConnection>;
  /** Reads and enables pagination through a set of `CustomerGroup`. */
  allCustomerGroups?: Maybe<CustomerGroupsConnection>;
  /** Reads and enables pagination through a set of `MarketingActivityInformation`. */
  allMarketingActivityInformations?: Maybe<MarketingActivityInformationsConnection>;
  /** Reads and enables pagination through a set of `Notification`. */
  allNotifications?: Maybe<NotificationsConnection>;
  /** Reads and enables pagination through a set of `OperatorGroup`. */
  allOperatorGroups?: Maybe<OperatorGroupsConnection>;
  /** Reads and enables pagination through a set of `OperatorName`. */
  allOperatorNames?: Maybe<OperatorNamesConnection>;
  /** Reads and enables pagination through a set of `Opportunity`. */
  allOpportunities?: Maybe<OpportunitiesConnection>;
  /** Reads and enables pagination through a set of `ProductInformation`. */
  allProductInformations?: Maybe<ProductInformationsConnection>;
  /** Reads and enables pagination through a set of `Product`. */
  allProducts?: Maybe<ProductsConnection>;
  /** Reads and enables pagination through a set of `RequestContent`. */
  allRequestContents?: Maybe<RequestContentsConnection>;
  /** Reads and enables pagination through a set of `UserInformationMemo`. */
  allUserInformationMemos?: Maybe<UserInformationMemosConnection>;
  /** Reads a single `Company` using its globally unique `ID`. */
  company?: Maybe<Company>;
  companyById?: Maybe<Company>;
  /** Reads a single `CompanyInfo` using its globally unique `ID`. */
  companyInfo?: Maybe<CompanyInfo>;
  companyInfoById?: Maybe<CompanyInfo>;
  /** Reads a single `CustomerGroup` using its globally unique `ID`. */
  customerGroup?: Maybe<CustomerGroup>;
  customerGroupById?: Maybe<CustomerGroup>;
  customerGroupByOpportunityIdAndSfContactId?: Maybe<CustomerGroup>;
  /** Reads a single `MarketingActivityInformation` using its globally unique `ID`. */
  marketingActivityInformation?: Maybe<MarketingActivityInformation>;
  marketingActivityInformationById?: Maybe<MarketingActivityInformation>;
  /** Fetches an object given its globally unique `ID`. */
  node?: Maybe<Node>;
  /** The root query type must be a `Node` to work well with Relay 1 mutations. This just resolves to `query`. */
  nodeId: Scalars['ID']['output'];
  /** Reads a single `Notification` using its globally unique `ID`. */
  notification?: Maybe<Notification>;
  notificationById?: Maybe<Notification>;
  /** Reads a single `OperatorGroup` using its globally unique `ID`. */
  operatorGroup?: Maybe<OperatorGroup>;
  operatorGroupById?: Maybe<OperatorGroup>;
  /** Reads a single `OperatorName` using its globally unique `ID`. */
  operatorName?: Maybe<OperatorName>;
  operatorNameByEmail?: Maybe<OperatorName>;
  /** Reads a single `Opportunity` using its globally unique `ID`. */
  opportunity?: Maybe<Opportunity>;
  opportunityById?: Maybe<Opportunity>;
  /** Reads a single `Product` using its globally unique `ID`. */
  product?: Maybe<Product>;
  productByCompanyIdAndProductName?: Maybe<Product>;
  productById?: Maybe<Product>;
  /** Reads a single `ProductInformation` using its globally unique `ID`. */
  productInformation?: Maybe<ProductInformation>;
  productInformationById?: Maybe<ProductInformation>;
  /**
   * Exposes the root query type nested one level down. This is helpful for Relay 1
   * which can only query top level fields if they are in a particular form.
   */
  query: Query;
  /** Reads a single `RequestContent` using its globally unique `ID`. */
  requestContent?: Maybe<RequestContent>;
  requestContentById?: Maybe<RequestContent>;
  /** Reads a single `UserInformationMemo` using its globally unique `ID`. */
  userInformationMemo?: Maybe<UserInformationMemo>;
  userInformationMemoById?: Maybe<UserInformationMemo>;
};


/** The root query type which gives access points into the data universe. */
export type QueryAlembicVersionArgs = {
  nodeId: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryAlembicVersionByVersionNumArgs = {
  versionNum: Scalars['String']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryAllAlembicVersionsArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<AlembicVersionCondition>;
  filter?: InputMaybe<AlembicVersionFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<AlembicVersionsOrderBy>>;
};


/** The root query type which gives access points into the data universe. */
export type QueryAllCompaniesArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<CompanyCondition>;
  filter?: InputMaybe<CompanyFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CompaniesOrderBy>>;
};


/** The root query type which gives access points into the data universe. */
export type QueryAllCompanyInfosArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<CompanyInfoCondition>;
  filter?: InputMaybe<CompanyInfoFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CompanyInfosOrderBy>>;
};


/** The root query type which gives access points into the data universe. */
export type QueryAllCustomerGroupsArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<CustomerGroupCondition>;
  filter?: InputMaybe<CustomerGroupFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<CustomerGroupsOrderBy>>;
};


/** The root query type which gives access points into the data universe. */
export type QueryAllMarketingActivityInformationsArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<MarketingActivityInformationCondition>;
  filter?: InputMaybe<MarketingActivityInformationFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<MarketingActivityInformationsOrderBy>>;
};


/** The root query type which gives access points into the data universe. */
export type QueryAllNotificationsArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<NotificationCondition>;
  filter?: InputMaybe<NotificationFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<NotificationsOrderBy>>;
};


/** The root query type which gives access points into the data universe. */
export type QueryAllOperatorGroupsArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<OperatorGroupCondition>;
  filter?: InputMaybe<OperatorGroupFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<OperatorGroupsOrderBy>>;
};


/** The root query type which gives access points into the data universe. */
export type QueryAllOperatorNamesArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<OperatorNameCondition>;
  filter?: InputMaybe<OperatorNameFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<OperatorNamesOrderBy>>;
};


/** The root query type which gives access points into the data universe. */
export type QueryAllOpportunitiesArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<OpportunityCondition>;
  filter?: InputMaybe<OpportunityFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<OpportunitiesOrderBy>>;
};


/** The root query type which gives access points into the data universe. */
export type QueryAllProductInformationsArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<ProductInformationCondition>;
  filter?: InputMaybe<ProductInformationFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<ProductInformationsOrderBy>>;
};


/** The root query type which gives access points into the data universe. */
export type QueryAllProductsArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<ProductCondition>;
  filter?: InputMaybe<ProductFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<ProductsOrderBy>>;
};


/** The root query type which gives access points into the data universe. */
export type QueryAllRequestContentsArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<RequestContentCondition>;
  filter?: InputMaybe<RequestContentFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<RequestContentsOrderBy>>;
};


/** The root query type which gives access points into the data universe. */
export type QueryAllUserInformationMemosArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<UserInformationMemoCondition>;
  filter?: InputMaybe<UserInformationMemoFilter>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<UserInformationMemosOrderBy>>;
};


/** The root query type which gives access points into the data universe. */
export type QueryCompanyArgs = {
  nodeId: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryCompanyByIdArgs = {
  id: Scalars['UUID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryCompanyInfoArgs = {
  nodeId: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryCompanyInfoByIdArgs = {
  id: Scalars['UUID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryCustomerGroupArgs = {
  nodeId: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryCustomerGroupByIdArgs = {
  id: Scalars['Int']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryCustomerGroupByOpportunityIdAndSfContactIdArgs = {
  opportunityId: Scalars['UUID']['input'];
  sfContactId: Scalars['String']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryMarketingActivityInformationArgs = {
  nodeId: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryMarketingActivityInformationByIdArgs = {
  id: Scalars['UUID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryNodeArgs = {
  nodeId: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryNotificationArgs = {
  nodeId: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryNotificationByIdArgs = {
  id: Scalars['UUID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryOperatorGroupArgs = {
  nodeId: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryOperatorGroupByIdArgs = {
  id: Scalars['Int']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryOperatorNameArgs = {
  nodeId: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryOperatorNameByEmailArgs = {
  email: Scalars['String']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryOpportunityArgs = {
  nodeId: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryOpportunityByIdArgs = {
  id: Scalars['UUID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryProductArgs = {
  nodeId: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryProductByCompanyIdAndProductNameArgs = {
  companyId: Scalars['UUID']['input'];
  productName: Scalars['String']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryProductByIdArgs = {
  id: Scalars['UUID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryProductInformationArgs = {
  nodeId: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryProductInformationByIdArgs = {
  id: Scalars['UUID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryRequestContentArgs = {
  nodeId: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryRequestContentByIdArgs = {
  id: Scalars['UUID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryUserInformationMemoArgs = {
  nodeId: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryUserInformationMemoByIdArgs = {
  id: Scalars['UUID']['input'];
};

/** ご依頼内容Pkey */
export type RequestContent = Node & {
  __typename?: 'RequestContent';
  adminCommentFirstProduction?: Maybe<Scalars['String']['output']>;
  adminCommentGoal?: Maybe<Scalars['String']['output']>;
  adminCommentIssue?: Maybe<Scalars['String']['output']>;
  adminCommentOrderBackground?: Maybe<Scalars['String']['output']>;
  adminCommentRegulation?: Maybe<Scalars['String']['output']>;
  anyAdditionalComments?: Maybe<Scalars['String']['output']>;
  createdAt?: Maybe<Scalars['Datetime']['output']>;
  creativeMaterialsAnswerStatus?: Maybe<Scalars['String']['output']>;
  creativeMaterialsFileKeys?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  creativeMaterialsFileNames?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  creativePreferredPlan?: Maybe<Scalars['String']['output']>;
  creativeRegulationAnswerStatus?: Maybe<Scalars['String']['output']>;
  creativeRegulationMaterialsFileKeys?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  creativeRegulationMaterialsFiles?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  creativeRegulationUrlOrText?: Maybe<Scalars['String']['output']>;
  /** ご依頼内容ID */
  id: Scalars['UUID']['output'];
  isCorporate?: Maybe<Scalars['Boolean']['output']>;
  lpExternalServiceExistence?: Maybe<Scalars['String']['output']>;
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID']['output'];
  orderBackground?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Product` that is related to this `RequestContent`. */
  productByProductId?: Maybe<Product>;
  /** プロダクトID */
  productId?: Maybe<Scalars['UUID']['output']>;
  qualitativeGoal?: Maybe<Scalars['String']['output']>;
  qualitativeIssue?: Maybe<Scalars['String']['output']>;
  quantitativeGoal?: Maybe<Scalars['String']['output']>;
  quantitativeGoalAnswerStatus?: Maybe<Scalars['String']['output']>;
  quantitativeIssue?: Maybe<Scalars['String']['output']>;
  reasonForChangeExistingCreative?: Maybe<Scalars['String']['output']>;
  requestFirstCreativeType?: Maybe<Scalars['String']['output']>;
  resultAdAnswerStatus?: Maybe<Scalars['String']['output']>;
  resultAdFileKeys?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  resultAdFiles?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  updatedAt?: Maybe<Scalars['Datetime']['output']>;
};

/**
 * A condition to be used against `RequestContent` object types. All fields are
 * tested for equality and combined with a logical ‘and.’
 */
export type RequestContentCondition = {
  /** Checks for equality with the object’s `adminCommentFirstProduction` field. */
  adminCommentFirstProduction?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentGoal` field. */
  adminCommentGoal?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentIssue` field. */
  adminCommentIssue?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentOrderBackground` field. */
  adminCommentOrderBackground?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `adminCommentRegulation` field. */
  adminCommentRegulation?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `anyAdditionalComments` field. */
  anyAdditionalComments?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `createdAt` field. */
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** Checks for equality with the object’s `creativeMaterialsAnswerStatus` field. */
  creativeMaterialsAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `creativeMaterialsFileKeys` field. */
  creativeMaterialsFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Checks for equality with the object’s `creativeMaterialsFileNames` field. */
  creativeMaterialsFileNames?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Checks for equality with the object’s `creativePreferredPlan` field. */
  creativePreferredPlan?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `creativeRegulationAnswerStatus` field. */
  creativeRegulationAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `creativeRegulationMaterialsFileKeys` field. */
  creativeRegulationMaterialsFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Checks for equality with the object’s `creativeRegulationMaterialsFiles` field. */
  creativeRegulationMaterialsFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Checks for equality with the object’s `creativeRegulationUrlOrText` field. */
  creativeRegulationUrlOrText?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `id` field. */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `isCorporate` field. */
  isCorporate?: InputMaybe<Scalars['Boolean']['input']>;
  /** Checks for equality with the object’s `lpExternalServiceExistence` field. */
  lpExternalServiceExistence?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `orderBackground` field. */
  orderBackground?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `productId` field. */
  productId?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `qualitativeGoal` field. */
  qualitativeGoal?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `qualitativeIssue` field. */
  qualitativeIssue?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `quantitativeGoal` field. */
  quantitativeGoal?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `quantitativeGoalAnswerStatus` field. */
  quantitativeGoalAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `quantitativeIssue` field. */
  quantitativeIssue?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `reasonForChangeExistingCreative` field. */
  reasonForChangeExistingCreative?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `requestFirstCreativeType` field. */
  requestFirstCreativeType?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `resultAdAnswerStatus` field. */
  resultAdAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `resultAdFileKeys` field. */
  resultAdFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Checks for equality with the object’s `resultAdFiles` field. */
  resultAdFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Checks for equality with the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** A filter to be used against `RequestContent` object types. All fields are combined with a logical ‘and.’ */
export type RequestContentFilter = {
  /** Filter by the object’s `adminCommentFirstProduction` field. */
  adminCommentFirstProduction?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentGoal` field. */
  adminCommentGoal?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentIssue` field. */
  adminCommentIssue?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentOrderBackground` field. */
  adminCommentOrderBackground?: InputMaybe<StringFilter>;
  /** Filter by the object’s `adminCommentRegulation` field. */
  adminCommentRegulation?: InputMaybe<StringFilter>;
  /** Checks for all expressions in this list. */
  and?: InputMaybe<Array<RequestContentFilter>>;
  /** Filter by the object’s `anyAdditionalComments` field. */
  anyAdditionalComments?: InputMaybe<StringFilter>;
  /** Filter by the object’s `createdAt` field. */
  createdAt?: InputMaybe<DatetimeFilter>;
  /** Filter by the object’s `creativeMaterialsAnswerStatus` field. */
  creativeMaterialsAnswerStatus?: InputMaybe<StringFilter>;
  /** Filter by the object’s `creativeMaterialsFileKeys` field. */
  creativeMaterialsFileKeys?: InputMaybe<StringListFilter>;
  /** Filter by the object’s `creativeMaterialsFileNames` field. */
  creativeMaterialsFileNames?: InputMaybe<StringListFilter>;
  /** Filter by the object’s `creativePreferredPlan` field. */
  creativePreferredPlan?: InputMaybe<StringFilter>;
  /** Filter by the object’s `creativeRegulationAnswerStatus` field. */
  creativeRegulationAnswerStatus?: InputMaybe<StringFilter>;
  /** Filter by the object’s `creativeRegulationMaterialsFileKeys` field. */
  creativeRegulationMaterialsFileKeys?: InputMaybe<StringListFilter>;
  /** Filter by the object’s `creativeRegulationMaterialsFiles` field. */
  creativeRegulationMaterialsFiles?: InputMaybe<StringListFilter>;
  /** Filter by the object’s `creativeRegulationUrlOrText` field. */
  creativeRegulationUrlOrText?: InputMaybe<StringFilter>;
  /** Filter by the object’s `id` field. */
  id?: InputMaybe<UuidFilter>;
  /** Filter by the object’s `isCorporate` field. */
  isCorporate?: InputMaybe<BooleanFilter>;
  /** Filter by the object’s `lpExternalServiceExistence` field. */
  lpExternalServiceExistence?: InputMaybe<StringFilter>;
  /** Negates the expression. */
  not?: InputMaybe<RequestContentFilter>;
  /** Checks for any expressions in this list. */
  or?: InputMaybe<Array<RequestContentFilter>>;
  /** Filter by the object’s `orderBackground` field. */
  orderBackground?: InputMaybe<StringFilter>;
  /** Filter by the object’s `productId` field. */
  productId?: InputMaybe<UuidFilter>;
  /** Filter by the object’s `qualitativeGoal` field. */
  qualitativeGoal?: InputMaybe<StringFilter>;
  /** Filter by the object’s `qualitativeIssue` field. */
  qualitativeIssue?: InputMaybe<StringFilter>;
  /** Filter by the object’s `quantitativeGoal` field. */
  quantitativeGoal?: InputMaybe<StringFilter>;
  /** Filter by the object’s `quantitativeGoalAnswerStatus` field. */
  quantitativeGoalAnswerStatus?: InputMaybe<StringFilter>;
  /** Filter by the object’s `quantitativeIssue` field. */
  quantitativeIssue?: InputMaybe<StringFilter>;
  /** Filter by the object’s `reasonForChangeExistingCreative` field. */
  reasonForChangeExistingCreative?: InputMaybe<StringFilter>;
  /** Filter by the object’s `requestFirstCreativeType` field. */
  requestFirstCreativeType?: InputMaybe<StringFilter>;
  /** Filter by the object’s `resultAdAnswerStatus` field. */
  resultAdAnswerStatus?: InputMaybe<StringFilter>;
  /** Filter by the object’s `resultAdFileKeys` field. */
  resultAdFileKeys?: InputMaybe<StringListFilter>;
  /** Filter by the object’s `resultAdFiles` field. */
  resultAdFiles?: InputMaybe<StringListFilter>;
  /** Filter by the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<DatetimeFilter>;
};

/** An input for mutations affecting `RequestContent` */
export type RequestContentInput = {
  adminCommentFirstProduction?: InputMaybe<Scalars['String']['input']>;
  adminCommentGoal?: InputMaybe<Scalars['String']['input']>;
  adminCommentIssue?: InputMaybe<Scalars['String']['input']>;
  adminCommentOrderBackground?: InputMaybe<Scalars['String']['input']>;
  adminCommentRegulation?: InputMaybe<Scalars['String']['input']>;
  anyAdditionalComments?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  creativeMaterialsAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  creativeMaterialsFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  creativeMaterialsFileNames?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  creativePreferredPlan?: InputMaybe<Scalars['String']['input']>;
  creativeRegulationAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  creativeRegulationMaterialsFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  creativeRegulationMaterialsFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  creativeRegulationUrlOrText?: InputMaybe<Scalars['String']['input']>;
  /** ご依頼内容ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  isCorporate?: InputMaybe<Scalars['Boolean']['input']>;
  lpExternalServiceExistence?: InputMaybe<Scalars['String']['input']>;
  orderBackground?: InputMaybe<Scalars['String']['input']>;
  /** プロダクトID */
  productId?: InputMaybe<Scalars['UUID']['input']>;
  qualitativeGoal?: InputMaybe<Scalars['String']['input']>;
  qualitativeIssue?: InputMaybe<Scalars['String']['input']>;
  quantitativeGoal?: InputMaybe<Scalars['String']['input']>;
  quantitativeGoalAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  quantitativeIssue?: InputMaybe<Scalars['String']['input']>;
  reasonForChangeExistingCreative?: InputMaybe<Scalars['String']['input']>;
  requestFirstCreativeType?: InputMaybe<Scalars['String']['input']>;
  resultAdAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  resultAdFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  resultAdFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** Represents an update to a `RequestContent`. Fields that are set will be updated. */
export type RequestContentPatch = {
  adminCommentFirstProduction?: InputMaybe<Scalars['String']['input']>;
  adminCommentGoal?: InputMaybe<Scalars['String']['input']>;
  adminCommentIssue?: InputMaybe<Scalars['String']['input']>;
  adminCommentOrderBackground?: InputMaybe<Scalars['String']['input']>;
  adminCommentRegulation?: InputMaybe<Scalars['String']['input']>;
  anyAdditionalComments?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  creativeMaterialsAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  creativeMaterialsFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  creativeMaterialsFileNames?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  creativePreferredPlan?: InputMaybe<Scalars['String']['input']>;
  creativeRegulationAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  creativeRegulationMaterialsFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  creativeRegulationMaterialsFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  creativeRegulationUrlOrText?: InputMaybe<Scalars['String']['input']>;
  /** ご依頼内容ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  isCorporate?: InputMaybe<Scalars['Boolean']['input']>;
  lpExternalServiceExistence?: InputMaybe<Scalars['String']['input']>;
  orderBackground?: InputMaybe<Scalars['String']['input']>;
  /** プロダクトID */
  productId?: InputMaybe<Scalars['UUID']['input']>;
  qualitativeGoal?: InputMaybe<Scalars['String']['input']>;
  qualitativeIssue?: InputMaybe<Scalars['String']['input']>;
  quantitativeGoal?: InputMaybe<Scalars['String']['input']>;
  quantitativeGoalAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  quantitativeIssue?: InputMaybe<Scalars['String']['input']>;
  reasonForChangeExistingCreative?: InputMaybe<Scalars['String']['input']>;
  requestFirstCreativeType?: InputMaybe<Scalars['String']['input']>;
  resultAdAnswerStatus?: InputMaybe<Scalars['String']['input']>;
  resultAdFileKeys?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  resultAdFiles?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** A connection to a list of `RequestContent` values. */
export type RequestContentsConnection = {
  __typename?: 'RequestContentsConnection';
  /** A list of edges which contains the `RequestContent` and cursor to aid in pagination. */
  edges: Array<RequestContentsEdge>;
  /** A list of `RequestContent` objects. */
  nodes: Array<Maybe<RequestContent>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `RequestContent` you could get from the connection. */
  totalCount: Scalars['Int']['output'];
};

/** A `RequestContent` edge in the connection. */
export type RequestContentsEdge = {
  __typename?: 'RequestContentsEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']['output']>;
  /** The `RequestContent` at the end of the edge. */
  node?: Maybe<RequestContent>;
};

/** Methods to use when ordering `RequestContent`. */
export enum RequestContentsOrderBy {
  AdminCommentFirstProductionAsc = 'ADMIN_COMMENT_FIRST_PRODUCTION_ASC',
  AdminCommentFirstProductionDesc = 'ADMIN_COMMENT_FIRST_PRODUCTION_DESC',
  AdminCommentGoalAsc = 'ADMIN_COMMENT_GOAL_ASC',
  AdminCommentGoalDesc = 'ADMIN_COMMENT_GOAL_DESC',
  AdminCommentIssueAsc = 'ADMIN_COMMENT_ISSUE_ASC',
  AdminCommentIssueDesc = 'ADMIN_COMMENT_ISSUE_DESC',
  AdminCommentOrderBackgroundAsc = 'ADMIN_COMMENT_ORDER_BACKGROUND_ASC',
  AdminCommentOrderBackgroundDesc = 'ADMIN_COMMENT_ORDER_BACKGROUND_DESC',
  AdminCommentRegulationAsc = 'ADMIN_COMMENT_REGULATION_ASC',
  AdminCommentRegulationDesc = 'ADMIN_COMMENT_REGULATION_DESC',
  AnyAdditionalCommentsAsc = 'ANY_ADDITIONAL_COMMENTS_ASC',
  AnyAdditionalCommentsDesc = 'ANY_ADDITIONAL_COMMENTS_DESC',
  CreatedAtAsc = 'CREATED_AT_ASC',
  CreatedAtDesc = 'CREATED_AT_DESC',
  CreativeMaterialsAnswerStatusAsc = 'CREATIVE_MATERIALS_ANSWER_STATUS_ASC',
  CreativeMaterialsAnswerStatusDesc = 'CREATIVE_MATERIALS_ANSWER_STATUS_DESC',
  CreativeMaterialsFileKeysAsc = 'CREATIVE_MATERIALS_FILE_KEYS_ASC',
  CreativeMaterialsFileKeysDesc = 'CREATIVE_MATERIALS_FILE_KEYS_DESC',
  CreativeMaterialsFileNamesAsc = 'CREATIVE_MATERIALS_FILE_NAMES_ASC',
  CreativeMaterialsFileNamesDesc = 'CREATIVE_MATERIALS_FILE_NAMES_DESC',
  CreativePreferredPlanAsc = 'CREATIVE_PREFERRED_PLAN_ASC',
  CreativePreferredPlanDesc = 'CREATIVE_PREFERRED_PLAN_DESC',
  CreativeRegulationAnswerStatusAsc = 'CREATIVE_REGULATION_ANSWER_STATUS_ASC',
  CreativeRegulationAnswerStatusDesc = 'CREATIVE_REGULATION_ANSWER_STATUS_DESC',
  CreativeRegulationMaterialsFilesAsc = 'CREATIVE_REGULATION_MATERIALS_FILES_ASC',
  CreativeRegulationMaterialsFilesDesc = 'CREATIVE_REGULATION_MATERIALS_FILES_DESC',
  CreativeRegulationMaterialsFileKeysAsc = 'CREATIVE_REGULATION_MATERIALS_FILE_KEYS_ASC',
  CreativeRegulationMaterialsFileKeysDesc = 'CREATIVE_REGULATION_MATERIALS_FILE_KEYS_DESC',
  CreativeRegulationUrlOrTextAsc = 'CREATIVE_REGULATION_URL_OR_TEXT_ASC',
  CreativeRegulationUrlOrTextDesc = 'CREATIVE_REGULATION_URL_OR_TEXT_DESC',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  IsCorporateAsc = 'IS_CORPORATE_ASC',
  IsCorporateDesc = 'IS_CORPORATE_DESC',
  LpExternalServiceExistenceAsc = 'LP_EXTERNAL_SERVICE_EXISTENCE_ASC',
  LpExternalServiceExistenceDesc = 'LP_EXTERNAL_SERVICE_EXISTENCE_DESC',
  Natural = 'NATURAL',
  OrderBackgroundAsc = 'ORDER_BACKGROUND_ASC',
  OrderBackgroundDesc = 'ORDER_BACKGROUND_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
  ProductIdAsc = 'PRODUCT_ID_ASC',
  ProductIdDesc = 'PRODUCT_ID_DESC',
  QualitativeGoalAsc = 'QUALITATIVE_GOAL_ASC',
  QualitativeGoalDesc = 'QUALITATIVE_GOAL_DESC',
  QualitativeIssueAsc = 'QUALITATIVE_ISSUE_ASC',
  QualitativeIssueDesc = 'QUALITATIVE_ISSUE_DESC',
  QuantitativeGoalAnswerStatusAsc = 'QUANTITATIVE_GOAL_ANSWER_STATUS_ASC',
  QuantitativeGoalAnswerStatusDesc = 'QUANTITATIVE_GOAL_ANSWER_STATUS_DESC',
  QuantitativeGoalAsc = 'QUANTITATIVE_GOAL_ASC',
  QuantitativeGoalDesc = 'QUANTITATIVE_GOAL_DESC',
  QuantitativeIssueAsc = 'QUANTITATIVE_ISSUE_ASC',
  QuantitativeIssueDesc = 'QUANTITATIVE_ISSUE_DESC',
  ReasonForChangeExistingCreativeAsc = 'REASON_FOR_CHANGE_EXISTING_CREATIVE_ASC',
  ReasonForChangeExistingCreativeDesc = 'REASON_FOR_CHANGE_EXISTING_CREATIVE_DESC',
  RequestFirstCreativeTypeAsc = 'REQUEST_FIRST_CREATIVE_TYPE_ASC',
  RequestFirstCreativeTypeDesc = 'REQUEST_FIRST_CREATIVE_TYPE_DESC',
  ResultAdAnswerStatusAsc = 'RESULT_AD_ANSWER_STATUS_ASC',
  ResultAdAnswerStatusDesc = 'RESULT_AD_ANSWER_STATUS_DESC',
  ResultAdFilesAsc = 'RESULT_AD_FILES_ASC',
  ResultAdFilesDesc = 'RESULT_AD_FILES_DESC',
  ResultAdFileKeysAsc = 'RESULT_AD_FILE_KEYS_ASC',
  ResultAdFileKeysDesc = 'RESULT_AD_FILE_KEYS_DESC',
  UpdatedAtAsc = 'UPDATED_AT_ASC',
  UpdatedAtDesc = 'UPDATED_AT_DESC'
}

/** A filter to be used against String fields. All fields are combined with a logical ‘and.’ */
export type StringFilter = {
  /** Not equal to the specified value, treating null like an ordinary value. */
  distinctFrom?: InputMaybe<Scalars['String']['input']>;
  /** Not equal to the specified value, treating null like an ordinary value (case-insensitive). */
  distinctFromInsensitive?: InputMaybe<Scalars['String']['input']>;
  /** Ends with the specified string (case-sensitive). */
  endsWith?: InputMaybe<Scalars['String']['input']>;
  /** Ends with the specified string (case-insensitive). */
  endsWithInsensitive?: InputMaybe<Scalars['String']['input']>;
  /** Equal to the specified value. */
  equalTo?: InputMaybe<Scalars['String']['input']>;
  /** Equal to the specified value (case-insensitive). */
  equalToInsensitive?: InputMaybe<Scalars['String']['input']>;
  /** Greater than the specified value. */
  greaterThan?: InputMaybe<Scalars['String']['input']>;
  /** Greater than the specified value (case-insensitive). */
  greaterThanInsensitive?: InputMaybe<Scalars['String']['input']>;
  /** Greater than or equal to the specified value. */
  greaterThanOrEqualTo?: InputMaybe<Scalars['String']['input']>;
  /** Greater than or equal to the specified value (case-insensitive). */
  greaterThanOrEqualToInsensitive?: InputMaybe<Scalars['String']['input']>;
  /** Included in the specified list. */
  in?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Included in the specified list (case-insensitive). */
  inInsensitive?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Contains the specified string (case-sensitive). */
  includes?: InputMaybe<Scalars['String']['input']>;
  /** Contains the specified string (case-insensitive). */
  includesInsensitive?: InputMaybe<Scalars['String']['input']>;
  /** Is null (if `true` is specified) or is not null (if `false` is specified). */
  isNull?: InputMaybe<Scalars['Boolean']['input']>;
  /** Less than the specified value. */
  lessThan?: InputMaybe<Scalars['String']['input']>;
  /** Less than the specified value (case-insensitive). */
  lessThanInsensitive?: InputMaybe<Scalars['String']['input']>;
  /** Less than or equal to the specified value. */
  lessThanOrEqualTo?: InputMaybe<Scalars['String']['input']>;
  /** Less than or equal to the specified value (case-insensitive). */
  lessThanOrEqualToInsensitive?: InputMaybe<Scalars['String']['input']>;
  /** Matches the specified pattern (case-sensitive). An underscore (_) matches any single character; a percent sign (%) matches any sequence of zero or more characters. */
  like?: InputMaybe<Scalars['String']['input']>;
  /** Matches the specified pattern (case-insensitive). An underscore (_) matches any single character; a percent sign (%) matches any sequence of zero or more characters. */
  likeInsensitive?: InputMaybe<Scalars['String']['input']>;
  /** Equal to the specified value, treating null like an ordinary value. */
  notDistinctFrom?: InputMaybe<Scalars['String']['input']>;
  /** Equal to the specified value, treating null like an ordinary value (case-insensitive). */
  notDistinctFromInsensitive?: InputMaybe<Scalars['String']['input']>;
  /** Does not end with the specified string (case-sensitive). */
  notEndsWith?: InputMaybe<Scalars['String']['input']>;
  /** Does not end with the specified string (case-insensitive). */
  notEndsWithInsensitive?: InputMaybe<Scalars['String']['input']>;
  /** Not equal to the specified value. */
  notEqualTo?: InputMaybe<Scalars['String']['input']>;
  /** Not equal to the specified value (case-insensitive). */
  notEqualToInsensitive?: InputMaybe<Scalars['String']['input']>;
  /** Not included in the specified list. */
  notIn?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Not included in the specified list (case-insensitive). */
  notInInsensitive?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Does not contain the specified string (case-sensitive). */
  notIncludes?: InputMaybe<Scalars['String']['input']>;
  /** Does not contain the specified string (case-insensitive). */
  notIncludesInsensitive?: InputMaybe<Scalars['String']['input']>;
  /** Does not match the specified pattern (case-sensitive). An underscore (_) matches any single character; a percent sign (%) matches any sequence of zero or more characters. */
  notLike?: InputMaybe<Scalars['String']['input']>;
  /** Does not match the specified pattern (case-insensitive). An underscore (_) matches any single character; a percent sign (%) matches any sequence of zero or more characters. */
  notLikeInsensitive?: InputMaybe<Scalars['String']['input']>;
  /** Does not start with the specified string (case-sensitive). */
  notStartsWith?: InputMaybe<Scalars['String']['input']>;
  /** Does not start with the specified string (case-insensitive). */
  notStartsWithInsensitive?: InputMaybe<Scalars['String']['input']>;
  /** Starts with the specified string (case-sensitive). */
  startsWith?: InputMaybe<Scalars['String']['input']>;
  /** Starts with the specified string (case-insensitive). */
  startsWithInsensitive?: InputMaybe<Scalars['String']['input']>;
};

/** A filter to be used against String List fields. All fields are combined with a logical ‘and.’ */
export type StringListFilter = {
  /** Any array item is equal to the specified value. */
  anyEqualTo?: InputMaybe<Scalars['String']['input']>;
  /** Any array item is greater than the specified value. */
  anyGreaterThan?: InputMaybe<Scalars['String']['input']>;
  /** Any array item is greater than or equal to the specified value. */
  anyGreaterThanOrEqualTo?: InputMaybe<Scalars['String']['input']>;
  /** Any array item is less than the specified value. */
  anyLessThan?: InputMaybe<Scalars['String']['input']>;
  /** Any array item is less than or equal to the specified value. */
  anyLessThanOrEqualTo?: InputMaybe<Scalars['String']['input']>;
  /** Any array item is not equal to the specified value. */
  anyNotEqualTo?: InputMaybe<Scalars['String']['input']>;
  /** Contained by the specified list of values. */
  containedBy?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Contains the specified list of values. */
  contains?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Not equal to the specified value, treating null like an ordinary value. */
  distinctFrom?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Equal to the specified value. */
  equalTo?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Greater than the specified value. */
  greaterThan?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Greater than or equal to the specified value. */
  greaterThanOrEqualTo?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Is null (if `true` is specified) or is not null (if `false` is specified). */
  isNull?: InputMaybe<Scalars['Boolean']['input']>;
  /** Less than the specified value. */
  lessThan?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Less than or equal to the specified value. */
  lessThanOrEqualTo?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Equal to the specified value, treating null like an ordinary value. */
  notDistinctFrom?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Not equal to the specified value. */
  notEqualTo?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Overlaps the specified list of values. */
  overlaps?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
};

/** A filter to be used against UUID fields. All fields are combined with a logical ‘and.’ */
export type UuidFilter = {
  /** Not equal to the specified value, treating null like an ordinary value. */
  distinctFrom?: InputMaybe<Scalars['UUID']['input']>;
  /** Equal to the specified value. */
  equalTo?: InputMaybe<Scalars['UUID']['input']>;
  /** Greater than the specified value. */
  greaterThan?: InputMaybe<Scalars['UUID']['input']>;
  /** Greater than or equal to the specified value. */
  greaterThanOrEqualTo?: InputMaybe<Scalars['UUID']['input']>;
  /** Included in the specified list. */
  in?: InputMaybe<Array<Scalars['UUID']['input']>>;
  /** Is null (if `true` is specified) or is not null (if `false` is specified). */
  isNull?: InputMaybe<Scalars['Boolean']['input']>;
  /** Less than the specified value. */
  lessThan?: InputMaybe<Scalars['UUID']['input']>;
  /** Less than or equal to the specified value. */
  lessThanOrEqualTo?: InputMaybe<Scalars['UUID']['input']>;
  /** Equal to the specified value, treating null like an ordinary value. */
  notDistinctFrom?: InputMaybe<Scalars['UUID']['input']>;
  /** Not equal to the specified value. */
  notEqualTo?: InputMaybe<Scalars['UUID']['input']>;
  /** Not included in the specified list. */
  notIn?: InputMaybe<Array<Scalars['UUID']['input']>>;
};

/** All input for the `updateAlembicVersionByVersionNum` mutation. */
export type UpdateAlembicVersionByVersionNumInput = {
  /** An object where the defined keys will be set on the `AlembicVersion` being updated. */
  alembicVersionPatch: AlembicVersionPatch;
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  versionNum: Scalars['String']['input'];
};

/** All input for the `updateAlembicVersion` mutation. */
export type UpdateAlembicVersionInput = {
  /** An object where the defined keys will be set on the `AlembicVersion` being updated. */
  alembicVersionPatch: AlembicVersionPatch;
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `AlembicVersion` to be updated. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our update `AlembicVersion` mutation. */
export type UpdateAlembicVersionPayload = {
  __typename?: 'UpdateAlembicVersionPayload';
  /** The `AlembicVersion` that was updated by this mutation. */
  alembicVersion?: Maybe<AlembicVersion>;
  /** An edge for our `AlembicVersion`. May be used by Relay 1. */
  alembicVersionEdge?: Maybe<AlembicVersionsEdge>;
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our update `AlembicVersion` mutation. */
export type UpdateAlembicVersionPayloadAlembicVersionEdgeArgs = {
  orderBy?: InputMaybe<Array<AlembicVersionsOrderBy>>;
};

/** All input for the `updateCompanyById` mutation. */
export type UpdateCompanyByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** An object where the defined keys will be set on the `Company` being updated. */
  companyPatch: CompanyPatch;
  /** 企業ID */
  id: Scalars['UUID']['input'];
};

/** All input for the `updateCompanyInfoById` mutation. */
export type UpdateCompanyInfoByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** An object where the defined keys will be set on the `CompanyInfo` being updated. */
  companyInfoPatch: CompanyInfoPatch;
  /** 契約情報/企業情報ID */
  id: Scalars['UUID']['input'];
};

/** All input for the `updateCompanyInfo` mutation. */
export type UpdateCompanyInfoInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** An object where the defined keys will be set on the `CompanyInfo` being updated. */
  companyInfoPatch: CompanyInfoPatch;
  /** The globally unique `ID` which will identify a single `CompanyInfo` to be updated. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our update `CompanyInfo` mutation. */
export type UpdateCompanyInfoPayload = {
  __typename?: 'UpdateCompanyInfoPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Company` that is related to this `CompanyInfo`. */
  companyByCompanyId?: Maybe<Company>;
  /** The `CompanyInfo` that was updated by this mutation. */
  companyInfo?: Maybe<CompanyInfo>;
  /** An edge for our `CompanyInfo`. May be used by Relay 1. */
  companyInfoEdge?: Maybe<CompanyInfosEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our update `CompanyInfo` mutation. */
export type UpdateCompanyInfoPayloadCompanyInfoEdgeArgs = {
  orderBy?: InputMaybe<Array<CompanyInfosOrderBy>>;
};

/** All input for the `updateCompany` mutation. */
export type UpdateCompanyInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** An object where the defined keys will be set on the `Company` being updated. */
  companyPatch: CompanyPatch;
  /** The globally unique `ID` which will identify a single `Company` to be updated. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our update `Company` mutation. */
export type UpdateCompanyPayload = {
  __typename?: 'UpdateCompanyPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `Company` that was updated by this mutation. */
  company?: Maybe<Company>;
  /** An edge for our `Company`. May be used by Relay 1. */
  companyEdge?: Maybe<CompaniesEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our update `Company` mutation. */
export type UpdateCompanyPayloadCompanyEdgeArgs = {
  orderBy?: InputMaybe<Array<CompaniesOrderBy>>;
};

/** All input for the `updateCustomerGroupById` mutation. */
export type UpdateCustomerGroupByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** An object where the defined keys will be set on the `CustomerGroup` being updated. */
  customerGroupPatch: CustomerGroupPatch;
  /** 顧客グループID */
  id: Scalars['Int']['input'];
};

/** All input for the `updateCustomerGroupByOpportunityIdAndSfContactId` mutation. */
export type UpdateCustomerGroupByOpportunityIdAndSfContactIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** An object where the defined keys will be set on the `CustomerGroup` being updated. */
  customerGroupPatch: CustomerGroupPatch;
  /** 商談ID */
  opportunityId: Scalars['UUID']['input'];
  /** SalesforceユーザID */
  sfContactId: Scalars['String']['input'];
};

/** All input for the `updateCustomerGroup` mutation. */
export type UpdateCustomerGroupInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** An object where the defined keys will be set on the `CustomerGroup` being updated. */
  customerGroupPatch: CustomerGroupPatch;
  /** The globally unique `ID` which will identify a single `CustomerGroup` to be updated. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our update `CustomerGroup` mutation. */
export type UpdateCustomerGroupPayload = {
  __typename?: 'UpdateCustomerGroupPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `CustomerGroup` that was updated by this mutation. */
  customerGroup?: Maybe<CustomerGroup>;
  /** An edge for our `CustomerGroup`. May be used by Relay 1. */
  customerGroupEdge?: Maybe<CustomerGroupsEdge>;
  /** Reads a single `Opportunity` that is related to this `CustomerGroup`. */
  opportunityByOpportunityId?: Maybe<Opportunity>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our update `CustomerGroup` mutation. */
export type UpdateCustomerGroupPayloadCustomerGroupEdgeArgs = {
  orderBy?: InputMaybe<Array<CustomerGroupsOrderBy>>;
};

/** All input for the `updateMarketingActivityInformationById` mutation. */
export type UpdateMarketingActivityInformationByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** マーケティング活動情報ID */
  id: Scalars['UUID']['input'];
  /** An object where the defined keys will be set on the `MarketingActivityInformation` being updated. */
  marketingActivityInformationPatch: MarketingActivityInformationPatch;
};

/** All input for the `updateMarketingActivityInformation` mutation. */
export type UpdateMarketingActivityInformationInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** An object where the defined keys will be set on the `MarketingActivityInformation` being updated. */
  marketingActivityInformationPatch: MarketingActivityInformationPatch;
  /** The globally unique `ID` which will identify a single `MarketingActivityInformation` to be updated. */
  nodeId: Scalars['ID']['input'];
};

/** The output of our update `MarketingActivityInformation` mutation. */
export type UpdateMarketingActivityInformationPayload = {
  __typename?: 'UpdateMarketingActivityInformationPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `MarketingActivityInformation` that was updated by this mutation. */
  marketingActivityInformation?: Maybe<MarketingActivityInformation>;
  /** An edge for our `MarketingActivityInformation`. May be used by Relay 1. */
  marketingActivityInformationEdge?: Maybe<MarketingActivityInformationsEdge>;
  /** Reads a single `Product` that is related to this `MarketingActivityInformation`. */
  productByProductId?: Maybe<Product>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our update `MarketingActivityInformation` mutation. */
export type UpdateMarketingActivityInformationPayloadMarketingActivityInformationEdgeArgs = {
  orderBy?: InputMaybe<Array<MarketingActivityInformationsOrderBy>>;
};

/** All input for the `updateNotificationById` mutation. */
export type UpdateNotificationByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** お知らせID */
  id: Scalars['UUID']['input'];
  /** An object where the defined keys will be set on the `Notification` being updated. */
  notificationPatch: NotificationPatch;
};

/** All input for the `updateNotification` mutation. */
export type UpdateNotificationInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `Notification` to be updated. */
  nodeId: Scalars['ID']['input'];
  /** An object where the defined keys will be set on the `Notification` being updated. */
  notificationPatch: NotificationPatch;
};

/** The output of our update `Notification` mutation. */
export type UpdateNotificationPayload = {
  __typename?: 'UpdateNotificationPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `Notification` that was updated by this mutation. */
  notification?: Maybe<Notification>;
  /** An edge for our `Notification`. May be used by Relay 1. */
  notificationEdge?: Maybe<NotificationsEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our update `Notification` mutation. */
export type UpdateNotificationPayloadNotificationEdgeArgs = {
  orderBy?: InputMaybe<Array<NotificationsOrderBy>>;
};

/** All input for the `updateOperatorGroupById` mutation. */
export type UpdateOperatorGroupByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** 管理者グループID */
  id: Scalars['Int']['input'];
  /** An object where the defined keys will be set on the `OperatorGroup` being updated. */
  operatorGroupPatch: OperatorGroupPatch;
};

/** All input for the `updateOperatorGroup` mutation. */
export type UpdateOperatorGroupInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `OperatorGroup` to be updated. */
  nodeId: Scalars['ID']['input'];
  /** An object where the defined keys will be set on the `OperatorGroup` being updated. */
  operatorGroupPatch: OperatorGroupPatch;
};

/** The output of our update `OperatorGroup` mutation. */
export type UpdateOperatorGroupPayload = {
  __typename?: 'UpdateOperatorGroupPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `OperatorGroup` that was updated by this mutation. */
  operatorGroup?: Maybe<OperatorGroup>;
  /** An edge for our `OperatorGroup`. May be used by Relay 1. */
  operatorGroupEdge?: Maybe<OperatorGroupsEdge>;
  /** Reads a single `Opportunity` that is related to this `OperatorGroup`. */
  opportunityByOpportunityId?: Maybe<Opportunity>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our update `OperatorGroup` mutation. */
export type UpdateOperatorGroupPayloadOperatorGroupEdgeArgs = {
  orderBy?: InputMaybe<Array<OperatorGroupsOrderBy>>;
};

/** All input for the `updateOperatorNameByEmail` mutation. */
export type UpdateOperatorNameByEmailInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  email: Scalars['String']['input'];
  /** An object where the defined keys will be set on the `OperatorName` being updated. */
  operatorNamePatch: OperatorNamePatch;
};

/** All input for the `updateOperatorName` mutation. */
export type UpdateOperatorNameInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `OperatorName` to be updated. */
  nodeId: Scalars['ID']['input'];
  /** An object where the defined keys will be set on the `OperatorName` being updated. */
  operatorNamePatch: OperatorNamePatch;
};

/** The output of our update `OperatorName` mutation. */
export type UpdateOperatorNamePayload = {
  __typename?: 'UpdateOperatorNamePayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `OperatorName` that was updated by this mutation. */
  operatorName?: Maybe<OperatorName>;
  /** An edge for our `OperatorName`. May be used by Relay 1. */
  operatorNameEdge?: Maybe<OperatorNamesEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our update `OperatorName` mutation. */
export type UpdateOperatorNamePayloadOperatorNameEdgeArgs = {
  orderBy?: InputMaybe<Array<OperatorNamesOrderBy>>;
};

/** All input for the `updateOpportunityById` mutation. */
export type UpdateOpportunityByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** 商談ID */
  id: Scalars['UUID']['input'];
  /** An object where the defined keys will be set on the `Opportunity` being updated. */
  opportunityPatch: OpportunityPatch;
};

/** All input for the `updateOpportunity` mutation. */
export type UpdateOpportunityInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `Opportunity` to be updated. */
  nodeId: Scalars['ID']['input'];
  /** An object where the defined keys will be set on the `Opportunity` being updated. */
  opportunityPatch: OpportunityPatch;
};

/** The output of our update `Opportunity` mutation. */
export type UpdateOpportunityPayload = {
  __typename?: 'UpdateOpportunityPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `Opportunity` that was updated by this mutation. */
  opportunity?: Maybe<Opportunity>;
  /** An edge for our `Opportunity`. May be used by Relay 1. */
  opportunityEdge?: Maybe<OpportunitiesEdge>;
  /** Reads a single `Product` that is related to this `Opportunity`. */
  productByProductId?: Maybe<Product>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our update `Opportunity` mutation. */
export type UpdateOpportunityPayloadOpportunityEdgeArgs = {
  orderBy?: InputMaybe<Array<OpportunitiesOrderBy>>;
};

/** All input for the `updateProductByCompanyIdAndProductName` mutation. */
export type UpdateProductByCompanyIdAndProductNameInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** 企業ID */
  companyId: Scalars['UUID']['input'];
  /** 商材名 */
  productName: Scalars['String']['input'];
  /** An object where the defined keys will be set on the `Product` being updated. */
  productPatch: ProductPatch;
};

/** All input for the `updateProductById` mutation. */
export type UpdateProductByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** 商材ID */
  id: Scalars['UUID']['input'];
  /** An object where the defined keys will be set on the `Product` being updated. */
  productPatch: ProductPatch;
};

/** All input for the `updateProductInformationById` mutation. */
export type UpdateProductInformationByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** 商材情報ID */
  id: Scalars['UUID']['input'];
  /** An object where the defined keys will be set on the `ProductInformation` being updated. */
  productInformationPatch: ProductInformationPatch;
};

/** All input for the `updateProductInformation` mutation. */
export type UpdateProductInformationInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `ProductInformation` to be updated. */
  nodeId: Scalars['ID']['input'];
  /** An object where the defined keys will be set on the `ProductInformation` being updated. */
  productInformationPatch: ProductInformationPatch;
};

/** The output of our update `ProductInformation` mutation. */
export type UpdateProductInformationPayload = {
  __typename?: 'UpdateProductInformationPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Product` that is related to this `ProductInformation`. */
  productByProductId?: Maybe<Product>;
  /** The `ProductInformation` that was updated by this mutation. */
  productInformation?: Maybe<ProductInformation>;
  /** An edge for our `ProductInformation`. May be used by Relay 1. */
  productInformationEdge?: Maybe<ProductInformationsEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our update `ProductInformation` mutation. */
export type UpdateProductInformationPayloadProductInformationEdgeArgs = {
  orderBy?: InputMaybe<Array<ProductInformationsOrderBy>>;
};

/** All input for the `updateProduct` mutation. */
export type UpdateProductInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `Product` to be updated. */
  nodeId: Scalars['ID']['input'];
  /** An object where the defined keys will be set on the `Product` being updated. */
  productPatch: ProductPatch;
};

/** The output of our update `Product` mutation. */
export type UpdateProductPayload = {
  __typename?: 'UpdateProductPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Company` that is related to this `Product`. */
  companyByCompanyId?: Maybe<Company>;
  /** The `Product` that was updated by this mutation. */
  product?: Maybe<Product>;
  /** An edge for our `Product`. May be used by Relay 1. */
  productEdge?: Maybe<ProductsEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our update `Product` mutation. */
export type UpdateProductPayloadProductEdgeArgs = {
  orderBy?: InputMaybe<Array<ProductsOrderBy>>;
};

/** All input for the `updateRequestContentById` mutation. */
export type UpdateRequestContentByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** ご依頼内容ID */
  id: Scalars['UUID']['input'];
  /** An object where the defined keys will be set on the `RequestContent` being updated. */
  requestContentPatch: RequestContentPatch;
};

/** All input for the `updateRequestContent` mutation. */
export type UpdateRequestContentInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `RequestContent` to be updated. */
  nodeId: Scalars['ID']['input'];
  /** An object where the defined keys will be set on the `RequestContent` being updated. */
  requestContentPatch: RequestContentPatch;
};

/** The output of our update `RequestContent` mutation. */
export type UpdateRequestContentPayload = {
  __typename?: 'UpdateRequestContentPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Product` that is related to this `RequestContent`. */
  productByProductId?: Maybe<Product>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** The `RequestContent` that was updated by this mutation. */
  requestContent?: Maybe<RequestContent>;
  /** An edge for our `RequestContent`. May be used by Relay 1. */
  requestContentEdge?: Maybe<RequestContentsEdge>;
};


/** The output of our update `RequestContent` mutation. */
export type UpdateRequestContentPayloadRequestContentEdgeArgs = {
  orderBy?: InputMaybe<Array<RequestContentsOrderBy>>;
};

/** All input for the `updateUserInformationMemoById` mutation. */
export type UpdateUserInformationMemoByIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** ご依頼内容ID */
  id: Scalars['UUID']['input'];
  /** An object where the defined keys will be set on the `UserInformationMemo` being updated. */
  userInformationMemoPatch: UserInformationMemoPatch;
};

/** All input for the `updateUserInformationMemo` mutation. */
export type UpdateUserInformationMemoInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `UserInformationMemo` to be updated. */
  nodeId: Scalars['ID']['input'];
  /** An object where the defined keys will be set on the `UserInformationMemo` being updated. */
  userInformationMemoPatch: UserInformationMemoPatch;
};

/** The output of our update `UserInformationMemo` mutation. */
export type UpdateUserInformationMemoPayload = {
  __typename?: 'UpdateUserInformationMemoPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** Reads a single `Product` that is related to this `UserInformationMemo`. */
  productByProductId?: Maybe<Product>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** The `UserInformationMemo` that was updated by this mutation. */
  userInformationMemo?: Maybe<UserInformationMemo>;
  /** An edge for our `UserInformationMemo`. May be used by Relay 1. */
  userInformationMemoEdge?: Maybe<UserInformationMemosEdge>;
};


/** The output of our update `UserInformationMemo` mutation. */
export type UpdateUserInformationMemoPayloadUserInformationMemoEdgeArgs = {
  orderBy?: InputMaybe<Array<UserInformationMemosOrderBy>>;
};

/** メモPkey */
export type UserInformationMemo = Node & {
  __typename?: 'UserInformationMemo';
  adminMemo?: Maybe<Scalars['String']['output']>;
  createdAt?: Maybe<Scalars['Datetime']['output']>;
  /** ご依頼内容ID */
  id: Scalars['UUID']['output'];
  memo?: Maybe<Scalars['String']['output']>;
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID']['output'];
  /** Reads a single `Product` that is related to this `UserInformationMemo`. */
  productByProductId?: Maybe<Product>;
  /** プロダクトID */
  productId?: Maybe<Scalars['UUID']['output']>;
  updatedAt?: Maybe<Scalars['Datetime']['output']>;
};

/**
 * A condition to be used against `UserInformationMemo` object types. All fields
 * are tested for equality and combined with a logical ‘and.’
 */
export type UserInformationMemoCondition = {
  /** Checks for equality with the object’s `adminMemo` field. */
  adminMemo?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `createdAt` field. */
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** Checks for equality with the object’s `id` field. */
  id?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `memo` field. */
  memo?: InputMaybe<Scalars['String']['input']>;
  /** Checks for equality with the object’s `productId` field. */
  productId?: InputMaybe<Scalars['UUID']['input']>;
  /** Checks for equality with the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** A filter to be used against `UserInformationMemo` object types. All fields are combined with a logical ‘and.’ */
export type UserInformationMemoFilter = {
  /** Filter by the object’s `adminMemo` field. */
  adminMemo?: InputMaybe<StringFilter>;
  /** Checks for all expressions in this list. */
  and?: InputMaybe<Array<UserInformationMemoFilter>>;
  /** Filter by the object’s `createdAt` field. */
  createdAt?: InputMaybe<DatetimeFilter>;
  /** Filter by the object’s `id` field. */
  id?: InputMaybe<UuidFilter>;
  /** Filter by the object’s `memo` field. */
  memo?: InputMaybe<StringFilter>;
  /** Negates the expression. */
  not?: InputMaybe<UserInformationMemoFilter>;
  /** Checks for any expressions in this list. */
  or?: InputMaybe<Array<UserInformationMemoFilter>>;
  /** Filter by the object’s `productId` field. */
  productId?: InputMaybe<UuidFilter>;
  /** Filter by the object’s `updatedAt` field. */
  updatedAt?: InputMaybe<DatetimeFilter>;
};

/** An input for mutations affecting `UserInformationMemo` */
export type UserInformationMemoInput = {
  adminMemo?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** ご依頼内容ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  memo?: InputMaybe<Scalars['String']['input']>;
  /** プロダクトID */
  productId?: InputMaybe<Scalars['UUID']['input']>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** Represents an update to a `UserInformationMemo`. Fields that are set will be updated. */
export type UserInformationMemoPatch = {
  adminMemo?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['Datetime']['input']>;
  /** ご依頼内容ID */
  id?: InputMaybe<Scalars['UUID']['input']>;
  memo?: InputMaybe<Scalars['String']['input']>;
  /** プロダクトID */
  productId?: InputMaybe<Scalars['UUID']['input']>;
  updatedAt?: InputMaybe<Scalars['Datetime']['input']>;
};

/** A connection to a list of `UserInformationMemo` values. */
export type UserInformationMemosConnection = {
  __typename?: 'UserInformationMemosConnection';
  /** A list of edges which contains the `UserInformationMemo` and cursor to aid in pagination. */
  edges: Array<UserInformationMemosEdge>;
  /** A list of `UserInformationMemo` objects. */
  nodes: Array<Maybe<UserInformationMemo>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `UserInformationMemo` you could get from the connection. */
  totalCount: Scalars['Int']['output'];
};

/** A `UserInformationMemo` edge in the connection. */
export type UserInformationMemosEdge = {
  __typename?: 'UserInformationMemosEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']['output']>;
  /** The `UserInformationMemo` at the end of the edge. */
  node?: Maybe<UserInformationMemo>;
};

/** Methods to use when ordering `UserInformationMemo`. */
export enum UserInformationMemosOrderBy {
  AdminMemoAsc = 'ADMIN_MEMO_ASC',
  AdminMemoDesc = 'ADMIN_MEMO_DESC',
  CreatedAtAsc = 'CREATED_AT_ASC',
  CreatedAtDesc = 'CREATED_AT_DESC',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  MemoAsc = 'MEMO_ASC',
  MemoDesc = 'MEMO_DESC',
  Natural = 'NATURAL',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
  ProductIdAsc = 'PRODUCT_ID_ASC',
  ProductIdDesc = 'PRODUCT_ID_DESC',
  UpdatedAtAsc = 'UPDATED_AT_ASC',
  UpdatedAtDesc = 'UPDATED_AT_DESC'
}
