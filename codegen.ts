
import type { CodegenConfig } from '@graphql-codegen/cli';

const config: CodegenConfig = {
  overwrite: true,
  schema: "http://localhost:5433/graphql",
  // documents: "src/**/*.tsx",
  generates: {
    "src/react/": {
      preset: "client",
      plugins: ["typescript-graphql-request", "typescript", "typescript-operations"]
    },
    "./graphql.schema.json": {
      plugins: ["introspection"]
    }
  }
};

export default config;
