import express, { Express } from 'express'
import cors from 'cors'
import { graphqlHTTP } from 'express-graphql'

import * as hello from './schema/hello'
import * as random from './schema/random'
import * as dice from './schema/dice'

const app: Express = express()
app.use(cors())

app.use(
  '/hello',
  graphqlHTTP({
    schema: hello.schema,
    rootValue: hello.rootValue,
    graphiql: true,
  }),
)

app.use(
  '/random',
  graphqlHTTP({
    schema: random.schema,
    rootValue: random.rootValue,
    graphiql: true,
  }),
)

app.use(
  '/dice',
  graphqlHTTP({
    schema: dice.schema,
    rootValue: dice.rootValue,
    graphiql: true,
  }),
)

const PORT = 4000
app.listen(PORT)
console.log(`Running a GraphQL API server at http://localhost:${PORT}/graphql`)
